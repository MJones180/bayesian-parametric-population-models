Installation
============

.. note::

   **System Dependencies:** Access to Python version ``≥3.6.0`` along with ``pip``.

**Clone Repository**

This repository is not indexed at any of the locations in which ``pip`` looks, so the code will have to be downloaded manually. First, clone the repository:

.. code-block:: bash

    $ git clone https://gitlab.com/dwysocki/bayesian-parametric-population-models.git PopModels

Then, you will need to move into the cloned directory:

.. code-block:: bash

    $ cd PopModels

-----

**Virtual Environment (Optional)**

.. note::

    The virtual environment instructions below are for ``pip``, but ``conda`` can also be used.
    In fact, if you are using an Apple M1 ARM chip, using ``mini-forge`` is heavily encouraged.

It is preferable that this project is run inside of a Python 3 `virtual environment <https://docs.python.org/3/library/venv.html>`_. To create a virtual environment the following command can be used:

.. code-block:: bash

    $ python3 -m venv venv

Then, activate the virtual environment using the following command:

.. code-block:: bash

    $ source venv/bin/activate

Anytime you would like to run the PopModels code, the virtual environment must be activated.
It can be deactivated by typing ``deactivate``.

-----

**Install Requirements**

Once the repository has been cloned, and the virtual environment has been created and activated (optional), the requirements must be installed. To do so, ``pip`` can be used:

.. code-block:: bash

    $ pip install -U .

The ``-U`` flag will upgrade any existing packages. In addition, please do not leave out the ``.``, it is important!

You will still need to install `LALSuite <https://wiki.ligo.org/DASWG/LALSuite>`_ yourself in order to use the (optional) module ``pop_models.vt``.

-----

**Test Installation**

The repostiory will be correctly installed if the following command works:

.. code-block:: bash

    $ driver

-----

**For PopModels Developers**

.. note::

    The instructions below only need to be followed if you are going to be making updates to the ``pop_models`` code.
    Simply running ``pop_models`` scripts through the CLI requires no further setup.

To prevent having to rebuild the repository after every change to the ``pop_models`` module, one of the following commands should be run:

.. code-block:: bash

    # Recommended:
    $ pip install -e .
    # Alternative option (risky):
    $ python3 setup.py develop
