pop_models.applications.bns.iid_gaussian_mass_with_eos module
=============================================================

.. automodule:: pop_models.applications.bns.iid_gaussian_mass_with_eos
    :members:
    :undoc-members:
    :show-inheritance:
