pop_models.applications.bbh.broken_powerlaw_simple module
=========================================================

.. automodule:: pop_models.applications.bbh.broken_powerlaw_simple
    :members:
    :undoc-members:
    :show-inheritance:
