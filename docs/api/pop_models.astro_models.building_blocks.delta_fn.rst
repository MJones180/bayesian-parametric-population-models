pop_models.astro_models.building_blocks.delta_fn module
=======================================================

.. automodule:: pop_models.astro_models.building_blocks.delta_fn
    :members:
    :undoc-members:
    :show-inheritance:
