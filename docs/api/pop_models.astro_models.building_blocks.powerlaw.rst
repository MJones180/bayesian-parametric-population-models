pop_models.astro_models.building_blocks.powerlaw module
=======================================================

.. automodule:: pop_models.astro_models.building_blocks.powerlaw
    :members:
    :undoc-members:
    :show-inheritance:
