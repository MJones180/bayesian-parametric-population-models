pop_models.astro_models.building_blocks.gaussian_mass module
============================================================

.. automodule:: pop_models.astro_models.building_blocks.gaussian_mass
    :members:
    :undoc-members:
    :show-inheritance:
