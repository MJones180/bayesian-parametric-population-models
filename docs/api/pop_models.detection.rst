pop_models.detection module
===========================

.. automodule:: pop_models.detection
    :members:
    :undoc-members:
    :show-inheritance:
