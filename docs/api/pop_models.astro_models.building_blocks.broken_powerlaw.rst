pop_models.astro_models.building_blocks.broken_powerlaw module
==============================================================

.. automodule:: pop_models.astro_models.building_blocks.broken_powerlaw
    :members:
    :undoc-members:
    :show-inheritance:
