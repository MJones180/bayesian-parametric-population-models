pop_models.posterior module
===========================

.. automodule:: pop_models.posterior
    :members:
    :undoc-members:
    :show-inheritance:
