CLI
===

All driver scripts can be run through the terminal by using the following command:

.. code-block:: bash

    driver <script> <config> [script dependent arguments] [override config values]

All config values can be overridden from the CLI by passing in arguments in the form ``<section>.<key>=<value>``.
For example, say you want to run **Inference** on the ``inference.ini`` config with debugging enabled.
Then, the following command can be used:

.. code-block:: none

    driver inference inference.ini -d

However, let's say we want to override the ``events`` file, along with the ``seed``.
The following CLI command can then be used:

.. code-block:: none

    driver inference inference.ini -d main.events=other_file random.seed=31415

The following scripts are available:
- ``inference``
- ``event_contribution``
- ``observable_quantiles``
- ``ppd``

-----

**Inference** - *Run population inference using values from a provided config file.*

.. code-block:: none

    usage: driver inference [-h] [-v] [-s] [-d] config_file

    positional arguments:
      config_file      Path to the config file to base this run on.

    optional arguments:
      -h, --help       show this help message and exit
      -v, --verbose    Enable verbose logging to the console; default is False.
      -s, --swmr_mode  Other processes may read from the file while running, will be more resistant to data
                       corruption; default is False.
      -d, --debug      Output debugging messages; default is False.

Config Information: ``config_templates/inference.ini``.

After running population inference, the **raw samples** will be saved.
These can be converted to **cleaned samples** by using the ``pop_models_extract_samples`` CLI command.

If you would like more information on the **raw samples**, then you can use the following command:

.. code-block:: none

    $ pop_models_h5_raw_samples <output/> summary

-----

**Event Contribution** - *Calculate the event contribution for a given event datafile using values from a provided config file.*

.. code-block:: none

    usage: driver event_contribution [-h] [-d] config_file

    positional arguments:
      config_file  Path to the config file to base this run on.

    optional arguments:
      -h, --help   show this help message and exit
      -d, --debug  Output debugging messages; default is False.

Config Information: ``config_templates/event_contribution.ini``.

-----

**Observable Quantiles** - *Compute and plot the observable quantiles.*

.. code-block:: none

    usage: driver observable_quantiles [-h] config_file

    positional arguments:
      config_file  Path to the config file to base this run on.

    optional arguments:
      -h, --help   show this help message and exit

Config Information: ``config_templates/observable_quantiles.ini``.

-----

**Posterior Predictive Distribution (PPD)** - *Draw samples from the PPD using values from a provided config file.*

.. code-block:: none

    usage: driver ppd [-h] [-d] config_file

    positional arguments:
      config_file  Path to the config file to base this run on.

    optional arguments:
      -h, --help   show this help message and exit
      -d, --debug  Output debugging messages; default is False.

Config Information: ``config_templates/ppd.ini``.
