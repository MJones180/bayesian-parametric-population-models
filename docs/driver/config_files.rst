Config Files
============

**Overview**

All driver scripts are run using a config file based approach.

Config files are files with the extension of ``.ini`` and have the format:

.. code-block:: ini

    [section]
    key=value

Essentially, config files consist of sections which are groupings of key-value pairs.

.. note::

    A section's name **cannot** be used twice.
    If two sections exist with the same name, an error will be raised and the program will exit.

Config templates that describe each of the driver scripts can be found in ``pop_models/applications/driver/config_templates/``.

-----

**Composability**

Config files are composable, meaning multiple sections can be in the same file, or they can be split across many files.
Any config files listed under the ``append_configs`` section will be recursively loaded in and merged together during runtime.

.. note::

    As of right now, the key names in ``append_configs`` play no role and can be named anything.
    The values contain the important information: the path to the config file that needs to be loaded in.

Below is an example demonstrating how sections can all be in the same config, or split across many.

1. All sections in one config file.

    .. code-block:: ini
        :caption: Config file named ``config.ini``

        [section_a]
        key1=value1

        [section_b]
        key2=value2
        key3=value3

        [section_c]
        key4=value4

        [section_d]
        key5=value5
        key6=value6

        [section_e]

2. Sections split up across multiple files.
   For this example, all config files reside in the same directory, but they do not have to.

    .. code-block:: ini
        :caption: Config file named ``config_root.ini``

        [append_configs]
        sub_config_1=config_sub_1.ini
        sub_config_2=config_sub_2.ini

        [section_a]
        key1=value1

    .. code-block:: ini
        :caption: Config file named ``config_sub_1.ini``

        [append_configs]
        sub_sub_config=config_sub_sub.ini

        [section_b]
        key2=value2
        key3=value3

    .. code-block:: ini
        :caption: Config file named ``config_sub_sub.ini``

        [section_c]
        key4=value4

    .. code-block:: ini
        :caption: Config file named ``config_sub_2.ini``

        [section_d]
        key5=value5
        key6=value6

        [section_e]

-----

**File and Config Paths**

Paths inside of config files can either be relative, with respect to the config file's path, or absolute.
This means, for config files loaded in under the ``append_configs`` section, paths are relative to the files in which they are coming from, **not** the root config file or working directory.
For instance, in the example above where the sections are split into multiple files, if ``key6`` in ``section_d`` were a relative path, it would be with respect to ``config_sub_2.ini``'s path.

-----

**Function and Class Paths**

For fields that import a function or class, the following options for specifying the location are valid (function/class is ``Foo``):

1. ``Foo``: will be imported from the default location (specified by driver script)
2. ``../file.py:Foo``: path relative to config
3. ``/path/to/file.py:Foo``: absolute path
4. ``pop_models.path.to.module:Foo``: absolute package path

-----

**Field Information**

All fields will automatically be typecasted as defined by the driver script.

.. note::

    Strings do not need to be wrapped in quotation marks – they are the default type.

Any fields that are labeled as ``REQUIRED`` **must** have a value, or the program will throw an error and exit.
Every other field can be left empty, or have the value of ``None``, and the default value will be used.
