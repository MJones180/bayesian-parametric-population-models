Parameter Files
===============

A population's parameters are defined in the prior from three different sources: variables, constants, and duplicates.

By default, each file should be of ``.json`` format.
The keys should map to parameter names in the provided population.
This can be accomplished by either writing out a parameter's full name, or by writing a Python Regular Expression that represents a group of parameters.
For example, the keys ``rate_pl0`` and ``rate_pl1`` can be used, but if they have the same values, then the key of ``rate_pl.*`` can also be used.

Python Regular Expressions (REs) are used to handle pattern matching.
Since all parameter name keys are processed as REs by default, RE syntax can be mixed in as needed.

Some simple examples:

- ``[0-9]`` - Match **one** digit ranging from 0-9
- ``[0-9]+`` - Match **one or more** digits ranging from 0-9
- ``.`` - Match **one** character
- ``.*`` - Match **zero or more** characters

More information on REs can be found `on the docs <https://docs.python.org/3/library/re.html>`_.

-----

**Variables File**

Mapping of parameter names to their individual distributions and the ``params`` associated with each.

Each distribution's name needs to be present in the ``distributions`` dictionary within the associated ``Prior`` class.
The ``params`` are the values that will be passed into each distribution.
For example, for the ``uniform`` distribution, the ``params`` should be a dictionary consisting of the keys ``min`` and ``max``.

There are two available formats for this file:

1. *Recommended* - JSON format.

    .. code-block:: javascript
        :caption: Example variables.json

        {
            "param_name_RE": {
                "dist": "dist-name",
                "params": {
                    "key": val,
                    ...
                }
            },
            ...
        }

2. *Experimental* - Custom format, the ``variables`` can either be named or unnamed.

    .. code-block:: javascript
        :caption: Example variables.custom

        param_name_RE ~ dist(key=value, ...)
        param_name_2_RE ~ dist(value, ...)
        ...

-----

**Constants File**

JSON mapping of parameter names to float values.

.. code-block:: javascript
    :caption: Example constants.json

    {
       "param_name_RE": float,
        ...
    }

-----

**Duplicates Files**

JSON mapping of parameter names to the parameters in which they are duplicating.

.. code-block:: javascript
    :caption: Example duplicates.json

    {
       "param_name_RE": "param_name_to_duplicate",
        ...
    }

