.. Bayesian parametric population models documentation master file, created by
   sphinx-quickstart on Mon Oct 23 17:24:18 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Bayesian parametric population models's documentation!
=================================================================

:Release: |version|
:Date: |today|

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   user_guide/index.rst
   driver/index.rst
   api/index.rst
   cli/index.rst
   examples/index.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
