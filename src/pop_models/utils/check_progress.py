"""
Utility for parsing sampler verbose output and estimating sampling rate and ETA.
"""
import argparse
import itertools
from operator import itemgetter
import re
import datetime
import numpy as np
import matplotlib as mpl

re_str = (
    "^(\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}.\\d{6}); Progress: (\\d+)%; Samples: (\\d+)$"
)

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)

def make_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "log_file",
        help="Text file showing verbose output from a sampling run.  Should "
             "have lines in the form of "
             "'[timestamp]; Progress: [P]%%; Samples: [S]'",
    )

    parser.add_argument(
        "--plot-distribution",
        metavar="PLOT_FILE",
        help="Optionally plot a histogram of the time per sample across the "
             "run.",
    )
    parser.add_argument(
        "--plot-timeseries",
        metavar="PLOT_FILE",
        help="Optionally plot the timeseries of the time per sample across the "
             "run.",
    )

    parser.add_argument(
        "--mpl-backend",
        metavar="BACKEND",
        help="Specify the backend for matplotlib.  Uses system default "
             "otherwise.",
    )

    return parser


def main():
    # Parse command line arguments.
    cli_parser = make_parser()
    cli_args = cli_parser.parse_args()

    # Set the matplotlib backend if provided.
    if cli_args.mpl_backend is not None:
        mpl.use(cli_args.mpl_backend)

    # Create regex object matching a progress line.
    rex = re.compile(re_str)

    # Store the progress in a dictionary mapping # samples to datetime object.
    progress_timestamps = {}

    # Iteratively improve our estimate of the number of samples per percent.
    # Our best estimate comes from taking the last progress update and dividing
    # the sample count by the percent.

    # Loop over all the lines of the log file and store the progress info.
    with open(cli_args.log_file, "r") as log_file:
        for line in log_file:
            match = rex.match(line)

            # Skip lines that do not match progress bar string.
            if not match:
                continue

            # Extract the timestamp, percent, and sample count from the match.
            timestamp_str, percent_str, n_samples_str = match.groups()
            timestamp = datetime.datetime.fromisoformat(timestamp_str)
            percent = int(percent_str)
            n_samples = int(n_samples_str)

            if n_samples in progress_timestamps:
                raise RuntimeError(
                    "Encountered multiple lines in log file corresponding to "
                    "the same number of samples.  File either corrupted or "
                    "contains output from multiple runs.  Please provide a "
                    "file from a single run."
                )

            progress_timestamps[n_samples] = timestamp

    # Count the number of reported timestamps.
    n_timestamps = len(progress_timestamps)

    # Record the latest percent, and determine whether we're completed.
    percent_latest = int(percent)
    is_complete = percent_latest == 100

    # Estimate the samples per percent and total number of samples from the
    # final percent and sample count.
    samples_per_percent = n_samples / percent_latest
    approx_total_samples = round(100*samples_per_percent)

    # Fail if we do not have two timestamps.
    if n_timestamps <= 1:
        raise RuntimeError(
            "There were {} reported times in the log file.  Need at least 2."
            .format(n_timestamps)
        )

    # Array which will hold the sampling rates for each pair of consecutive
    # progress updates.  Units: samples per minute.
    sampling_rates = np.empty(n_timestamps-1, dtype=float)

    # Sort the (n_samples, timestamp) pairs by n_samples, then create a sliding
    # window to compare each pair of consecutive elements and store the local
    # sampling rate.
    progress_pairs = pairwise(
        sorted(progress_timestamps.items(), key=itemgetter(0))
    )
    for i, ((n_0, t_0), (n_1, t_1)) in enumerate(progress_pairs):
        dn = n_1 - n_0
        dt = (t_1 - t_0).total_seconds() / 60.0
        sampling_rates[i] = dn / dt

    # Store the latest timestamp and sample count for approximating the end time.
    n_samples_latest = n_1
    timestamp_latest = t_1

    # Compute average sampling rate.
    r_mean = sampling_rates.mean()

    # Display statistics on sampling rates.
    print("Mean:", r_mean, "samples per minute")
    print("Stdev:", sampling_rates.std(), "samples per minute")

    r_median = np.median(sampling_rates)
    r_lo90, r_hi90 = np.percentile(sampling_rates, [5, 95])
    print(
        "Median +/- 90%: {} +{} / -{} samples per minute"
        .format(r_median, r_hi90-r_median, r_median-r_lo90)
    )

    print("Min:", sampling_rates.min(), "samples per minute")
    print("Max:", sampling_rates.max(), "samples per minute")

    # Predict the completion time if we have not reached 100%
    if not is_complete:
        percent_to_go = 100 - percent_latest
        minutes_to_go = percent_to_go * samples_per_percent / r_mean
        timestamp_final_approx = (
            timestamp_latest + datetime.timedelta(minutes=minutes_to_go)
        )

        print("Predicted completion time:", timestamp_final_approx)

    # Plot histogram if --plot-distribution is provided.
    if cli_args.plot_distribution is not None:
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(figsize=(5,4))

        ax.hist(
            sampling_rates,
            bins="auto", histtype="step", color="C0",
            density=True,
        )

        ax.set_xlabel("Samples per minute")
        ax.set_ylabel("Histogram")

        fig.savefig(cli_args.plot_distribution)

        plt.close(fig)

    # Plot timeseries if --plot-timeseries is provided.
    if cli_args.plot_timeseries is not None:
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(figsize=(5,4))

        ax.plot(sampling_rates, color="C0")

        ax.set_xlabel("Steps")
        ax.set_ylabel("Samples per minute")

        fig.savefig(cli_args.plot_timeseries)
        plt.close(fig)
