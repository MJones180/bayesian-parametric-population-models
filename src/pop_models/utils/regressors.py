import numpy
from sklearn.ensemble import ExtraTreesRegressor

class TruncatedRandomForestRegressor(ExtraTreesRegressor):
    def __init__(
            self,
            fill_value=numpy.NINF,
            n_estimators='warn',
            criterion='mse',
            max_depth=None,
            min_samples_split=2,
            min_samples_leaf=1,
            min_weight_fraction_leaf=0.0,
            max_features='auto',
            max_leaf_nodes=None,
            min_impurity_decrease=0.0,
            min_impurity_split=None,
            bootstrap=False,
            oob_score=False,
            n_jobs=None,
            random_state=None,
            verbose=0,
            warm_start=False,
        ) -> None:
        super().__init__(
            n_estimators=n_estimators,
            criterion=criterion,
            max_depth=max_depth,
            min_samples_split=min_samples_split,
            min_samples_leaf=min_samples_leaf,
            min_weight_fraction_leaf=min_weight_fraction_leaf,
            max_features=max_features,
            max_leaf_nodes=max_leaf_nodes,
            min_impurity_decrease=min_impurity_decrease,
            min_impurity_split=min_impurity_split,
            bootstrap=bootstrap,
            oob_score=oob_score,
            n_jobs=n_jobs,
            random_state=random_state,
            verbose=verbose,
            warm_start=warm_start,
        )
        self.__fill_value = fill_value
        self.__X_min = numpy.inf
        self.__X_max = numpy.NINF

    @property
    def X_min(self):
        return self.__X_min

    @property
    def X_max(self):
        return self.__X_max

    def fit(self, X, y, sample_weight=None):
        super().fit(X, y, sample_weight=sample_weight)
        self.__X_min = numpy.minimum(self.__X_min, numpy.min(X, axis=0))
        self.__X_max = numpy.maximum(self.__X_max, numpy.max(X, axis=0))

    def predict(self, X):
        support = numpy.all((self.__X_min <= X) & (X <= self.__X_max), axis=-1)
        y = numpy.full_like(support, self.__fill_value, dtype=X.dtype)
        X_support = X[support]
        if X_support.size != 0:
            y[support] = super().predict(X_support)
        return y
