import typing
import datetime
import sys

class DebugVerboseOutput(object):
    def __init__(self):
        self._enabled = set() # type: typing.Set[str]
        self._full_output = False # type: bool

    def full_output_on(self) -> None:
        self._full_output = True

    def full_output_off(self) -> None:
        self._full_output = False

    def enable_modes(self, *modes: str) -> None:
        self._enabled.update(modes)

    def disable_modes(self, *modes: str) -> None:
        self._enabled.difference_update(modes)

    def check(self, mode: str) -> bool:
        return self._full_output or (mode in self._enabled)

    def __call__(
            self,
            *values,
            mode: typing.Optional[str]=None,
            sep: str=" ", end: str="\n", file=sys.stdout,
            flush: bool=False,
        ) -> None:
        # Return early if we're not writing anything.
        if (mode is not None) and (not self._full_output):
            if mode is None:
                return
            if mode not in self._enabled:
                return

        prefix = "{} [debug={}]::".format(datetime.datetime.now(), mode)

        print(prefix, *values, sep=sep, end=end, file=file, flush=flush)

debug_verbose = DebugVerboseOutput()
