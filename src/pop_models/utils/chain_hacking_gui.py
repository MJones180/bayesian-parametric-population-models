"""
Tool for manual hacking of MCMC chains to resolve issues of broken chains.
"""

import argparse

import tkinter
import tkinter.filedialog
import matplotlib as mpl
mpl.use("TkAgg")
import matplotlib.pyplot as plt

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

import numpy
import numpy as np

from pop_models.posterior import (
    H5RawPosteriorSamples, H5CleanedPosteriorSamples,
)

# Enum for chain selection states
DESELECTED = 0
SELECTED = 1
DELETED = 2

# Grid size for chains in pixels
grid_unit = 20
grid_columns = 10

# Colors, z-orders, and opacities for different chain selection states
status_colors = {
    DESELECTED : "#FFFFFF",
    SELECTED : "#1170AA",
    DELETED : "#C85200",
}
status_colors_plot = {
    DESELECTED : "#888888",
    SELECTED : "#1170AA",
    DELETED : "#C85200",
}
status_zorders = {
    DESELECTED : 0,
    SELECTED : 1,
    DELETED : -1,
}
status_alphas = {
    DESELECTED : 0.8,
    SELECTED : 1.0,
    DELETED : 0.2,
}

# Logic for status change on cell click.
status_click_changes = {
    DESELECTED : SELECTED,
    SELECTED : DESELECTED,
    DELETED : DESELECTED,
}

class ChainHackingGUI(object):
    def __init__(self, posterior_chain_input: str) -> None:
        # Load in posterior chain inputs.
        self.load_input(posterior_chain_input)
        # Parse out chain data and create bookkeeping.
        self.parse_data()
        # Create base application.
        self.setup_app()
        # Populate menu bar.
        self.setup_menu()
        # Create frame layout.
        self.setup_frames()
        # Create plots.
        self.setup_plots()
        # Create chain grid view.
        self.setup_chains_grid()

    def setup_app(self) -> None:
        """Setup basic application"""
        # Create root application.
        self.root = tkinter.Tk()
        self.root.wm_title("PopModels Chain Hacking")

        # Init flag to indicate we're not running yet
        self.is_running = False

    def setup_menu(self) -> None:
        """Setup menu bar"""
        # Create top menu.
        self.menu = tkinter.Menu(self.root)
        # File operations
        self.file_menu = tkinter.Menu(self.menu)
        self.file_menu.add_command(
            command=self.save_chains,
            label="Save Chains",
        )
        self.file_menu.add_command(
            command=self.save_samples,
            label="Save Samples",
        )
        self.file_menu.add_separator()
        self.file_menu.add_command(
            command=self.close_prompt,
            label="Exit",
        )
        self.menu.add_cascade(
            menu=self.file_menu,
            label="File",
        )
        # Edit operations
        self.edit_menu = tkinter.Menu(self.menu)
        self.edit_menu.add_command(
            command=self.prune,
            label="Prune",
        )
        self.edit_menu.add_separator()
        self.edit_menu.add_command(
            command=self.select_all,
            label="Select All",
        )
        self.edit_menu.add_command(
            command=self.deselect_all,
            label="Deselect All",
        )
        self.menu.add_cascade(
            menu=self.edit_menu,
            label="Edit",
        )

        self.root.config(menu=self.menu)

    def setup_frames(self) -> None:
        """Setup separate regions within window (i.e., frames)"""
        # self.scroll_plots = tkinter.Scrollbar(self.root)
        # self.scroll_plots.pack(fill=tkinter.Y, side=tkinter.LEFT, expand=False)
        # self.canvas_plots = tkinter.Canvas(
        #     self.root,
        #     bd=0, highlightthickness=0, yscrollbar=self.scroll_plots,
        # )
        # self.canvas_plots.pack(side=tkinter.LEFT, fill=tkinter.BOTH, expand=True)
        # self.scroll_plots.config(command=self.canvas_plots.yview)

        # # reset the view
        # self.canvas_plots.xview_moveto(0)
        # self.canvas_plots.yview_moveto(0)

        # # create a frame inside the canvas which will be scrolled with it
        # self.interior = interior = Frame(canvas)
        # interior_id = canvas.create_window(0, 0, window=interior,
        #                                    anchor=NW)

        self.frame_plots = VerticalScrolledFrame(self.root, width=600)

        # self.frame_plots = tkinter.Frame(
        #     self.root,
        #     width=400,# yscrollcommand=self.frame_scroll,
        # )
        self.frame_plots.pack(side=tkinter.LEFT)

        self.grid_width = grid_unit*grid_columns
        self.grid_height = grid_unit*int(np.ceil(self.n_chains/grid_columns))
        self.canvas_chains = tkinter.Canvas(
            self.root,
            width=self.grid_width, height=self.grid_height,
            bg="#AAAAAA",
        )
        self.canvas_chains.pack(side=tkinter.LEFT)

    def load_input(self, posterior_chain_input: str) -> None:
        """Load input chain data"""
        self.post_input = H5RawPosteriorSamples(posterior_chain_input, mode="r")

    def parse_data(self) -> None:
        """Set up bookkeeping for input chains"""
        # Load in chains.
        self.log_post = self.post_input.get_posterior_log_prob(...)
        self.log_prior = self.post_input.get_prior_log_prob(...)
        self.params = self.post_input.get_params(...)
        self.variable_names = self.post_input.variable_names
        self.n_variables = len(self.variable_names)

        # Create chain bookkeeping.  All chains start deselected, and altered
        # flag is set at first to make sure they are all drawn.
        self.n_samples, self.n_chains = self.log_post.shape
        self.chain_status = np.full(
            self.n_chains, fill_value=DESELECTED,
            dtype=np.uint8,
        )
        self.chains_altered = np.ones(self.n_chains, dtype=bool)

    def setup_plots(self) -> None:
        """Create initial plots"""
        # Initial color and zorder
        color = status_colors_plot[DESELECTED]
        zorder = status_zorders[DESELECTED]

        # Arrays to store figure components.
        self.figures = np.empty(self.n_variables+1, dtype=object)
        self.axes = np.empty(self.n_variables+1, dtype=object)
        self.lines = np.empty((self.n_variables+1, self.n_chains), dtype=object)
        self.canvases = np.empty(self.n_variables+1, dtype=object)

        # Create figure for log(post) and plot.
        fig_log_post, ax_log_post = plt.subplots(figsize=(5,3), dpi=100)
        self.figures[0], self.axes[0] = fig_log_post, ax_log_post
        self.lines[0] = ax_log_post.plot(
            self.log_post,
            color=color, zorder=zorder,
        )
        ax_log_post.set_ylabel("Log post", fontsize=14)

        for param_num, param_name in enumerate(self.variable_names):
            fig, ax = plt.subplots(figsize=(5,3), dpi=100)
            self.figures[param_num+1], self.axes[param_num+1] = fig, ax
            self.lines[param_num+1] = ax.plot(
                self.params[param_name],
                color=color, zorder=zorder,
            )
            ax.set_ylabel(param_name, fontsize=14)

            if param_num < self.n_variables-1:
                ax.set_xticklabels([])
            else:
                ax.set_xlabel("Iteration", fontsize=14)

        for i in range(self.n_variables+1):
            fig = self.figures[i]
            canvas = FigureCanvasTkAgg(fig, master=self.frame_plots)
            self.canvases[i] = canvas
            canvas.draw()
            canvas.get_tk_widget().pack(
                side=tkinter.TOP, fill=tkinter.X, expand=True,
            )

    # fig = Figure(figsize=(5, 4), dpi=100)
    # t = np.arange(0, 3, .01)
    # fig.add_subplot(111).plot(t, 2 * np.sin(2 * np.pi * t))

    # canvas = FigureCanvasTkAgg(fig, master=root)  # A tk.DrawingArea.
    # canvas.draw()
    # canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)




    def setup_chains_grid(self) -> None:
        """Create grid of chain statuses"""
        self.canvas_chains.bind("<Button-1>", self.callback_chains_grid_click)

        self.chains_canvas_items = np.empty(self.n_chains, dtype=int)
        for i in range(self.n_chains):
            grid_x_start = (i % grid_columns)*grid_unit
            grid_y_start = (i // grid_columns)*grid_unit

            x1 = grid_x_start+1
            y1 = grid_y_start+1
            x2 = grid_x_start+grid_unit-1
            y2 = grid_y_start+grid_unit-1

            color = status_colors[DESELECTED]

            rectangle_id = self.canvas_chains.create_rectangle(
                x1, y1, x2, y2,
                fill=color,
            )
            self.chains_canvas_items[i] = rectangle_id

    def callback_chains_grid_click(self, event) -> None:
        # Determine which chain was selected.
        i = event.x // grid_unit
        j = event.y // grid_unit
        chain_num = j*grid_columns + i

        # If out of bounds, skip
        if chain_num >= self.n_chains:
            return

        # Change the status according to lookup table of rules.
        self.chain_status[chain_num] = status_click_changes[
            self.chain_status[chain_num]
        ]

        # Update grid cell accordingly.
        self.update_chain_grid_cell(chain_num)
        # Update lines
        self.update_chain_lines(chain_num)

        # Re-draw canvases
        for canvas in self.canvases:
            canvas.draw()


    def update_chain_grid_cell(self, chain_num: int):
        """Update single chain grid cell"""
        rectangle_id = self.chains_canvas_items[chain_num]
        status = self.chain_status[chain_num]
        color = status_colors[status]
        self.canvas_chains.itemconfig(rectangle_id, fill=color)

    def update_chain_lines(self, chain_num: int):
        """Update all lines for a single chain"""
        status = self.chain_status[chain_num]
        color = status_colors_plot[status]
        zorder = status_zorders[status]
        alpha = status_alphas[status]
        for line_for_param in self.lines:
            line = line_for_param[chain_num]
            line.set_color(color)
            line.set_zorder(zorder)
            line.set_alpha(alpha)

    def __enter__(self) -> None:
        self.start()

        return self

    def close_prompt(self) -> None:
        ## TODO: add prompt before calling close
        self.close()

    def start(self) -> None:
        # Set flag to indicate we're still running
        self.is_running = True

        # Start the main loop.
        self.root.mainloop()

    def close(self) -> None:
        # Stop mainloop and cleanup.
        self.root.quit()
        self.root.destroy()

        # Init flag to indicate we're not running anymore
        self.is_running = False

    def __exit__(self, type, value, traceback) -> None:
        # If app is still running, close it
        if self.is_running:
            self.close()

    def save_chains(self) -> None:
        # Prompt for output directory name
        dirname = tkinter.filedialog.askdirectory()

        # Pre-compute some data
        row_idx = self.chain_status != DELETED
        n_walkers = numpy.count_nonzero(row_idx)
        init_state = self.post_input.get_samples(0)[row_idx]

        # Create new output file.
        output_chains = H5RawPosteriorSamples.create(
            dirname,
            self.post_input.param_names,
            init_state,
            constants=self.post_input.constants,
            duplicates=self.post_input.duplicates,
            dtype=self.post_input.dtype,
            chunk_size=self.post_input.chunk_size,
            zero_padding_length=self.post_input.zero_padding_length,
            force=False,
        )

        # Save output data one sample at a time.
        for i in range(1, self.n_samples):
            output_chains.append(
                self.post_input.get_samples(i)[row_idx],
                self.post_input.get_posterior_log_prob(i)[row_idx],
                self.post_input.get_prior_log_prob(i)[row_idx],
            )

    def save_samples(self) -> None:
        print("Not implemented")

    def prune(self) -> None:
        for chain_num in range(self.n_chains):
            # Delete any selected chains.
            if self.chain_status[chain_num] == SELECTED:
                # Mark for deletion.
                self.chain_status[chain_num] = DELETED
                # Update grid
                self.update_chain_grid_cell(chain_num)
                # Update lines
                self.update_chain_lines(chain_num)

        # Re-draw canvases
        for canvas in self.canvases:
            canvas.draw()


    def select_all(self) -> None:
        # Mark all as selected.
        self.chain_status[self.chain_status != DELETED] = SELECTED
        # Update visuals for all
        for chain_num in range(self.n_chains):
            self.update_chain_grid_cell(chain_num)
            self.update_chain_lines(chain_num)
        # Re-draw canvases
        for canvas in self.canvases:
            canvas.draw()

    def deselect_all(self) -> None:
        # Mark all as deselected.
        self.chain_status[self.chain_status != DELETED] = DESELECTED
        # Update visuals for all
        for chain_num in range(self.n_chains):
            self.update_chain_grid_cell(chain_num)
            self.update_chain_lines(chain_num)
        # Re-draw canvases
        for canvas in self.canvases:
            canvas.draw()


class VerticalScrolledFrame(tkinter.Frame):
    """A pure Tkinter scrollable frame that actually works!
    * Use the 'interior' attribute to place widgets inside the scrollable frame
    * Construct and pack/place/grid normally
    * This frame only allows vertical scrolling

    Taken from  http://tkinter.unpythonic.net/wiki/VerticalScrolledFrame
    """
    def __init__(self, parent, *args, **kw):
        super().__init__(parent, *args, **kw)

        # create a canvas object and a vertical scrollbar for scrolling it
        vscrollbar = tkinter.Scrollbar(self, orient=tkinter.VERTICAL)
        vscrollbar.pack(fill=tkinter.Y, side=tkinter.RIGHT, expand=False)
        canvas = tkinter.Canvas(
            self, bd=0, highlightthickness=0,
            yscrollcommand=vscrollbar.set,
        )
        canvas.pack(side=tkinter.LEFT, fill=tkinter.BOTH, expand=True)
        vscrollbar.config(command=canvas.yview)

        # reset the view
        canvas.xview_moveto(0)
        canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        self.interior = interior = tkinter.Frame(canvas)
        interior_id = canvas.create_window(
            0, 0, window=interior, anchor=tkinter.NW)

        # track changes to the canvas and frame width and sync them,
        # also updating the scrollbar
        def _configure_interior(event):
            # update the scrollbars to match the size of the inner frame
            size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
            canvas.config(scrollregion="0 0 %s %s" % size)
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the canvas's width to fit the inner frame
                canvas.config(width=interior.winfo_reqwidth())
        interior.bind('<Configure>', _configure_interior)

        def _configure_canvas(event):
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the inner frame's width to fill the canvas
                canvas.itemconfigure(interior_id, width=canvas.winfo_width())
        canvas.bind('<Configure>', _configure_canvas)


def make_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posterior_chain_input",
        help="Input raw posteriors file.",
    )

    return parser


def main() -> int:
    cli_parser = make_parser()
    cli_args = cli_parser.parse_args()

    with ChainHackingGUI(cli_args.posterior_chain_input) as app:
        pass

    return 0
