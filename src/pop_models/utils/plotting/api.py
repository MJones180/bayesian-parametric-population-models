def limit_ax_oom(
        ax, n_oom: int, x_or_y: str,
        measure_from: str="upper", base: float=10.0,
    ) -> None:
    r"""
    Takes a :class:`matplotlib.axes._subplots.AxesSubplot` object, and limits
    its limits to ``n_oom`` orders of magnitude (in the logarithmic base set by
    the ``base`` parameter, ``10.0`` by default) from either the upper or lower
    data limit (set with ``measure_from`` argument, "upper" by default).  Either
    operates on x- or y-axis, set by passing "x" or "y" for ``x_or_y`` argument.

    If the data limits are already within the specified orders of magnitude, no
    changes will be made.
    """
    import math
    import numpy

    # Validate inputs.
    if x_or_y not in {"x", "y"}:
        raise KeyError("Invalid choice for 'x_or_y', must be either 'x' or 'y'")
    if measure_from not in {"upper", "lower"}:
        raise KeyError(
            "Invalid choice for 'measure_from', must be either 'upper' or "
            "'lower'"
        )

    # Get data limits along specified axis.
    if x_or_y == "x":
        data_lo, data_hi = ax.dataLim.x0, ax.dataLim.x1
    else:
        data_lo, data_hi = ax.dataLim.y0, ax.dataLim.y1

    # Convert data limits to log-scale, rounding to the next integer.
    data_log_lo = numpy.floor(numpy.log(data_lo) / numpy.log(base))
    data_log_hi = numpy.ceil(numpy.log(data_hi) / numpy.log(base))

    # Do nothing if data limits already close enough.
    if data_log_hi - data_log_lo <= n_oom:
        return

    # Determine the new bound to set.
    if measure_from == "upper":
        data_lo = base ** (data_log_hi - n_oom)
    else:
        data_hi = base ** (data_log_lo + n_oom)

    # Alter the specified axis limits.
    if x_or_y == "x":
        ax.set_xlim([data_lo, data_hi])
    else:
        ax.set_ylim([data_lo, data_hi])
