"""
Makes corner plots overlaying multiple datasets, where some datasets may have
extra/missing variables.

Example usage:
```
from pop_models.utils.plotting.corner_overlay import CornerOverlay

n_points = 10000

overlay = CornerOverlay(["x", "y", "z"])

kwargs = {
  "levels" : [0.9, 0.5],
  "plot_density" : False,
  "plot_datapoints" : False,
  "no_fill_contours" : True,
  "contour_kwargs" : {
    "linestyles" : ["solid", "dashed"],
  },
}

d1 = {
  "x" : numpy.random.normal(size=n_points),
  "z" : numpy.random.normal(size=n_points),
}
d2 = {
  "x" : 0.7*numpy.random.normal(size=n_points),
  "y" : 1.5*numpy.random.normal(size=n_points),
}
d3 = {
  "x" : 0.7*numpy.random.normal(size=n_points),
  "y" : 1.5*numpy.random.normal(size=n_points),
  "z" : 0.5*numpy.random.normal(size=n_points),
}

overlay.corner(d1, label="Dataset 1", **kwargs)
overlay.corner(d2, label="Dataset 2", **kwargs)
overlay.corner(d3, label="Dataset 3", **kwargs)

overlay.legend()

overlay.fig.savefig("test_plot.png")
```

Author: Daniel Wysocki
"""
import numpy

__all__ = [
    "CornerOverlay",
]

_rescalings = {
    "linear" : lambda x: x,
    "log" : numpy.log10,
}

class CornerOverlay(object):
    def __init__(
            self,
            names,
            fig=None, figsize=None, varlength=3.0,
            names_formatted=None, rescale_parameters=None, set_int=None,
            only_1d: bool=False, no_legend: bool=False,
        ):
        import itertools
        import matplotlib as mpl
        import matplotlib.pyplot as plt

        self.only_1d = only_1d

        if fig is None:
            n_vars = len(names)
            if only_1d:
                if no_legend:
                    figsize = (
                        (varlength*n_vars, 1.2*varlength) if figsize is None
                        else figsize
                    )
                    self.fig, _axes = plt.subplots(
                        1, n_vars,
                        squeeze=False,
                        figsize=figsize,
                        constrained_layout=True,
                    )
                    self.axes = _axes[0,:]
                    self.ax_legend = None
                else:
                    figsize = (
                        (varlength*n_vars, varlength*1.75) if figsize is None
                        else figsize
                    )
                    self.fig = plt.figure(
                        figsize=figsize,
                        constrained_layout=True,
                    )

                    gs = mpl.gridspec.GridSpec(
                        2, n_vars,
                        height_ratios=(0.2, 0.8),
                        figure=self.fig,
                    )

                    self.ax_legend = self.fig.add_subplot(gs[0, :])
                    self.axes = numpy.asarray([
                        self.fig.add_subplot(gs[1,col]) for col in range(n_vars)
                    ])

                    self.ax_legend.axis("off")

            else:
                figsize = (
                    (varlength*n_vars, varlength*n_vars) if figsize is None
                    else figsize
                )
                self.fig, self.axes = plt.subplots(
                    n_vars, n_vars,
                    squeeze=True,
                    figsize=figsize,
                    constrained_layout=True,
                )
                self.ax_legend = None if no_legend else self.axes[0,-1]

            if only_1d:
                for col in range(n_vars):
                    self.axes[col].set_yticklabels([])
            else:
                for row, col in itertools.product(range(n_vars), repeat=2):
                    # Hide unused axes
                    if col > row:
                        self.axes[row,col].axis("off")
                    # Hide redundant tick labels
                    if row < n_vars-1:
                        self.axes[row,col].set_xticklabels([])
                    if col > 0:
                        self.axes[row,col].set_yticklabels([])
        else:
            self.fig = fig
            self.axes = fig.axes

        self.rescale_parameters = (
            {} if rescale_parameters is None
            else rescale_parameters
        )
        self.rescale_functions = {
            name : _rescalings[self.rescale_parameters.get(name, "linear")]
            for name in names
        }

        if set_int is None:
            self._set_int = []
        else:
            self._set_int = set_int
            for name in set_int:
                rescale = self.rescale_functions[name]
                self.rescale_functions[name] = lambda x: rescale(x.astype(int))

        self.names = names
        self.labels = []
        self.handles = []

        self.color_cycle = itertools.cycle(
            plt.rcParams["axes.prop_cycle"].by_key()["color"]
        )

        # Handle formatting of parameter names, including overriden formats and
        # different scalings.
        if names_formatted is None:
            names_formatted = names

        names_formatted = [
            "$\log_{{10}}$ {}".format(name_fmt)
            if self.rescale_parameters.get(name) == "log"
            else name_fmt
            for name, name_fmt in zip(names, names_formatted)
        ]

        for i, name in enumerate(names_formatted):
            if only_1d:
                self.axes[i].set_xlabel(name)
            else:
                self.axes[i,0].set_ylabel(name)
                self.axes[-1,i].set_xlabel(name)

    def corner(
            self,
            data,
            label=None, constants=None,
            show_fraction=None,
            **kwargs
        ):
        from itertools import islice
        import corner
        import matplotlib as mpl
        import matplotlib.pyplot as plt

        # Ensure `constants` is a dict.
        if constants is None:
            constants = {}

        # Will want to make this plot with interactive mode off, so that dummy
        # figures don't pop up.
        is_interactive = mpl.is_interactive()
        if is_interactive:
            plt.ioff()

        # Use color cycle if no color provided
        if "color" not in kwargs:
            color = next(self.color_cycle)
            kwargs["color"] = color
        else:
            color = kwargs["color"]

        # Use full alpha if no alpha provided
        alpha = kwargs.get("alpha", 1.0)

        # Ensure color and alpha get passed to places they wouldn't normally
        if "contour_kwargs" not in kwargs:
            kwargs["contour_kwargs"] = {
                "colors" : color,
                "alpha" : alpha,
            }
        else:
            if "colors" not in kwargs["contour_kwargs"]:
                # Copy to be safe and store color
                kwargs["contour_kwargs"] = kwargs["contour_kwargs"].copy()
                kwargs["contour_kwargs"]["colors"] = color
            if "alpha" not in kwargs["contour_kwargs"]:
                # Copy to be safe and store alpha
                kwargs["contour_kwargs"] = kwargs["contour_kwargs"].copy()
                kwargs["contour_kwargs"]["alpha"] = alpha
        if "hist_kwargs" not in kwargs:
            kwargs["hist_kwargs"] = {}

        # Include some extra default histogram kwargs
        kwargs["hist_kwargs"]["color"] = (
            kwargs["hist_kwargs"].get("color", color)
        )
        kwargs["hist_kwargs"]["alpha"] = (
            kwargs["hist_kwargs"].get("alpha", alpha)
        )
        kwargs["hist_kwargs"]["histtype"] = (
            kwargs["hist_kwargs"].get("histtype", "step")
        )

        # Construct a list of variable names in `data`, sorted in the same order
        # as they appear in `self.names`.
        names_subset = [name for name in self.names if name in data]

        # Handle special case of 1D marginal histograms only
        if self.only_1d:
            for name in names_subset:
                # Grab data values, and perform any rescaling.
                xs = self.rescale_functions[name](data[name])
                # Set up histogram parameters.
                col = self.names.index(name)
                ax = self.axes[col]
                ranges = kwargs.get("range")
                if ranges is None:
                    hist_range = [xs.min(), xs.max()]
                else:
                    raise NotImplementedError(
                        "Currently do not support manual ranges being passed "
                        "in for 1-D overlays."
                    )

                weights = kwargs.get("weights")

                if name in self._set_int:
                    x_min, x_max = hist_range
                    hist_range=None
                    bins = numpy.arange(x_min-0.5, x_max+1.5, 1.0)
                    ax.xaxis.set_major_locator(
                        mpl.ticker.MaxNLocator(integer=True, nbins="auto")
                    )
                    ax.xaxis.set_minor_locator(
                        mpl.ticker.MaxNLocator(integer=True)
                    )
                else:
                    bins = kwargs.get("bins", 20)
                    hist_range = numpy.sort(hist_range)

                # Overlay the histogram on the appropriate subplot.
                ax.hist(
                    xs, bins, weights=weights,
                    range=hist_range, **kwargs["hist_kwargs"],
                )
        # Use corner.py if we are at least 2D
        elif len(names_subset) > 1:
            # Make a fake `matplotlib.figure` object, which will trick `corner`
            # into only overplotting on the desired locations.
            fake_fig = OverlaySubsetFigure(self.axes, self.names, names_subset)
            # Make the input data to pass to `corner`.
            xs = numpy.column_stack(tuple(
                self.rescale_functions[name](data[name])
                for name in names_subset
            ))

            # Specify range if provided
            if show_fraction is None:
                plot_range = None
            else:
                plot_range = [show_fraction]*len(names_subset)

            # Overlay the corner plot.
            corner.corner(xs, fig=fake_fig, range=plot_range, **kwargs)
        # Fall back to just a manual histogram if we're 1D
        ## NOTE: This is only necessary because of a bug in corner.py
        ## https://github.com/dfm/corner.py/issues/110
        elif len(names_subset) == 1:
            # Extract/transform the data to be plotted.
            name = names_subset[0]
            xs = self.rescale_functions[name](data[name])
            # Set up histogram parameters.
            col = self.names.index(name)
            ax = self.axes[col,col]
            ranges = kwargs.get("range")
            if ranges is None:
                hist_range = [xs.min(), xs.max()]
            else:
                raise NotImplementedError(
                    "Currently do not support manual ranges being passed in "
                    "for 1-D overlays."
                )
            bins = kwargs.get("bins", 20)
            weights = kwargs.get("weights")
            # Overlay the histogram on the appropriate diagonal.
            ax.hist(
                xs, bins, weights=weights,
                range=numpy.sort(hist_range), **kwargs["hist_kwargs"],
            )

        # Skip to just constants overlay if there are no variables to plot.
        else:
            pass

        # Overlay any constants.
        # Iterate over columns
        for col, col_name in enumerate(self.names):
            # Pull out appropriate axis
            ax = self.axes[col] if self.only_1d else self.axes[col,col]
            # Plot on the 1-D marginal axis
            if col_name in constants:
                ax.axvline(
                    self.rescale_functions[col_name](constants[col_name]),
                    color=color,
                )

            # Skip 2-D marginals if we're only doing 1-D marginals
            if self.only_1d:
                continue

            # Go down the column, and overlay the constants.
            for row, row_name in islice(enumerate(self.names), col+1, None):
                # Pull out appropriate axis
                ax = self.axes[row,col]
                # Plot the column
                if col_name in constants:
                    ax.axvline(
                        self.rescale_functions[col_name](constants[col_name]),
                        color=color,
                    )

                # Plot the row
                if row_name in constants:
                    ax.axhline(
                        self.rescale_functions[row_name](constants[row_name]),
                        color=color,
                    )

                # Plot the intersection of the row and column
                if (col_name in constants) and (row_name in constants):
                    ax.plot(
                        self.rescale_functions[col_name](constants[col_name]),
                        self.rescale_functions[row_name](constants[row_name]),
                        marker="s", color=color,
                    )

        # If a label was provided, store bookkeeping for legend.
        if label is not None:
            self.labels.append(label)
            self.handles.append(
                mpl.lines.Line2D([0], [0], color=color)
            )

        # Fix datalims
        self._update_datalims(names_subset)

        # Turn interactive mode back on, if it was initially.
        if is_interactive:
            plt.ion()

    def _update_datalims(self, names_updated):
        for i, name in enumerate(self.names):
            # Skip unaffected ones.
            if name not in names_updated:
                continue

            # Get 1-D marginal axis
            ax_diag = self.axes[i] if self.only_1d else self.axes[i,i]
            x0, x1 = ax_diag.dataLim.x0*0.99, ax_diag.dataLim.x1*1.01
            y1 = ax_diag.dataLim.y1

            # Update y-axis for 1-D marginal
            ax_diag.set_ylim([0.0, y1*1.01])

            # Skip over 2-D marginals if we are only plotting 1-D marginals
            if self.only_1d:
                continue

            # Update x-axes for 2-D marginals
            for row in range(i, len(self.names)):
                self.axes[row,i].set_xlim([x0, x1])
            # Update y-axes for 2-D marginals
            for col in range(0, i):
                self.axes[i,col].set_ylim([x0, x1])

    def truths(self, truths, color="black"):
        """
        Overplot 'truth' values for parameters.
        """
        from itertools import islice

        # Rescale truths
        truths = {
            name : self.rescale_functions[name](truths[name])
            for name in self.names
        }

        # Iterate over columns
        for col, col_name in enumerate(self.names):
            # Skip anything not present in `truths`
            if col_name not in truths:
                continue

            # Get the 1-D marginal axis
            ax = self.axes[col] if self.only_1d else self.axes[col,col]

            # Plot on the 1-D marginal axis
            ax.axvline(truths[col_name], color=color)

            # Skip 2-D marginals if we're only doing 1-D marginals
            if self.only_1d:
                continue

            # Go down the column, and overlay the truth.
            for row, row_name in islice(enumerate(self.names), col+1, None):
                self.axes[row,col].axvline(truths[col_name], color=color)
                self.axes[row,col].axhline(truths[row_name], color=color)
                self.axes[row,col].plot(
                    truths[col_name], truths[row_name],
                    marker="s", color=color,
                )

    def legend(self, handles=None, labels=None, **kwargs):
        if self.ax_legend is None:
            raise RuntimeError(
                "This overlay was initialized to have no legend."
            )

        # Determine whether to use provided handles/labels, or ones passed as
        # arguments.
        handles = self.handles if handles is None else handles
        labels = self.labels if labels is None else labels

        # Display the legend.
        return self.ax_legend.legend(handles=handles, labels=labels, **kwargs)

    def cleanup(self) -> None:
        """
        Clean up any common artifacts of overlays.
        """
        # Hide any x-ticklabels that shouldn't be there in 2-D plots
        if self.axes.ndim == 2:
            n_rows, n_cols = self.axes.shape
            for row in range(n_rows-1):
                for col in range(row+1):
                    self.axes[row,col].set_xticklabels([])

    @staticmethod
    def make_parser():
        import argparse

        parser = argparse.ArgumentParser()

        parser.add_argument(
            "output_plot",
            help="Filename to store plot in.",
        )

        parser.add_argument(
            "--overplot-posteriors",
            action="append", nargs="+",
            help="Overplot another posterior file.  First argument must be the "
                 "filename, and one can optionally follow with key=value "
                 "pairs.  Supported keys are: 'color', 'alpha', 'label', and "
                 "'remap'.  'remap' should be followed by a comma-delimited "
                 "string, one for each remapped parameter, with "
                 "[name]->[new name] specifying the re-mappings.",
        )

        parser.add_argument(
            "--param-names",
            nargs="+",
            help="List of parameter names to include.  By default uses all "
                 "parameters present in at least one file.",
        )
        parser.add_argument(
            "--param-labels",
            nargs="+",
            help="List of parameter labels to replace the raw parameter names. "
                 "Must match the order that `--param-names` follows, with one "
                 "entry for each parameter.",
        )

        parser.add_argument(
            "--set-scale",
            action="append", nargs=2, metavar=("PARAM_NAME", "SCALE"),
            help="Set the scale on a chosen parameter.  Scale can be one of "
                 "'linear' or 'log'.",
        )

        parser.add_argument(
            "--show-fraction",
            type=float,
            help="Only show the given fraction of points (default is 1.0).  "
                 "Excluded points will be determined by equal-tailed "
                 "quantiles.",
        )

        parser.add_argument(
            "--set-int",
            action="append", metavar=("PARAM_NAME"),
            help="Set a parameter `x` to be replaced by `int(x)`.",
        )

        parser.add_argument(
            "--levels",
            type=float, nargs="+",
            default=[0.9, 0.5],
            help="Contour levels (in enclosed probability) to plot.",
        )

        parser.add_argument(
            "--truths",
            help="Provide a mapping of parameter names to 'truth' values in a "
                 "JSON file.",
        )
        parser.add_argument(
            "--truth-color",
            default="black",
            help="Color to display '--truths' values in, default is black.",
        )

        parser.add_argument(
            "--suppress-missing-labels",
            action="store_true",
            help="If label is not provided explicitly, don't fill one in.",
        )

        parser.add_argument(
            "--only-1d",
            action="store_true",
            help="Only plot 1-D marginals, instead of the full corner plot.",
        )

        parser.add_argument(
            "--fig-size", metavar=("WIDTH", "HEIGHT"),
            type=float, nargs=2,
            help="Width and height in inches to fix the figure size to.",
        )

        parser.add_argument(
            "--var-length",
            type=float, default=3.0,
            help="Width/height to add to the total figure size for each "
                 "variable.",
        )

        parser.add_argument(
            "--legend-title",
            help="Add a title to the legend.",
        )
        parser.add_argument(
            "--no-legend",
            action="store_true",
            help="Suppress legend.",
        )

        parser.add_argument(
            "--mpl-backend",
            default="Agg",
            help="Use the specified backend for matplotlib.",
        )

        return parser

    @classmethod
    def cli(cls):
        import os
        import json
        import itertools
        from ...posterior import H5CleanedPosteriorSamples

        # Parse command line.
        cli_parser = cls.make_parser()
        cli_args = cli_parser.parse_args()

        # Set matplotlib backend, and load pyplot
        import matplotlib as mpl
        mpl.use(cli_args.mpl_backend)
        import matplotlib.pyplot as plt

        # Determine linestyles.
        linestyles = (
            ["solid", "dashed", "dotted", "densely dashdotted"]
            [:len(cli_args.levels)]
        )

        color_cycle = itertools.cycle("C{}".format(i) for i in range(10))

        # Parse out settings for each overplot
        overplot_settings = []
        for post_info in cli_args.overplot_posteriors:
            post_filename = post_info[0]
            post_settings = dict(kv.split("=", 1) for kv in post_info[1:])

            default_label = (
                None if cli_args.suppress_missing_labels
                else os.path.basename(post_filename)
            )
            label = post_settings.get("label", default_label)
            color = post_settings.get("color", next(color_cycle))
            alpha = post_settings.get("alpha", 1.0)

            if "remap" in post_settings:
                remappings = dict(
                    kv.split("->")
                    for kv in post_settings["remap"].split(",")
                )
            else:
                remappings = {}

            overplot_settings.append({
                "filename" : post_filename,
                "label" : label,
                "color" : color,
                "alpha" : float(alpha),
                "remappings" : remappings,
            })


        # Determine which parameter names to use.
        # If none were provided explicitly, build up a list by visiting each
        # file, and appending any of its parameter names in the order they
        # appear, if missing.  This ensures the sorted order is somewhat
        # reasonable.
        if cli_args.param_names is None:
            param_names = []

            for post_info in overplot_settings:
                post_filename = post_info["filename"]
                with H5CleanedPosteriorSamples(post_filename) as post_file:
                    remapped_names = [
                        post_info["remappings"].get(name, name)
                        for name in post_file.variable_names
                    ]
                    for name in remapped_names:
                        if name not in param_names:
                            param_names.append(name)
        else:
            # Use user-provided values.
            param_names = cli_args.param_names

        rescale_parameters = (
            None if cli_args.set_scale is None
            else dict(cli_args.set_scale)
        )
        overlay = cls(
            param_names,
            names_formatted=cli_args.param_labels,
            rescale_parameters=rescale_parameters,
            set_int=cli_args.set_int,
            figsize=cli_args.fig_size, varlength=cli_args.var_length,
            only_1d=cli_args.only_1d, no_legend=cli_args.no_legend,
        )
        for post_info in overplot_settings:
            post_filename = post_info["filename"]

            with H5CleanedPosteriorSamples(post_filename) as post_file:
                # Access the posterior samples.
                parameter_samples = post_file.get_params(...)
                # Throw away constants and duplicates.
                for param_name in post_file.constants:
                    del parameter_samples[param_name]
                for param_name in post_file.duplicates:
                    del parameter_samples[param_name]
                # Create new `parameter_samples` dictionary with any param name
                # remappings applied.
                parameter_samples = {
                    post_info["remappings"].get(param_name, param_name) : values
                    for param_name, values in parameter_samples.items()
                }
                # Create new `constants` dictionary with any param name
                # remappings applied
                constants = {
                    post_info["remappings"].get(param_name, param_name) : value
                    for param_name, value in post_file.constants.items()
                }

                # Throw away anything we aren't using.
                for param_name in list(parameter_samples.keys()):
                    if param_name not in param_names:
                        del parameter_samples[param_name]
                for param_name in list(constants.keys()):
                    if param_name not in param_names:
                        del constants[param_name]

            # Make the plot
            overlay.corner(
                parameter_samples,
                constants=constants,
                show_fraction=cli_args.show_fraction,
                label=post_info["label"],
                color=post_info["color"], alpha=post_info["alpha"],
                levels=cli_args.levels,
                plot_density=False,
                plot_datapoints=False,
                no_fill_contours=True,
                hist_kwargs={"density" : True},
                contour_kwargs={"linestyles" : linestyles},
            )

        if cli_args.truths is not None:
            with open(cli_args.truths, "r") as truths_file:
                truths = json.load(truths_file)

            overlay.truths(truths, color=cli_args.truth_color)

        # Add a legend.
        if not cli_args.no_legend:
            if cli_args.only_1d:
                overlay.legend(
                    title=cli_args.legend_title,
                    ncol=len(param_names),
                    mode="expand", borderaxespad=0,
                    loc="center",
                )
            else:
                overlay.legend(title=cli_args.legend_title)

        # Clean up any artifacts.
        overlay.cleanup()

        # Save to a file.
        overlay.fig.savefig(cli_args.output_plot)


class OverlaySubsetFigure(object):
    def __init__(self, axes, names_full, names_subset):
        import matplotlib.pyplot as plt

        n_vars_subset = len(names_subset)
        _axes = numpy.empty((n_vars_subset, n_vars_subset), dtype=object)
        self._dummy_figs = []

        for row_subset, row_name in enumerate(names_subset):
            row_full = names_full.index(row_name)
            for col_subset, col_name in enumerate(names_subset):
                col_full = names_full.index(col_name)
                # Axes on the upper diagonal should go un-touched.
                if col_subset > row_subset:
                    dummy_fig, ax = plt.subplots()
                    self._dummy_figs.append(dummy_fig)
                # Axes on the lower diagonal will be used directly.
                else:
                    ax = axes[row_full, col_full]

                _axes[row_subset, col_subset] = ax

        # Flatten axes
        self.axes = list(_axes.flatten())

    def subplots_adjust(self, *args, **kwargs):
        """
        Overriding the one method of Figure that corner.py currently calls.
        If future versions call other methods of Figure, they will need to be
        implemented as well (but they should do nothing, unless corner.py
        changes its behavior significantly).
        """
        pass

    def close(self):
        import matplotlib.pyplot as plt
        for fig in self._dummy_figs:
            plt.close(fig)
