import numpy

from pop_models.posterior import (
    H5RawPosteriorSamples,
    H5CleanedPosteriorSamples,
)
from .max_posterior import compute_burnin, plot_burnins
#from .acorr import full_acorr_analysis, plot_acorrs
from . import integrated_acorr

def make_parser():
    import argparse

    parser = argparse.ArgumentParser()

    burnin_group = parser.add_mutually_exclusive_group()
    acorr_group = parser.add_mutually_exclusive_group()

    parser.add_argument("input_raw_samples")
    parser.add_argument("output_cleaned_samples")

    burnin_group.add_argument(
        "--burnin-plot",
        help="Plot distribution of burnin lengths to this file.",
    )
    acorr_group.add_argument(
        "--acorr-plot",
        help="Plot integrated autocorrelations to this file.",
    )

    parser.add_argument(
        "--acorr-safety-factor",
        metavar="M",
        type=float, default=5.0,
        help="Integrated auto-correlation safety factor to use if --acorr-plot "
             "is provided.",
    )
    parser.add_argument(
        "--lag-max",
        type=int, default=100,
        help="Maximum time lag to consider.  Default is 100.",
    )

    burnin_group.add_argument(
        "--fixed-burnin",
        type=int,
        help="Assume a fixed burnin length."
    )
    acorr_group.add_argument(
        "--fixed-thinning",
        type=int,
        help="Assume a fixed thinning."
    )

    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib.",
    )

    parser.add_argument(
        "--force",
        action="store_true",
        help="Force writing to output file if it already exists.",
    )

    parser.add_argument(
        "--swmr-mode",
        action="store_true",
        help="Read file in single-writer multiple reader mode.",
    )

    return parser

def main(raw_args=None):
    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    cli_parser = make_parser()
    cli_args = cli_parser.parse_args(raw_args)

    # Load matplotlib
    import matplotlib
    matplotlib.use(cli_args.mpl_backend)
    import matplotlib.pyplot as plt

    # Load in raw chains.
    f = H5RawPosteriorSamples(
        cli_args.input_raw_samples, "r",
        swmr_mode=cli_args.swmr_mode,
    )

    # Determine burn-in time.
    if cli_args.burnin_plot is not None:
        n_burnin, n_burnins = compute_burnin(
            f.get_posterior_log_prob(slice(1,None)),
            f.n_dim,
            retall=True,
        )
        print("Burnin is:", n_burnin)
        fig, ax = plot_burnins(n_burnins)
        fig.savefig(cli_args.burnin_plot)
        plt.close(fig)
    elif cli_args.fixed_burnin is not None:
        n_burnin = cli_args.fixed_burnin
    else:
        n_burnin = 0

    # Determine auto-correlation and appropriate lag time.
    if cli_args.acorr_plot is not None:
        samples_post_burnin = f.get_samples(slice(n_burnin,None))
#        posterior_log_prob_post_burnin = (
#            f.get_posterior_log_prob(slice(n_burnin,None))
#        )
        (
            n_acorr,
            (acorrs, acorr_errs),
            (acorr_times, acorr_time_errs),
        ) = (
            integrated_acorr.acorr_time(
                samples_post_burnin, retall=True,
                m=cli_args.acorr_safety_factor,
            )
        )
#        delta_n_acorr = numpy.tile(n_acorr, (f.n_walkers, f.n_dim))
        # print(
        #     "Auto-correlation time is {dn} for the worst parameter."
        #     .format(dn=n_acorr)
        # )
        # print(
        #     "Auto-correlation time for all parameters are",
        #     ", ".join([str(x) for x in acorr_times[n_acorr]]),
        # )
        # Take the most auto-correlated walker and parameter throughout.
        n_thinning = numpy.max(n_acorr)
        print("Thinning is", n_thinning)
        # Plot the autocorrelation diagnostics.
        print(acorrs.shape)
        integrated_acorr.plot_integrated_auto_correlation(
            cli_args.acorr_plot,
            acorr_times, None,
            param_names=f.variable_names,
            safety_factor=cli_args.acorr_safety_factor,
        )
    elif cli_args.fixed_thinning is not None:
        n_thinning = cli_args.fixed_thinning
    else:
        n_thinning = 1

    with f as h5_raw:
        H5CleanedPosteriorSamples.from_raw_manual(
            cli_args.output_cleaned_samples, h5_raw,
            burnin=n_burnin, thinning=n_thinning,
            force=cli_args.force,
        )
