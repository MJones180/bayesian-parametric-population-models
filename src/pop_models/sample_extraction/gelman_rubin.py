from __future__ import division


def R_statistic_normal(chains):
    import numpy
    from ..utils import covariance

    n_samples, n_walkers, n_params = numpy.shape(chains)

    # Sample variance of each walker.
    # s_sq[i,j] gives sample variance of the jth parameter for the ith walker.
    s_sq = numpy.var(chains, axis=0, ddof=1)

    # Sample average of s_sq taken over all walkers.
    # W[j] gives the sample average of the jth parameter.
    W = numpy.mean(s_sq, axis=0)

    # Sample average along each individual chain, and the average of those
    # averages.
    # x_bar[i,j] gives the sample average of the jth parameter for the ith
    # walker, and x_bar_bar[j] gives the sample average of the jth parameter
    # across all walkers
    x_bar = numpy.mean(chains, axis=0)
    x_bar_bar = numpy.mean(x_bar, axis=0)

    # TODO: what does B denote? Some kind of variance.
    B_by_n = numpy.sum(numpy.square(x_bar-x_bar_bar), axis=0) / (n_walkers-1)

    # Estimate of variance of target distribution, assuming Gaussianity
    sigma_sq_hat = B_by_n + (n_samples-1)/n_samples * W

    return sigma_sq_hat / W


def R_statistic_t(chains):
    import numpy
    from ..utils import covariance

    n_samples, n_walkers, n_params = numpy.shape(chains)

    # Sample variance of each walker.
    # s_sq[i,j] gives sample variance of the jth parameter for the ith walker.
    s_sq = numpy.var(chains, axis=0, ddof=1)

    # Sample average of s_sq taken over all walkers.
    # W[j] gives the sample average of the jth parameter.
    W = numpy.mean(s_sq, axis=0)

    # Sample average along each individual chain, and the average of those
    # averages.
    # x_bar[i,j] gives the sample average of the jth parameter for the ith
    # walker, and x_bar_bar[j] gives the sample average of the jth parameter
    # across all walkers
    x_bar = numpy.mean(chains, axis=0)
    x_bar_bar = numpy.mean(x_bar, axis=0)

    # TODO: what does B denote? Some kind of variance.
    B_by_n = numpy.sum(numpy.square(x_bar-x_bar_bar), axis=0) / (n_walkers-1)

    # Estimate of variance of target distribution, assuming Gaussianity
    sigma_sq_hat = B_by_n + (n_samples-1)/n_samples * W

    # Relax Gaussianity assumption to Student's t-distribution.
    # Estimate variance of t-distribution
    V_hat = sigma_sq_hat + B_by_n / n_walkers
    # Estimate the variance of the variance estimator
    # Sigma_ssq_xbar_sq = numpy.array([
    #     covariance([s_sq[...,i], numpy.square(x_bar[...,i])])
    #     for i in range(n_params)
    # ])
    # Sigma_ssq_xbar = numpy.array([
    #     numpy.cov([s_sq[...,i], x_bar[...,i]])
    #     for i in range(n_params)
    # ])
    var_V_hat = (
        numpy.square((n_samples-1)/n_samples) / n_walkers *
        numpy.var(s_sq, axis=0)
    )
    var_V_hat += (
        2*numpy.square((n_walkers+1)/n_walkers)/(n_walkers-1) *
        numpy.square(B_by_n)
    )
    var_V_hat += (
        2 * (n_walkers+1)*(n_samples-1) / (n_samples*numpy.square(n_walkers)) *
        (
            covariance(s_sq, numpy.square(x_bar), axis=0) -
            2.0 * x_bar_bar * covariance(s_sq, x_bar, axis=0)
        )
    )
    # Estimate the d.o.f. of the t-distribution
    dof = 2 * numpy.square(V_hat) / var_V_hat

    # Return the R-statistic
    return V_hat / W * dof / (dof-2)


__R_stats = {
    "normal" : R_statistic_normal,
    "t" : R_statistic_t,
}


def R_statistic(chains, dist="t"):
    if dist in __R_stats:
        return __R_stats[dist](chains)
    else:
        raise ValueError(
            "Distribution '{dist}' not found. "
            "Valid choices are: {dists}."
            .format(
                dist=dist,
                dists=", ".join([
                    "'{dist}'".format(dist=dist)
                    for dist in __R_stats
                ])
            )
        )


def burnin_time(
        chains, dist="t",
        tolerance=0.1,
        initial_grid_size=20, n_min=2,
        max_retries=10,
        return_R_stat=False,
    ):
    import numpy

    from ..utils import bisect_int

    n_samples, n_walkers, n_dim = numpy.shape(chains)

    # Optimal R-statistic is 1.0, but since we really want the user to specify
    # the acceptable *distance* from 1.0, we must add 1.0 to it. Negative values
    # aren't allowed, though.
    if tolerance < 0:
        raise ValueError("Cannot specify a negative tolerance.")
    sqrt_R_tol = 1 + tolerance


    # Initialize array that will hold the R-statistic for each sample size.
    # We will not necessarily evaluate it at every point, but we need to
    # initialize the array, so we fill it with nan's. Eventhough the R-statistic
    # is undefined for n=0 and n=1, to make array indexing simple we include
    # extra slots for them, so R_stats[n] is truly the R-statistic for chains of
    # length n.
    sqrt_R_stats = numpy.empty((n_samples+1, n_dim), dtype=numpy.float64)
    sqrt_R_stats[:] = numpy.nan

    def sqrt_R(n, d):
        # R-statistic is undefined for n < 2
        if n < 2:
            return numpy.nan
        # R-statistic hasn't been computed yet, so compute it and store it.
        elif numpy.all(numpy.isnan(sqrt_R_stats[n])):
            sqrt_R_stats[n] = numpy.sqrt(R_statistic(chains[:n], dist=dist))

        # R-statistic is defined, and has been tabulated (possibly during this
        # function call) so return it.
        return sqrt_R_stats[n,d]


    # Evaluate the R-statistic on an initial coarse grid.
    # We do this because the R-statistic can be noisy for small values of n, and
    # we don't want to risk a random fluctuation bringing it below the
    # tolerance. Since it should peak early on, and drop off as n increases, we
    # bound our search space by trying to find that peak. We make the grid
    # logarithmic (in base 2) because the peak is much more likely to be at
    # small n. We might have duplicates if the grid is too fine for the number
    # of samples available, so we only take the unique ones.
    n_coarse = numpy.logspace(
        numpy.log2(n_min), numpy.log2(n_samples), initial_grid_size,
        base=2, dtype=int,
    )
    for n in n_coarse:
        sqrt_R(n, 0)
    sqrt_R_coarse = sqrt_R_stats[n_coarse]
    n_mins = n_coarse[numpy.nanargmax(sqrt_R_coarse, axis=0)]

    del n_coarse, sqrt_R_coarse


    n_chosen = 0

    for d in range(n_dim):
        is_converged = False
        n_best = n_samples + 1

        lower_bound = n_mins[d]
        upper_bound = n_samples

        for _ in range(max_retries):
            n_cand = bisect_int(
                sqrt_R, lower_bound, upper_bound,
                preferred_positive=False,
                intercept=sqrt_R_tol,
                args=(d,)
            )

            if n_cand is None:
                break
            else:
                lower_bound = n_cand
                n_best = n_cand
                is_converged = True

        if n_best is None:
            n_chosen = None
            break
        elif n_best > n_chosen:
            n_chosen = n_best

    if return_R_stat:
        out = n_chosen, sqrt_R_stats
    else:
        out = n_chosen

    return out



def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing input raw posterior samples.",
    )

    parser.add_argument(
        "plot_file",
        help="File to output plot to.",
    )

    parser.add_argument(
        "--distribution",
        default="t", choices=["t", "normal"],
        help="Form to assume for target distribution.",
    )

    parser.add_argument(
        "--tolerance",
        type=float,
        help="Value of sqrt(R_hat) considered within tolerance of convergence.",
    )

    parser.add_argument(
        "--min-samples",
        type=int, default=100,
        help="Minimum number of samples to consider in computing R statistic "
             "(default is 100).",
    )

    parser.add_argument(
        "--sample-step",
        type=int, default=100,
        help="Difference between successive sample sizes to compute R "
             "statistic on (default is 100).",
    )

    parser.add_argument(
        "--param-names",
        nargs="+",
        help="Specify list of parameter names to use in plot.",
    )

    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib.",
    )

    return parser.parse_args(raw_args)




def _main(raw_args=None):
    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import h5py
    import numpy
    import matplotlib
    matplotlib.use(args.mpl_backend)
    import matplotlib.pyplot as plt

    with h5py.File(args.posteriors, "r") as posteriors:
        pos = posteriors["pos"].value
        n_samples, n_walkers, n_params = pos.shape

    if args.param_names is None:
        param_names = ["Param #{}".format(i) for i in range(n_params)]
    else:
        param_names = args.param_names

    R_stat = {
        "normal" : R_statistic_normal,
        "t" : R_statistic,
    }[args.distribution]

    ns = numpy.arange(
        args.min_samples,
        n_samples+args.sample_step,
        args.sample_step,
    )

    R_hats = numpy.array([R_stat(pos[:n]) for n in ns])
    sqrt_R_hats = numpy.sqrt(R_hats)

    fig, ax = plt.subplots()

    for sqrt_R_hat, name in zip(sqrt_R_hats.T, param_names):
        ax.plot(ns, sqrt_R_hat, label=name)

    if args.tolerance is not None:
        ax.axhline(args.tolerance, color="black", linestyle="dashed")

        # Compute the number of samples where sqrt(R_hat) first goes below the
        # tolerance along all parameters.
        i_tol = max(numpy.argmax(sqrt_R_hats < args.tolerance, axis=0))
        n_tol = ns[i_tol]
        print("Converges after {n} samples.".format(n=n_tol))

        ax.axvline(n_tol, color="black", linestyle="dashed")


    ax.set_xlabel("# of samples")
    ax.set_ylabel(r"$\sqrt{\hat{R}}$")

    ax.set_xscale("log")
    ax.set_yscale("log")

    ax.legend(loc="best")

    fig.savefig(args.plot_file)
