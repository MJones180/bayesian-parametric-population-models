from __future__ import division, print_function, unicode_literals

import numpy

LN_TEN = numpy.log(10.0)


def intensity(
        indiv_params, pop_params,
        aux_info,
        spins=False,
        **kwargs
    ):
    from . import prob

    rate = aux_info["rate"]

    return rate * prob.joint_pdf(indiv_params, pop_params, spins=spins)


def expval_mc(
        pop_params, aux_info,
        raw_interpolator=None,
        err_abs=1e-5, err_rel=1e-3,
        return_err=False,
        rand_state=None,
        **kwargs
    ):
    # TODO: incorporate aligned-spin VT's
    import numpy

    from .. import mc
    from . import prob

    rate = aux_info["rate"]

    def p(N):
        return prob.joint_rvs(N, pop_params, rand_state=rand_state, spins=False)

    def efficiency_fn(indiv_params):
        import numpy

        m1, m2 = prob.get_masses(indiv_params)

        return numpy.vectorize(raw_interpolator)(m1, m2)


    I, err_abs, err_rel = mc.integrate_adaptive(
        p, efficiency_fn,
        err_abs=err_abs, err_rel=err_rel,
    )

    if return_err:
        return rate*I, err_abs, err_rel
    else:
        return rate*I


def log_prior_pop(
        pop_params, aux_info,
        pop_prior_settings=None,
        spins=False,
        **kwargs
    ):
    from . import prob
    import numpy

    param_names = prob.param_names(spins=spins)

    pop_params_dict = dict(zip(param_names, pop_params))

    # Initialize variable to accumulate the value of log(prior)
    # NOTE: Currently only support uniform priors, so this never gets modified.
    #       However, future additional priors may modify it, so we keep it in
    #       preparation.
    log_prior = 0.0

    for param in param_names:
        prior_settings = pop_prior_settings[param]
        prior_dist = prior_settings["dist"]
        prior_params = prior_settings["params"]

        # Skip over constants and duplicates.
        if prior_dist in ["constant", "duplicate"]:
            continue
        # Add contribution for uniform prior.
        elif prior_dist == "uniform":
            min_val = prior_params["min"]
            max_val = prior_params["max"]
            val = pop_params_dict[param]
            if not (min_val <= val <= max_val):
                return -numpy.inf
        # Prior not implemented.
        else:
            raise NotImplementedError(
                "No implementation for prior '{}'".format(prior_dist)
            )

    # Enforce ordering on m_min and m_max.
    if pop_params_dict["m_min"] >= pop_params_dict["m_max"]:
        return -numpy.inf


    # Enforce limits on spin parameters (if any)
    if spins:
        alpha, beta = prob.spin_mu_sigma_sq_2_alpha_beta(
            pop_params_dict["mu_chi"], pop_params_dict["sigma_sq_chi"],
        )
        # Enforce positive-definite constraint on alpha,beta.
        if not (alpha > 0 and beta > 0):
            return -numpy.inf

        # Enforce ordering on chi_min and chi_max.
        if pop_params_dict["chi_min"] >= pop_params_dict["chi_max"]:
            return -numpy.inf


    # Return accumulated log(prior).
    return log_prior


def init_from_prior(
        nwalkers, nevents,
        constants, duplicates, pop_prior_settings,
        spins=False,
        rand_state=None,
    ):
    """
    Draw samples from the population prior distribution to initialize the
    walkers.
    """
    import numpy
    from ..prob import sample_with_cond
    from ..utils import check_random_state
    from . import prob, utils

    rand_state = check_random_state(rand_state)

    param_names = prob.param_names(spins=spins)


    def sample_prior_single(N, prior_settings):
        """
        Samples from a prior with given settings.
        """
        # Determines the type of distribution
        dist = prior_settings["dist"]
        # Determines the parameters of the distribution (e.g., {min,max})
        params = prior_settings["params"]

        # Sample from uniform distribution.
        if dist == "uniform":
            return rand_state.uniform(params["min"], params["max"], N)
        # Type of distribution is unknown.
        else:
            raise NotImplementedError(
                "No implementation for prior '{}'".format(dist)
            )

    def sample_prior(N):
        """
        Iterate over all free parameters and draw ``N`` samples from their
        priors. Then combine into an array, where each column holds the values
        from one parameter.
        """
        return numpy.column_stack(tuple((
            sample_prior_single(N, pop_prior_settings[param])
            for param in param_names
            # Skip over params which are fixed to constants, or are duplicates
            # of other params.
            if pop_prior_settings[param]["dist"]
            not in ["constant", "duplicate"]
        )))


    def cond(samples):
        n_samples, n_params = numpy.shape(samples)

        params = dict(zip(
            param_names,
            utils.get_params(samples, constants, duplicates, param_names),
        ))

        condition = params["m_min"] < params["m_max"]

        if spins:
            alpha_chi, beta_chi = prob.spin_mu_sigma_sq_2_alpha_beta(
                params["mu_chi"], params["sigma_sq_chi"],
            )
            condition &= (alpha_chi > 0) & (beta_chi > 0)
            condition &= params["chi_min"] < params["chi_max"]

        if numpy.size(condition) != n_samples:
            condition = numpy.tile(condition, n_samples)

        return condition


    return sample_with_cond(sample_prior, shape=nwalkers, cond=cond)


def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "events",
        nargs="*",
        help="List of posterior sample files, one for each event.",
    )
    parser.add_argument(
        "VTs",
        help="HDF5 file containing VTs.",
    )
    parser.add_argument(
        "pop_priors",
        help="JSON file specifying population priors.",
    )
    parser.add_argument(
        "posterior_output",
        help="HDF5 file to store posterior samples in.",
    )

    parser.add_argument(
        "--constants",
        help="JSON file fixing population parameters to constants.",
    )

    parser.add_argument(
        "--spins",
        action="store_true",
        help="Turn on the spin distribution."
    )

    parser.add_argument(
        "--n-walkers",
        default=None, type=int,
        help="Number of walkers to use, defaults to twice the number of "
             "dimensions.",
    )
    parser.add_argument(
        "--n-samples",
        default=100, type=int,
        help="Number of MCMC samples per walker.",
    )
    parser.add_argument(
        "--n-threads",
        default=1, type=int,
        help="Number of threads to use in MCMC.",
    )

    parser.add_argument(
        "--mass-prior",
        default="uniform",
        choices=["uniform"],
        help="Type of prior used for component masses.",
    )

    parser.add_argument(
        "--spin-prior",
        default="uniform",
        choices=["uniform", "Veitch_alt"],
        help="Type of prior used for aligned spin components. "
             "Will use max(chi_max) from the prior if 'Veitch_alt' is used.",
    )

    parser.add_argument(
        "--spin-max",
        type=float, default=1.0,
        help="Maximum allowed component spin magnitude.",
    )

    parser.add_argument(
        "--mc-err-abs",
        type=float, default=1e-5,
        help="Allowed absolute error for Monte Carlo integrator.",
    )
    parser.add_argument(
        "--mc-err-rel",
        type=float, default=1e-3,
        help="Allowed relative error for Monte Carlo integrator.",
    )

    parser.add_argument(
        "--seed",
        type=int, default=None,
        help="Random seed.",
    )

    parser.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="Use verbose output.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    import sys
    import six
    import json
    import h5py
    import numpy

    from .. import vt
    from .. import mcmc
    from .. import utils
    from . import prob

    if raw_args is None:
        raw_args = sys.argv[1:]

    cli_args = _get_args(raw_args)

    rand_state = numpy.random.RandomState(cli_args.seed)

    # Determine number of dimension and parameter names.
    # Depends on whether or not we included spins.
    param_names = prob.param_names(spins=cli_args.spins)
    ndim_pop = prob.ndim_pop(spins=cli_args.spins)


    # Load the population priors from a JSON file.
    with open(cli_args.pop_priors, "r") as priors_file:
        pop_prior_settings = json.load(priors_file)

        # TODO: validate file structure


    # No duplicates used. Model already assumes p(m1) = p(m2)
    duplicates = {}

    # Load in constants file.
    if cli_args.constants is None:
        # No constants to be loaded.
        constants = {}
    else:
        # Load constants in from a JSON file.
        with open(cli_args.constants, "r") as constants_file:
            constants = json.load(constants_file)

        ## Validate file structured properly. ##
        # Check that it's a dict.
        if not isinstance(constants, dict):
            raise TypeError(
                "Constants file must be a dictionary mapping param -> value"
            )
        # Check that the keys are all param names.
        unknown_params = [
            param for param in constants
            if param not in param_names
        ]
        if len(unknown_params) != 0:
            raise ValueError(
                "Unrecognized params in constants file:\n{}"
                .format(utils.format_set_in_quotes(unknown_params))
            )
        # Check that all of the param names map to floats.
        # NOTE: May have to change this if we ever allow for MCMC's over things
        # other than floats (integers?), but currently our underlying sampler
        # only supports floats.
        bad_values = []
        for param, value in six.iteritems(constants):
            if not isinstance(value, float):
                bad_values.append(param)
        if len(bad_values) != 0:
            raise TypeError(
                "The following constants mapped to invalid values:\n{}"
                .format(utils.format_set_in_quotes(bad_values))
            )


    # Parse constants file, and check for any conflicts with priors file.
    conflicting_priors = []
    for param in constants:
        # Check if parameter has already been given a prior. If so, we're going
        # to raise an exception once we've found all such parameters. We could
        # just override those priors, but it's better to be safe.
        if param in pop_prior_settings:
            conflicting_priors.append(param)
        # For bookkeeping purposes, set the parameter's prior distribution to
        # "constant". Map "params" to an empty dict for consistency with other
        # distributions that require params.
        else:
            pop_prior_settings[param] = {
                "dist": "constant",
                "params": {}
            }

    # Raise an exception if any parameters were set to constants, when they were
    # already given priors. List all of the offending params so the user can
    # correct the issue.
    if len(conflicting_priors) != 0:
        raise ValueError(
            "The following parameters were set to constants, but also had "
            "priors set in 'pop_priors' file. Those priors would need to "
            "be ignored, so remove them from the 'pop_priors' file and rerun "
            "if that is desired.\n{}"
            .format(utils.format_set_in_quotes(conflicting_priors))
        )


    # Raise an exception if any params do not have priors specified now.
    missing_params = [
        param for param in param_names
        if param not in pop_prior_settings
    ]
    if len(missing_params) != 0:
        raise ValueError(
            "The following parameters have not been given priors:\n{}"
            .format(utils.format_set_in_quotes(missing_params))
        )


    # Open tabular files for event posteriors.
    data_posterior_samples = []
    for event_fname in cli_args.events:
        data_table = numpy.genfromtxt(event_fname, names=True)

        if cli_args.spins:
            data_posterior_samples.append(numpy.column_stack((
                data_table["m1_source"], data_table["m2_source"],
                data_table["a1z"], data_table["a2z"],
            )))
        else:
            data_posterior_samples.append(numpy.column_stack((
                data_table["m1_source"], data_table["m2_source"],
            )))

        del data_table

    n_events = len(cli_args.events)

    # Compute array of priors for each posterior sample, so that we are doing
    # a Monte Carlo integral over the likelihood instead of the posterior, when
    # computing the event-based terms in the full MCMC.
    # If the prior is uniform, then no re-weighting is required, so we just set
    # it to ``None``, and ``pop_models.mcmc.run_mcmc`` takes care of the rest.
    if cli_args.mass_prior == "uniform":
        prior = None
    else:
        raise NotImplementedError

    if cli_args.spins:
        if cli_args.spin_prior == "uniform":
            # Does not affect prior from before
            prior = prior
        elif cli_args.spin_prior == "Veitch_alt":
            # Prior on each spin is
            # -ln(abs(chi_iz / chi_max)) / (2*chi_max)
            # We only care about proportionality, so we drop the 2*chi_max,
            # but keep the minus sign (to avoid negative logarithms) implicitly
            # by flipping the ratio inside of the logarithm
            prior_chi = []
            for event_samples in data_posterior_samples:
                chi1z, chi2z = prob.get_spins(event_samples)
                prior_chi.append(
                    numpy.log(numpy.abs(args.spin_max/chi1z)) *
                    numpy.log(numpy.abs(args.spin_max/chi2z))
                )

            # Combine with existing prior (unless it was uniform, in which case
            # just overwrite it).
            if prior is None:
                prior = prior_chi
            else:
                prior = [p*pchi for p, pchi in zip(prior, prior_chi)]

    # Load in <VT>'s.
    # TODO: Add aligned spin VT's
    with h5py.File(cli_args.VTs, "r") as VTs:
        raw_interpolator = vt.interpolate_hdf5(VTs)

    # Determine number of dimensions for MCMC
    ndim = ndim_pop - len(constants) - len(duplicates)
    # Set number of walkers for MCMC. If already provided use that, otherwise
    # use 2*ndim, which is the minimum allowed by the sampler.
    n_walkers = 2*ndim if cli_args.n_walkers is None else cli_args.n_walkers

    # Initialize walkers by drawing from priors.
    init_state = init_from_prior(
        n_walkers, n_events,
        constants, duplicates, pop_prior_settings,
        spins=cli_args.spins,
        rand_state=rand_state,
    )


    # Create file to store posterior samples.
    # Fails if file already exists, to avoid accidentally deleting precious
    # samples.
    # TODO: Come up with a mechanism to resume from last walker positions if
    #   file exists.
    # TODO: Periodically update a counter indicating the number of samples
    #   drawn so far. This way if it's interrupted, we know which samples can
    #   be trusted, and which might be corrupted or just have the contents of
    #   previous memory.
    with h5py.File(cli_args.posterior_output, "w-") as posterior_output:
        # Store initial position.
        posterior_output.create_dataset(
            "init_pos", data=init_state,
        )
        # Create empty arrays for storing walker position and log_prob.
        posterior_pos = posterior_output.create_dataset(
            "pos", (cli_args.n_samples, n_walkers, ndim),
        )
        posterior_log_prob = posterior_output.create_dataset(
            "log_prob", (cli_args.n_samples, n_walkers),
        )

        # Store whether or not spins were used
        posterior_output.attrs["spins"] = cli_args.spins

        # Store priors
        posterior_output.attrs["priors"] = json.dumps(pop_prior_settings)
        # Store constants
        constants_group = posterior_output.create_group("constants")
        for param, value in six.iteritems(constants):
            constants_group.attrs[param] = value
        # Store duplicates
        duplicates_group = posterior_output.create_group("duplicates")
        for target, source in six.iteritems(duplicates):
            duplicates_group.attrs[target] = source

        args = []
        kwargs = {
            "raw_interpolator": raw_interpolator,
            "pop_prior_settings": pop_prior_settings,
            "err_abs": cli_args.mc_err_abs,
            "err_rel": cli_args.mc_err_rel,
            "spins": cli_args.spins,
            "rand_state": rand_state,
        }

        mcmc.run_mcmc(
            intensity, expval_mc, data_posterior_samples,
            log_prior_pop,
            init_state,
            param_names,
            constants=constants, duplicates=duplicates,
            event_posterior_sample_priors=prior,
            args=args, kwargs=kwargs,
            before_prior_aux_fn=before_prior_aux_fn,
            out_pos=posterior_pos, out_log_prob=posterior_log_prob,
            nsamples=cli_args.n_samples,
            rand_state=rand_state,
            nthreads=cli_args.n_threads, pool=None,
            runtime_sortingfn=None,
            dtype=numpy.float64,
            verbose=cli_args.verbose,
        )


# Function which pre-computes quantities that are used at multiple steps in the
# MCMC, to reduce run time. This specifically computes the rate from the
# log10(rate).
def before_prior_aux_fn(pop_params, **kwargs):
    log10_rate = pop_params[0]

    rate = 10 ** log10_rate
    aux_info = {"rate": rate}

    return aux_info
