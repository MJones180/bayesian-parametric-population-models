from __future__ import division, print_function, unicode_literals

color_1 = "#1f77b4"
color_2 = "#2ca02c"

def get_percentiles(data, weights=None, axis=0):
    import numpy
    from .. import utils

    percentiles = numpy.array([2.50, 16.0, 50.0, 84.0, 97.5])
    quantiles = percentiles / 100.0

    data_percentiles = utils.quantile(
        data, quantiles, weights=weights,
        axis=axis,
    )

    return {
        "median": data_percentiles[2],
        "1 sigma": (data_percentiles[1], data_percentiles[3]),
        "2 sigma": (data_percentiles[0], data_percentiles[4]),
    }

def plot_percentiles(ax, X, percentiles, color="black", alpha=0.25, label=None):
    for lo, hi in [percentiles["1 sigma"], percentiles["2 sigma"]]:
        ax.fill_between(
            X[1:-1], lo[1:-1], hi[1:-1],
            color=color, alpha=alpha,
        )
    ax.plot(
        X[1:-1], percentiles["median"][1:-1],
        color=color,
        label=label,
    )


def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing MCMC samples.",
    )
    parser.add_argument(
        "ppds",
        help="Text file containing PPD samples.",
    )
    parser.add_argument(
        "output_plot_dir",
        help="Directory to output plots into.",
    )

    parser.add_argument(
        "--n-samples",
        default=1000, type=int,
        help="Number of samples to use in plots.",
    )

    parser.add_argument(
        "--max-kde-samples",
        type=int,
        help="Maximum number of PPD samples to use when building a KDE. "
             "Use this if there are large memory concerns.",
    )

    parser.add_argument(
        "--rate-logU-to-jeffereys",
        action="store_true",
        help="Assuming rate is log-uniform, switch to Jefferey's prior.",
    )

    parser.add_argument(
        "--log-scale-n-oom",
        type=int, default=6,
        help="Number of orders-of-magnitude below peak to show in log-scale "
             "plots.",
    )

    parser.add_argument(
        "--seed",
        type=int,
        help="Seed for random number generator.",
    )

    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib plots.",
    )

    parser.add_argument(
        "--plot-format",
        default="png",
        help="File format to save plots as.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    import os.path
    import sys
    import h5py
    import numpy
    import scipy.stats as stats

    from . import prob
    from . import utils
    from ..utils import empirical_distribution_function, gaussian_kde

    if raw_args is None:
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import matplotlib
    matplotlib.use(args.mpl_backend)
    import matplotlib.pyplot as plt

    import seaborn as sns

    matplotlib.rc("text", usetex=True)

    sns.set_context("talk", font_scale=2, rc={"lines.linewidth": 2.5})
    sns.set_style("ticks")
    sns.set_palette("colorblind")

    rand_state = numpy.random.RandomState(args.seed)

    with h5py.File(args.posteriors, "r") as post_file:
        spins = post_file.attrs["spins"]
        param_names = prob.param_names(spins=spins)

        posteriors = post_file["pos"].value
        n_samples = len(posteriors)

        constants = dict(post_file["constants"].attrs)
        duplicates = dict(post_file["duplicates"].attrs)

        posteriors_dict = dict(zip(
            param_names,
            utils.get_params(
                posteriors, constants, duplicates, param_names,
            )
        ))


        m_min_abs = numpy.min(posteriors_dict["m_min"])
        m_max_abs = numpy.max(posteriors_dict["m_max"])

        if spins:
            chi_min_abs = numpy.min(posteriors_dict["chi_min"])
            chi_max_abs = numpy.max(posteriors_dict["chi_max"])

        if args.rate_logU_to_jeffereys:
            weights = numpy.sqrt(
                numpy.power(10.0, posteriors_dict["log10_rate"])
            )
            if numpy.isscalar(weights):
                weights = numpy.tile(weights, n_samples)
        else:
            weights = None


        ppd_samples = numpy.loadtxt(args.ppds)

        n_ppd_samples = len(ppd_samples)

        # If there are too many PPD samples, downselect, according to the limit
        # set by --max-kde-samples option.
        if args.max_kde_samples is None:
            pass
        elif n_ppd_samples <= args.max_kde_samples:
            pass
        else:
            idx_samples = rand_state.choice(
                n_ppd_samples, size=args.max_kde_samples,
                replace=False,
            )
            ppd_samples = ppd_samples[idx_samples]

        m1_samples, m2_samples = prob.get_masses(ppd_samples)
        m_samples = numpy.concatenate((m1_samples, m2_samples))

        q_samples = m2_samples / m1_samples
        M_samples = m1_samples + m2_samples

        m = numpy.linspace(m_min_abs, m_max_abs, args.n_samples)
        q = numpy.linspace(m_min_abs/m_max_abs, 1.0, args.n_samples)
        M = 2*m

        if spins:
            chi1z_samples, chi2z_samples = prob.get_spins(ppd_samples)
            chiz_samples = numpy.concatenate((chi1z_samples, chi2z_samples))

            chi_eff_samples = (
                m1_samples*chi1z_samples + m2_samples*chi2z_samples
            ) / M_samples

            chiz = numpy.linspace(chi_min_abs, chi_max_abs, args.n_samples)
            chi_eff = chiz

        m_pm = numpy.empty((n_samples, args.n_samples), dtype=numpy.float64)
        m_R_pm = numpy.empty_like(m_pm)

        if spins:
            pchiz = numpy.empty_like(m_pm)
            R_pchiz = numpy.empty_like(m_pm)

        for i, post_sample in enumerate(posteriors):
            weight = weights[i]

            pop_params = utils.get_params(
                post_sample, constants, duplicates, param_names,
            )
            log10_rate, mu, log10_sigma, m_min, m_max = (
                prob.get_base_pop_params(pop_params)
            )
            rate = numpy.power(10.0, log10_rate)

            pm = prob.marginal_mass_pdf(m, mu, log10_sigma, m_min, m_max)

            m_pm[i] = m * pm
            m_R_pm[i] = m * rate * pm

            del pm

            if spins:
                mu_chi, sigma_sq_chi, chi_min, chi_max = (
                    prob.get_spin_pop_params(pop_params)
                )

                pchiz[i] = prob.marginal_spin_pdf(
                    chiz,
                    mu_chi, sigma_sq_chi,
                    chi_min, chi_max,
                )
                R_pchiz[i] = rate * pchiz[i]


        m_pm_percentile = get_percentiles(m_pm, weights=weights)
        m_R_pm_percentile = get_percentiles(m_R_pm, weights=weights)

        if spins:
            pchiz_percentile = get_percentiles(pchiz, weights=weights)
            R_pchiz_percentile = get_percentiles(R_pchiz, weights=weights)

        fig_m_pm, ax_m_pm = plt.subplots()
        fig_R_pm, ax_R_m_pm = plt.subplots()
        fig_ppd_mi, ax_ppd_mi = plt.subplots()
        fig_ppd_m, ax_ppd_m = plt.subplots()
        fig_ppd_q_M, ax_ppd_q_M = plt.subplots()

        if spins:
            fig_pchiz, ax_pchiz = plt.subplots()
            fig_R_pchiz, ax_R_pchiz = plt.subplots()
            fig_ppd_chiiz, ax_ppd_chiiz = plt.subplots()
            fig_ppd_chiz, ax_ppd_chiz = plt.subplots()
            fig_ppd_q_chieff, ax_ppd_q_chieff = plt.subplots()

        figs = [
            fig_m_pm,
            fig_R_pm,
            fig_ppd_mi,
            fig_ppd_m,
            fig_ppd_q_M,
        ]
        if spins:
            figs += [
                fig_pchiz,
                fig_R_pchiz,
                fig_ppd_chiiz,
                fig_ppd_chiz,
                fig_ppd_q_chieff,
            ]


        axes_ppd_1d = [
            ax_ppd_mi,
            ax_ppd_m,
        ]
        if spins:
            axes_ppd_1d += [
                ax_ppd_chiiz,
                ax_ppd_chiz,
            ]

        # Logarithmic x-axes
        ax_logx = [
        ]
        for ax in ax_logx:
            ax.set_xscale("log")

        # Logarithmic y-axes
        ax_logy = [
        ]
        for ax in ax_logy:
            ax.set_yscale("log")

        # Label axes
        for ax in [ax_m_pm, ax_R_m_pm, ax_ppd_mi, ax_ppd_m]:
            ax.set_xlabel(r"$m$ [M$_\odot$]")

        ax_ppd_q_M.set_xlabel(r"$q$")

        if spins:
            for ax in [ax_pchiz, ax_R_pchiz, ax_ppd_chiiz, ax_ppd_chiz]:
                ax.set_xlabel(r"$\chi_z$")

            ax_ppd_q_chieff.set_xlabel(r"$q$")


        ax_m_pm.set_ylabel(
            r"$m \, p(m)$"
        )
        ax_R_m_pm.set_ylabel(
            r"$m \, \mathcal{R} \, p(m)$ [Gpc$^{-3}$ yr$^{-1}$]"
        )

        ax_ppd_q_M.set_ylabel(r"$M/\mathrm{M}_\odot$")

        if spins:
            ax_pchiz.set_ylabel(
                r"$p(\chi_z)$"
            )
            ax_R_pchiz.set_ylabel(
                r"$\mathcal{R} \, p(\chi_z)$"
            )

            ax_ppd_q_chieff.set_ylabel(r"$\chi_{\mathrm{eff}}$")

        for ax in axes_ppd_1d:
            ax.set_ylabel(r"$\mathrm{PPD}$")


        plot_percentiles(ax_m_pm, m, m_pm_percentile)
        plot_percentiles(ax_R_m_pm, m, m_R_pm_percentile)

        if spins:
            plot_percentiles(ax_pchiz, chiz, pchiz_percentile)
            plot_percentiles(ax_R_pchiz, chiz, R_pchiz_percentile)


        # Plot PPDs
        ppd_m1 = gaussian_kde(m1_samples, bw_method="scott")(m)
        ppd_m2 = gaussian_kde(m2_samples, bw_method="scott")(m)

        ax_ppd_mi.plot(m, ppd_m1, color=color_1)
        ax_ppd_mi.plot(m, ppd_m2, color=color_2)

        del ppd_m1, ppd_m2

        ppd_m = gaussian_kde(m_samples, bw_method="scott")(m)
        ax_ppd_m.plot(m, ppd_m, color="black")

        del ppd_m

        q_mesh, M_mesh = numpy.meshgrid(q, M)

        ppd_q_M = (
            gaussian_kde([q_samples, M_samples], bw_method="scott")
        )([q_mesh.ravel(), M_mesh.ravel()]).reshape(q_mesh.shape)

        fig_ppd_q_M.colorbar(
            ax_ppd_q_M.contourf(q, M, ppd_q_M),
            ax=ax_ppd_q_M,
        ).set_label(r"$\mathrm{PPD}$")

        del q_mesh, M_mesh, ppd_q_M

        if spins:
            ppd_chi1z = gaussian_kde(chi1z_samples, bw_method="scott")(chiz)
            ppd_chi2z = gaussian_kde(chi2z_samples, bw_method="scott")(chiz)

            ax_ppd_chiiz.plot(chiz, ppd_chi1z, color=color_1)
            ax_ppd_chiiz.plot(chiz, ppd_chi2z, color=color_2)

            del ppd_chi1z, ppd_chi2z

            ppd_chiz = gaussian_kde(chiz_samples, bw_method="scott")(chiz)
            ax_ppd_chiz.plot(chiz, ppd_chiz, color="black")

            del ppd_chiz

            q_mesh, chieff_mesh = numpy.meshgrid(q, chi_eff)

            ppd_q_chieff = (
                gaussian_kde([q_samples, chi_eff_samples], bw_method="scott")
            )([q_mesh.ravel(), chieff_mesh.ravel()]).reshape(q_mesh.shape)

            fig_ppd_q_chieff.colorbar(
                ax_ppd_q_chieff.contourf(q, chi_eff, ppd_q_chieff),
                ax=ax_ppd_q_chieff,
            ).set_label(r"$\mathrm{PPD}$")

            del q_mesh, chieff_mesh, ppd_q_chieff


        for ax in [ax_m_pm, ax_R_m_pm]:
            ax.set_xlim([m_min_abs, m_max_abs])

        for fig in figs:
            fig.tight_layout()

        def plot_fname(basename):
            return os.path.join(
                args.output_plot_dir,
                ".".join([basename, args.plot_format])
            )

        fig_m_pm.savefig(plot_fname("dist_m_pm"))
        fig_R_pm.savefig(plot_fname("dist_m_R_pm"))

        fig_ppd_mi.savefig(plot_fname("ppd_mi"))
        fig_ppd_m.savefig(plot_fname("ppd_m"))

        fig_ppd_q_M.savefig(plot_fname("ppd_q_M"))

        if spins:
            fig_pchiz.savefig(plot_fname("dist_pchiz"))
            fig_R_pchiz.savefig(plot_fname("dist_R_pchiz"))

            fig_ppd_chiiz.savefig(plot_fname("ppd_chiiz"))
            fig_ppd_chiz.savefig(plot_fname("ppd_chiz"))

            fig_ppd_q_chieff.savefig(plot_fname("ppd_q_chieff"))
