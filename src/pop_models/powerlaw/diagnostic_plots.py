from __future__ import division, print_function

lnprob_label = r"$\ln p$"
rate_label = r"$\log\mathcal{R}$"
alpha_label = r"$\alpha$"

labels_pop = [rate_label, alpha_label]


def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing MCMC samples.",
    )

    parser.add_argument(
        "chain_plot",
        help="File to store plot of MCMC chain to.",
    )
    parser.add_argument(
        "corner_plot",
        help="File to store corner plot of MCMC samples to.",
    )

    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib plots.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    import sys
    import operator
    import h5py

    if raw_args is None:
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import matplotlib
    matplotlib.use(args.mpl_backend)
    import matplotlib.pyplot as plt
    import corner

    with h5py.File(args.posteriors, "r") as f:
        lnprob = f["log_prob"][:]
        pos = f["pos"][:]

        rate = pos[...,0]
        alpha = pos[...,1]

        ndim = pos.shape[-1]

        samples = pos.reshape((-1, ndim))

        labels = labels_pop

        fig_chain, axes_chain = plt.subplots(3, sharex=True, figsize=(8,9))
        ax_chain_lnprob, ax_chain_rate, ax_chain_alpha = axes_chain

        ax_chain_lnprob.plot(lnprob, color="k", alpha=0.4)
        ax_chain_lnprob.set_ylabel(lnprob_label)

        ax_chain_rate.plot(rate, color="k", alpha=0.4)
        ax_chain_rate.set_ylabel(rate_label)

        ax_chain_alpha.plot(alpha, color="k", alpha=0.4)
        ax_chain_alpha.set_ylabel(alpha_label)

        ax_chain_alpha.set_xlabel("iteration")

        fig_chain.tight_layout()
        fig_chain.savefig(args.chain_plot)
        plt.close(fig_chain)


        fig_corner = corner.corner(
            samples,
            labels=labels,
        )
        fig_corner.savefig(args.corner_plot)


if __name__ == "__main__":
    import sys
    raw_args = sys.argv[1:]
    sys.exit(_main(raw_args))
