import numpy
from pop_models.utils import debug_verbose

def sample_with_cond(
        func, shape=(1,), cond=None,
        func_args=None, func_kwargs=None,
        cond_args=None, cond_kwargs=None,
    ):
    """
    Returns an array of shape ``shape`` containing samples from a
    non-deterministic function ``func``, which must optionally satisfy some
    condition ``cond``. If a condition is given, ``func`` is called repeatedly
    to replace those samples which do not satisfy ``cond``. ``func`` must be a
    function which takes a valid numpy array shape specifier
    (int or tuple of ints), and returns an array of that shape.

    WARNING: If ``func`` never returns any samples that satisfy ``cond``, this
    function will never terminate.
    """
    if func_args is None:
        func_args = []
    if func_kwargs is None:
        func_kwargs = {}
    if cond_args is None:
        cond_args = []
    if cond_kwargs is None:
        cond_kwargs = {}

    # Simple case: no condition given.
    if cond is None:
        debug_verbose(
            "Drawing samples from distribution without condition.",
            mode="sample_with_cond", flush=True,
        )
        return func(shape, *func_args, **func_kwargs)

    # Generate initial samples
    debug_verbose(
        "Drawing initial samples from distribution.",
        mode="sample_with_cond", flush=True,
    )
    samples = numpy.asanyarray(func(shape, *func_args, **func_kwargs))

    # Create index array of samples which fail to meet the condition,
    # and count the number of new samples that will be required.
    bad = ~cond(samples, *cond_args, **cond_kwargs)
    N = numpy.count_nonzero(bad)
    debug_verbose(
        "Initial samples from distribution included", N, "bad points.",
        mode="sample_with_cond", flush=True,
    )

    # Iteratively replace the bad samples until all samples meet the condition.
    while N:
        debug_verbose(
            "Drawing", N, "samples from distribution.",
            mode="sample_with_cond", flush=True,
        )

        # Overwrite bad samples with new (possibly bad) samples
        samples[bad] = func(N, *func_args, **func_kwargs)

        # Update index array of bad samples. Note that ``cond`` is only called
        # on the known bad samples, in case the function call is expensive.
        # Note that evaluating ``samples[bad]`` creates a new array, not a
        # view, and may waste a lot of memory.
        new_bad = ~cond(samples[bad], *cond_args, **cond_kwargs)
        bad[bad] = new_bad

        # Count the number of new samples still needed.
        # If zero, the loop terminates.
        N = numpy.count_nonzero(new_bad)

    return samples


def oversample_with_cond(func, size=1, cond=None, oversampling=2):
    assert oversampling >= 1

    # Simple case: no condition given.
    if cond is None:
        return func(size)

    import numpy

    # Generate initial samples
    samples = numpy.asanyarray(func(int(size*oversampling)))

    # Pick out the samples that satisfy the condition,
    # and count the number of good samples for indexing purposes.
    good_samples = samples[cond(samples)]
    N_good = len(good_samples)

    # If we drew enough good samples, return the desired number.
    # Otherwise, add the good samples to the beginning of ``ret``,
    # and continue drawing more samples.
    if N_good >= size:
        return good_samples[:size]
    else:
        # Initialize array with samples to be returned.
        ret = numpy.empty(size, dtype=samples.dtype)
        ret[:N_good] = good_samples

    # Calculate the index to start adding samples to, and the number of samples
    # that are still needed.
    idx_start = N_good
    N_bad = size - idx_start

    while N_bad:
        # Generate more samples
        samples = numpy.asanyarray(func(int(size*oversampling)))

        # Pick out the samples that satisfy the condition,
        # and count the number of good samples for indexing purposes.
        good_samples = samples[cond(samples)]
        N_good = len(good_samples)

        # If we drew enough samples, add just enough to the end of ``ret``
        # and break. Otherwise, add them to the end of ``ret`` and continue.
        if N_good >= N_bad:
            ret[idx_start:idx_start+N_bad] = good_samples[:N_bad]
            break
        else:
            ret[idx_start:idx_start+N_good] = good_samples

            # Calculate the index to start adding samples to, and the number of
            # samples that are still needed.
            idx_start += N_good
            N_bad = size - idx_start

    return ret


def rejection_sample(
        sampler, acceptance_func,
        acceptance_func_norm=1.0,
        size=1,
        rand_state=None,
    ):
    import numpy

    if rand_state is None:
        rand_state = numpy.random.RandomState()

    # Generate initial samples
    samples = sampler(size)
