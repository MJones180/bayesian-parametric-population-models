from __future__ import division, print_function


def clean_chain(
        log_prob, pos, log_threshold,
        c=10,
        fixed_burnin=None, fixed_acorr=None,
    ):
    import numpy
    import emcee

    n_samples, n_dim = numpy.shape(pos)

    if fixed_burnin is None:
        # Determine first index where log_prob is within the threshold.
        i_start = numpy.argmax(log_prob >= log_threshold)
    else:
        i_start = fixed_burnin

    if fixed_acorr is None:
        acorr_time_max = 1
        for i in range(n_dim):
            acorr_time = emcee.autocorr.integrated_time(pos[i_start:,i], c=c)
            if acorr_time > acorr_time_max:
                acorr_time_max = acorr_time

        acorr_time = int(numpy.ceil(acorr_time))
    else:
        acorr_time = fixed_acorr

    return log_prob[i_start::acorr_time], pos[i_start::acorr_time]


def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors_in",
        help="HDF5 file containing input raw posterior samples.",
    )
    parser.add_argument(
        "posteriors_out",
        help="HDF5 file to store extracted posterior samples.",
    )
    parser.add_argument(
        "--scale-factor",
        type=float, default=0.5,
        help="Fraction of max probability sample to use as cutoff.",
    )
    parser.add_argument(
        "--acorr-min",
        type=int, default=10,
        help="Minimum number of autocorrelation times needed to trust the "
             "estimate (default 10)",
    )

    parser.add_argument(
        "--fixed-burnin-time",
        type=int,
        help="Don't compute burnin time, just use this value.",
    )
    parser.add_argument(
        "--fixed-acorr-time",
        type=int,
        help="Don't compute autocorr time, just use this value.",
    )

    parser.add_argument(
        "--keep-all",
        action="store_true",
        help="Don't filter out any samples, keep them all.",
    )

    parser.add_argument(
        "--keep-stuck-walkers",
        action="store_true",
        help="Include walkers stuck at log(prob) = 0.",
    )


    return parser.parse_args(raw_args)


def _main(raw_args=None):
    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import numpy
    import h5py
    import six

    with h5py.File(args.posteriors_out, "w-") as posteriors_out:
        with h5py.File(args.posteriors_in, "r") as posteriors_in:
            log_prob = posteriors_in["log_prob"][:]
            pos = posteriors_in["pos"][:]

            n_samples, n_walkers, n_dim = numpy.shape(pos)

            # Copy attributes
            for k, v in six.iteritems(posteriors_in.attrs):
                posteriors_out.attrs[k] = v

            # Copy any datasets and groups that aren't "log_prob" or "pos", as
            # they will be modified and then copied.
            for name, value in six.iteritems(posteriors_in):
                # Skip datasets we're already going to make later.
                if name not in ["pos", "log_prob"]:
                    posteriors_in.copy(name, posteriors_out)


        # Pick out walkers not stuck in zero probability regions, unless
        # otherwise specified
        if args.keep_stuck_walkers:
            good_walkers = list(range(n_walkers))
        else:
            good_walkers = []
            for i in range(n_walkers):
                # Only keep walkers where the probability is non-zero at the
                # last iteration.
                if log_prob[-1,i] > -numpy.inf:
                    good_walkers.append(i)

        log_prob_max = numpy.max(log_prob)

        # log(scale_factor * prob) = log(scale_factor) + log(prob)
        log_scale_factor = numpy.log(args.scale_factor)
        log_threshold = log_scale_factor + log_prob_max

        if args.keep_all:
            cleaned_chains = [
                (log_prob[:,i], pos[:,i,:])
                for i in good_walkers
            ]
        else:
            cleaned_chains = [
                clean_chain(
                    log_prob[:,i], pos[:,i,:],
                    log_threshold, c=args.acorr_min,
                    fixed_burnin=args.fixed_burnin_time,
                    fixed_acorr=args.fixed_acorr_time,
                )
                for i in good_walkers
            ]

        n_clean_samples = sum(
            len(log_prob_clean)
            for log_prob_clean, pos_clean in cleaned_chains
        )


        out_pos = posteriors_out.create_dataset(
            "pos", (n_clean_samples, n_dim),
        )
        out_log_prob = posteriors_out.create_dataset(
            "log_prob", (n_clean_samples,),
        )

        i = 0
        for log_prob_clean, pos_clean in cleaned_chains:
            size = len(log_prob_clean)

            out_log_prob[i:i+size] = log_prob_clean
            out_pos[i:i+size] = pos_clean

            i += size

        # Sanity check, should never trigger.
        assert i == n_clean_samples, "Sample counting error."
