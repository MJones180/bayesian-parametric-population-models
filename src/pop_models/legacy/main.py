from . import (
    O2RandPPaper,
)

subparser_modules = [
    O2RandPPaper.main,
]

def make_parser():
    import argparse

    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers()

    for module in subparser_modules:
        module.populate_subparser(subparsers)

    return parser


def main(raw_args=None):
    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    cli_parser = make_parser()
    cli_args = cli_parser.parse_args(raw_args)

    if cli_args.command is None:
        cli_parser.error("No command provided")

    return cli_args.main_func(cli_args)
