from ..coordinate import Coordinate, CoordinateSystem, CoordinateTransforms
from . import transformations as _transformations
import math

__all__ = [
    "m_source_coord",
    "m1_source_coord",
    "m2_source_coord",
    "mA_source_coord",
    "mB_source_coord",
    "Mc_source_coord",
    "M_source_coord",
    "m_det_coord",
    "m1_det_coord",
    "m2_det_coord",
    "Mc_det_coord",
    "M_det_coord",
    "q_coord",
    "eta_coord",
    "delta_coord",
    "chi_coord",
    "chix_coord",
    "chiy_coord",
    "chiz_coord",
    "costilt_coord",
    "phi_coord",
    "chi1_coord",
    "chi1x_coord",
    "chi1y_coord",
    "chi1z_coord",
    "costilt1_coord",
    "phi1_coord",
    "chi2_coord",
    "chi2x_coord",
    "chi2y_coord",
    "chi2z_coord",
    "costilt2_coord",
    "phi2_coord",
    "chieff_coord",
    "chiminus_coord",
    "Lambda_coord",
    "Lambda1_coord",
    "Lambda2_coord",
    "LambdaTilde_coord",
    "deltaLambdaTilde_coord",
    "z_coord",
    "incl_coord",
    "ra_coord",
    "dec_coord",
    "Z_metal_coord",
    "coordinates",
    "transformations",
]

m_source_coord = Coordinate(
    "m_source",
    lower_limit=0.0, fill_value=1.0,
)

m1_source_coord = Coordinate(
    "m1_source",
    lower_limit=0.0, fill_value=1.0,
)
m2_source_coord = Coordinate(
    "m2_source",
    lower_limit=0.0, fill_value=1.0,
)

mA_source_coord = Coordinate(
    "mA_source",
    lower_limit=0.0, fill_value=1.0,
)
mB_source_coord = Coordinate(
    "mB_source",
    lower_limit=0.0, fill_value=1.0,
)

Mc_source_coord = Coordinate(
    "Mc_source",
    lower_limit=0.0, fill_value=1.0,
)
M_source_coord = Coordinate(
    "M_source",
    lower_limit=0.0, fill_value=1.0,
)

m_det_coord = Coordinate(
    "m_det",
    lower_limit=0.0, fill_value=1.0,
)

m1_det_coord = Coordinate(
    "m1_det",
    lower_limit=0.0, fill_value=1.0,
)
m2_det_coord = Coordinate(
    "m2_det",
    lower_limit=0.0, fill_value=1.0,
)

Mc_det_coord = Coordinate(
    "Mc_det",
    lower_limit=0.0, fill_value=1.0,
)
M_det_coord = Coordinate(
    "M_det",
    lower_limit=0.0, fill_value=1.0,
)

q_coord = Coordinate(
    "q",
    lower_limit=0.0, upper_limit=1.0, fill_value=1.0,
)
eta_coord = Coordinate(
    "eta",
    lower_limit=0.0, upper_limit=0.25, fill_value=0.25,
)
delta_coord = Coordinate(
    "delta",
    lower_limit=0.0, upper_limit=1.0, fill_value=0.5,
)

chi_coord = Coordinate(
    "chi",
    lower_limit=0.0, upper_limit=1.0, fill_value=0.0,
)
chix_coord = Coordinate(
    "chix",
    lower_limit=-1.0, upper_limit=+1.0, fill_value=0.0,
)
chiy_coord = Coordinate(
    "chiy",
    lower_limit=-1.0, upper_limit=+1.0, fill_value=0.0,
)
chiz_coord = Coordinate(
    "chiz",
    lower_limit=-1.0, upper_limit=+1.0, fill_value=0.0,
)
costilt_coord = Coordinate(
    "costilt",
    lower_limit=-1.0, upper_limit=+1.0, fill_value=0.0,
)
phi_coord = Coordinate(
    "phi",
    lower_limit=0.0, upper_limit=2*math.pi, fill_value=math.pi,
)

chi1_coord = Coordinate(
    "chi1",
    lower_limit=0.0, upper_limit=1.0, fill_value=0.0,
)
chi1x_coord = Coordinate(
    "chi1x",
    lower_limit=-1.0, upper_limit=+1.0, fill_value=0.0,
)
chi1y_coord = Coordinate(
    "chi1y",
    lower_limit=-1.0, upper_limit=+1.0, fill_value=0.0,
)
chi1z_coord = Coordinate(
    "chi1z",
    lower_limit=-1.0, upper_limit=+1.0, fill_value=0.0,
)
costilt1_coord = Coordinate(
    "costilt1",
    lower_limit=-1.0, upper_limit=+1.0, fill_value=0.0,
)
phi1_coord = Coordinate(
    "phi1",
    lower_limit=0.0, upper_limit=2*math.pi, fill_value=math.pi,
)

chi2_coord = Coordinate(
    "chi2",
    lower_limit=0.0, upper_limit=1.0, fill_value=0.0,
)
chi2x_coord = Coordinate(
    "chi2x",
    lower_limit=-1.0, upper_limit=+1.0, fill_value=0.0,
)
chi2y_coord = Coordinate(
    "chi2y",
    lower_limit=-1.0, upper_limit=+1.0, fill_value=0.0,
)
chi2z_coord = Coordinate(
    "chi2z",
    lower_limit=-1.0, upper_limit=+1.0, fill_value=0.0,
)
costilt2_coord = Coordinate(
    "costilt2",
    lower_limit=-1.0, upper_limit=+1.0, fill_value=0.0,
)
phi2_coord = Coordinate(
    "phi2",
    lower_limit=0.0, upper_limit=2*math.pi, fill_value=0.0,
)

chieff_coord = Coordinate(
    "chieff",
    lower_limit=-1.0, upper_limit=+1.0, fill_value=0.0,
)
chiminus_coord = Coordinate(
    "chiminus",
    lower_limit=-1.0, upper_limit=+1.0, fill_value=0.0,
)

Lambda_coord = Coordinate(
    "Lambda",
    lower_limit=0.0, fill_value=0.0,
)
Lambda1_coord = Coordinate(
    "Lambda1",
    lower_limit=0.0, fill_value=0.0,
)
Lambda2_coord = Coordinate(
    "Lambda2",
    lower_limit=0.0, fill_value=0.0,
)
LambdaTilde_coord = Coordinate(
    "LambdaTilde",
    lower_limit=0.0, fill_value=0.0,
)
deltaLambdaTilde_coord = Coordinate(
    "deltaLambdaTilde",
    fill_value=0.0,
)

z_coord = Coordinate(
    "z",
    lower_limit=0.0, fill_value=0.0,
)

incl_coord = Coordinate(
    "i",
    lower_limit=0.0, upper_limit=math.pi, fill_value=0.0,
)
ra_coord = Coordinate(
    "ra",
    lower_limit=0.0, upper_limit=2.0*math.pi, fill_value=math.pi,
)
dec_coord = Coordinate(
    "dec",
    lower_limit=0.0, upper_limit=math.pi, fill_value=0.5*math.pi,
)

Z_metal_coord = Coordinate(
    "Z_metal",
    lower_limit=0.0, upper_limit=1.0, fill_value=0.0,
)

coordinates = {
    coord.name : coord
    for coord in [
        m_source_coord,
        m1_source_coord, m2_source_coord,
        Mc_source_coord, M_source_coord,
        m_det_coord,
        m1_det_coord, m2_det_coord,
        Mc_det_coord, M_det_coord,
        q_coord, eta_coord, delta_coord,
        chi_coord,
        chix_coord, chiy_coord, chiz_coord,
        costilt_coord, phi_coord,
        chi1_coord,
        chi1x_coord, chi1y_coord, chi1z_coord,
        costilt1_coord, phi1_coord,
        chi2_coord,
        chi2x_coord, chi2y_coord, chi2z_coord,
        costilt2_coord, phi2_coord,
        chieff_coord, chiminus_coord,
        Lambda_coord,
        Lambda1_coord, Lambda2_coord, LambdaTilde_coord, deltaLambdaTilde_coord,
        z_coord,
        incl_coord, ra_coord, dec_coord,
    ]
}


transformations = CoordinateTransforms({
    (
        CoordinateSystem(mA_source_coord, mB_source_coord),
        CoordinateSystem(m1_source_coord, m2_source_coord),
    ) : _transformations.m1m2_ordering,
    (
        CoordinateSystem(m_source_coord, z_coord),
        CoordinateSystem(m_det_coord),
    ) : _transformations.msourcez_to_mdet,
    (
        CoordinateSystem(m1_source_coord, z_coord),
        CoordinateSystem(m1_det_coord),
    ) : _transformations.msourcez_to_mdet,
    (
        CoordinateSystem(m2_source_coord, z_coord),
        CoordinateSystem(m2_det_coord),
    ) : _transformations.msourcez_to_mdet,
    (
        CoordinateSystem(M_source_coord, z_coord),
        CoordinateSystem(M_det_coord),
    ) : _transformations.msourcez_to_mdet,
    (
        CoordinateSystem(Mc_source_coord, z_coord),
        CoordinateSystem(Mc_det_coord),
    ) : _transformations.msourcez_to_mdet,
    (
        CoordinateSystem(m_det_coord, z_coord),
        CoordinateSystem(m_source_coord),
    ) : _transformations.mdetz_to_msource,
    (
        CoordinateSystem(m1_det_coord, z_coord),
        CoordinateSystem(m1_source_coord),
    ) : _transformations.mdetz_to_msource,
    (
        CoordinateSystem(m2_det_coord, z_coord),
        CoordinateSystem(m2_source_coord),
    ) : _transformations.mdetz_to_msource,
    (
        CoordinateSystem(M_det_coord, z_coord),
        CoordinateSystem(M_source_coord),
    ) : _transformations.mdetz_to_msource,
    (
        CoordinateSystem(Mc_det_coord, z_coord),
        CoordinateSystem(Mc_source_coord),
    ) : _transformations.mdetz_to_msource,
    (
        CoordinateSystem(m1_source_coord, m2_source_coord),
        CoordinateSystem(M_source_coord),
    ) : _transformations.m1m2_to_M,
    (
        CoordinateSystem(m1_det_coord, m2_det_coord),
        CoordinateSystem(M_det_coord),
    ) : _transformations.m1m2_to_M,
    (
        CoordinateSystem(m1_source_coord, m2_source_coord),
        CoordinateSystem(q_coord),
    ) : _transformations.m1m2_to_q,
    (
        CoordinateSystem(m1_det_coord, m2_det_coord),
        CoordinateSystem(q_coord),
    ) : _transformations.m1m2_to_q,
    (
        CoordinateSystem(m1_source_coord, q_coord),
        CoordinateSystem(m2_source_coord),
    ) : _transformations.m1q_to_m2,
    (
        CoordinateSystem(m1_det_coord, q_coord),
        CoordinateSystem(m2_det_coord),
    ) : _transformations.m1q_to_m2,
    (
        CoordinateSystem(m2_source_coord, q_coord),
        CoordinateSystem(m1_source_coord),
    ) : _transformations.m2q_to_m1,
    (
        CoordinateSystem(m2_det_coord, q_coord),
        CoordinateSystem(m1_det_coord),
    ) : _transformations.m2q_to_m1,
    (
        CoordinateSystem(m1_source_coord, m2_source_coord),
        CoordinateSystem(Mc_source_coord),
    ) : _transformations.m1m2_to_mc,
    (
        CoordinateSystem(m1_det_coord, m2_det_coord),
        CoordinateSystem(Mc_det_coord),
    ) : _transformations.m1m2_to_mc,
    (
        CoordinateSystem(m1_source_coord, m2_source_coord),
        CoordinateSystem(eta_coord),
    ) : _transformations.m1m2_to_eta,
    (
        CoordinateSystem(m1_det_coord, m2_det_coord),
        CoordinateSystem(eta_coord),
    ) : _transformations.m1m2_to_eta,
    (
        CoordinateSystem(m1_source_coord, m2_source_coord),
        CoordinateSystem(delta_coord),
    ) : _transformations.m1m2_to_delta,
    (
        CoordinateSystem(m1_det_coord, m2_det_coord),
        CoordinateSystem(delta_coord),
    ) : _transformations.m1m2_to_delta,
    (
        CoordinateSystem(delta_coord),
        CoordinateSystem(eta_coord),
    ) : _transformations.delta_to_eta,
    (
        CoordinateSystem(eta_coord),
        CoordinateSystem(delta_coord),
    ) : _transformations.eta_to_delta,
    (
        CoordinateSystem(m1_source_coord, m2_source_coord),
        CoordinateSystem(Mc_source_coord, eta_coord),
    ) : _transformations.m1m2_to_mceta,
    (
        CoordinateSystem(Mc_source_coord, delta_coord),
        CoordinateSystem(m1_source_coord, m2_source_coord),
    ) : _transformations.mcdelta_to_m1m2,
    (
        CoordinateSystem(Mc_det_coord, delta_coord),
        CoordinateSystem(m1_det_coord, m2_det_coord),
    ) : _transformations.mcdelta_to_m1m2,
    (
        CoordinateSystem(
            m1_source_coord, m2_source_coord,
            chi1z_coord, chi2z_coord,
        ),
        CoordinateSystem(chieff_coord),
    ) : _transformations.m1m2chi1zchi2z_to_chieff,
    (
        CoordinateSystem(
            m1_source_coord, m2_source_coord,
            chi1_coord, chi2_coord,
            costilt1_coord, costilt2_coord,
        ),
        CoordinateSystem(chieff_coord),
    ) : _transformations.m1m2chi1chi2costilt1costilt2_to_chieff,
    (
        CoordinateSystem(
            m1_source_coord, m2_source_coord,
            chi1z_coord, chi2z_coord,
        ),
        CoordinateSystem(chiminus_coord),
    ) : _transformations.m1m2chi1zchi2z_to_chiminus,
    (
        CoordinateSystem(
            m1_source_coord, m2_source_coord,
            chi1_coord, chi2_coord,
            costilt1_coord, costilt2_coord,
        ),
        CoordinateSystem(chiminus_coord),
    ) : _transformations.m1m2chi1chi2costilt1costilt2_to_chiminus,
    (
        CoordinateSystem(
            m1_source_coord, m2_source_coord,
            chieff_coord, chiminus_coord,
        ),
        CoordinateSystem(chi1z_coord, chi2z_coord),
    ) : _transformations.m1m2chieffchiminus_to_chi1zchi2z,
    (
        CoordinateSystem(
            Mc_source_coord, delta_coord,
            chieff_coord, chiminus_coord,
        ),
        CoordinateSystem(chi1z_coord, chi2z_coord),
    ) : _transformations.mcdeltachieffchiminus_to_chi1zchi2z,
    (
        CoordinateSystem(chi_coord, costilt_coord),
        CoordinateSystem(chiz_coord),
    ) : _transformations.chicostilt_to_chiz,
    (
        CoordinateSystem(chi1_coord, costilt1_coord),
        CoordinateSystem(chi1z_coord),
    ) : _transformations.chicostilt_to_chiz,
    (
        CoordinateSystem(chi2_coord, costilt2_coord),
        CoordinateSystem(chi2z_coord),
    ) : _transformations.chicostilt_to_chiz,
    (
        CoordinateSystem(chix_coord, chiy_coord, chiz_coord),
        CoordinateSystem(chi_coord, costilt_coord, phi_coord),
    ) : _transformations.cart2polar,
    (
        CoordinateSystem(chi1x_coord, chi1y_coord, chi1z_coord),
        CoordinateSystem(chi1_coord, costilt1_coord, phi1_coord),
    ) : _transformations.cart2polar,
    (
        CoordinateSystem(chi2x_coord, chi2y_coord, chi2z_coord),
        CoordinateSystem(chi2_coord, costilt2_coord, phi2_coord),
    ) : _transformations.cart2polar,
    (
        CoordinateSystem(chi_coord, costilt_coord, phi_coord),
        CoordinateSystem(chix_coord, chiy_coord, chiz_coord),
    ) : _transformations.polar2cart,
    (
        CoordinateSystem(chi1_coord, costilt1_coord, phi1_coord),
        CoordinateSystem(chi1x_coord, chi1y_coord, chi1z_coord),
    ) : _transformations.polar2cart,
    (
        CoordinateSystem(chi2_coord, costilt2_coord, phi2_coord),
        CoordinateSystem(chi2x_coord, chi2y_coord, chi2z_coord),
    ) : _transformations.polar2cart,
    (
        CoordinateSystem(Mc_source_coord, eta_coord),
        CoordinateSystem(m1_source_coord, m2_source_coord),
    ) : _transformations.mceta_to_m1m2,
    (
        CoordinateSystem(
            m1_source_coord, m2_source_coord,
            Lambda1_coord, Lambda2_coord,
        ),
        CoordinateSystem(
            LambdaTilde_coord, deltaLambdaTilde_coord,
        ),
    ) : _transformations.m1m2lambda1lambda2_to_lambdatildedeltalambdatilde,
    (
        CoordinateSystem(
            m1_source_coord, m2_source_coord,
            Lambda1_coord, Lambda2_coord,
        ),
        CoordinateSystem(LambdaTilde_coord),
    ) : _transformations.m1m2lambda1lambda2_to_lambdatilde,
    (
        CoordinateSystem(
            m1_source_coord, m2_source_coord,
            Lambda1_coord, Lambda2_coord,
        ),
        CoordinateSystem(deltaLambdaTilde_coord),
    ) : _transformations.m1m2lambda1lambda2_to_deltalambdatilde,
    (
        CoordinateSystem(
            eta_coord,
            Lambda1_coord, Lambda2_coord,
        ),
        CoordinateSystem(
            LambdaTilde_coord, deltaLambdaTilde_coord,
        ),
    ) : _transformations.etalambda1lambda2_to_lambdatildedeltalambdatilde,
    (
        CoordinateSystem(
            eta_coord,
            Lambda1_coord, Lambda2_coord,
        ),
        CoordinateSystem(LambdaTilde_coord),
    ) : _transformations.etalambda1lambda2_to_lambdatilde,
    (
        CoordinateSystem(
            eta_coord,
            Lambda1_coord, Lambda2_coord,
        ),
        CoordinateSystem(deltaLambdaTilde_coord),
    ) : _transformations.etalambda1lambda2_to_deltalambdatilde,
    (
        CoordinateSystem(
            eta_coord,
            LambdaTilde_coord, deltaLambdaTilde_coord,
        ),
        CoordinateSystem(
            Lambda1_coord, Lambda2_coord,
        ),
    ) : _transformations.etalambdatildedeltalambdatilde_to_lambda1lambda2,
    (
        CoordinateSystem(
            m1_source_coord, m2_source_coord,
            LambdaTilde_coord, deltaLambdaTilde_coord,
        ),
        CoordinateSystem(
            Lambda1_coord, Lambda2_coord,
        ),
    ) : _transformations.m1m2lambdatildedeltalambdatilde_to_lambda1lambda2,
    (
        CoordinateSystem(
            delta_coord,
            LambdaTilde_coord, deltaLambdaTilde_coord,
        ),
        CoordinateSystem(
            Lambda1_coord, Lambda2_coord,
        ),
    ) : _transformations.deltalambdatildedeltalambdatilde_to_lambda1lambda2,
})


# Converts parameter names to lalinference versions.
lalinf_name_convert = {
    "m1_source" : "m1_source",
    "m2_source" : "m2_source",
    "Mc_source" : "mc_source",
    "m1_det" : "m1",
    "m2_det" : "m2",
    "Mc_det" : "mc",
    "q" : "q",
    "eta" : "eta",
    "costilt1" : "costilt1",
    "costilt2" : "costilt2",
    "chieff" : "chi_eff",
    "chi1" : "a1",
    "chi1x" : "a1x",
    "chi1y" : "a1y",
    "chi1z" : "a1z",
    "chi2" : "a2",
    "chi2x" : "a2x",
    "chi2y" : "a2y",
    "chi2z" : "a2z",
    "z" : "z",
    "Lambda1" : "lambda1",
    "Lambda2" : "lambda2",
    "LambdaTilde" : "lambdat",
    "deltaLambdaTilde" : "dlambdat",
}

# Alias: will become standard name later.
name_convert_to_lalinf = lalinf_name_convert
name_convert_from_lalinf = dict(
    zip(name_convert_to_lalinf.values(), name_convert_to_lalinf.keys())
)

# Converts parameter names to GWTC-1 data release versions
name_convert_to_GWTC1 = {

}
name_convert_from_GWTC1 = dict(
    zip(name_convert_to_GWTC1.values(), name_convert_to_GWTC1.keys())
)
