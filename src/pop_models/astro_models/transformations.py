from ..types import Observables
from pop_models.xpy_tools import get_xpy

def m1m2_ordering(mAmB: Observables) -> Observables:
    xpy = get_xpy(untouched=True)

    mA, mB = mAmB

    i_sorted = mA >= mB
    m1 = xpy.where(i_sorted, mA, mB)
    m2 = xpy.where(i_sorted, mB, mA)

    return m1, m2

def mdetz_to_msource(mdetz: Observables) -> Observables:
    mdet, z = mdetz
    msource = mdet / (1 + z)

    return msource,


def msourcez_to_mdet(msourcez: Observables) -> Observables:
    msource, z = msourcez
    mdet = msource * (1 + z)

    return mdet,


def m1m2_to_M(m1m2: Observables) -> Observables:
    m1, m2 = m1m2

    return m1 + m2,


def m1m2_to_q(m1m2: Observables) -> Observables:
    m1, m2 = m1m2

    return m2 / m1,


def m1q_to_m2(m1q: Observables) -> Observables:
    m1, q = m1q

    return m1 * q,


def m2q_to_m1(m2q: Observables) -> Observables:
    m2, q = m2q

    return m2 / q,


def Mq_to_m1m2(Mq: Observables) -> Observables:
    M, q = Mq

    m1 = M / (1 + q)
    m2 = q * m1

    return m1, m2


def m1m2chi1zchi2z_to_chieff(m1m2chi1zchi2z: Observables) -> Observables:
    m1, m2, chi1z, chi2z = m1m2chi1zchi2z

    return (m1*chi1z + m2*chi2z) / (m1 + m2),

def m1m2chi1chi2costilt1costilt2_to_chieff(
        m1m2chi1chi2costilt1costilt2: Observables,
    ) -> Observables:
    m1, m2, chi1, chi2, costilt1, costilt2 = m1m2chi1chi2costilt1costilt2

    chi1z, chi2z = chi1*costilt1, chi2*costilt2

    return (m1*chi1z + m2*chi2z) / (m1 + m2),


def m1m2chi1zchi2z_to_chiminus(m1m2chi1zchi2z: Observables) -> Observables:
    m1, m2, chi1z, chi2z = m1m2chi1zchi2z

    return (m1*chi1z - m2*chi2z) / (m1 + m2),

def m1m2chieffchiminus_to_chi1zchi2z(
        m1m2chieffchiminus: Observables,
    ) -> Observables:
    m1, m2, chieff, chiminus = m1m2chieffchiminus

    half_M = 0.5 * (m1 + m2)

    chi1z = half_M * (chieff + chiminus) / m1
    chi2z = half_M * (chieff - chiminus) / m2

    return chi1z, chi2z

def mcdeltachieffchiminus_to_chi1zchi2z(
        mcdeltachieffchiminus: Observables,
    ) -> Observables:
    mcdelta = mcdeltachieffchiminus[:2]
    chieffchiminus = mcdeltachieffchiminus[2:]
    m1m2 = mcdelta_to_m1m2(mcdelta)
    m1m2chieffchiminus = m1m2 + chieffchiminus

    return m1m2chieffchiminus_to_chi1zchi2z(m1m2chieffchiminus)

def m1m2chi1chi2costilt1costilt2_to_chiminus(
        m1m2chi1chi2costilt1costilt2: Observables,
    ) -> Observables:
    m1, m2, chi1, chi2, costilt1, costilt2 = m1m2chi1chi2costilt1costilt2

    chi1z, chi2z = chi1*costilt1, chi2*costilt2

    return (m1*chi1z - m2*chi2z) / (m1 + m2),


def chicostilt_to_chiz(chicostilt: Observables) -> Observables:
    chi, costilt = chicostilt

    return chi * costilt,


def m1m2_to_mc(m1m2: Observables) -> Observables:
    m1, m2 = m1m2

    M = m1 + m2
    m1m2 = m1 * m2

    M_c = m1m2**0.6 * M**-0.2

    return M_c,


def m1m2_to_eta(m1m2: Observables) -> Observables:
    m1, m2 = m1m2

    M = m1 + m2

    eta = m1 * m2 * M**-2.0

    return eta,


def m1m2_to_delta(m1m2: Observables) -> Observables:
    m1, m2 = m1m2

    delta = (m1-m2) / (m1+m2)

    return delta,


def delta_to_eta(delta: Observables) -> Observables:
    delta, = delta

    eta = 0.25 * (1.0 - delta*delta)

    return eta,


def eta_to_delta(eta: Observables) -> Observables:
    eta, = eta

    delta = (1.0 - 4.0*eta)**0.5

    return delta,


def mcdelta_to_m1m2(mcdelta: Observables) -> Observables:
    mc, delta = mcdelta

    eta, = delta_to_eta((delta,))

    return mceta_to_m1m2((mc, eta))


def m1m2_to_mceta(m1m2: Observables) -> Observables:
    m1, m2 = m1m2

    M = m1 + m2

    eta = m1 * m2 * M**-2.0
    M_c = eta**(3.0/5.0) * M

    return M_c, eta


def mceta_to_m1m2(mceta: Observables) -> Observables:
    xpy = get_xpy(untouched=True)

    M_c, eta = mceta

    x = xpy.maximum(1 - 4*eta, 0.0)
    sqrtx = xpy.sqrt(x)

    # Temporary value of m_1. Multiplication by 1+sqrtx comes later
    m_1 = 0.5 * M_c * eta**-0.6

    m_2 = m_1 * (1.0 - sqrtx)
    m_1 *= 1.0 + sqrtx

    return m_1, m_2


def cart2polar(xyz: Observables) -> Observables:
    xpy = get_xpy(untouched=True)

    x, y, z = xyz

    r = xpy.sqrt(x*x + y*y + z*z)
    costh = z / r
    phi = xpy.arctan2(y, x)

    # If r is zero, we assume the cos(theta) is also zero
    costh[r==0] = 0.0

    return r, costh, phi


def polar2cart(rcosthphi: Observables) -> Observables:
    xpy = get_xpy(untouched=True)

    r, costh, phi = rcosthphi

    sinth = xpy.sin(xpy.arccos(costh))

    x = r * sinth * xpy.cos(phi)
    y = r * sinth * xpy.sin(phi)
    z = r * costh

    return x, y, z


def _comp_lambda_tilde_parts(eta, Lambda1, Lambda2):
    eta_sq = eta*eta
    sqrt_one_minus_4eta = (1.0 - 4.0*eta)**0.5

    LambdaPlus = Lambda1 + Lambda2
    LambdaMinus = Lambda1 - Lambda2

    return eta_sq, sqrt_one_minus_4eta, LambdaPlus, LambdaMinus


def _comp_lambda_tilde(
        eta, eta_sq, LambdaPlus, LambdaMinus, sqrt_one_minus_4eta,
    ):
    # From doi:10.1103/PhysRevD.89.103012
    return (8.0 / 13.0) * (
        (1.0 + 7.0*eta - 31.0*eta_sq) * LambdaPlus +
        sqrt_one_minus_4eta * (1.0 + 9.0*eta - 11.0*eta_sq) * LambdaMinus
    )

def _comp_delta_lambda_tilde(
        eta, eta_sq, LambdaPlus, LambdaMinus, sqrt_one_minus_4eta,
    ):
    # From doi:10.1103/PhysRevD.89.103012
    return 0.5 * (
        (
            sqrt_one_minus_4eta *
            (
                1.0 -
                (13272.0/1319.0)*eta +
                (8944.0/1319.0)*eta_sq
            ) *
            LambdaPlus
        ) +
        (
            (
                1.0 -
                (15910.0/1319.0)*eta +
                (32850.0/1319.0)*eta_sq +
                (3380.0/1319.0)*(eta_sq*eta)
            ) *
            LambdaMinus
        )
    )

def etalambda1lambda2_to_lambdatildedeltalambdatilde(
        etalambda1lambda2: Observables,
    ) -> Observables:
    eta, Lambda1, Lambda2 = etalambda1lambda2

    eta_sq, sqrt_one_minus_4eta, LambdaPlus, LambdaMinus = (
        _comp_lambda_tilde_parts(eta, Lambda1, Lambda2)
    )

    LambdaTilde = _comp_lambda_tilde(
        eta, eta_sq, LambdaPlus, LambdaMinus, sqrt_one_minus_4eta,
    )
    deltaLambdaTilde = _comp_delta_lambda_tilde(
        eta, eta_sq, LambdaPlus, LambdaMinus, sqrt_one_minus_4eta,
    )

    return LambdaTilde, deltaLambdaTilde


def etalambda1lambda2_to_lambdatilde(
        etalambda1lambda2: Observables,
    ) -> Observables:
    eta, Lambda1, Lambda2 = etalambda1lambda2

    eta_sq, sqrt_one_minus_4eta, LambdaPlus, LambdaMinus = (
        _comp_lambda_tilde_parts(eta, Lambda1, Lambda2)
    )

    LambdaTilde = _comp_lambda_tilde(
        eta, eta_sq, LambdaPlus, LambdaMinus, sqrt_one_minus_4eta,
    )

    return LambdaTilde,


def etalambda1lambda2_to_deltalambdatilde(
        etalambda1lambda2: Observables,
    ) -> Observables:
    eta, Lambda1, Lambda2 = etalambda1lambda2

    eta_sq, sqrt_one_minus_4eta, LambdaPlus, LambdaMinus = (
        _comp_lambda_tilde_parts(eta, Lambda1, Lambda2)
    )

    deltaLambdaTilde = _comp_delta_lambda_tilde(
        eta, eta_sq, LambdaPlus, LambdaMinus, sqrt_one_minus_4eta,
    )

    return deltaLambdaTilde,

def etalambdatildedeltalambdatilde_to_lambda1lambda2(
        etalambdatildedeltalambdatilde: Observables,
    ) -> Observables:
    xpy = get_xpy(untouched=True)

    eta, LambdaTilde, deltaLambdaTilde = etalambdatildedeltalambdatilde
    eta_sq = xpy.square(eta)

    a = (8.0/13.0)*(1.0+7.0*eta-31.0*eta_sq)
    b = (8.0/13.0)*xpy.sqrt(1.0-4.0*eta)*(1.0+9.0*eta-11.0*eta_sq)
    c = 0.5 * xpy.sqrt(1.0-4.0*eta) * (
        1.0 - 13272.0*eta/1319.0 + 8944.0*eta_sq/1319.0
    )
    d = 0.5 * (
        1.0 - 15910.0*eta/1319.0 + 32850.0*eta_sq/1319.0 + 3380.0*eta**3/1319.0
    )
    den = (a+b)*(c-d) - (a-b)*(c+d)
    Lambda1 = ( (c-d)*LambdaTilde - (a-b)*deltaLambdaTilde )/den
    Lambda2 = (-(c+d)*LambdaTilde + (a+b)*deltaLambdaTilde )/den
    # Adjust lam1 and lam2 if lam1 becomes negative
    # lam2 should be adjusted such that lamt is held fixed
#    if lam1<0:
#        lam1 = 0
#        lam2 = lamt / (a-b)
    return Lambda1, Lambda2


def m1m2lambdatildedeltalambdatilde_to_lambda1lambda2(
        m1m2lambdatildedeltalambdatilde: Observables,
    ) -> Observables:
    m1m2 = m1m2lambdatildedeltalambdatilde[:2]
    lambdatildedeltalambdatilde = m1m2lambdatildedeltalambdatilde[2:]
    eta = m1m2_to_eta(m1m2)
    etalambdatildedeltalambdatilde = eta + lambdatildedeltalambdatilde

    return etalambdatildedeltalambdatilde_to_lambda1lambda2(
        etalambdatildedeltalambdatilde
    )


def deltalambdatildedeltalambdatilde_to_lambda1lambda2(
        deltalambdatildedeltalambdatilde: Observables,
    ) -> Observables:
    delta = deltalambdatildedeltalambdatilde[:1]
    lambdatildedeltalambdatilde = deltalambdatildedeltalambdatilde[1:]
    eta = delta_to_eta(delta)
    etalambdatildedeltalambdatilde = eta + lambdatildedeltalambdatilde

    return etalambdatildedeltalambdatilde_to_lambda1lambda2(
        etalambdatildedeltalambdatilde
    )


def m1m2lambda1lambda2_to_lambdatildedeltalambdatilde(
        m1m2lambda1lambda2: Observables,
    ) -> Observables:
    m1, m2, Lambda1, Lambda2 = m1m2lambda1lambda2
    eta, = m1m2_to_eta((m1, m2))

    return etalambda1lambda2_to_lambdatildedeltalambdatilde((
        eta, Lambda1, Lambda2,
    ))


def m1m2lambda1lambda2_to_lambdatilde(
        m1m2lambda1lambda2: Observables,
    ) -> Observables:
    m1, m2, Lambda1, Lambda2 = m1m2lambda1lambda2
    eta, = m1m2_to_eta((m1, m2))

    return etalambda1lambda2_to_lambdatilde((eta, Lambda1, Lambda2))

def m1m2lambda1lambda2_to_deltalambdatilde(
        m1m2lambda1lambda2: Observables,
    ) -> Observables:
    m1, m2, Lambda1, Lambda2 = m1m2lambda1lambda2
    eta, = m1m2_to_eta((m1, m2))

    return etalambda1lambda2_to_deltalambdatilde((eta, Lambda1, Lambda2))
