import types
import typing
from pop_models.utils import debug_verbose

import numpy

from pop_models.optimization import jit

from pop_models.prob import sample_with_cond
from pop_models.coordinate import CoordinateSystem, CoordinateTransforms
from pop_models.population import Population
from pop_models.types import Numeric, WhereType, Observables, Parameters

from pop_models.astro_models import coordinates

from pop_models.utils import debug_verbose

coordinate_system = CoordinateSystem(
    coordinates.m1_source_coord,
    coordinates.m2_source_coord,
)


@jit(parallel=True, forceobj=True)
def powerlaw_rvs(
        samples_shape,
        powers, x_mins, x_maxs,
        random_state=None,
        xpy=numpy,
    ):
    # Get a RandomState object if we don't have one already.
    if random_state is None:
        random_state = xpy.random.RandomState()

    # Ensure the `samples_shape` input is a tuple.
    if numpy.ndim(samples_shape) == 0:
        samples_shape = (samples_shape,)
    else:
        samples_shape = tuple(samples_shape)

    # Broadcast all parameters to compatible shape.
    powers, x_mins, x_maxs = xpy.broadcast_arrays(powers, x_mins, x_maxs)

    # Determine shape of various parts of the computation.
    # Output needs to have shape `params_shape + samples_shape`, though for
    # broadcasting reasons we do most of the computation in the transpose-space.
    params_shape = powers.shape
    output_shape = params_shape + samples_shape
    output_shape_T = output_shape[::-1]

    one_plus_powers_T = 1.0 + powers.T

    U = random_state.uniform(size=output_shape_T)

    L = x_mins.T ** one_plus_powers_T
    H = x_maxs.T ** one_plus_powers_T

    return (((1.0-U)*L + U*H) ** (1.0 / one_plus_powers_T)).T


def marginal_m1_pdf_noMmax(
        m_1,
        alpha_m, m_min, m_max,
        where=True, out=None,
    ):
    import numpy

    # Ensure everything is a numpy array.
    m_1 = numpy.asarray(m_1)
    alpha_m = numpy.asarray(alpha_m)
    m_min = numpy.asarray(m_min)
    m_max = numpy.asarray(m_max)
    where = numpy.asarray(where)

    # Broadcast things to the appropriate shapes.
    alpha_m, m_min, m_max = numpy.broadcast_arrays(alpha_m, m_min, m_max)

    # Determine the shapes of the parameters and hyperparameters.
    S = m_1.shape
    T = alpha_m.shape
    # Compute the output shape, as well as the shape of its transpose.
    TS = T+S
    TS_T = S[::-1]+T[::-1]

    # Broadcast `where` to the output shape.
    where_TS = numpy.broadcast_to(where.T, TS_T).T

    # Initialize the output array if not provided.
    out_type = m_1.dtype
    if out is None:
        pdf = numpy.zeros(TS, dtype=out_type)
    else:
        pdf = out
        pdf[where_TS] = 0.0

    # Create temporary arrays to re-use.
    tmp_TS = numpy.empty_like(pdf)
    tmp_i_T = numpy.empty(T, dtype=bool)

    # Determine where the support is so we can evaluate it there.
    i_eval_tmp = numpy.zeros(TS, dtype=bool)
    i_big_enough = numpy.less_equal.outer(
        m_min, m_1,
        where=where_TS, out=i_eval_tmp,
    )
    i_small_enough = numpy.greater_equal.outer(
        m_max, m_1,
        where=where_TS,
    )
    i_eval = numpy.logical_and(
        i_big_enough, i_small_enough,
        where=where_TS,
        out=i_eval_tmp,
    )


    # Compute the normalizing constant D.
    Dinv = numpy.empty_like(pdf)

    # Handle the special case of alpha_m == 1 separately.
    i_one_base = numpy.equal(alpha_m, 1.0, out=tmp_i_T)
    i_one = numpy.broadcast_to(i_one_base, TS_T).T

    # Store ln(m_max) in D^-1 where it appears.
    ln_mmax = numpy.log(
        numpy.broadcast_to(m_max.T, TS_T).T,
        where=i_one, out=Dinv,
    )

    # Store ln(m_min) in tmp_TS, then subtract it from D^-1 where it appears.
    ln_mmin = numpy.log(
        numpy.broadcast_to(m_min.T, TS_T).T,
        where=i_one, out=tmp_TS,
    )
    numpy.subtract(ln_mmax, ln_mmin, where=i_one, out=Dinv)

    # Handle the remaining non-special cases of betaalpha != 1.
    i_nonspecial_base = numpy.invert(i_one_base, out=tmp_i_T)
    i_nonspecial = numpy.broadcast_to(i_nonspecial_base, TS_T).T

    # Compute (1-alpha_m).T as we will use this multiple times
    one_minus_alpha_T = numpy.subtract(1.0, alpha_m, where=where).T

    # Store m_max^(1-alpha) in D^-1 where it appears.
    m_max_to_the_one_minus_alpha = numpy.power(
        m_max.T, one_minus_alpha_T,
        where=i_nonspecial.T, out=Dinv.T,
    ).T

    # Store m_min^(1-alpha) in tmp_TS, then subtract it from D^-1 where it
    # appears.
    m_min_to_the_one_minus_alpha = numpy.power(
        m_min.T, one_minus_alpha_T,
        where=i_nonspecial.T, out=tmp_TS.T,
    ).T
    m_to_powers_difference = numpy.subtract(
        m_max_to_the_one_minus_alpha, m_min_to_the_one_minus_alpha,
        where=i_nonspecial, out=Dinv,
    )

    # Put in the 1/(1-alpha) term in D^-1 and now it's complete.
    numpy.divide(
        Dinv.T, one_minus_alpha_T,
        where=i_nonspecial.T, out=Dinv.T,
    )

    # Compute the m_1 term, and store it in the pdf output.
    numpy.power.outer(
        m_1.T, -alpha_m.T,
        where=i_eval.T, out=pdf.T,
    )

    # Divide by the normalizing constant, and the pdf is complete.
    numpy.divide(
        pdf, Dinv,
        where=i_eval, out=pdf,
    )

    return pdf


def conditional_q_pdf(
        q, m_1,
        beta_q, m_min, m_max, M_max,
        where=True, out=None,
    ):
    import numpy

    # Ensure everything is a numpy array.
    q = numpy.asarray(q)
    m_1 = numpy.asarray(m_1)
    beta_q = numpy.asarray(beta_q)
    m_min = numpy.asarray(m_min)
    m_max = numpy.asarray(m_max)
    where = numpy.asarray(where)

    # Broadcast things to the appropriate shapes.
    q, m_1 = numpy.broadcast_arrays(q, m_1)
    beta_q, m_min, m_max = numpy.broadcast_arrays(beta_q, m_min, m_max)

    # Determine the shapes of the parameters and hyperparameters.
    S = q.shape
    T = beta_q.shape
    # Compute the output shape, as well as the shape of its transpose.
    TS = T+S
    TS_T = S[::-1]+T[::-1]

    # Broadcast `where` to the output shape.
    where_TS = numpy.broadcast_to(where.T, TS_T).T

    # Initialize the output array if not provided.
    out_type = q.dtype
    if out is None:
        pdf = numpy.zeros(TS, dtype=out_type)
    else:
        pdf = out
        pdf[where_TS] = 0.0

    # Create some temporary arrays to re-use later.
    tmp_TS = numpy.empty_like(pdf)
    tmp_i_T = numpy.empty(T, dtype=bool)

    # Compute q^beta, and store it in the output array.  We want this to have
    # Shape TS, but it will have shape TS_T if we don't transpose things, so we
    # perform transposes and inverse transposes.
    q_to_the_beta = numpy.power.outer(
        q.T, beta_q.T,
        where=where_TS.T, out=pdf.T,
    ).T

    ## Compute the minimum and maximum mass ratios for each m_1.
    ## Given in Equation 8 of LIGO-T1800428-v2.
    ## Want these both to have shape TS.
    # q_min = m_min / m_1
    q_min = numpy.divide.outer(m_min, m_1)
    # q_max = min(m_1, M_max-m_1) / m_1
    if M_max is None:
        q_max = 1.0
    else:
        Mmax_minus_m1 = M_max - m_1
        min_m1_and_Mmax_minus_m1 = numpy.minimum(
            m_1, Mmax_minus_m1,
            out=Mmax_minus_m1,
        )
        q_max = numpy.divide(
            min_m1_and_Mmax_minus_m1, m_1,
            out=min_m1_and_Mmax_minus_m1,
        )
        q_max = 1.0
    q_max = numpy.broadcast_to(q_max, TS)

    # Compute the normalizing constant C(m_1).
    Cinv = numpy.empty_like(pdf)

    # Handle the special case of beta_q == -1 separately.
    i_neg_one_base = numpy.equal(beta_q, -1.0, out=tmp_i_T)
    i_neg_one = numpy.broadcast_to(i_neg_one_base, TS_T).T

    # Store ln(q_max) in C^-1 where it appears.
    ln_qmax = numpy.log(q_max, where=i_neg_one, out=Cinv)

    # Store ln(q_min) in tmp_TS, then subtract it from C^-1 where it appears.
    ln_qmin = numpy.log(q_min, where=i_neg_one, out=tmp_TS)
    numpy.subtract(ln_qmax, ln_qmin, where=i_neg_one, out=Cinv)

    # Handle the remaining non-special cases of beta != -1.
    i_nonspecial_base = numpy.invert(i_neg_one_base, out=tmp_i_T)
    i_nonspecial = numpy.broadcast_to(i_nonspecial_base, TS_T).T

    # Compute (1+beta_q).T as we will use this multiple times
    one_plus_beta_T = numpy.add(1.0, beta_q, where=where).T

    # Store q_max^(1+beta) in C^-1 where it appears.
    q_max_to_the_one_plus_beta = numpy.power(
        q_max.T, one_plus_beta_T,
        where=i_nonspecial.T, out=Cinv.T,
    ).T

    # Store q_min^(1+beta) in tmp_TS, then subtract it from C^-1 where it
    # appears.
    q_min_to_the_one_plus_beta = numpy.power(
        q_min.T, one_plus_beta_T,
        where=i_nonspecial.T, out=tmp_TS.T,
    ).T
    q_to_powers_difference = numpy.subtract(
        q_max_to_the_one_plus_beta, q_min_to_the_one_plus_beta,
        where=i_nonspecial, out=Cinv,
    )

    # Put in the 1/(1+beta) term in C^-1 and now it's complete.
    numpy.divide(
        Cinv.T, one_plus_beta_T,
        where=i_nonspecial.T, out=Cinv.T,
    )

    # Divide the PDF by C^-1 to get the final PDF on the support.
    numpy.divide(pdf, Cinv, where=where_TS, out=pdf)

    # Determine where there is no support and zero out the PDF
    i_nosupport_tmp = numpy.zeros(TS, dtype=bool)
    i_too_low = numpy.less(q, q_min, where=where_TS, out=i_nosupport_tmp)
    i_too_high = numpy.greater(q, q_max, where=where_TS)
    i_nosupport = numpy.logical_or(
        i_too_low, i_too_high,
        where=where_TS, out=i_nosupport_tmp,
    )
    pdf[i_nosupport] = 0.0

    return pdf


def conditional_m2_pdf(
        m_2, m_1,
        beta_q, m_min, m_max, M_max,
        where=True, out=None,
    ):
    import numpy

    # Ensure everything is a numpy array.
    m_2 = numpy.asarray(m_2)
    m_1 = numpy.asarray(m_1)
    beta_q = numpy.asarray(beta_q)
    m_min = numpy.asarray(m_min)
    m_max = numpy.asarray(m_max)
    where = numpy.asarray(where)

    # Broadcast things to the appropriate shapes.
    m_2, m_1 = numpy.broadcast_arrays(m_2, m_1)
    beta_q, m_min, m_max = numpy.broadcast_arrays(beta_q, m_min, m_max)


    # Determine the shapes of the parameters and hyperparameters.
    S = m_2.shape
    T = beta_q.shape
    # Compute the shape of the transpose of the output array.
    TS_T = S[::-1]+T[::-1]

    # Broadcast `where` to the output shape.
    where_TS = numpy.broadcast_to(where.T, TS_T).T

    # Transform to the mass ratio.
    q = m_2 / m_1

    # Compute p(q | m_1)
    pdf_q_m1 = conditional_q_pdf(
        q, m_1,
        beta_q, m_min, m_max, M_max,
        where=where, out=out,
    )

    # Divide result by m_1 to get p(m_2 | m_1)
    pdf = numpy.divide(pdf_q_m1, m_1, where=where_TS, out=out)

    return pdf


def mass_pdf_const(
        alpha_m, beta_q, m_min, m_max, M_max,
        where=True,
        n_samples=10000,
        random_state=None,
    ):
    import numpy

    if random_state is None:
        random_state = numpy.random.RandomState()


    # Ensure everything is a numpy array.
    alpha_m = numpy.asarray(alpha_m)
    beta_q = numpy.asarray(beta_q)
    m_min = numpy.asarray(m_min)
    m_max = numpy.asarray(m_max)
    where = numpy.asarray(where)

    # Broadcast things to the appropriate shapes.
    (
        alpha_m, beta_q, m_min, m_max, where,
    ) = numpy.broadcast_arrays(
        alpha_m, beta_q, m_min, m_max, where,
    )

    # If M_max is big enough, const will always be 1.0
    if (M_max is None) or (numpy.max(m_max) <= 0.5*M_max):
        return numpy.broadcast_to(1.0, alpha_m.shape)

    n_samples_float = float(n_samples)

    const = numpy.zeros_like(alpha_m)

    ## TODO: Vectorize this loop.  rvs function should support it now
    for idx, _ in numpy.ndenumerate(const):
        # Skip indices not indicated by `where` array.
        if not where[idx]:
            continue

        m1_m2 = mass_joint_rvs_noMmax(
            n_samples,
            alpha_m[idx], beta_q[idx], m_min[idx], m_max[idx],
            random_state=random_state,
        )

        i_bad = numpy.sum(m1_m2, axis=1) > M_max
        n_bad = numpy.count_nonzero(i_bad)

        if n_bad == 0:
            const[idx] = 1.0
        else:
            const[idx] = n_samples_float / n_bad

    return const


def mass_joint_pdf(
        m_1, m_2,
        alpha_m, beta_q, m_min, m_max, M_max,
        const=None,
        out=None, where=True,
        random_state=None,
    ):
    import numpy

    # Ensure everything is a numpy array.
    m_1 = numpy.asarray(m_1)
    m_2 = numpy.asarray(m_2)
    alpha_m = numpy.asarray(alpha_m)
    beta_q = numpy.asarray(beta_q)
    m_min = numpy.asarray(m_min)
    m_max = numpy.asarray(m_max)
    where = numpy.asarray(where)

    # Broadcast things to the appropriate shapes.
    m_1, m_2 = numpy.broadcast_arrays(m_1, m_2)
    (
        alpha_m, beta_q, m_min, m_max,
    ) = numpy.broadcast_arrays(
        alpha_m, beta_q, m_min, m_max,
    )


    # Determine the shapes of the parameters and hyperparameters.
    S = m_1.shape
    T = alpha_m.shape
    # Compute the output shape, as well as the shape of its transpose.
    TS = T+S
    TS_T = S[::-1]+T[::-1]


    # Broadcast `where` to the output shape.
    where_TS = numpy.broadcast_to(where.T, TS_T).T
    # Determine where we need to evaluate the PDF.
    if M_max is None:
        i_eval = where_TS
    else:
        i_eval = where_TS & (m_1 + m_2 <= M_max)

    # Initialize the output array if not provided.
    out_type = m_1.dtype
    if out is None:
        pdf = numpy.zeros(TS, dtype=out_type)
    else:
        pdf = out
        pdf[where_TS] = 0.0

    if const is None:
        const = mass_pdf_const(
            alpha_m, beta_q, m_min, m_max, M_max,
            where=where,
            random_state=random_state,
        )

    tmp_TS = numpy.empty_like(pdf)

    # Compute p(m_1)
    pm1 = marginal_m1_pdf_noMmax(
        m_1,
        alpha_m, m_min, m_max,
        where=where, out=tmp_TS,
    )

    # Compute const * p(m_1)
    c_pm1 = numpy.multiply(
        numpy.broadcast_to(const.T, TS_T).T, pm1,
        where=i_eval, out=pdf,
    )

    # Compute p(m_2 | m_1)
    pm2 = conditional_m2_pdf(
        m_2, m_1,
        beta_q, m_min, m_max, M_max,
        where=where, out=tmp_TS,
    )

    # Compute const * p(m_1) * p(m_2 | m_1).
    numpy.multiply(pdf, pm2, where=i_eval, out=pdf)

    return pdf

import sys
@jit(parallel=True, forceobj=True)
def mass_joint_rvs_noMmax(
        N,
        alpha_m, beta_q, m_min, m_max,
        where=True,
        random_state=None,
    ):
    if random_state is None:
        random_state = numpy.random.RandomState()

    m_1 = powerlaw_rvs(N, -alpha_m, m_min, m_max, random_state=random_state)

    q_min = m_min / m_1
    q_max = 1.0

    q = powerlaw_rvs(1, beta_q, q_min, q_max, random_state=random_state)[...,0]

    ## TODO: remove this explicit for-loop, by doing array broadcasting properly
    # q = numpy.empty_like(m_1)
    # for idx in numpy.ndindex(*alpha_m.shape):
    #     q[idx] = powerlaw_rvs(
    #         1,
    #         beta_q, q_min[idx], q_max,
    #         random_state=random_state,
    #     )[...,0]
    m_2 = m_1 * q

    return numpy.column_stack((m_1, m_2))


def mass_joint_rvs(
        N,
        alpha_m, beta_q, m_min, m_max, M_max,
        random_state=None,
    ):
    if random_state is None:
        random_state = numpy.random.RandomState()

    # Skip over conditional if `M_max` is `None`
    if M_max is None:
        return mass_joint_rvs_noMmax(
            N,
            alpha_m, beta_q, m_min, m_max,
            random_state=random_state,
        )

    def rvs(N):
        return mass_joint_rvs_noMmax(
            N,
            alpha_m, beta_q, m_min, m_max,
            random_state=random_state,
        )

    def cond(m1_m2):
        """
        Given an array, where each row contains a pair ``(m_1, m_2)``, returns
        an array whose value is ``True`` when ``m_1 + m_2 <= M_max`` and
        ``False`` otherwise.
        """
        return numpy.sum(m1_m2, axis=1) <= M_max

    debug_verbose(
        "Drawing", N, "samples from mass PDF with condition",
        mode="bbh_modelAB", flush=True,
    )
    m1_m2 = sample_with_cond(rvs, shape=N, cond=cond)
    debug_verbose(
        "Successfully drew mass PDF samples.",
        mode="bbh_modelAB", flush=True,
    )
    return m1_m2


class ComponentMassPowerlawPopulation(Population):
    def __init__(
            self,
            M_max: typing.Optional[float],
            xpy: types.ModuleType=numpy,
            rate_name: str="rate",
            m1_index_name: str="alpha", q_index_name: str="beta",
            mmin_name: str="m_min", mmax_name: str="m_max",
            transformations: CoordinateTransforms=coordinates.transformations,
        ):
        param_names = (
            rate_name, m1_index_name, q_index_name, mmin_name, mmax_name,
        )

        super().__init__(
            coordinate_system, param_names,
            transformations=transformations,
        )

        self.M_max = M_max
        self.xpy = numpy
        self.rate_name = rate_name
        self.m1_index_name = m1_index_name
        self.q_index_name = q_index_name
        self.mmin_name = mmin_name
        self.mmax_name = mmax_name

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        m1_source, m2_source = observables

        alpha_m = parameters[self.m1_index_name]
        beta_q = parameters[self.q_index_name]
        m_min = parameters[self.mmin_name]
        m_max = parameters[self.mmax_name]

        return mass_joint_pdf(
            m1_source, m2_source,
            alpha_m, beta_q, m_min, m_max, self.M_max,
            where=where,
        )

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        alpha_m = parameters[self.m1_index_name]
        beta_q = parameters[self.q_index_name]
        m_min = parameters[self.mmin_name]
        m_max = parameters[self.mmax_name]

        param_shape = alpha_m.shape
        out_shape = param_shape + (n_samples,)

        where_arr = self.xpy.broadcast_to(where, param_shape)

        # ## TODO: Avoid un-necessary packing/un-packing.
        # m1, m2 = mass_joint_rvs(
        #     n_samples,
        #     alpha_m, beta_q, m_min, m_max, self.M_max,
        #     where=where,
        #     random_state=random_state,
        # ).T

        ## TEMPORARY FIX: need to get `where` argument used correctly by
        ## underlying code.
        m1 = self.xpy.zeros(out_shape, dtype=alpha_m.dtype)
        m2 = self.xpy.zeros(out_shape, dtype=alpha_m.dtype)

        for idx in numpy.ndindex(*param_shape):
            # Skip samples not included in `where` array.
            if not where_arr[idx]:
                continue

            # Draw samples for params[idx]
            m1[idx], m2[idx] = mass_joint_rvs(
                n_samples,
                alpha_m[idx], beta_q[idx], m_min[idx], m_max[idx], self.M_max,
                random_state=random_state,
            ).T


        return m1, m2

    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return parameters[self.rate_name]

    def params_valid(self, parameters: Parameters) -> WhereType:
        m_min = parameters[self.mmin_name]
        m_max = parameters[self.mmax_name]

        limits_sorted = m_min < m_max
        total_mass_enforced = (m_min + m_max) <= self.M_max

        debug_verbose(
            "Powerlaw cutoffs sorted?", limits_sorted,
            mode="params_valid_powerlaw",
        )
        debug_verbose(
            "Total mass enforced?", total_mass_enforced,
            mode="params_valid_powerlaw", flush=True
        )

        return limits_sorted & total_mass_enforced

    def to_coords(
            self,
            coord_system: CoordinateSystem,
            use_transformations: bool=True,
        ) -> Population:
        if coord_system == CoordinateSystem(coordinates.m1_source_coord):
            return MarginalM1PowerlawPopulation(
                self.M_max,
                xpy=self.xpy,
                rate_name=self.rate_name,
                m1_index_name=self.m1_index_name,
                q_index_name=self.q_index_name,
                mmin_name=self.mmin_name, mmax_name=self.mmax_name,
            )
        else:
            return super().to_coords(
                coord_system, use_transformations=use_transformations,
            )


class MarginalM1PowerlawPopulation(Population):
    def __init__(
            self,
            M_max,
            xpy: types.ModuleType=numpy,
            rate_name: str="rate",
            m1_index_name: str="alpha", q_index_name: str="beta",
            mmin_name: str="m_min", mmax_name: str="m_max",
            transformations: CoordinateTransforms=coordinates.transformations,
        ):
        param_names = (
            rate_name, m1_index_name, q_index_name, mmin_name, mmax_name,
        )

        super().__init__(
            CoordinateSystem(coordinates.m1_source_coord), param_names,
            transformations=transformations,
        )

        self.M_max = M_max
        self.xpy = numpy
        self.rate_name = rate_name
        self.m1_index_name = m1_index_name
        self.q_index_name = q_index_name
        self.mmin_name = mmin_name
        self.mmax_name = mmax_name

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        m1_source, = observables

        alpha_m = parameters[self.m1_index_name]
        m_min = parameters[self.mmin_name]
        m_max = parameters[self.mmax_name]

        return marginal_m1_pdf_noMmax(
            m1_source,
            alpha_m, m_min, m_max,
            where=where,
        )

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        alpha_m = parameters[self.m1_index_name]
        m_min = parameters[self.mmin_name]
        m_max = parameters[self.mmax_name]

        param_shape = alpha_m.shape
        out_shape = param_shape + (n_samples,)

        where_arr = self.xpy.broadcast_to(where, param_shape)

        ## TEMPORARY FIX: need to get `where` argument used correctly by
        ## underlying code.
        m1 = self.xpy.zeros(out_shape, dtype=alpha_m.dtype)

        for idx in numpy.ndindex(*param_shape):
            # Skip samples not included in `where` array.
            if not where_arr[idx]:
                continue

            m1[idx] = powerlaw_rvs(
                n_samples,
                alpha_m[idx], m_min[idx], m_max[idx],
                random_state=random_state,
                xpy=self.xpy,
            )

        return m1,

    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return parameters[self.rate_name]

    def params_valid(self, parameters: Parameters) -> WhereType:
        m_min = parameters[self.mmin_name]
        m_max = parameters[self.mmax_name]

        limits_sorted = m_min < m_max
        total_mass_enforced = (m_min + m_max) <= self.M_max

        debug_verbose(
            "Powerlaw cutoffs sorted?", limits_sorted,
            mode="params_valid_powerlaw",
        )
        debug_verbose(
            "Total mass enforced?", total_mass_enforced,
            mode="params_valid_powerlaw", flush=True
        )

        return limits_sorted & total_mass_enforced

    def to_coords(
            self,
            coord_system: CoordinateSystem,
            use_transformations: bool=True,
        ) -> Population:
        if coord_system.equiv(coordinate_system):
            return ComponentMassPowerlawPopulation(
                self.M_max,
                xpy=self.xpy,
                rate_name=self.rate_name,
                m1_index_name=self.m1_index_name,
                q_index_name=self.q_index_name,
                mmin_name=self.mmin_name, mmax_name=self.mmax_name,
            ).to_coords(
                coordinate_system, use_transformations=use_transformations,
            )
        else:
            return super().to_coords(
                coord_system, use_transformations=use_transformations,
            )
