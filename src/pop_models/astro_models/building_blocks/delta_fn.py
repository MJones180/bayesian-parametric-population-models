import types
import typing

import numpy

from ...coordinate import (
    Coordinate, CoordinateSystem,
    CoordinateTransforms, transformations_nil,
)
from ...population import Population
from ...types import Numeric, WhereType, Observables, Parameters
from ...stats import beta_pdf


class DeltaFnPopulation(Population):
    def __init__(
            self,
            coord_system: CoordinateSystem,
            values: typing.List[float],
            transformations: CoordinateTransforms=transformations_nil,
            xpy: types.ModuleType=numpy,
            rate_name: str="rate",
        ):
        if len(values) != len(coord_system):
            raise ValueError(
                "'values' must be a list of numbers, one for each coordinate"
            )

        param_names = (rate_name,)

        super().__init__(
            coord_system, param_names,
            transformations=transformations,
        )

        self.values = values
        self.xpy = xpy

        self.rate_name = rate_name

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        raise RuntimeError(
            "Cannot evaluate the PDF for a {}, as it diverges."
            .format(self.__class__.__name__)
        )

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        params_shape = self.xpy.shape(next(iter(parameters.values())))
        shape = params_shape + (n_samples,)

        return tuple(self.xpy.broadcast_to(v, shape) for v in self.values)

    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return parameters[self.rate_name]
