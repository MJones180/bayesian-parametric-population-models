import typing

import math
import numpy

from ...coordinate import CoordinateSystem, CoordinateTransforms
from ...population import Population
from ...types import Parameters, Observables, WhereType
from ..coordinates import ra_coord, dec_coord, transformations

coord_system = CoordinateSystem(ra_coord, dec_coord)

class IsotropicSkyLocationPopulation(Population):
    def __init__(
            self,
            transformations: CoordinateTransforms=transformations,
        ) -> None:
        param_names = ()
        super().__init__(
            coord_system, param_names,
            transformations=transformations,
        )

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: typing.Optional[numpy.random.RandomState]=None,
        ) -> Observables:
        if random_state is None:
            random_state = numpy.random.RandomState()

        ra_samples = random_state.uniform(0.0, 2*math.pi, n_samples)
        dec_samples = numpy.arccos(
            random_state.uniform(0.0, 1.0, n_samples)
        )

        return ra_samples, dec_samples
