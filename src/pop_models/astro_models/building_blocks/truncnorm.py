import types
import typing

import numpy

from pop_models.coordinate import (
    Coordinate, CoordinateSystem,
    CoordinateTransforms, transformations_nil,
)
from pop_models.population import Population
from pop_models.types import Numeric, WhereType, Observables, Parameters
from pop_models.stats import truncnorm_pdf

from pop_models.utils import debug_verbose

class TruncnormPopulation(Population):
    def __init__(
            self,
            coordinate: Coordinate,
            xpy: types.ModuleType=numpy,
            lower: float=0.0, upper: float=1.0,
            rate_name: str="rate",
            mu_name: str="mu",
            sigma_name: str="sigma",
            transformations: CoordinateTransforms=transformations_nil,
        ):
        coord_system = CoordinateSystem(coordinate)
        param_names = (rate_name, mu_name, sigma_name)

        super().__init__(
            coord_system, param_names,
            transformations=transformations,
        )

        self.xpy = xpy

        self.lower = lower
        self.upper = upper

        self.rate_name = rate_name
        self.mu_name = mu_name
        self.sigma_name = sigma_name

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        # Extract the one observable, and mu, sigma
        x, = observables
        mu = parameters[self.mu_name]
        sigma = parameters[self.sigma_name]

        return truncnorm_pdf(
            x,
            mu, sigma, self.lower, self.upper,
            where=where,
        )

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        import scipy.stats

        if random_state is None:
            random_state = self.xpy.random.RandomState()

        # Extract mu and sigma
        mu = parameters[self.mu_name]
        sigma = parameters[self.sigma_name]

        # Filter out values we don't want to evaluate.
        params_shape = mu.shape
        mu, sigma = mu[where], sigma[where]

        out_shape = params_shape + (n_samples,)

        # Initialize array to output samples in.
        samples = numpy.full(
            out_shape, 0.5*(self.upper+self.lower),
            dtype=numpy.float64,
        )

        # The initial shape of the samples we'll draw.  Note that the last axis
        # has already been possibly shrunken due to down-selecting from `where`.
        sample_shape = (n_samples, mu.size)

        # Draw samples from a truncnorm distribution for all of the desired
        # indices, and overwrite the output samples at those indices.  Due to
        # the way scipy.stats operates, this needs to be done in the
        # transpose-space.
        samples[where] = scipy.stats.truncnorm(
            (self.lower-mu)/sigma, (self.upper-mu)/sigma,
            loc=mu, scale=sigma,
        ).rvs(sample_shape).T

        return samples,

    def params_valid(self, parameters: Parameters) -> WhereType:
        sigma = parameters[self.sigma_name]

        sigma_valid = sigma > 0.0
        debug_verbose(
            "Truncnorm sigma valid?", sigma_valid,
            mode="params_valid_truncnorm", flush=True,
        )

        return sigma_valid


    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return parameters[self.rate_name]
