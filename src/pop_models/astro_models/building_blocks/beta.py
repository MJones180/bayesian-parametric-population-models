import warnings
import types
import typing

import numpy

from pop_models.coordinate import (
    Coordinate, CoordinateSystem,
    CoordinateTransforms, transformations_nil,
)
from pop_models.population import Population
from pop_models.types import Numeric, WhereType, Observables, Parameters
from pop_models.stats import beta_pdf

from pop_models.utils import debug_verbose

def mean_variance_to_alpha_beta(
        mean, variance, loc=0.0, scale=1.0,
        xpy=numpy,
    ):
    import numpy

    a, c = loc, loc+scale

    a_minus_mean = a - mean
    c_minus_mean = c - mean

    shared_term = (
        (a_minus_mean*c_minus_mean + variance) /
        (scale * variance)
    )

    alpha =  shared_term * a_minus_mean
    beta  = -shared_term * c_minus_mean

    return alpha, beta


def alpha_beta_to_mean_variance(
        alpha, beta, loc=0.0, scale=1.0,
        xpy=numpy,
    ):
    import numpy

    a, c = loc, loc+scale
    alpha_plus_beta = alpha + beta

    mean = (alpha*c + beta*a) / alpha_plus_beta

    variance = (
        xpy.square(scale) * alpha*beta /
        (xpy.square(alpha_plus_beta) * (alpha_plus_beta + 1.0))
    )

    return mean, variance


class BetaPopulation(Population):
    supported_parameterizations = frozenset({
        "mean_variance",
        "mean_log10variance",
        "alpha_beta",
    })
    def __init__(
            self,
            coordinate: Coordinate,
            loc: float=0.0,
            scale: float=1.0,
            parameterization: str="mean_variance",
            transformations: CoordinateTransforms=transformations_nil,
            xpy: types.ModuleType=numpy,
            rate_name: str="rate",
            mean_name: str="mean",
            variance_name: str="variance",
            alpha_name: str="alpha",
            beta_name: str="beta",
            no_singularities: bool=False,
        ):
        if parameterization not in self.supported_parameterizations:
            raise KeyError(
                "Unknown parameterization: {}\nOnly supports: {}"
                .format(parameterization, self.supported_parameterizations)
            )

        coord_system = CoordinateSystem(coordinate)

        if parameterization == "mean_variance":
            param_names = (rate_name, mean_name, variance_name)
        elif parameterization == "mean_log10variance":
            param_names = (rate_name, mean_name, "log10_"+variance_name)
        elif parameterization == "alpha_beta":
            param_names = (rate_name, alpha_name, beta_name)

        super().__init__(
            coord_system, param_names,
            transformations=transformations,
        )

        self.loc = loc
        self.scale = scale
        self.parameterization = parameterization
        self.xpy = xpy

        self.rate_name = rate_name
        self.mean_name = mean_name
        self.variance_name = variance_name
        self.alpha_name = alpha_name
        self.beta_name = beta_name

        self.no_singularities = no_singularities

        self._setup_max_variance()

    def _setup_max_variance(self):
        """
        Computes the maximum allowed variance for this population.
        """
        # TODO: generalize param_min.
        param_min = 1.0 if self.no_singularities else 0.0

        self._max_variance = self.scale / (4.0 * (2.0*param_min + 1.0))


    @property
    def max_variance(self) -> float:
        return self._max_variance

    def get_alpha_beta(
            self,
            parameters: Parameters,
        ) -> typing.Tuple[Numeric,Numeric]:
        if self.parameterization == "alpha_beta":
            return parameters[self.alpha_name], parameters[self.beta_name]
        elif self.parameterization == "mean_variance":
            mean = parameters[self.mean_name]
            variance = parameters[self.variance_name]

            return mean_variance_to_alpha_beta(
                mean, variance, loc=self.loc, scale=self.scale,
            )
        elif self.parameterization == "mean_log10variance":
            mean = parameters[self.mean_name]
            variance = 10**parameters["log10_"+self.variance_name]

            return mean_variance_to_alpha_beta(
                mean, variance, loc=self.loc, scale=self.scale,
            )
        else:
            # Should be inaccessible.
            raise RuntimeError("Using an unknown parameterization")

    def pdf(
            self,
            observables: Observables,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        # Extract the one observable, and (alpha, beta)
        x, = observables
        alpha, beta = self.get_alpha_beta(parameters)

        return beta_pdf(x, alpha, beta, where=where)

    def rvs(
            self,
            n_samples: int,
            parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=None,
            **kwargs
        ) -> Observables:
        if random_state is None:
            random_state = self.xpy.random.RandomState()

        # Determine alpha, beta from the parameters, and then filter out values
        # we don't want to evaluate.
        alpha, beta = self.get_alpha_beta(parameters)
        params_shape = alpha.shape
        alpha, beta = alpha[where], beta[where]

        out_shape = params_shape + (n_samples,)

        # Initialize array to output samples in.
        samples = numpy.full(out_shape, self.loc, dtype=numpy.float64)

        # The initial shape of the samples we'll draw.  Note that the last axis
        # has already been possibly shrunken due to down-selecting from `where`.
        sample_shape = (n_samples, alpha.size)

        # Draw samples from a beta distribution with support on [0, 1],
        # but only where the `where` array is `True`.  Due to the way numpy
        # random samples work, we do this in the transpose-space, and then
        # transpose the result afterwards.
        beta_samples_where = random_state.beta(
            alpha, beta,
            size=sample_shape,
        ).T
        samples[where] = beta_samples_where*self.scale + self.loc

        return samples,

    def params_valid(self, parameters: Parameters) -> WhereType:
        alpha, beta = self.get_alpha_beta(parameters)

        param_min = 1.0 if self.no_singularities else 0.0

        alpha_valid = (alpha >= param_min)
        beta_valid = (beta >= param_min)

        debug_verbose(
            "Beta population in", self.coord_system[0].name,
            ": alpha is valid?", alpha_valid,
            mode="params_valid_beta",
        )
        debug_verbose(
            "Beta population in", self.coord_system[0].name,
            ": beta is valid?", beta_valid,
            mode="params_valid_beta", flush=True,
        )

        return self.xpy.bitwise_and(alpha_valid, beta_valid, out=alpha_valid)

    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return parameters[self.rate_name]

    def _random_mean_variance_helper(
            self,
            n_samples: int,
            random_state: numpy.random.RandomState=None,
        ) -> typing.Tuple[Numeric,Numeric]:
        """
        Draw random means and variances from a triangular region that
        encompasses most valid values.

        NOTE: A small region is excluded, so only use this as an approximation.
              This should be fixed in a future revision.
        """
        warnings.warn(
            "Sampling beta distribution means and variances from a triangle "
            "that excludes a small number of valid parameters."
        )

        # Seed the RNG.
        if random_state is None:
            random_state = self.xpy.random.RandomState()

        # Draw uniform random numbers which will be transformed.
        sqrt_r1 = self.xpy.sqrt(random_state.uniform(size=n_samples))
        r2 = random_state.uniform(size=n_samples)

        # Vertices of the triangle we are sampling uniformly on.
        vertex_shape = (2, 1)
        A = self.xpy.asarray([0, 0]).reshape(vertex_shape)
        B = self.xpy.asarray([self.scale, 0]).reshape(vertex_shape)
        C = self.xpy.asarray([0.5*self.scale, self.max_variance]).reshape(
            vertex_shape
        )

        # Draw means and variances from the triangle.  Mean does not yet account
        # for self.loc != 0.
        means_shifted, variances = (
            (1 - sqrt_r1)*A + sqrt_r1*(1 - r2)*B + sqrt_r1*r2*C
        )

        # Shift mean by appropriate amount.
        means = means_shifted + self.loc

        return means, variances

    def random_mean_variance(
            self,
            n_samples: int,
            random_state: numpy.random.RandomState=None,
        ) -> typing.Tuple[Numeric,Numeric]:
        """
        Draw random means and variances uniformly from the allowed values.

        NOTE: A small region is excluded, so only use this as an approximation.
              This should be fixed in a future revision.
        """
        # Seed the RNG.
        if random_state is None:
            random_state = self.xpy.random.RandomState()

        # Initialize output arrays.
        means = self.xpy.empty(n_samples)
        variances = self.xpy.empty(n_samples)

        # Iteratively draw candidate means and variances.  The underlying
        # function includes a small fraction of invalid values, so some will be
        # rejected.
        n_drawn = 0
        while n_drawn < n_samples:
            # Draw candidates.
            means_, variances_ = self._random_mean_variance_helper(
                n_samples, random_state=random_state,
            )

            # Determine indices which are valid.
            parameters_ = {
                self.mean_name : means_,
                self.variance_name : variances_,
            }
            i_valid = self.params_valid(parameters_)
            n_valid = self.xpy.count_nonzero(i_valid)

            # Make sure we don't take more samples than we need, so falsely mark
            # samples at the beginning as invalid if needed.
            n_extra = max(0, n_valid - (n_samples - n_drawn))
            n_valid -= n_extra
            i_valid &= i_valid.cumsum() <= n_valid

            # Store the valid candidates.
            means[n_drawn:n_drawn+n_valid] = means_[i_valid]
            variances[n_drawn:n_drawn+n_valid] = variances_[i_valid]

            # Keep track of how many samples we have.
            n_drawn += n_valid

        # Return means and variances.
        return means, variances
