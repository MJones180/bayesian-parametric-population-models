"""
Compute sensitive volumes for ground-based gravitational wave interferometers.

The core of this module was built off of a module written by Will Farr,
available at
<https://git.ligo.org/RatesAndPopulations/O2Populations/blob/fe81c5d064283c94e12a19567e020b9e9930efef/code/vt.py>
"""
import typing

import astropy.cosmology as cosmo
import astropy.units as u
import multiprocessing as multi
import numpy
import numpy as np
from numpy import (
    asarray,
    array, linspace,
    sin, cos,
    square,
    trapz,
)
import h5py

from pop_models.coordinate import CoordinateSystem
from pop_models.population import Population
from pop_models.types import Numeric, Observables, Parameters, WhereType
from pop_models.astro_models.coordinates import (
    m1_source_coord, m2_source_coord,
    chi1z_coord, chi2z_coord,
    z_coord,
)
from pop_models.astro_models.building_blocks.simple_cosmology import (
    CosmoPopulation,
    UniformComovingVolumePopulation, MadauDickinsonPopulation,
)

from pop_models.poisson_mean import PoissonMean
from pop_models.volume import SensitiveVolume


def draw_thetas(N):
    """Draw `N` random angular factors for the SNR.

    Theta is as defined in [Finn & Chernoff
    (1993)](https://ui.adsabs.harvard.edu/#abs/1993PhRvD..47.2198F/abstract).
    """

    cos_thetas = np.random.uniform(low=-1, high=1, size=N)
    cos_incs = np.random.uniform(low=-1, high=1, size=N)
    phis = np.random.uniform(low=0, high=2*np.pi, size=N)
    zetas = np.random.uniform(low=0, high=2*np.pi, size=N)

    Fps = 0.5*cos(2*zetas)*(1 + square(cos_thetas))*cos(2*phis) - sin(2*zetas)*cos_thetas*sin(2*phis)
    Fxs = 0.5*sin(2*zetas)*(1 + square(cos_thetas))*cos(2*phis) + cos(2*zetas)*cos_thetas*sin(2*phis)

    return np.sqrt(0.25*square(Fps)*square(1 + square(cos_incs)) + square(Fxs)*square(cos_incs))



def next_pow_two(x):
    """Return the next (integer) power of two above `x`.

    """

    x2 = 1
    while x2 < x:
        x2 = x2 << 1
    return x2

def optimal_snr(
        m1_intrinsic, m2_intrinsic, s1z, s2z, z,
        fmin=19.0, dfmin=0.0, fref=40.0, psdstart=20.0,
        psd_fn=None,
        approximant=None,
    ):
    """Return the optimal SNR of a signal.

    :param m1_intrinsic: The source-frame mass 1.

    :param m2_intrinsic: The source-frame mass 2.

    :param s1z: The z-component of spin 1.

    :param s2z: The z-component of spin 2.

    :param z: The redshift.

    :param fmin: The starting frequency for waveform generation.

    :param psd_fn: A function that returns the detector PSD at a given
      frequency (default is early aLIGO high sensitivity, defined in
      [P1200087](https://dcc.ligo.org/LIGO-P1200087/public).

    :return: The SNR of a face-on, overhead source.

    """
    import lal
    import lalsimulation as ls

    if psd_fn is None:
        psd_fn = ls.SimNoisePSDaLIGOEarlyHighSensitivityP1200087

    if approximant is None:
        approximant = ls.IMRPhenomPv2

    # Get dL, Gpc
    dL = cosmo.Planck15.luminosity_distance(z).to(u.Gpc).value

    # Basic setup: min frequency for w.f., PSD start freq, etc.
#    fmin = 19.0


    # NOTE: Added s1z and s2z for aligned spin VT
    # This is a conservative estimate of the chirp time + MR time (2 seconds)
    tmax = ls.SimInspiralChirpTimeBound(
        fmin,
        m1_intrinsic*(1+z)*lal.MSUN_SI, m2_intrinsic*(1+z)*lal.MSUN_SI,
        s1z, s2z,
    ) + 2.0

    df = max(1.0/next_pow_two(tmax), dfmin)
    fmax = 2048.0 # Hz --- based on max freq of 5-5 inspiral

    # NOTE: Added s1z and s2z for aligned spin VT
    # Generate the waveform, redshifted as we would see it in the detector, but with zero angles (i.e. phase = 0, inclination = 0)
## Will's version -- apparently from a different version of lalsim
    hp, hc = ls.SimInspiralChooseFDWaveform(
        (1+z)*m1_intrinsic*lal.MSUN_SI, (1+z)*m2_intrinsic*lal.MSUN_SI,
        0.0, 0.0, s1z, 0.0, 0.0, s2z,
        dL*1e9*lal.PC_SI,
        0.0, 0.0, 0.0, 0.0, 0.0,
        df, fmin, fmax, fref, None, approximant,
    )
##    hp, hc = ls.SimInspiralChooseFDWaveform(0.0, df, (1+z)*m1_intrinsic*lal.MSUN_SI, (1+z)*m2_intrinsic*lal.MSUN_SI, 0.0, 0.0, s1z, 0.0, 0.0, s2z, fmin, fmax, fref, dL*1e9*lal.PC_SI, 0.0, 0.0, 0.0, None, None, 0, 0, approximant)

    Nf = int(round(fmax/df)) + 1
    fs = linspace(0, fmax, Nf)
    sel = fs > psdstart

    # PSD
    sffs = lal.CreateREAL8FrequencySeries(
        "psds", 0, 0.0, df, lal.DimensionlessUnit, fs.shape[0],
    )
    psd_fn(sffs, psdstart)

    return ls.MeasureSNRFD(hp, sffs, psdstart, -1.0)


# Variable only used by ``fraction_above_threshold``. The value is constant, and
# needs to only be computed once. It also takes a non-negligible amount of time
# to compute, and so it is computed the first time ``fraction_above_threshold``
# is called, as one might import this module without calling that function.
# NOTE: This is a change from Will Farr's original code.
_thetas = None

def fraction_above_threshold(
        m1_intrinsic, m2_intrinsic, s1z, s2z, z,
        snr_thresh,
        fmin=19.0, dfmin=0.0, fref=40.0, psdstart=20.0,
        psd_fn=None, approximant=None,
    ):
    """Returns the fraction of sources above a given threshold.

    :param m1_intrinsic: Source-frame mass 1.

    :param m2_intrinsic: Source-frame mass 2.

    :param s1z: The z-component of spin 1.

    :param s2z: The z-component of spin 2.

    :param z: Redshift.

    :param snr_thresh: SNR threshold.

    :param fmin: The starting frequency for waveform generation.

    :param psd_fn: Function computing the PSD (see :func:`optimal_snr`).

    :return: The fraction of sources that are above the given
      threshold.

    """
    global _thetas

    import lalsimulation as ls

    if psd_fn is None:
        psd_fn = ls.SimNoisePSDaLIGOEarlyHighSensitivityP1200087

    # Compute ``_thetas`` once and for all. It is stored as a global variable,
    # but it should also not be used outside of this function.
    if _thetas is None:
        _thetas = draw_thetas(10000)

    if z == 0.0:
        return 1.0

    rho_max = optimal_snr(
        m1_intrinsic, m2_intrinsic, s1z, s2z, z,
        fmin=fmin, dfmin=dfmin, fref=fref, psdstart=psdstart,
        psd_fn=psd_fn,
        approximant=approximant,
    )

    # From Finn & Chernoff, we have SNR ~ theta*integrand, assuming that the polarisations are
    # orthogonal
    theta_min = (asarray(snr_thresh) / rho_max)[()]

    if theta_min > 1:
        return 0.0
    else:
        return np.mean(_thetas > theta_min)

def vt_from_mass_spin(
        m1, m2, s1z, s2z,
        thresh,
        analysis_time,
        calfactor=1.0,
        fmin=19.0, dfmin=0.0, fref=40.0, psdstart=20.0,
        zmax=1.5,
        psd_fn=None,
        approximant=None,
    ):
    """Returns the sensitive time-volume for a given system.

    :param m1: Source-frame mass 1.

    :param m2: Source-frame mass 2.

    :param s1z: The z-component of spin 1.

    :param s2z: The z-component of spin 2.

    :param analysis_time: The total detector-frame searched time.

    :param calfactor: Fudge factor applied multiplicatively to the final result.

    :param fmin: The starting frequency for waveform generation.

    :param psd_fn: Function giving the assumed single-detector PSD
      (see :func:`optimal_snr`).

    :return: The sensitive time-volume in comoving Gpc^3-yr (assuming
      analysis_time is given in years).

    """
    import lalsimulation as ls

    if psd_fn is None:
        psd_fn = ls.SimNoisePSDaLIGOEarlyHighSensitivityP1200087

    def integrand(z):
        if z == 0.0:
            return 0.0
        else:
            return (
                4*np.pi *
                cosmo.Planck15.differential_comoving_volume(z)
                  .to(u.Gpc**3 / u.sr).value /
                (1+z) *
                fraction_above_threshold(
                    m1, m2, s1z, s2z, z,
                    thresh,
                    fmin=fmin, dfmin=dfmin, fref=fref, psdstart=psdstart,
                    psd_fn=psd_fn,
                    approximant=approximant,
                )
            )

    zmin = 0.001
    assert fraction_above_threshold(
          m1, m2, s1z, s2z, zmax,
          thresh,
          fmin=fmin, dfmin=dfmin, fref=fref, psdstart=psdstart,
          psd_fn=psd_fn,
          approximant=approximant,
      ) == 0.0
    # Bad assertion: some sources may be un-detectable, and VT will be zero.
    # No need to assert that is untrue.
    # assert fraction_above_threshold(
    #     m1, m2, s1z, s2z, zmin,
    #     thresh,
    #     fmin=fmin, dfmin=dfmin, psd_fn=psd_fn,
    #     approximant=approximant,
    # ) > 0.0
    while zmax - zmin > 1e-3:
        zhalf = 0.5*(zmax+zmin)
        fhalf = fraction_above_threshold(
            m1, m2, s1z, s2z, zhalf,
            thresh,
            fmin=fmin, dfmin=dfmin, fref=fref, psdstart=psdstart,
            psd_fn=psd_fn,
            approximant=approximant,
        )

        if fhalf > 0.0:
            zmin=zhalf
        else:
            zmax=zhalf

    zs = linspace(0.0, zmax, 20)
    ys = array([integrand(z) for z in zs])
    return calfactor*analysis_time*trapz(ys, zs)

class VTFromMassSpinTuple(object):
    def __init__(
            self,
            thresh, analyt, calfactor,
            psd_fn,
            fmin=19.0, dfmin=0.0, fref=40.0, psdstart=20.0,
            zmax=1.5, approximant=None,
        ):
        self.thresh = thresh
        self.analyt = analyt
        self.calfactor = calfactor
        self.psd_fn = psd_fn
        self.fmin = fmin
        self.dfmin = dfmin
        self.fref = fref
        self.psdstart = psdstart
        self.zmax = zmax
        self.approximant = approximant

    def __call__(self, m1m2s1zs2z):
        m1, m2, s1z, s2z = m1m2s1zs2z
        return vt_from_mass_spin(
            m1, m2, s1z, s2z,
            self.thresh, self.analyt,
            calfactor=self.calfactor,
            fmin=self.fmin, dfmin=self.dfmin, fref=self.fref,
            psdstart=self.psdstart,
            zmax=self.zmax,
            psd_fn=self.psd_fn,
            approximant=self.approximant,
        )

def vts_from_masses_spins(
        m1s, m2s, s1zs, s2zs, thresh, analysis_time,
        calfactor=1.0,
        fmin=19.0, dfmin=0.0, fref=40.0, psdstart=20.0, zmax=1.5,
        psd_fn=None,
        approximant=None,
        processes=None,
        random_state=None,
    ):
    """Returns array of VTs corresponding to the given systems.

    Uses multiprocessing for more efficient computation.
    """
    import numpy
    import lalsimulation as ls

    if random_state is None:
        random_state = numpy.random.RandomState()

    if psd_fn is None:
        psd_fn = ls.SimNoisePSDaLIGOEarlyHighSensitivityP1200087

    vt_ms_tuple = VTFromMassSpinTuple(
        thresh, analysis_time, calfactor, psd_fn,
        fmin=fmin, dfmin=dfmin, fref=fref, psdstart=psdstart, zmax=zmax,
        approximant=approximant,
    )

    # Re-order the samples randomly, as a rudimentary form of load balancing.
    # If we assign the same thread only samples that are close in parameter
    # space, some threads will be overburdened with parameters that take a long
    # time to evaluate (i.e., low mass). Randomizing can't hurt, and will tend
    # to improve performance significantly.
    n = len(m1s)
    i = random_state.choice(n, n, replace=False)

    vts = numpy.empty(n, dtype=numpy.float64)

    pool = multi.Pool(processes=processes)

    try:
        vts[i] = pool.map(vt_ms_tuple, zip(m1s[i], m2s[i], s1zs[i], s2zs[i]))
    finally:
        pool.close()

    return vts


class RegularGridVTInterpolator(SensitiveVolume):
    def __init__(self, hdf5_file, scale_factor=None):
        import numpy
        from scipy.interpolate import RegularGridInterpolator

        mode = hdf5_file.attrs["mode"]
        # Backwards compatibility for tables made in Python 2
        if isinstance(mode, bytes):
            mode = mode.decode("utf-8")

        if mode == "zero spin":
            self.__zero_spin_init(hdf5_file)
        elif mode == "aligned spin":
            self.__aligned_spin_init(hdf5_file)
        else:
            raise NotImplementedError(
                "Unknown mode: {mode}".format(mode=mode)
            )

        self.scale_factor = scale_factor

        self.mode = mode
        self.m_min = hdf5_file.attrs["m_min"]
        self.m_max = hdf5_file.attrs["m_max"]
        self.M_max = hdf5_file.attrs["M_max"]


        VT_values = hdf5_file["VT"][()]
        if scale_factor is not None:
            VT_values *= scale_factor

        self.__log_VT = numpy.log(VT_values)

        self.__interpolator = RegularGridInterpolator(
            self.__points, self.__log_VT,
            method="linear", bounds_error=False, fill_value=-numpy.inf,
        )

        self.__max = VT_values.max()


    def __call__(self, observables):
        import numpy

        if self.mode == "zero spin":
            eval_points = self.__zero_spin_eval_points(*observables)
        elif self.mode == "aligned spin":
            eval_points = self.__aligned_spin_eval_points(*observables)
        else:
            raise NotImplementedError

        return numpy.exp(self.__interpolator(eval_points))



    def __zero_spin_init(self, hdf5_file):
        coord_system = CoordinateSystem(m1_source_coord, m2_source_coord)
        super().__init__(coord_system)

        logM = hdf5_file["logM"][()]
        qtilde = hdf5_file["qtilde"][()]

        self.__points = logM, qtilde


    def __aligned_spin_init(self, hdf5_file):
        coord_system = CoordinateSystem(
            m1_source_coord, m2_source_coord,
            chi1z_coord, chi2z_coord,
        )
        super().__init__(coord_system)

        logM = hdf5_file["logM"][()]
        qtilde = hdf5_file["qtilde"][()]
        s1z = hdf5_file["s1z"][()]
        s2z = hdf5_file["s2z"][()]

        self.__points = logM, qtilde, s1z, s2z


    def __zero_spin_eval_points(self, m1, m2):
        import numpy

        logM, qtilde = mass_grid_coords(m1, m2, self.m_min)
        return numpy.stack((logM, qtilde), axis=-1)


    def __aligned_spin_eval_points(self, m1, m2, s1z, s2z):
        import numpy

        logM, qtilde = mass_grid_coords(m1, m2, self.m_min)
        return numpy.stack((logM, qtilde, s1z, s2z), axis=-1)


    def max(self):
        return self.__max


class CorrectedVT(SensitiveVolume):
    def __init__(self, VT, coeffs, basis_fns):
        super().__init__(VT.coord_system)

        self.VT = VT
        self.coeffs = coeffs
        self.basis_fns = basis_fns
        self.mode = VT.mode

    def __call__(self, observables):
        correction_factor = sum(
            c * f(*observables)
            for c, f in zip(self.coeffs, self.basis_fns)
        )
        return self.VT(observables) * correction_factor


_correction_basis_scalar_zero_spin = [
    lambda m1, m2: 1.0,
]
_correction_basis_scalar_aligned_spin = [
    lambda m1, m2, a1z, a2z: 1.0,
]

_correction_basis_linear_zero_spin = (
    _correction_basis_scalar_zero_spin +
    [
        lambda m1, m2: m1,
        lambda m1, m2: m2,
    ]
)
_correction_basis_linear_aligned_spin = (
    _correction_basis_scalar_aligned_spin +
    [
        lambda m1, m2, a1z, a2z: m1,
        lambda m1, m2, a1z, a2z: m2,
    ]
)

_correction_basis_quadratic_zero_spin = (
    _correction_basis_linear_zero_spin +
    [
        lambda m1, m2: m1*m2,
        lambda m1, m2: m1**2,
        lambda m1, m2: m2**2,
    ]
)
_correction_basis_quadratic_aligned_spin = (
    _correction_basis_linear_aligned_spin +
    [
        lambda m1, m2, a1z, a2z: m1*m2,
        lambda m1, m2, a1z, a2z: m1**2,
        lambda m1, m2, a1z, a2z: m2**2,
    ]
)
_correction_basis_quintic_zero_spin = (
    _correction_basis_quadratic_zero_spin +
    [
        lambda m1, m2: m1**2 * m2,
        lambda m1, m2: m1 * m2**2,
        lambda m1, m2: m1**3,
        lambda m1, m2: m2**3,
    ]
)
_correction_basis_quintic_aligned_spin = (
    _correction_basis_quadratic_aligned_spin +
    [
        lambda m1, m2, a1z, a2z: m1**2 * m2,
        lambda m1, m2, a1z, a2z: m1 * m2**2,
        lambda m1, m2, a1z, a2z: m1**3,
        lambda m1, m2, a1z, a2z: m2**3,
    ]
)

def _Mc(m1, m2):
    return (m1*m2)**0.6 * (m1+m2)**-0.2

def _logMc(m1, m2):
    return 0.6*(numpy.log(m1) + numpy.log(m2)) - 0.2*numpy.log(m1+m2)

def _eta(m1, m2):
    return m1*m2 * (m1+m2)**-2.0

_correction_basis_linear_Mc_eta_zero_spin = (
    _correction_basis_scalar_zero_spin +
    [
        lambda m1, m2: _Mc(m1, m2),
        lambda m1, m2: _eta(m1, m2),
    ]
)
_correction_basis_linear_Mc_eta_aligned_spin = (
    _correction_basis_scalar_aligned_spin +
    [
        lambda m1, m2, a1z, a2z: _Mc(m1, m2),
        lambda m1, m2, a1z, a2z: _eta(m1, m2),
    ]
)

_correction_basis_quadratic_Mc_eta_zero_spin = (
    _correction_basis_linear_zero_spin +
    [
        lambda m1, m2: _Mc(m1, m2)*_eta(m1, m2),
        lambda m1, m2: _Mc(m1, m2)**2,
        lambda m1, m2: _eta(m1, m2)**2,
    ]
)
_correction_basis_quadratic_Mc_eta_aligned_spin = (
    _correction_basis_linear_aligned_spin +
    [
        lambda m1, m2, a1z, a2z: _Mc(m1, m2)*_eta(m1, m2),
        lambda m1, m2, a1z, a2z: _Mc(m1, m2)**2,
        lambda m1, m2, a1z, a2z: _eta(m1, m2)**2,
    ]
)

_correction_basis_linear_logMc_eta_zero_spin = (
    _correction_basis_scalar_zero_spin +
    [
        lambda m1, m2: _logMc(m1, m2),
        lambda m1, m2: _eta(m1, m2),
    ]
)
_correction_basis_linear_logMc_eta_aligned_spin = (
    _correction_basis_scalar_aligned_spin +
    [
        lambda m1, m2, a1z, a2z: _logMc(m1, m2),
        lambda m1, m2, a1z, a2z: _eta(m1, m2),
    ]
)

_correction_basis_quadratic_logMc_eta_zero_spin = (
    _correction_basis_linear_zero_spin +
    [
        lambda m1, m2: _logMc(m1, m2)*_eta(m1, m2),
        lambda m1, m2: _logMc(m1, m2)**2,
        lambda m1, m2: _eta(m1, m2)**2,
    ]
)
_correction_basis_quadratic_logMc_eta_aligned_spin = (
    _correction_basis_linear_aligned_spin +
    [
        lambda m1, m2, a1z, a2z: _logMc(m1, m2)*_eta(m1, m2),
        lambda m1, m2, a1z, a2z: _logMc(m1, m2)**2,
        lambda m1, m2, a1z, a2z: _eta(m1, m2)**2,
    ]
)


_correction_bases_zero_spin = {
    "scalar" : _correction_basis_scalar_zero_spin,
    "linear" : _correction_basis_linear_zero_spin,
    "quadratic" : _correction_basis_quadratic_zero_spin,
    "quintic" : _correction_basis_quintic_zero_spin,
    "linear_Mc_eta" : _correction_basis_linear_Mc_eta_zero_spin,
    "quadratic_Mc_eta" : _correction_basis_quadratic_Mc_eta_zero_spin,
    "linear_logMc_eta" : _correction_basis_linear_logMc_eta_zero_spin,
    "quadratic_logMc_eta" : _correction_basis_quadratic_logMc_eta_zero_spin,
}
_correction_bases_aligned_spin = {
    "scalar" : _correction_basis_scalar_aligned_spin,
    "linear" : _correction_basis_linear_aligned_spin,
    "quadratic" : _correction_basis_quadratic_aligned_spin,
    "quintic" : _correction_basis_quintic_aligned_spin,
    "linear_Mc_eta" : _correction_basis_linear_Mc_eta_aligned_spin,
    "quadratic_Mc_eta" : _correction_basis_quadratic_Mc_eta_aligned_spin,
    "linear_logMc_eta" : _correction_basis_linear_logMc_eta_aligned_spin,
    "quadratic_logMc_eta" : _correction_basis_quadratic_logMc_eta_aligned_spin,
}

correction_bases = {
    "zero spin" : _correction_bases_zero_spin,
    "aligned spin" : _correction_bases_aligned_spin,
}


class ReweightedInjectionPoissonMean(PoissonMean):
    def __init__(
            self,
            population: Population,
            injection_file: h5py.File,
            ifar_threshold: float=1.0,
        ) -> None:
        # m1m2z_coord_system = CoordinateSystem(
        #     m1_source_coord, m2_source_coord,
        #     z_coord,
        # )
        m1m2_coord_system = CoordinateSystem(
            m1_source_coord, m2_source_coord,
        )
        super().__init__(population.to_coords(m1m2_coord_system))

        self._injection_file = injection_file
        self._ifar_threshold = ifar_threshold
        self._setup()

    def _setup(self) -> None:
        self._n_generated = int(self._injection_file.attrs["total_generated"])
        self._n_generated_sq = self._n_generated*self._n_generated
        self._analysis_time = (
            self._injection_file.attrs["analysis_time_s"] / (365.25*24*3600)
        )
        self._surveyed_VT = self._injection_file.attrs["surveyed_vt_Gpc3_yr"]

        self._ifar_pycbc_full = (
            self._injection_file["injections/ifar_pycbc_full"][()]
        )
        self._ifar_pycbc_bbh = (
            self._injection_file["injections/ifar_pycbc_bbh"][()]
        )
        self._ifar_gstlal = (
            self._injection_file["injections/ifar_gstlal"][()]
        )
        self._found = (
            (self._ifar_pycbc_full > self.ifar_threshold) |
            (self._ifar_pycbc_bbh > self.ifar_threshold) |
            (self._ifar_gstlal > self.ifar_threshold)
        )
        self._n_found = numpy.count_nonzero(self._found)


        self._m1_samples = (
            self._injection_file["injections/mass1_source"][self._found]
        )
        self._m2_samples = (
            self._injection_file["injections/mass2_source"][self._found]
        )
#        self._z_samples = (
#            self._injection_file["injections/redshift"][self._found]
#        )
        self._observables = (
            self._m1_samples, self._m2_samples,
#            self._z_samples,
        )

        # Override injection PDF with redshift-free model
        from pop_models.astro_models.building_blocks.powerlaw import (
            ComponentMassPowerlawPopulation,
        )
        fiducial_pop = ComponentMassPowerlawPopulation(M_max=None)
        self._pm1m2z = fiducial_pop.pdf(
            self._observables,
            {"alpha" : 2.35, "beta" : 2.0, "m_min" : 2.0, "m_max" : 100.0},
        )

    @property
    def ifar_threshold(self) -> float:
        return self._ifar_threshold

    def __call__(
            self,
            parameters: Parameters, where: WhereType=True,
        ) -> Numeric:
        pm1m2z_desired = self.population.pdf(self._observables, parameters)

        weights = pm1m2z_desired / self._pm1m2z

        rate = self.population.normalization(parameters)

        return (
            rate * self._analysis_time * self._surveyed_VT / self._n_generated *
            numpy.sum(weights, axis=-1)
        )



class VTCombiner(object):
    @staticmethod
    def combine(
            output_filename: str,
            vt_info: typing.Iterable[typing.Tuple[str, dict]],
            force=False,
        ) -> None:
        import h5py
        import numpy

        hdf5_mode = "w" if force else "w-"

        first_file = True

        logM_grid = None
        qtilde_grid = None
        s1z_grid = None
        s2z_grid = None
        VT_grid = None
        VT_mode = None

        m_min = None
        m_max = None
        M_max = None

        for vt_filename, vt_kwargs in vt_info:
            with h5py.File(vt_filename, "r") as vt_file:
                # Load in everything if this is the first file.
                if first_file:
                    # Load the mass grid
                    logM_grid = vt_file["logM"][()]
                    qtilde_grid = vt_file["qtilde"][()]

                    # Determine what kind of spin support there is, and load the
                    # spin grid if there is one.
                    VT_mode = vt_file.attrs["mode"]
                    if VT_mode == "aligned spin":
                        s1z_grid = vt_file["s1z"][()]
                        s2z_grid = vt_file["s2z"][()]

                    # Load in the VT grid itself, and multiply by the scale
                    # factor if given.
                    VT_grid = (
                        vt_kwargs["scale_factor"] * vt_file["VT"][()]
                        if "scale_factor" in vt_kwargs
                        else vt_file["VT"][()]
                    )

                    # Load in the mass cutoffs.
                    m_min = vt_file.attrs["m_min"]
                    m_max = vt_file.attrs["m_max"]
                    M_max = vt_file.attrs["M_max"]

                    # Keep track of the fact that we've now parsed first file.
                    first_file = False
                # If this is not the first file, validate that everything is
                # compatible with the first file, and add the VT's if so.
                # Otherwise raise an exception.
                else:
                    # Keep track of what is invalid.
                    invalid = []

                    # Validate mass grids.
                    if numpy.any(logM_grid != vt_file["logM"][()]):
                        invalid.append("/logM")
                    if numpy.any(qtilde_grid != vt_file["qtilde"][()]):
                        invalid.append("/qtilde")

                    # Validate VT mode (zero or aligned spin).
                    if VT_mode != vt_file.attrs["mode"]:
                        invalid.append("attrs['mode']")

                    # If everything is consistently aligned spin, validate
                    # aligned spin grids.
                    both_aligned_spin = (
                        (VT_mode == "aligned spin") and
                        (vt_file.attrs["mode"] == "aligned spin")
                    )
                    if both_aligned_spin:
                        if numpy.any(s1z_grid != vt_file["s1z"][()]):
                            invalid.append("/s1z")
                        if numpy.any(s2z_grid != vt_file["s2z"][()]):
                            invalid.append("/s2z")

                    # Validate VT grids are compatible in shape, and if so,
                    # add in the contribution from this file.
                    if VT_grid.shape != vt_file["VT"].shape:
                        invalid.append("/VT")
                    else:
                        VT_grid += (
                            vt_kwargs["scale_factor"] * vt_file["VT"][()]
                            if "scale_factor" in vt_kwargs
                            else vt_file["VT"][()]
                        )

                    # Validate mass cutoffs.
                    if m_min != vt_file.attrs["m_min"]:
                        invalid.append("attrs['m_min']")
                    if m_max != vt_file.attrs["m_max"]:
                        invalid.append("attrs['m_max']")
                    if M_max != vt_file.attrs["M_max"]:
                        invalid.append("attrs['M_max']")

                    # Raise an exception if anything was invalid.
                    if len(invalid) != 0:
                        raise ValueError(
                            "The following fields in {} were incompatible with "
                            "the first VT file: {}"
                            .format(vt_filename, ", ".join(invalid))
                        )

        # Store combined result in output VT file
        with h5py.File(output_filename, hdf5_mode) as vt_file:
            # Store mass grid.
            vt_file.create_dataset("logM", data=logM_grid)
            vt_file.create_dataset("qtilde", data=qtilde_grid)

            # Store VT mode, and if it's "aligned spin", the spin grid too.
            vt_file.attrs["mode"] = VT_mode
            if VT_mode == "aligned spin":
                vt_file.create_dataset("s1z", data=s1z_grid)
                vt_file.create_dataset("s2z", data=s2z_grid)

            # Store the VT grid.
            vt_file.create_dataset("VT", data=VT_grid)

            # Store the mass cutoffs.
            vt_file.attrs["m_min"] = m_min
            vt_file.attrs["m_max"] = m_max
            vt_file.attrs["M_max"] = M_max

    @staticmethod
    def make_parser():
        import argparse

        parser = argparse.ArgumentParser()

        parser.add_argument(
            "output_file",
            help="File to store VT file to.",
        )

        parser.add_argument(
            "--VT-input",
            action="append", nargs="+", required=True,
            help="Include an additional VT file.  May use this keyword as many "
                 "times as needed.  First argument is VT filename, and "
                 "following arguments may be 'key=value' pairs.  Currently "
                 "accepted keys are: 'scale_factor'.",
        )

        parser.add_argument(
            "-f", "--force",
            action="store_true",
            help="Overwrite output file if it already exists.",
        )

        return parser


    @classmethod
    def cli(
            cls,
            raw_args: typing.Optional[typing.Sequence[str]]=None,
        ) -> typing.Optional[int]:
        import h5py
        import ast

        if raw_args is None:
            import sys
            raw_args = sys.argv[1:]

        cli_parser = cls.make_parser()
        cli_args = cli_parser.parse_args(raw_args)

        vt_info = []
        for vt_input in cli_args.VT_input:
            vt_filename = vt_input[0]

            kwargs = {}
            for kv in vt_input[1:]:
                k, v = kv.split("=")
                # Try to parse the value as something other than a string, but
                # downgrade to a string if not possible.
                try:
                    v_parsed = ast.literal_eval(v)
                except ValueError:
                    v_parsed = v

                kwargs[k] = v_parsed

            vt_info.append((vt_filename, kwargs))

        cls.combine(cli_args.output_file, vt_info, force=cli_args.force)


def mass_grid_coords(m1, m2, m_min, eps=1e-8):
    import numpy

    m1, m2 = numpy.asarray(m1), numpy.asarray(m2)

    M = m1 + m2

    logM = numpy.log(M)

    # Note: when computing qtilde, there is a coordinate singularity at
    # m1 = m2 = m_min.  While normally, equal mass corresponds to qtilde = 1,
    # here the most extreme mass ratio is also equal mass, so we should have
    # qtilde = 0.  Here we break the singularity by just forcing qtilde = 1 in
    # these cases.  We also allow for some numerical wiggle room beyond that
    # point, by also including any points within some small epsilon of the
    # singularity.  VT does not change on a fast enough scale for this to make
    # any measurable difference.
    i_good = M > 2*m_min + eps

    m1, m2, M = m1[i_good], m2[i_good], M[i_good]

    qtilde = numpy.ones_like(logM)
    qtilde[i_good] = M * (m2 - m_min) / (m1 * (M - 2*m_min))

    return logM, qtilde


def mass_grid_coords_inverse(logM, qtilde, m_min):
    import numpy

    M = numpy.exp(logM)

    m1 = M * (M - m_min) / (M * (1+qtilde) - 2*m_min*qtilde)
    m2 = (
        (M * (M*qtilde + m_min*(1-2*qtilde)))
        /
        (M*(1+qtilde) - 2*m_min*qtilde)
    )

    return m1, m2


# def parse_psd_fn(psd_name: str="SimNoisePSDaLIGOEarlyHighSensitivityP1200087"):
#     import lalsimulation

#     # Try to load from LALSimulation, raising an exception if it's missing.
#     try:
#         lal_psd_fn = getattr(lalsimulation, psd_name)
#     except AttributeError as err:
#         raise ImportError(
#             "PSD '{}' not found in lalsimulation.".format(psd_name)
#         )

#     # Process the PSD function's signature.  Should be one of two cases:
#     # 1) 'psd_name(double f) -> double'
#     # 2) 'psd_name(REAL8FrequencySeries psd, double flow) -> int'
#     # For case 1, we construct a REAL8FrequencySeries, and loop over the
#     # frequencies one-by-one when evaluating it, starting from psdstart.
#     # For case 2, we construct a REAL8FrequencySeries, and pass it to the PSD
#     # function from LALSimulation, which then writes to the series in-place.

#     # First split on the ' -> ' string which separates the function and its
#     # inputs from its output.
#     lhs, return_type = lal_psd_fn.__doc__.split(" -> ")
#     #

class VTPlotter(object):
    @staticmethod
    def make_parser():
        import argparse

        # Parent parser for shared arguments.
        parent_settings_parser = argparse.ArgumentParser()
        parent_settings_parser.add_argument(
            "output",
            help="Name of file to store plot in.",
        )
        parent_settings_parser.add_argument(
            "--variable-mode", metavar="MODE",
            choices=["m1_m2_zero_spin", "equal_mass_equal_spin"],
            default="m1_m2_zero_spin",
            help="What to use for the independent variables.",
        )
        parent_settings_parser.add_argument(
            "--n-points", metavar="N",
            type=int, default=50,
            help="Number of plotting points along each dimension.",
        )
        parent_settings_parser.add_argument(
            "--n-levels", metavar="N",
            type=int, default=100,
            help="Number of levels to use in contour plots.",
        )
        parent_settings_parser.add_argument(
            "--colormap", metavar="CMAP",
            default="viridis",
            help="Colormap used in plotting.",
        )
        parent_settings_parser.add_argument(
            "--fig-size", metavar=("WIDTH_INCH", "HEIGHT_INCH"),
            type=int, nargs=2,
            help="Dimensions of the figure.",
        )
        parent_settings_parser.add_argument(
            "--log-scale",
            action="store_true",
            help="Use a logarithmic scale for VT.",
        )
        parent_settings_parser.add_argument(
            "--mpl-backend",
            default="Agg",
            help="Backend to use for matplotlib.",
        )

        parser = argparse.ArgumentParser()

        subparsers = parser.add_subparsers(dest="command")

        ## Plot a single VT table ##
        single_parser = subparsers.add_parser(
            "single",
            parents=[parent_settings_parser], add_help=False,
        )
        single_parser.add_argument(
            "VT_file",
            help="VT file to plot.",
        )

        ## Plot a comparison between two VT tables ##
        comp_parser = subparsers.add_parser(
            "compare",
            parents=[parent_settings_parser], add_help=False,
        )
        comp_parser.add_argument(
            "VT_file_base",
            help="Base VT file to compare against.",
        )
        comp_parser.add_argument(
            "VT_file_alt",
            help="Alternate VT file to compare.",
        )

        comp_parser.add_argument(
            "--comparison-mode", metavar="MODE",
            choices=["diff", "ratio", "rel_err"],
            default="diff",
            help="Which type of comparison to do.",
        )

        return parser

    @staticmethod
    def cli(raw_args=None) -> int:
        import sys
        if raw_args is None:
            raw_args = sys.argv[1:]

        cli_parser = VTPlotter.make_parser()
        cli_args = cli_parser.parse_args(raw_args)

        import matplotlib as mpl
        mpl.use(cli_args.mpl_backend)
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(figsize=cli_args.fig_size)

        if cli_args.command == "single":
            VTPlotter.plot_single(
                ax, cli_args.VT_file,
                variable_mode=cli_args.variable_mode,
                log_scale=cli_args.log_scale,
                n_points=cli_args.n_points, n_levels=cli_args.n_levels,
                cmap=cli_args.colormap,
            )
            fig.tight_layout()
            fig.savefig(cli_args.output)

        elif cli_args.command == "compare":
            raise NotImplementedError()

        else:
            raise RuntimeError("Reached inaccessible code.")

        return 0

    @staticmethod
    def plot_single(
            ax, VT_filename: str,
            variable_mode: str="m1_m2_zero_spin",
            log_scale: bool=False,
            n_points: int=50, n_levels: int=100,
            cmap: str="viridis",
        ):
        from numpy import (
            isfinite, linspace, log10, meshgrid, nan, newaxis,
            empty, zeros_like,
        )
        import h5py

        # Load in VT file.
        with h5py.File(VT_filename, "r") as vt_file:
            VT = RegularGridVTInterpolator(vt_file)

        if variable_mode == "m1_m2_zero_spin":
            m1_grid = linspace(VT.m_min, VT.m_max, n_points)
            m2_grid = linspace(VT.m_min, min(0.5*VT.M_max, VT.m_max), n_points)

            m1_mesh, m2_mesh = meshgrid(m1_grid, m2_grid, indexing="xy")

            if VT.mode == "zero spin":
                observables = (m1_mesh, m2_mesh)
            elif VT.mode == "aligned spin":
                spin = zeros_like(m1_mesh)
                observables = (m1_mesh, m2_mesh, spin, spin)

            X, Y = m1_mesh, m2_mesh
            VT_grid = VT(observables)
            VT_grid[m1_mesh < m2_mesh] = nan

            ax.set_xlabel(r"$m_1 \, [M_\odot]$")
            ax.set_ylabel(r"$m_2 \, [M_\odot]$")

            if log_scale:
                Z = log10(VT_grid)
                Z_label = (
                    r"$\log_{10}[ VT / \mathrm{Gpc}^{-3} \, \mathrm{yr}^{-1} ]$"
                )
            else:
                Z = VT_grid
                Z_label = r"$VT / \mathrm{Gpc}^{-3} \, \mathrm{yr}^{-1}$"

            ctr = ax.contourf(
                m1_grid, m2_grid, Z, n_levels,
                cmap=cmap,
            )

            cbar = ax.figure.colorbar(ctr, ax=ax)
            cbar.set_label(Z_label)


        elif variable_mode == "equal_mass_equal_spin":
            log10_masses = linspace(log10(VT.m_min), log10(VT.m_max), n_points)
            masses = 10.0**log10_masses
            spins = linspace(-1.0, +1.0, n_points)

            VT_0 = empty(n_points, dtype=float)
            VT_s = empty((n_points, n_points), dtype=float)

            for i, m in enumerate(masses):
                VT_0[i] = VT((m, m, 0.0, 0.0))

                for j, s in enumerate(spins):
                    VT_s[i,j] = VT((m, m, s, s))

            VT_ratio = VT_s / VT_0[...,newaxis]
            VT_ratio[~isfinite(VT_ratio)] = nan

            if log_scale:
                Z = log10(VT_ratio)
                Z_label = (
                    r"$\log_{10}[ VT / VT(\chi=0) ]$"
                )
            else:
                Z = VT_ratio
                Z_label = r"$VT / VT(\chi=0)$"


            ctr = ax.contourf(
                masses, spins, Z, n_levels,
                cmap=cmap,
            )

            cbar = ax.figure.colorbar(ctr, ax=ax)
            cbar.set_label(Z_label)


        else:
            raise KeyError("Unknown variable mode: {}".format(variable_mode))


def make_parser():
    """
    Parse command line arguments when run in CLI mode.
    """
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument("m_min", type=float)
    parser.add_argument("m_max", type=float)
    parser.add_argument("duration", type=float,
                        help="Duration of analyzed data in days")
    parser.add_argument("output")

    parser.add_argument("--log-total-mass-samples", type=int, default=50)
    parser.add_argument("--mass-ratio-samples", type=int, default=15)
    parser.add_argument("--spin-samples", type=int, default=31)

    parser.add_argument("--threshold", type=float, default=8.0)
    parser.add_argument("--calfactor", type=float, default=1.0)
    parser.add_argument("--psd-fn",
        default="SimNoisePSDaLIGOEarlyHighSensitivityP1200087"
    )

    parser.add_argument("--days-per-year", type=float, default=365.25)

    parser.add_argument("--f-min", type=float, default=19.0)
    parser.add_argument("--f-ref", type=float, default=40.0)
    parser.add_argument("--psd-start", type=float, default=20.0)
    parser.add_argument("--z-max", type=float, default=1.5)

    dfmin_group = parser.add_mutually_exclusive_group()
    dfmin_group.add_argument("--df-min", type=float)
    dfmin_group.add_argument("--df-min-inv", type=float)
    dfmin_group.add_argument("--df-min-pow-two", type=int)

    parser.add_argument(
        "--zero-spin",
        action="store_true",
        help="Assume zero spin.",
    )


    parser.add_argument(
        "--total-mass-max",
        type=float,
        help="Stop computing VT's past this value of m1+m2.",
    )

    parser.add_argument(
        "--approximant",
        default="IMRPhenomPv2",
        help="Waveform approximant to use when calculating SNR.",
    )

    parser.add_argument(
        "--n-threads",
        type=int,
        help="Number of threads to use to compute VT. "
             "Uses all available threads if not specified.",
    )
    parser.add_argument(
        "--seed",
        type=int,
        help="Seed for random number generation.",
    )

    parser.add_argument(
        "--verbose",
        action="store_true",
        help="Produce verbose output.",
    )

    return parser


def _main(raw_args=None):
    import sys
    import numpy
    import h5py
    import lalsimulation

    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    cli_parser = make_parser()
    args = cli_parser.parse_args(raw_args)

    random_state = numpy.random.RandomState(args.seed)

    # Load PSD function from lalsimulation.
    try:
        psd_fn = getattr(lalsimulation, args.psd_fn)
    except AttributeError as err:
        raise ImportError(
            "PSD '{}' not found in lalsimulation.".format(args.psd_fn)
        )

    duration = args.duration / args.days_per_year

    # Set minimum frequency bin width
    if args.df_min_pow_two is not None:
        dfmin = 2.0 ** args.df_min_pow_two
    elif args.df_min is not None:
        pow_two = numpy.log2(args.df_min)
        if abs(int(pow_two) - pow_two) > 1e-8:
            raise ValueError("--df-min must be a power of two")
        dfmin = args.df_min
    elif args.df_min_inv is not None:
        pow_two = numpy.log2(args.df_min_inv)
        if abs(int(pow_two) - pow_two) > 1e-8:
            raise ValueError("--df-min-inv must be a power of two")
        dfmin = 1.0 / args.df_min_inv
    else:
        dfmin = 0.0

    M_min = 2*args.m_min
    M_max = 2*args.m_max if args.total_mass_max is None else args.total_mass_max

    log_M_min, log_M_max = numpy.log([M_min, M_max])

    approximant = getattr(lalsimulation, args.approximant)

    with h5py.File(args.output, "w-") as f:
        logMs = numpy.linspace(
            log_M_min, log_M_max,
            args.log_total_mass_samples,
        )
        qtildes = numpy.linspace(0.0, 1.0, args.mass_ratio_samples)

        if args.zero_spin:
            LOGM, QTILDE = numpy.meshgrid(logMs, qtildes, indexing="ij")
            M1, M2 = mass_grid_coords_inverse(LOGM, QTILDE, args.m_min)
            s1z = numpy.zeros_like(M1).ravel()
            s2z = s1z
        else:
            spins = numpy.linspace(-1.0, 1.0, args.spin_samples)
            LOGM, QTILDE, S1Z, S2Z = (
                numpy.meshgrid(logMs, qtildes, spins, spins, indexing="ij")
            )
            M1, M2 = mass_grid_coords_inverse(LOGM, QTILDE, args.m_min)
            s1z, s2z = S1Z.ravel(), S2Z.ravel()
            m1, m2 = M1.ravel(), M2.ravel()

        m1, m2 = M1.ravel(), M2.ravel()

        VTs = vts_from_masses_spins(
            m1, m2, s1z, s2z,
            args.threshold, duration,
            calfactor=args.calfactor,
            fmin=args.f_min, dfmin=dfmin, fref=args.f_ref,
            psdstart=args.psd_start,
            zmax=args.z_max,
            psd_fn=psd_fn, approximant=approximant,
            processes=args.n_threads,
            random_state=random_state,
        ).reshape(M1.shape)

        f.create_dataset("logM", data=logMs)
        f.create_dataset("qtilde", data=qtildes)

        if not args.zero_spin:
            f.create_dataset("s1z", data=spins)
            f.create_dataset("s2z", data=spins)

            f.attrs["mode"] = "aligned spin"
        else:
            f.attrs["mode"] = "zero spin"

        f.create_dataset("VT", data=VTs)

        f.attrs["m_min"] = args.m_min
        f.attrs["m_max"] = args.m_max
        f.attrs["M_max"] = M_max
