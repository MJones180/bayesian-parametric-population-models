from pop_models.population import (
    Population, ConditionalPopulation, TruncatedPopulation,
)
from pop_models.coordinate import CoordinateSystem, CoordinateTransforms
from pop_models.credible_regions import CredibleRegions1D
from pop_models.types import Numeric, WhereType, Observables, Parameters
from pop_models.utils import debug_verbose
from pop_models.utils.collections import LRUDict

from pop_models.astro_models.coordinates import (
    m1_source_coord, m2_source_coord,
    Lambda1_coord, Lambda2_coord,
    transformations,
)

import numpy
import typing

Lambda1_Lambda2_coord_system = CoordinateSystem(Lambda1_coord, Lambda2_coord)


# Reference pressure.
# Note that 1.21e+44 is 1 newton in geometrized units
pressure_reference = 4.43784199e-13 * 1.2102954584096219e+44

spectral_eos_parameter_examples = {
    "AP4"  : [+0.8651, +0.1548, -0.0151, -0.0002],
    "H4"   : [+1.0526, +0.1695, -0.1200, +0.0150],
    "MPA1" : [+1.0215, +0.1653, -0.0235, -0.0004],
    "SLY"  : [+0.9865, +0.1110, -0.0301, +0.0022],
    "WFF1" : [+0.6785, +0.2626, -0.0215, -0.0008],
}
spectral_eos_parameter_examples = {
    name : {
        "gamma{}".format(i+1) : indices[i]
        for i in range(4)
    }
    for name, indices in spectral_eos_parameter_examples.items()
}


def _determine_eos_coords(parameters: Parameters) -> str:
    if all("gamma{i}".format(i=i) in parameters for i in range(1,5)):
        return "spectral"
    elif all("legendre{i}".format(i=i) in parameters for i in range(1,5)):
        return "spectral_legendre"
    else:
        raise ValueError("Does not have a known EOS coordinate system.")


def polytrope_eos(eos_parameters: tuple) -> typing.Any:
    import lalsimulation

    logP1, gamma1, gamma2, gamma3 = eos_parameters

    return lalsimulation.SimNeutronStarEOS4ParameterPiecewisePolytrope(
        logP1,
        gamma1, gamma2, gamma3,
    )

_EOS_cache = LRUDict(maxsize=2048)
def spectral_eos(eos_parameters: tuple) -> typing.Any:
    import lalsimulation

    # Return cached EOS if available.
    if eos_parameters in _EOS_cache:
        return _EOS_cache[eos_parameters]

    # Compute EOS, and cache for re-use.
    gamma1, gamma2, gamma3, gamma4 = eos_parameters
    eos = lalsimulation.SimNeutronStarEOS4ParameterSpectralDecomposition(
        gamma1, gamma2, gamma3, gamma4,
    )
    _EOS_cache[eos_parameters] = eos

    return eos


def spectral_eos_adiabatic_index(
        x: Numeric, spectral_parameters: tuple,
    ) -> Numeric:
    x_sq = x * x
    x_cu = x_sq * x

    gamma1, gamma2, gamma3, gamma4 = spectral_parameters

    log_gamma = (
        gamma1 +
        gamma2 * x +
        gamma3 * x_sq +
        gamma4 * x_cu
    )

    return numpy.exp(log_gamma)


def lambda_from_eos_and_mass(eos_fam: typing.Any, mass: Numeric) -> Numeric:
    import lal
    import lalsimulation

    mass_SI = mass * lal.MSUN_SI

    k2 = lalsimulation.SimNeutronStarLoveNumberK2(mass_SI, eos_fam)
    r = lalsimulation.SimNeutronStarRadius(mass_SI, eos_fam)

    m = mass_SI * lal.G_SI / lal.C_SI**2
    lam = 2.0 / (3.0*lal.G_SI) * k2 * r**5

    return lal.G_SI * lam * (1/m)**5


def lambda_tilde_from_masses_and_lambdas(m1, m2, lambda1, lambda2):
    a = m1 + 12.0*m2
    b = m2 + 12.0*m1

    numer = a * m1**4 * lambda1 + b * m2**4 * lambda2
    denom = (m1 + m2)**5

    return (16.0/13.0) * numer / denom

def delta_lambda_tilde_from_masses_and_lambdas(m1, m2, lambda1, lambda2):
    a = m1 + 12.0*m2
    b = m2 + 12.0*m1

    numer = a * m1**4 * lambda1 + b * m2**4 * lambda2
    denom = (m1 + m2)**5

    return (16.0/13.0) * numer / denom



def radius_from_eos_and_mass(eos_fam: typing.Any, mass: Numeric) -> Numeric:
    import lal
    import lalsimulation

    mass_SI = numpy.asarray(mass * lal.MSUN_SI)
    r_SI = numpy.empty_like(mass_SI)

    for idx, m_SI in numpy.ndenumerate(mass_SI):
        r_SI[idx] = lalsimulation.SimNeutronStarRadius(m_SI, eos_fam)

    return r_SI


_x_min = 0.0
_x_max = 12.3081
_x_grid = numpy.linspace(_x_min, _x_max, 500)


def is_valid_adiabatic_index(spectral_parameters: tuple):
    # Confirm that the adiabatic index is within a tolerable range.
    Gamma = spectral_eos_adiabatic_index(_x_grid, spectral_parameters)
    return (0.6 < Gamma).all() and (Gamma < 4.5).all()


def has_enough_points(eos: typing.Any) -> bool:
    import lalsimulation

    min_points = 8

    logpmin = 75.5
    logpmax = numpy.log(lalsimulation.SimNeutronStarEOSMaxPressure(eos))

    dlogp = (logpmax - logpmin) / 100

    m_prev = 0.0

    for i in range(min_points):
        p = numpy.exp(logpmin + i*dlogp)

        r, m, k = lalsimulation.SimNeutronStarTOVODEIntegrate(p, eos)

        if m <= m_prev:
            return False

        m_prev = m

    return True


def eos_max_sound_speed(eos: typing.Any, eos_fam: typing.Any) -> float:
    import lalsimulation

    # Maximum allowed mass
    m_max_kg = lalsimulation.SimNeutronStarMaximumMass(eos_fam)
    # Central pressure
    p_max = lalsimulation.SimNeutronStarCentralPressure(m_max_kg, eos_fam)
    # Pseudo-enthalpy at the core.
    h_max = lalsimulation.SimNeutronStarEOSPseudoEnthalpyOfPressure(p_max, eos)
    # Maximum sound speed should occur at the max pseudo-enthalpy for a typical
    # TOV neutron star.
    return lalsimulation.SimNeutronStarEOSSpeedOfSoundGeometerized(h_max, eos)


def is_causal_eos(eos: typing.Any, eos_fam: typing.Any) -> bool:
    c_max = eos_max_sound_speed(eos, eos_fam)

    # Confirm that the sound speed is less than speed of light, with an added
    # buffer (10%) to account for imperfect modeling.
    c_buffer = 0.1
    return c_max < (1.0 + c_buffer)


def eos_mass_range(eos_fam: typing.Any) -> typing.Tuple[float,float]:
    import lal
    import lalsimulation

    m_min = lalsimulation.SimNeutronStarFamMinimumMass(eos_fam)
    m_max = lalsimulation.SimNeutronStarMaximumMass(eos_fam)

    return m_min / lal.MSUN_SI, m_max / lal.MSUN_SI


def spectral_eos_rho_of_p(
        pressure: numpy.ndarray, spectral_parameters: tuple,
    ) -> numpy.ndarray:
    import lalsimulation

    gamma1, gamma2, gamma3, gamma4 = spectral_parameters

    pressure_shape = pressure.shape
    params_shape = gamma1.shape

    rho = numpy.empty(params_shape+pressure_shape, dtype=pressure.dtype)

    for i in numpy.ndindex(*params_shape):
        params_at_i = gamma1[i], gamma2[i], gamma3[i], gamma4[i]

        eos = spectral_eos(params_at_i)

        p_max = lalsimulation.SimNeutronStarEOSMaxPressure(eos)

        for j, p in numpy.ndenumerate(pressure):
            if p > p_max:
                rho[i+j] = 0.0
                continue

            h = lalsimulation.SimNeutronStarEOSPseudoEnthalpyOfPressure(p, eos)
            rho[i+j] = (
                lalsimulation.SimNeutronStarEOSRestMassDensityOfPseudoEnthalpy(
                    h, eos,
                )
            )

    return rho


def spectral_eos_p_of_rho(
        density: numpy.ndarray, spectral_parameters: tuple,
        p_min: float=5e31, p_max: float=6e35,
        n_interp: int=128,
    ) -> numpy.ndarray:
    import lalsimulation

    gamma1, gamma2, gamma3, gamma4 = spectral_parameters

    density_shape = density.shape
    params_shape = gamma1.shape

    log10_density = numpy.log10(density)

    p_out = numpy.empty(params_shape+density_shape, dtype=density.dtype)

    for i in numpy.ndindex(*params_shape):
        params_at_i = gamma1[i], gamma2[i], gamma3[i], gamma4[i]

        eos = spectral_eos(params_at_i)

        # Set up independent variable grid.
        p_max_i = min(p_max, lalsimulation.SimNeutronStarEOSMaxPressure(eos))
        log10_p_grid = numpy.linspace(*numpy.log10([p_min, p_max_i]), n_interp)
        p_grid = numpy.power(10.0, log10_p_grid)
        rho_grid = numpy.empty_like(p_grid)

        # Compute dependent variable everywhere on the grid.
        for j, p in numpy.ndenumerate(p_grid):
            h = lalsimulation.SimNeutronStarEOSPseudoEnthalpyOfPressure(p, eos)
            rho_grid[j] = (
                lalsimulation.SimNeutronStarEOSRestMassDensityOfPseudoEnthalpy(
                    h, eos,
                )
            )

        # Interpolate in log-space.  Any extrapolated values are fixed to a
        # pressure of zero.
        log10_rho_grid = numpy.log10(rho_grid)
        log10_p_out = numpy.interp(
            log10_density, log10_rho_grid, log10_p_grid,
            left=numpy.NINF, right=numpy.NINF,
        )
        p_out[i] = numpy.power(10.0, log10_p_out)

    return p_out


def gamma2legendre1(gamma1, gamma3, xmax=_x_max):
    return gamma1 + (1.0/3.0) * xmax*xmax * gamma3

def gamma2legendre2(gamma2, gamma4, xmax=_x_max):
    return xmax*gamma2 + 0.6 * xmax**3 * gamma4

def gamma2legendre3(gamma3, xmax=_x_max):
    return (2.0/3.0) * xmax*xmax * gamma3

def gamma2legendre4(gamma4, xmax=_x_max):
    return 0.4 * xmax**3 * gamma4

def gamma2legendre(gamma1, gamma2, gamma3, gamma4, xmax=_x_max):
    return (
        gamma2legendre1(gamma1, gamma3, xmax=xmax),
        gamma2legendre2(gamma2, gamma4, xmax=xmax),
        gamma2legendre3(gamma3, xmax=xmax),
        gamma2legendre4(gamma4, xmax=xmax),
    )

def legendre2gamma1(legendre1, legendre3):
    return legendre1 - 0.5*legendre3

def legendre2gamma2(legendre2, legendre4, xmax=_x_max):
    return (legendre2 - 1.5*legendre4) / xmax

def legendre2gamma3(legendre3, xmax=_x_max):
    return 1.5 * xmax**-2.0 * legendre3

def legendre2gamma4(legendre4, xmax=_x_max):
    return 2.5 * xmax**-3.0 * legendre4

def legendre2gamma(legendre1, legendre2, legendre3, legendre4, xmax=_x_max):
    return (
        legendre2gamma1(legendre1, legendre3),
        legendre2gamma2(legendre2, legendre4, xmax=xmax),
        legendre2gamma3(legendre3, xmax=xmax),
        legendre2gamma4(legendre4, xmax=xmax),
    )

eos_coordinate_options = [
    "spectral", "spectral_legendre",
]
def eos_coords_to_spectral(eos_parameters, eos_coordinates: str):
    if eos_coordinates == "spectral":
        return eos_parameters
    elif eos_coordinates == "spectral_legendre":
        return legendre2gamma(*eos_parameters)
    else:
        raise KeyError(
            "EOS in unsupported coordinate system: {}"
            .format(eos_coordinates)
        )
def eos_coords_from_spectral(eos_parameters, eos_coordinates: str):
    if eos_coordinates == "spectral":
        return eos_parameters
    elif eos_coordinates == "spectral_legendre":
        return gamma2legendre(*eos_parameters)
    else:
        raise KeyError(
            "EOS in unsupported coordinate system: {}"
            .format(eos_coordinates)
        )

def get_eos_parameters(parameters: Parameters, eos_coordinates: str):
    if eos_coordinates == "spectral":
        eos_parameters = (
            parameters["gamma1"],
            parameters["gamma2"],
            parameters["gamma3"],
            parameters["gamma4"],
        )
    elif eos_coordinates == "spectral_legendre":
        eos_parameters = (
            parameters["legendre1"],
            parameters["legendre2"],
            parameters["legendre3"],
            parameters["legendre4"],
        )
    else:
        raise KeyError(
            "EOS in unsupported coordinate system: {}"
            .format(eos_coordinates)
        )

    return eos_parameters


class EOSPopulation(ConditionalPopulation):
    def __init__(
            self,
            base_population: Population,
            eos_fn: typing.Callable[[Parameters], typing.Any],
            eos_coordinates="spectral",
        ):
        # Create wrapper around base population, which truncates it based on the
        # EOS's valid mass range.
        pop_y = MarginalizedEOSPopulation(
            base_population,
            eos_fn, eos_coordinates,
        )
        # Determine constituent coordinate systems.
        coord_system_x = CoordinateSystem(Lambda1_coord, Lambda2_coord)
        coord_system_y = CoordinateSystem(m1_source_coord, m2_source_coord)
        # Determine parameter names relevant to the conditional d.o.f.
        param_names_x = tuple("gamma"+str(i+1) for i in range(4))

        super().__init__(
            pop_y,
            coord_system_x, coord_system_y,
            param_names_x,
        )

        self.eos_fn = eos_fn

        # Check that eos coordinates is one supported, and then save it.
        if eos_coordinates not in eos_coordinate_options:
            raise KeyError(
                "EOS coordinates must be one of: {}; Got: {}"
                .format(eos_coordinate_options, eos_coordinates)
            )
        self.eos_coordinates = eos_coordinates

    def eos_coords_to_spectral(self, parameters: Parameters):
        return eos_coords_to_spectral(
            get_eos_parameters(parameters, self.eos_coordinates),
            self.eos_coordinates,
        )

    def cond_rvs(
            self,
            observables_y: Observables,
            parameters: Parameters,
            where: WhereType=True,
            random_state: typing.Optional[numpy.random.RandomState]=None,
        ) -> Observables:
        import lalsimulation

        # Determine relevant shapes
        params_shape = next(iter(parameters.values())).shape
        obs_shape = observables_y[0].shape
        obs_only_shape = obs_shape[len(params_shape):]

        # Ensure we can use ``where`` as an array.
        where_arr = numpy.broadcast_to(where, params_shape)

        # Extract m1,m2 from the input observables
        m1, m2 = observables_y

        # Initialize Lambda1, Lambda2 arrays for output.
        Lambda1 = numpy.zeros_like(observables_y[0])
        Lambda2 = numpy.zeros_like(observables_y[0])

        for i in numpy.ndindex(*params_shape):
            # Leave point un-initialized if exluded by `where`.
            if not where_arr[i]:
                continue

            parameters_at_i = {
                param_name : values[i]
                for param_name, values in parameters.items()
            }
            spectral_parameters_at_i = self.eos_coords_to_spectral(
                parameters_at_i
            )

            debug_verbose(
                "cond_rvs: About to create EOS for",
                spectral_parameters_at_i,
                mode="eos_segfault_debug", flush=True,
            )
            eos = self.eos_fn(spectral_parameters_at_i)
            eos_fam = lalsimulation.CreateSimNeutronStarFamily(eos)
            debug_verbose(
                "cond_rvs: Successfully created EOS for",
                spectral_parameters_at_i,
                mode="eos_segfault_debug", flush=True,
            )

            # Compute the tidal deformabilities associated with each sample.
            for j in numpy.ndindex(*obs_only_shape):
                ij = i + j
                Lambda1[ij] = lambda_from_eos_and_mass(eos_fam, m1[ij])
                Lambda2[ij] = lambda_from_eos_and_mass(eos_fam, m2[ij])

        return Lambda1, Lambda2




    # def rvs(
    #         self,
    #         n_samples: int,
    #         parameters: Parameters,
    #         where: WhereType=True,
    #         random_state: numpy.random.RandomState=None,
    #         **kwargs
    #     ) -> Observables:
    #     import lalsimulation

    #     params_shape = next(iter(parameters.values())).shape
    #     where = numpy.broadcast_to(where, params_shape)

    #     # Initial draws of observables from the base population.
    #     # Need to rejection sample according to mass limits of E.O.S., so these
    #     # will be partially overwritten.
    #     base_samples = self.base_population.rvs(
    #         n_samples, parameters,
    #         where=where, random_state=random_state,
    #         **kwargs
    #     )

    #     n_observables = len(base_samples)


    #     # Initialize Lambda1, Lambda2 arrays for output.
    #     Lambda1 = numpy.zeros_like(base_samples[0])
    #     Lambda2 = numpy.zeros_like(base_samples[0])

    #     for i in numpy.ndindex(*params_shape):
    #         # Leave point un-initialized if exluded by `where`.
    #         if not where[i]:
    #             continue

    #         parameters_at_i = {
    #             param_name : values[i]
    #             for param_name, values in parameters.items()
    #         }
    #         spectral_parameters_at_i = self.eos_coords_to_spectral(
    #             parameters_at_i
    #         )

    #         eos = self.eos_fn(spectral_parameters_at_i)
    #         eos_fam = lalsimulation.CreateSimNeutronStarFamily(eos)

    #         eos_m_min, eos_m_max = eos_mass_range(eos_fam)

    #         while True:
    #             ## TODO: Avoid validating samples that have already been
    #             ## validated.  Should just update `i_invalid` in-place.

    #             # Extract subset of samples that correspond to the parameters
    #             # at index `i`.  Also convert those samples to m1, m2.
    #             base_samples_at_i = [s[i] for s in base_samples]
    #             m1_at_i, m2_at_i = self.get_m1_m2(base_samples_at_i)

    #             # Determine which m1, m2 samples violate the mass-limits for
    #             # the EOS specified by index `i`, and count how many there are.
    #             i_invalid = (m2_at_i < eos_m_min) | (m1_at_i > eos_m_max)
    #             n_rejected = numpy.count_nonzero(i_invalid)

    #             # If no samples violate the mass limits, we've succeeded and
    #             # can move on to the next EOS.
    #             if n_rejected == 0:
    #                 break

    #             # Draw new samples to replace all of the rejected samples.  Some
    #             # of these may be rejected still, so they will be inspected on
    #             # the next iteration of this loop.
    #             new_samples_at_i = self.base_population.rvs(
    #                 n_rejected, parameters_at_i,
    #                 where=True, random_state=random_state,
    #                 **kwargs
    #             )

    #             # Overwrite rejected base samples with new samples.
    #             for k in range(n_observables):
    #                 base_samples[k][i][i_invalid] = new_samples_at_i[k]

    #         # Now that all samples have been drawn, determine the values of
    #         # m1 and m2 in the final sample set.
    #         m1_at_i, m2_at_i = self.get_m1_m2(base_samples_at_i)

    #         # Now that we only have parameters that are compatible with this
    #         # EOS, compute the tidal deformabilities associated with each
    #         # sample.
    #         for j in range(n_samples):
    #             ij = i + (j,)
    #             Lambda1[ij] = lambda_from_eos_and_mass(eos_fam, m1_at_i[j])
    #             Lambda2[ij] = lambda_from_eos_and_mass(eos_fam, m2_at_i[j])

    #     ## TODO: Reorder to match coordinate system's ordering.
    #     return base_samples + (Lambda1, Lambda2)


    # def normalization(
    #         self,
    #         parameters: Parameters,
    #         where: WhereType=True,
    #         **kwargs
    #     ) -> Numeric:
    #     return self.base_population.normalization(
    #         parameters, where=where, **kwargs
    #     )


class MarginalizedEOSPopulation(TruncatedPopulation):
    def __init__(
            self,
            base_population: Population,
            eos_fn: typing.Callable[[Parameters], typing.Any],
            eos_coordinates="spectral",
        ) -> None:
        param_names_trunc = ()
        coord_system_trunc = CoordinateSystem(m1_source_coord, m2_source_coord)
        super().__init__(base_population, coord_system_trunc, param_names_trunc)

        self.eos_fn = eos_fn

        # Check that eos coordinates is one supported, and then save it.
        if eos_coordinates not in eos_coordinate_options:
            raise KeyError(
                "EOS coordinates must be one of: {}; Got: {}"
                .format(eos_coordinate_options, eos_coordinates)
            )
        self.eos_coordinates = eos_coordinates

    def eos_coords_to_spectral(self, parameters: Parameters):
        return eos_coords_to_spectral(
            get_eos_parameters(parameters, self.eos_coordinates),
            self.eos_coordinates,
        )

    def reject_samples(
            self,
            observables_trunc: Observables,
            parameters: Parameters,
            where: WhereType=True,
        ) -> WhereType:
        """
        Rejects samples with masses which violate the EOS constraints.
        """
        import lalsimulation

        # Determine relevant parameters and shapes.
        params_shape = next(iter(parameters.values())).shape
        spectral_parameters = self.eos_coords_to_spectral(parameters)

        where_arr = numpy.broadcast_to(where, params_shape)

        # Extract m1, m2 from observables.
        m1, m2 = observables_trunc

        i_invalid = numpy.ones_like(m1, dtype=bool)
        for i in numpy.ndindex(*params_shape):
            if not where_arr[i]:
                continue

            # Pull out relevant portion for this index.
            spectral_parameters_at_i = tuple(p[i] for p in spectral_parameters)

            # Determine EOS parameters.
            debug_verbose(
                "reject_samples: About to create EOS for",
                spectral_parameters_at_i,
                mode="eos_segfault_debug", flush=True,
            )
            eos = self.eos_fn(spectral_parameters_at_i)
            eos_fam = lalsimulation.CreateSimNeutronStarFamily(eos)
            eos_m_min, eos_m_max = eos_mass_range(eos_fam)
            debug_verbose(
                "reject_samples: Successfully created EOS for",
                spectral_parameters_at_i,
                mode="eos_segfault_debug", flush=True,
            )

            # Determine which m1, m2 samples violate the mass-limits for the EOS
            # specified by index `i`.
            i_invalid[i] = (m2[i] < eos_m_min) | (m1[i] > eos_m_max)

        return i_invalid


class EOSStats(object):
    def __init__(self, parameters: Parameters, weights=None):
        self.parameters = parameters
        self.weights = weights
        self.eos_coordinates = _determine_eos_coords(parameters)

    def plot_eos_ci(
            self,
            levels=(0.50, 0.90),
            density_limits=None,
            pressure_limits=None,
            filename=None,
            gammas_true=None,
        ):
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots(figsize=[6,4])

        density = numpy.logspace(*numpy.log10([1e17, 3e18]), 100)

        def eos_fn(density, parameters, eos_coordinates=self.eos_coordinates):
            eos_params = eos_coords_to_spectral(
                get_eos_parameters(parameters, eos_coordinates),
                eos_coordinates,
            )
            return spectral_eos_p_of_rho(density, eos_params)

        ci = CredibleRegions1D.from_samples(
            eos_fn,
            density, self.parameters, self.weights,
        )
        # Compute and store median
        ci.median(cache=True)
        # Compute and store all requested levels
        for level in levels:
            ci.equal_prob_bounds(level, cache=True)

        # Plot results
        ci.plot(
            ax,
            include_median=True,
            equal_prob_bounds=levels,
            orientation="horizontal",
            x_scale="log", y_scale="log",
        )

        # Overlay truth for synthetic data runs
        if gammas_true is not None:
            # Compute rho(p) for true EOS.
            parameters_true = {
                "gamma{idx}".format(idx=i+1) : numpy.asarray(gamma_idx)
                for i, gamma_idx in enumerate(gammas_true)
            }
            p_of_rho_true = eos_fn(density, parameters_true,
                eos_coordinates="spectral",
            )
            # Plot p versus rho(p) for true EOS.
            ax.plot(density, p_of_rho_true, color="C3", linestyle="dashed")

        if density_limits is not None:
            ax.set_xlim(density_limits)
        if pressure_limits is not None:
            ax.set_ylim(pressure_limits)

        ax.set_xlabel("Rest Mass Density [kg m$^{-3}$]")
        ax.set_ylabel("Pressure [Pa]")

        fig.tight_layout()

        # Save CI's to file if requested.
        if filename is not None:
            ci.save(filename, "w")

        return fig, ax


    def plot_eos_saturation_densities(
            self,
            density_nuc: float=2.8e17,
            multiples: typing.Sequence[int]=(1, 2, 6),
            filename=None,
            gammas_true=None,
        ):
        import matplotlib.pyplot as plt

        density = numpy.asarray([density_nuc*mult for mult in multiples])

        def eos_fn(density, parameters, eos_coordinates=self.eos_coordinates):
            eos_params = eos_coords_to_spectral(
                get_eos_parameters(parameters, eos_coordinates),
                eos_coordinates,
            )
            return spectral_eos_p_of_rho(density, eos_params)

        pressure = eos_fn(density, self.parameters)

        # Overlay truth for synthetic data runs
        if gammas_true is None:
            pressure_true = None
        else:
            # Compute rho(p) for true EOS.
            parameters_true = {
                "gamma{idx}".format(idx=i+1) : numpy.asarray(gamma_idx)
                for i, gamma_idx in enumerate(gammas_true)
            }
            pressure_true = eos_fn(
                density, parameters_true,
                eos_coordinates="spectral",
            )

        fig, ax = plt.subplots(figsize=[6,4])

        colors = ["C{}".format(i) for i in range(10)]
        for i, (color, mult) in enumerate(zip(colors, multiples)):
            p = pressure[...,i]

            label = r"$\rho_{\mathrm{nuc}}$"
            if mult != 1:
                label = r"${}$ {}".format(mult, label)

            ax.hist(
                numpy.log10(p),
                color=color, histtype="step", density=True,
                bins="auto",
                label=label,
            )

            if pressure_true is not None:
                p_true = pressure_true[...,i]
                ax.axvline(numpy.log10(p_true), color=color, linestyle="dashed")

        ax.legend(title="Density")

        ax.set_xlabel("log$_{10}$ Pressure [Pa]")
        ax.set_ylabel("Histogram")

        fig.tight_layout()

        # Save CI's to file if requested.
        if filename is not None:
            numpy.savetxt(
                filename, pressure,
                header=" ".join(str(mult) for mult in multiples),
            )

        return fig, ax

    def plot_radius_at_ref_mass(
            self,
            masses=(1.4, 1.8, 2.0),
            filename=None,
            gammas_true=None,
        ):
        import matplotlib.pyplot as plt
        import lalsimulation

        # Determine shape of parameters arrays.
        params_shape = numpy.shape(next(iter(self.parameters.values())))

        # Overlay truth for synthetic data runs
        if gammas_true is None:
            radii_true = None
        else:
            # Compute R(m) for true EOS in km.
            eos = spectral_eos(gammas_true)
            eos_fam = lalsimulation.CreateSimNeutronStarFamily(eos)
            radii_true = [
                1e-3 * radius_from_eos_and_mass(eos_fam, mass)
                for mass in masses
            ]

        # Tabulate radius posteriors for each mass.
        radii = numpy.empty(params_shape + (len(masses),), dtype=numpy.float64)
        for idx in numpy.ndindex(*params_shape):
            gammas_at_idx = tuple(
                self.parameters[name][idx]
                for name in ["gamma1", "gamma2", "gamma3", "gamma4"]
            )
            eos = spectral_eos(gammas_at_idx)
            eos_fam = lalsimulation.CreateSimNeutronStarFamily(eos)
            eos_m_min, eos_m_max = eos_mass_range(eos_fam)
            for i, mass in enumerate(masses):
                if eos_m_min <= mass <= eos_m_max:
                    radii[idx][i] = (
                        1e-3 * radius_from_eos_and_mass(eos_fam, mass)
                    )

        fig, ax = plt.subplots(figsize=[7.1,4], constrained_layout=True)

        for i, mass in enumerate(masses):
            color = "C{}".format(i)

            ax.hist(
                radii[...,i],
                color=color, histtype="step", density=True,
                bins="auto",
                label=str(mass),
            )

            if gammas_true is not None:
                ax.axvline(radii_true[i], color=color, linestyle="dashed")

        ax.legend(title="Mass [$M_\odot$]", fontsize=14)

        ax.set_xlabel("Radius [km]", fontsize=16)
        ax.set_ylabel("Histogram", fontsize=16)

        # Save CI's to file if requested.
        if filename is not None:
            numpy.savetxt(
                filename, radii,
                header=" ".join(str(mass) for mass in masses),
            )

        return fig, ax


    def plot_radius_posterior_vs_mass(
            self,
            m_min: float=0.9, m_max: float=2.1,
            n_mass_points: int=30, n_radii_points: int=100,
            filename: str=None, force: bool=False,
            gammas_true: typing.Optional[typing.Tuple[float,...]]=None,
        ):
        import h5py
        import scipy.stats
        import matplotlib.pyplot as plt
        import lalsimulation

        # Determine shape of parameters arrays.
        params_shape = numpy.shape(next(iter(self.parameters.values())))

        # Make mass grid for evaluation.
        mass_grid = numpy.linspace(m_min, m_max, n_mass_points)

        # Overlay truth for synthetic data runs
        if gammas_true is None:
            radii_true = None
        else:
            # Compute R(m) for true EOS in km.
            eos = spectral_eos(gammas_true)
            eos_fam = lalsimulation.CreateSimNeutronStarFamily(eos)
            m_min_eos, m_max_eos = eos_mass_range(eos_fam)
            radii_true = numpy.empty(n_mass_points, dtype=numpy.float64)

            for i, mass in enumerate(mass_grid):
                if m_min_eos <= mass <= m_max_eos:
                    radii_true[i] = (
                        1e-3 * radius_from_eos_and_mass(eos_fam, mass)
                    )
                else:
                    radii_true[i] = numpy.nan

        # Tabulate radius posteriors for each mass.
        radii = numpy.empty(params_shape+mass_grid.shape, dtype=numpy.float64)
        for idx in numpy.ndindex(*params_shape):
            gammas_at_idx = tuple(
                self.parameters[name][idx]
                for name in ["gamma1", "gamma2", "gamma3", "gamma4"]
            )
            eos = spectral_eos(gammas_at_idx)
            eos_fam = lalsimulation.CreateSimNeutronStarFamily(eos)
            eos_m_min, eos_m_max = eos_mass_range(eos_fam)
            for i, mass in enumerate(mass_grid):
                if eos_m_min <= mass <= eos_m_max:
                    radii[idx][i] = (
                        1e-3 * radius_from_eos_and_mass(eos_fam, mass)
                    )
                else:
                    radii[idx][i] = numpy.nan

        # Save results to HDF5 file if provided.
        if filename is not None:
            mode = "w" if force else "w-"
            with h5py.File(filename, mode) as h5_file:
                h5_file.create_dataset("mass", data=mass_grid)
                h5_file.create_dataset("radius", data=radii)

        fig, ax = plt.subplots(figsize=[7.1,4], constrained_layout=True)

        # Create grid for plotting.
        r_min = numpy.floor(numpy.nanmin(radii))
        r_max = numpy.ceil(numpy.nanmax(radii))
        radii_grid = numpy.linspace(r_min, r_max, n_radii_points)
        # Compute posterior density for R(m) at each value of m
        radius_post = numpy.empty((n_mass_points, n_radii_points))
        radius_quantiles = numpy.empty((n_mass_points, 2))
        for i, mass in enumerate(mass_grid):
            # Compute a KDE for the radius posterior PDF at this mass.
            kde = scipy.stats.gaussian_kde(
                radii[...,i][~numpy.isnan(radii[...,i])],
                bw_method="scott",
            )
            radius_post[i] = kde(radii_grid)

            ## TODO: also find the 90% quantiles of the KDE by first using
            ## kde.integrate_box(r_min, r) to get CDF(r), and then interpolate
            ## to get r for CDF(r) == 0.05 and CDF(r) == 0.95.

            # Find the 90% quantiles by integrating the KDE to get a CDF, and
            # then interpolating to get the inverse CDF.
            kde_cdf = [kde.integrate_box(r_min, r) for r in radii_grid]
            radius_quantiles[i] = numpy.interp(
                [0.05, 0.95],
                kde_cdf, radii_grid,
            )

        # Plot the posterior on R(m) vs m
        ax.contourf(
            radii_grid, mass_grid, radius_post,
            cmap="viridis",
        )
        # Plot the 90% quantiles on R(m) vs m
        ax.plot(
            radius_quantiles[...,0], mass_grid,
            color="white", linestyle="solid", linewidth=0.5,
        )
        ax.plot(
            radius_quantiles[...,1], mass_grid,
            color="white", linestyle="solid", linewidth=0.5,
        )

        # Overplot the true R(m) if this is a synthetic data study.
        if gammas_true is not None:
            ax.plot(
                radii_true, mass_grid,
                color="white", linestyle="dashed", linewidth=4,
            )

        ax.set_xlabel(r"Radius [km]", fontsize=16)
        ax.set_ylabel(r"Mass [$M_\odot$]", fontsize=16)

        return fig, ax


    def plot_eos_mass_range_corner(
            self,
            levels=(0.50, 0.90),
            quantiles=(0.1, 0.9),
            filename=None,
            gammas_true=None,
        ):
        import matplotlib.pyplot as plt
        import corner
        import lalsimulation

        gamma1, gamma2, gamma3, gamma4 = eos_coords_to_spectral(
            get_eos_parameters(self.parameters, self.eos_coordinates),
            self.eos_coordinates,
        )
        n_samples = gamma1.size

        mass_range_samples = numpy.empty((n_samples, 2), dtype=gamma1.dtype)

        for idx in range(n_samples):
            eos = spectral_eos(
                (gamma1[idx], gamma2[idx], gamma3[idx], gamma4[idx])
            )
            eos_fam = lalsimulation.CreateSimNeutronStarFamily(eos)

            mass_range_samples[idx] = eos_mass_range(eos_fam)

        if gammas_true is not None:
            eos = spectral_eos(gammas_true)
            eos_fam = lalsimulation.CreateSimNeutronStarFamily(eos)
            mass_range_true = eos_mass_range(eos_fam)
        else:
            mass_range_true = None

        if filename is not None:
            numpy.savetxt(
                filename, mass_range_samples,
                header="mmin mmax",
            )

        return corner.corner(
            mass_range_samples, weights=self.weights,
            levels=levels, quantiles=quantiles,
            truths=mass_range_true,
            labels=[
                r"$m_{\mathrm{min}} \, [M_\odot]$",
                r"$m_{\mathrm{max}} \, [M_\odot]$",
            ],
            label_kwargs={
                "fontsize": 14,
            },
            no_fill_contours=True, plot_datapoints=False, plot_density=False,
        )

    def plot_eos_lambda_at_mref(
            self,
            m_ref: float=1.4,
            bins: int=20,
            filename: typing.Optional[str]=None,
            gammas_true: typing.Optional[typing.Tuple[float,...]]=None,
        ):
        import lalsimulation
        import matplotlib.pyplot as plt
        gamma1, gamma2, gamma3, gamma4 = eos_coords_to_spectral(
            get_eos_parameters(self.parameters, self.eos_coordinates),
            self.eos_coordinates,
        )
        n_samples = gamma1.size

        mass_range_samples = numpy.empty((n_samples, 2), dtype=gamma1.dtype)

        lambda_refs = numpy.empty(n_samples, dtype=float)
        for idx in range(n_samples):
            eos = spectral_eos(
                (gamma1[idx], gamma2[idx], gamma3[idx], gamma4[idx])
            )
            eos_fam = lalsimulation.CreateSimNeutronStarFamily(eos)
            lambda_refs[idx] = lambda_from_eos_and_mass(eos_fam, m_ref)

        if filename is not None:
            numpy.savetxt(
                filename, lambda_refs,
                header="lambda_ref",
                footer="m_ref = {}Msun".format(m_ref),
            )

        if gammas_true is not None:
            eos = spectral_eos(gammas_true)
            eos_fam = lalsimulation.CreateSimNeutronStarFamily(eos)
            lambda_ref_true = lambda_from_eos_and_mass(eos_fam, m_ref)
        else:
            lambda_ref_true = None

        fig, ax = plt.subplots(figsize=[6,4])

        ax.hist(lambda_refs, bins=bins, color="C0", density=True)

        if lambda_ref_true is not None:
            ax.axvline(lambda_ref_true, color="C3")

        ax.set_xlabel(r"$\Lambda_{{{:.1f}}}$".format(m_ref))
        ax.set_ylabel("Histogram")

        return fig, ax

    @staticmethod
    def cli(raw_args=None):
        if raw_args is None:
            import sys
            raw_args = sys.argv[1:]

        import argparse
        parser = argparse.ArgumentParser()

        parser.add_argument(
            "posterior",
            help="Posterior samples/grid file or directory.  Must specify in "
                 "next argument which format is being used.",
        )
        parser.add_argument(
            "posterior_format",
            choices=["raw_samples", "cleaned_samples", "grid"],
            help="Format that posterior samples are using.",
        )

        subparsers = parser.add_subparsers(dest="command")

        ## EOS credible intervals ##
        subparser_plot_eos_ci = subparsers.add_parser("plot_eos_ci")

        subparser_plot_eos_ci.add_argument(
            "plot_filename",
            help="Name of file to store plot in.",
        )
        subparser_plot_eos_ci.add_argument(
            "--data-filename",
            help="Name of file to store data behind figure in.",
        )

        subparser_plot_eos_ci.add_argument(
            "--levels",
            type=float, nargs="+", default=[0.50, 0.90],
            help="Which probability levels to plot in EOS.",
        )

        subparser_plot_eos_ci.add_argument(
            "--density-limits",
            type=float, nargs=2,
            help="Limits to impose on density plot in kg m-3 units.",
        )
        subparser_plot_eos_ci.add_argument(
            "--pressure-limits",
            type=float, nargs=2,
            help="Limits to impose on pressure plot in Pa units.",
        )

        subparser_plot_eos_ci.add_argument(
            "--overlay-spectral-eos",
            type=float, nargs=4,
            help="Parameters of a spectral EOS to overlay.",
        )

        subparser_plot_eos_ci.add_argument(
            "--mpl-backend",
            default="Agg",
            help="Backend to use for matplotlib.",
        )

        ## EOS saturation densities ##
        subparser_plot_eos_sat_dens = subparsers.add_parser("plot_eos_sat_dens")

        subparser_plot_eos_sat_dens.add_argument(
            "plot_filename",
            help="Name of file to store plot in.",
        )
        subparser_plot_eos_sat_dens.add_argument(
            "--data-filename",
            help="Name of file to store data behind figure in.",
        )

        subparser_plot_eos_sat_dens.add_argument(
            "--multiples",
            type=int, nargs="+", default=[1, 2, 6],
            help="Which multiples of the saturation density to use.",
        )

        subparser_plot_eos_sat_dens.add_argument(
            "--overlay-spectral-eos",
            type=float, nargs=4,
            help="Parameters of a spectral EOS to overlay.",
        )

        subparser_plot_eos_sat_dens.add_argument(
            "--mpl-backend",
            default="Agg",
            help="Backend to use for matplotlib.",
        )

        ## Radius at reference masses ##
        subparser_plot_radius_at_ref_mass = subparsers.add_parser(
            "plot_radius_at_ref_mass"
        )

        subparser_plot_radius_at_ref_mass.add_argument(
            "plot_filename",
            help="Name of file to store plot in.",
        )
        subparser_plot_radius_at_ref_mass.add_argument(
            "--data-filename",
            help="Name of file to store data behind figure in.",
        )

        subparser_plot_radius_at_ref_mass.add_argument(
            "-m", "--reference-masses",
            type=float, nargs="+", default=[1.4, 1.8, 2.0],
            help="Specify reference masses to use.",
        )

        subparser_plot_radius_at_ref_mass.add_argument(
            "--overlay-spectral-eos",
            type=float, nargs=4,
            help="Parameters of a spectral EOS to overlay.",
        )

        subparser_plot_radius_at_ref_mass.add_argument(
            "--mpl-backend",
            default="Agg",
            help="Backend to use for matplotlib.",
        )

        ## Radius posterior versus mass ##
        subparser_plot_radius_posterior_vs_mass = subparsers.add_parser(
            "plot_radius_posterior_vs_mass"
        )

        subparser_plot_radius_posterior_vs_mass.add_argument(
            "plot_filename",
            help="Name of file to store plot in.",
        )
        subparser_plot_radius_posterior_vs_mass.add_argument(
            "--data-filename",
            help="Name of HDF5 file to store data behind figure in.",
        )

        subparser_plot_radius_posterior_vs_mass.add_argument(
            "-m", "--mass-range", metavar=("MMIN", "MMAX"),
            nargs=2, type=float, default=(0.9, 2.1),
            help="Specify range of masses to use.",
        )

        subparser_plot_radius_posterior_vs_mass.add_argument(
            "--n-mass-points",
            type=int, default=30,
            help="Number of masses to evaluate the R(m) posterior at.",
        )
        subparser_plot_radius_posterior_vs_mass.add_argument(
            "--n-radii-points",
            type=int, default=100,
            help="Number of radii to plot the R(m) posterior density at.",
        )

        subparser_plot_radius_posterior_vs_mass.add_argument(
            "--overlay-spectral-eos",
            type=float, nargs=4,
            help="Parameters of a spectral EOS to overlay.",
        )

        subparser_plot_radius_posterior_vs_mass.add_argument(
            "-f", "--force",
            action="store_true",
            help="Force overwrite of HDF5 output file if it exists.",
        )

        subparser_plot_radius_posterior_vs_mass.add_argument(
            "--mpl-backend",
            default="Agg",
            help="Backend to use for matplotlib.",
        )


        ## EOS mass range corner plot ##
        subparser_plot_mass_range_corner = subparsers.add_parser(
            "plot_eos_mass_range_corner"
        )

        subparser_plot_mass_range_corner.add_argument(
            "plot_filename",
            help="Name of file to store plot in.",
        )
        subparser_plot_mass_range_corner.add_argument(
            "--data-filename",
            help="Name of file to store data behind figure in.",
        )

        subparser_plot_mass_range_corner.add_argument(
            "--levels",
            type=float, nargs="+", default=[0.50, 0.90],
            help="Which probability levels to plot contours for.",
        )
        subparser_plot_mass_range_corner.add_argument(
            "--quantiles",
            type=float, nargs="+", default=[0.10, 0.90],
            help="Which probability levels to plot quantile lines for.",
        )

        subparser_plot_mass_range_corner.add_argument(
            "--overlay-spectral-eos",
            type=float, nargs=4,
            help="Parameters of a spectral EOS to overlay.",
        )

        subparser_plot_mass_range_corner.add_argument(
            "--mpl-backend",
            default="Agg",
            help="Backend to use for matplotlib.",
        )

        ## EOS lambda at reference mass plot ##
        subparser_plot_lambda_ref = subparsers.add_parser(
            "plot_eos_lambda_at_mref"
        )

        subparser_plot_lambda_ref.add_argument(
            "plot_filename",
            help="Name of file to store plot in.",
        )
        subparser_plot_lambda_ref.add_argument(
            "--data-filename",
            help="Name of file to store data behind figure in.",
        )

        subparser_plot_lambda_ref.add_argument(
            "--reference-mass",
            type=float, default=1.4,
            help="Reference mass (default is 1.4 Msun).",
        )
        subparser_plot_lambda_ref.add_argument(
            "--n-bins",
            type=int, default=20,
            help="Number of histogram bins (default is 20).",
        )

        subparser_plot_lambda_ref.add_argument(
            "--overlay-spectral-eos",
            type=float, nargs=4,
            help="Parameters of a spectral EOS to overlay.",
        )

        subparser_plot_lambda_ref.add_argument(
            "--mpl-backend",
            default="Agg",
            help="Backend to use for matplotlib.",
        )


        cli_args = parser.parse_args(raw_args)


        file_needs_closing = False
        try:
            if cli_args.posterior_format == "raw_samples":
                from ..posterior import H5RawPosteriorSamples
                # Load in the samples.
                post_file = H5RawPosteriorSamples(cli_args.posterior, "r")
                file_needs_closing = True
                # Extract the samples.
                parameters = post_file.get_params(...)

                # Flatten the arrays to be 1-D, since they're
                # (n_samples, n_walkers) right now
                for name in list(parameters.keys()):
                    parameters[name] = parameters[name].flatten()

                # No weights.
                weights = None

            elif cli_args.posterior_format == "cleaned_samples":
                from ..posterior import H5CleanedPosteriorSamples
                # Load in the samples.
                post_file = H5CleanedPosteriorSamples(cli_args.posterior)
                file_needs_closing = True
                # Extract the samples.
                parameters = post_file.get_params(...)
                # No weights.
                weights = None

            else:
                raise NotImplementedError(
                    "Sorry, format '{}' not yet implemented."
                    .format(cli_args.posterior_format)
                )

            eos_stats = EOSStats(parameters, weights=weights)

            if cli_args.command == "plot_eos_ci":
                import matplotlib
                matplotlib.use(cli_args.mpl_backend)

                gammas_true = (
                    None if cli_args.overlay_spectral_eos is None
                    else tuple(cli_args.overlay_spectral_eos)
                )

                fig, ax = eos_stats.plot_eos_ci(
                    levels=cli_args.levels, filename=cli_args.data_filename,
                    density_limits=cli_args.density_limits,
                    pressure_limits=cli_args.pressure_limits,
                    gammas_true=gammas_true,
                )
                fig.savefig(cli_args.plot_filename)

            elif cli_args.command == "plot_eos_sat_dens":
                import matplotlib
                matplotlib.use(cli_args.mpl_backend)

                gammas_true = (
                    None if cli_args.overlay_spectral_eos is None
                    else tuple(cli_args.overlay_spectral_eos)
                )

                fig, ax = eos_stats.plot_eos_saturation_densities(
                    multiples=cli_args.multiples,
                    filename=cli_args.data_filename,
                    gammas_true=gammas_true,
                )
                fig.savefig(cli_args.plot_filename)

            elif cli_args.command == "plot_radius_at_ref_mass":
                import matplotlib
                matplotlib.use(cli_args.mpl_backend)

                gammas_true = (
                    None if cli_args.overlay_spectral_eos is None
                    else tuple(cli_args.overlay_spectral_eos)
                )

                fig, ax = eos_stats.plot_radius_at_ref_mass(
                    masses=cli_args.reference_masses,
                    filename=cli_args.data_filename,
                    gammas_true=gammas_true,
                )
                fig.savefig(cli_args.plot_filename)

            elif cli_args.command == "plot_radius_posterior_vs_mass":
                import matplotlib
                matplotlib.use(cli_args.mpl_backend)

                gammas_true = (
                    None if cli_args.overlay_spectral_eos is None
                    else tuple(cli_args.overlay_spectral_eos)
                )
                m_min, m_max = cli_args.mass_range

                fig, ax = eos_stats.plot_radius_posterior_vs_mass(
                    m_min=m_min, m_max=m_max,
                    n_mass_points=cli_args.n_mass_points,
                    n_radii_points=cli_args.n_radii_points,
                    filename=cli_args.data_filename, force=cli_args.force,
                    gammas_true=gammas_true,
                )
                fig.savefig(cli_args.plot_filename)

            elif cli_args.command == "plot_eos_mass_range_corner":
                import matplotlib
                matplotlib.use(cli_args.mpl_backend)

                gammas_true = (
                    None if cli_args.overlay_spectral_eos is None
                    else tuple(cli_args.overlay_spectral_eos)
                )

                fig = eos_stats.plot_eos_mass_range_corner(
                    levels=cli_args.levels, quantiles=cli_args.quantiles,
                    filename=cli_args.data_filename,
                    gammas_true=gammas_true,
                )
                fig.savefig(cli_args.plot_filename)

            elif cli_args.command == "plot_eos_lambda_at_mref":
                import matplotlib
                matplotlib.use(cli_args.mpl_backend)

                gammas_true = (
                    None if cli_args.overlay_spectral_eos is None
                    else tuple(cli_args.overlay_spectral_eos)
                )

                fig, ax = eos_stats.plot_eos_lambda_at_mref(
                    m_ref=cli_args.reference_mass,
                    bins=cli_args.n_bins,
                    filename=cli_args.data_filename,
                    gammas_true=gammas_true,
                )
                fig.tight_layout()
                fig.savefig(cli_args.plot_filename)

            else:
                raise NotImplementedError(
                    "Sorry, command '{}' not yet implemented."
                    .format(cli_args.command)
                )

        finally:
            if file_needs_closing:
                post_file.close()
