import numpy

from ...coordinate import CoordinateSystem, CoordinateTransforms
from ...population import Population
from ...types import WhereType, Numeric, Observables, Parameters

from ..coordinates import (
    m_source_coord, Z_metal_coord,
    transformations,
)

from .population import StellarEvolutionPopulation


class FryerRapidPopulation(StellarEvolutionPopulation):
    def __init__(
            self,
            initial_population: Population,
            rate_name: str="rate",
            transformations: CoordinateTransforms=transformations,
        ) -> None:
        coord_system_init = CoordinateSystem(m_source_coord, Z_metal_coord)
        coord_system_final = CoordinateSystem(m_source_coord)

        param_names_evo = (rate_name,)
        super().__init__(
            initial_population,
            coord_system_init, coord_system_final,
            param_names_evo,
            transformations=transformations,
        )

        self.rate_name = rate_name

    def normalization(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return parameters[self.rate_name]

    def initial_to_final(
            self,
            observables_init: Observables,
            parameters: Parameters,
        ) -> Observables:
        m_init, Z_metal = observables_init

        m_final = numpy.empty_like(m_init)

        # Look at objects < 22 Msun
        idx = m_init < 22.0
        m_init_at_idx, Z_metal_at_idx = m_init[idx], Z_metal[idx]
        m_final[idx] = (
            1.1 +
            0.2 * numpy.exp((m_init_at_idx - 11.0) / 7.5) +
            10.0 * (1.0 + Z_metal_at_idx) *
            numpy.exp(-numpy.square((m_init_at_idx-23.5)/(1.0+Z_metal_at_idx)))
        )

        # Look at objects in range [22, 30] Msun
        idx = (m_init >= 22.0) & (m_init < 30.0)
        m_init_at_idx, Z_metal_at_idx = m_init[idx], Z_metal[idx]
        m_final[idx] = (
            1.1 +
            0.2 * numpy.exp(0.25 * (m_init_at_idx - 11.0)) -
            (2.0 + Z_metal_at_idx) * numpy.exp(0.4 * (m_init_at_idx - 26.0)) -
            1.85 +
            0.25 * Z_metal_at_idx +
            10.0 * (1.0 + Z_metal_at_idx) *
            numpy.exp(-numpy.square((m_init_at_idx-23.5)/(1.0+Z_metal_at_idx)))
        )

        # Look at objects in range [30, 50] Msun
        idx = (m_init >= 30.0) & (m_init < 50.0)
        m_init_at_idx, Z_metal_at_idx = m_init[idx], Z_metal[idx]
        m_final[idx] = (
            numpy.minimum(
                33.35 + (4.75 + 1.25*Z_metal_at_idx) * (m_init_at_idx - 34.0),
                (
                    m_init_at_idx -
                    numpy.sqrt(Z_metal_at_idx) * (1.3*m_init_at_idx - 18.35)
                ),
            ) -
            1.85 +
            0.05 * Z_metal_at_idx * (75.0 - m_init_at_idx)
        )

        # Look at objects in range [50, 90] Msun
        idx = (m_init >= 50.0) & (m_init < 90.0)
        m_init_at_idx = m_init[idx]
        m_final[idx] = 1.8 + 0.04 * (90.0 - m_init_at_idx)

        # Look at objects > 90 Msun
        idx = m_init >= 90.0
        m_init_at_idx = m_init[idx]
        m_final[idx] = 1.8 + numpy.log10(m_init_at_idx - 89.0)

        return m_final,
