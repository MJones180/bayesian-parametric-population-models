import typing
import numpy

from ...coordinates import coordinates, transformations
from ....coordinate import CoordinateSystem, CoordinateTransforms
from ....detection import GaussianProcessDetection

__all__ = [
    "CBCGaussianProcessDetection",
]

class CBCGaussianProcessDetection(GaussianProcessDetection):
    def __init__(
            self,
            coord_system: CoordinateSystem,
            gaussian_process,
            shift=0.0, scale=1.0,
            transformations: CoordinateTransforms=transformations,
            truth: typing.Optional[numpy.ndarray]=None,
            backend="sklearn",
        ) -> None:
        super().__init__(
            coord_system=coord_system,
            gaussian_process=gaussian_process,
            shift=shift, scale=scale,
            transformations=transformations,
            truth=truth,
            backend=backend,
        )

    def save(self, filename):
        """
        Save this CBCGaussianProcessDetection to a JSON file.
        """
        return self.gaussian_process.save(filename)


    @classmethod
    def load(cls, filename, shift=0.0, scale=1.0, backend="sklearn"):
        """
        Load a CBCGaussianProcessDetection from an HDF5 file.
        """
        if backend == "sklearn":
            import warnings
            import pickle

            warnings.warn(
                "Loading a Gaussian Process from a pickle file. This is "
                "dangerous, or going to crash, if the version of python or "
                "scikit-learn is different than what produced the file."
            )

            with open(filename, "rb") as gp_file:
                gaussian_process = pickle.load(gp_file)
        elif backend == "gp_api":
            from gp_api.gaussian_process import GaussianProcess
            gaussian_process = GaussianProcess.load(filename)
        else:
            raise KeyError("Unknown backend")

        coord_system = CoordinateSystem(*(
            coordinates[name] for name in gaussian_process.param_names
        ))

        # Return appropriate CBCGaussianDetection object.
        return cls(
            coord_system,
            gaussian_process,
            shift=shift, scale=scale,
        )
