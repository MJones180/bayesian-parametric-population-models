from __future__ import print_function


def max_autocorr(samples):
    import itertools as it
    import numpy
    import emcee.autocorr

    n_samples, n_walkers, n_params = numpy.shape(samples)

    t_all_walkers_params = numpy.empty(
        (n_walkers, n_params),
        dtype=samples.dtype,
    )

    for w, p in it.product(range(n_walkers), range(n_params)):
        print(w,p)
        print(samples[:,w,p])
        t_all_walkers_params[w,p] = emcee.autocorr.integrated_time(
            samples[:,w,p],
        )

    t_all_params = numpy.max(t_all_walkers_params, axis=0)
    t = numpy.max(t_all_params)

    return int(numpy.ceil(t))


'''
def max_autocorr(samples):
    import numpy
    import emcee.autocorr

    samples_walker_avg = numpy.mean(samples, axis=1)

    t_all_params = emcee.autocorr.integrated_time(samples_walker_avg, axis=0)
    t = numpy.max(t_all_params)

    return int(numpy.ceil(t))
'''


def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing posterior samples.",
    )

    parser.add_argument(
        "--burnin",
        type=int, default=0,
        help="Burnin time.",
    )
    parser.add_argument(
        "--params-of-interest",
        type=int, nargs="+", default=None,
        help="List of indices of parameters of interest, default uses all.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import h5py

    with h5py.File(args.posteriors, "r") as posteriors:
        i_samples = slice(args.burnin, None, None)
        i_walkers = slice(None, None, None)
        i_params = args.params_of_interest or slice(None, None, None)

        pos = posteriors["pos"][i_samples,i_walkers,i_params]

        print(max_autocorr(pos))
