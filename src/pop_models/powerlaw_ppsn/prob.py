from __future__ import division, print_function


# Names of all (possibly free) parameters of this model, in the order they
# should appear in any array of samples (e.g., MCMC posterior samples).
param_names = [
    "log10_rate",
    "alpha_m", "beta_m", "m_min", "m_max",
    "mu_pp", "sigma_pp", "lambda_pp",
    "delta_m1", "delta_m2",
]
# Number of (possibly free) parameters for this population model.
ndim_pop = len(param_names)


def mass_turnon(m, m_min, delta_m):
    import numpy

    m = numpy.asarray(m)
    pdf = numpy.zeros_like(m)

    # Indices where we just fill in ones.
    i_ones = m >= m_min + delta_m
    # Indices where we evaluate the function non-trivially.
    i_func = (m >= m_min) & ~i_ones

    # Fill in the ones.
    pdf[i_ones] = 1.0

    # No longer need m's outside the range specified by ``i_func``.
    m = m[i_func]

    # Pre-compute m - m_min.
    m_minus_mmin = m - m_min

    # Compute the term that goes in the exponential.
    Z = delta_m/m_minus_mmin + delta_m/(m_minus_mmin-delta_m)

    # Evaluate the PDF where it is non-trivial.
    pdf[i_func] = numpy.reciprocal(numpy.exp(Z) + 1)

    # Return the PDF evaluated at every ``m``.
    return pdf


def mass_ratio_dist(m_1, m_2, beta_m, is_sorted=False):
    r"""
    Returns the un-normalized mass ratio distribution function

    .. math::
       \left(\frac{m_2}{m_1}\right)^\beta
    """
    import numpy

    pdf = numpy.zeros_like(m_1)

    i_sorted = True if is_sorted else m_1 >= m_2

    pdf[i_sorted] = numpy.power(m_2[i_sorted], beta_m)
    pdf[i_sorted] *= numpy.power(m_1[i_sorted], -(beta_m + 1))

    return pdf


def powerlaw_dist(x, index):
    import numpy
    return numpy.power(x, index)


def mass_powerlaw_component(
        m_1, m_2,
        alpha_m, beta_m, m_min, m_max, M_max,
        delta_m1, delta_m2,
        is_sorted=False,
        pdf_const=None,
    ):
    r"""
    Computes the powerlaw component of the mass distribution.
    """
    if pdf_const is None:
        pdf_const = mass_powerlaw_component_normalization(
            alpha_m, beta_m, m_min, m_max, M_max,
            delta_m1, delta_m2,
        )

    m_1, m_2 = numpy.asarray(m_1), numpy.asarray(m_2)
    m_1, m_2 = numpy.broadcast_arrays(m_1, m_2)

    i_support = (
        (m_min <= m_2) &
        (m_1 <= m_max) &
        (m_1 + m_2 <= M_max)
    )
    if not is_sorted:
        i_support &= m_2 <= m_1

    pdf = numpy.zeros_like(m_1)

    m_1, m_2 = m_1[i_support], m_2[i_support]

    pdf[i_support] = (
        # Missing extra factor N(m_1)
        # Which is Int[q^beta S(m2) H(m2-m1), {q, mmin/m1, 1}]
        mass_turnon(m_1, m_min, delta_m1) *
        mass_turnon(m_2, m_min, delta_m2) *
        powerlaw_dist(m_1, -(alpha_m + beta_m + 1)) *
        powerlaw_dist(m_2, beta_m)
    ) / pdf_const

    return pdf


def mass_powerlaw_component_normalization(
        alpha_m, beta_m, m_min, m_max, M_max,
        delta_m1, delta_m2,
        reterr=False,
    ):
    import scipy.integrate

    def func(m_2, m_1):
        return mass_powerlaw_component(
            m_1, m_2,
            alpha_m, beta_m, m_min, m_max, M_max,
            delta_m1, delta_m2,
            is_sorted=True,
            pdf_const=1.0,
        )

    result, result_err = scipy.integrate.dblquad(
        func,
        m_min, m_max,
        lambda m_1: m_min, lambda m_1: m_1,
    )

    if reterr:
        return result, result_err
    else:
        return result



def mass_gaussian(m, m_min, mu, sigma):
    import numpy
    import scipy.stats

    a = (m_min - mu) / sigma
    b = numpy.inf

    return scipy.stats.truncnorm(a, b, loc=mu, scale=sigma).pdf(m)


def mass_ppsn_component(
        m_1, m_2,
        beta_m, m_min,
        mu_pp, sigma_pp,
        delta_m1, delta_m2,
        is_sorted=False,
        pdf_const=None,
    ):
    r"""
    Computes the PPSN component of the mass distribution.
    """
    if pdf_const is None:
        pdf_const = mass_ppsn_component_normalization(
            beta_m, m_min,
            mu_pp, sigma_pp,
            delta_m1, delta_m2,
        )

    m_1, m_2 = numpy.asarray(m_1), numpy.asarray(m_2)
    m_1, m_2 = numpy.broadcast_arrays(m_1, m_2)

    i_support = m_min <= m_2
    if not is_sorted:
        i_support &= m_2 <= m_1

    pdf = numpy.zeros_like(m_1)

    m_1, m_2 = m_1[i_support], m_2[i_support]

    pdf[i_support] = (
        mass_turnon(m_1, m_min, delta_m1) *
        mass_turnon(m_2, m_min, delta_m2) *
        mass_gaussian(m_1, m_min, mu_pp, sigma_pp) *
        mass_ratio_dist(m_1, m_2, beta_m, is_sorted=is_sorted)
    ) / pdf_const

    return pdf


def mass_ppsn_component_normalization(
        beta_m, m_min,
        mu_pp, sigma_pp,
        delta_m1, delta_m2,
        reterr=False,
    ):
    import numpy
    import scipy.integrate

    def func(m_2, m_1):
        return mass_ppsn_component(
            m_1, m_2,
            beta_m, m_min,
            mu_pp, sigma_pp,
            delta_m1, delta_m2,
            is_sorted=True,
            pdf_const=1.0,
        )

    result, result_err = scipy.integrate.dblquad(
        func,
        m_min, mu_pp+6*sigma_pp,
        lambda m_1: m_min, lambda m_1: m_1,
    )

    if reterr:
        return result, result_err
    else:
        return result


def mass_powerlaw_ppsn_mixture_pdf(
        m_1, m_2,
        params, M_max,
        is_sorted=False,
        pdf_const_powerlaw=None, pdf_const_ppsn=None,
    ):
    (
        alpha_m, beta_m, m_min, m_max,
        mu_pp, sigma_pp, lambda_pp,
        delta_m1, delta_m2,
    ) = params

    pdf_powerlaw = mass_powerlaw_component(
        m_1, m_2,
        alpha_m, beta_m, m_min, m_max, M_max,
        delta_m1, delta_m2,
        is_sorted=is_sorted,
        pdf_const=pdf_const_powerlaw,
    )
    pdf_ppsn = mass_ppsn_component(
        m_1, m_2,
        beta_m, m_min,
        mu_pp, sigma_pp,
        delta_m1, delta_m2,
        is_sorted=is_sorted,
        pdf_const=pdf_const_ppsn,
    )

    return (1-lambda_pp)*pdf_powerlaw + lambda_pp*pdf_ppsn


def mass_powerlaw_ppsn_mixture_marginal_m1_pdf(
        m_1, params, M_max,
        pdf_const_powerlaw=None, pdf_const_ppsn=None,
    ):
    import numpy
    import scipy

    (
        alpha_m, beta_m, m_min, m_max,
        mu_pp, sigma_pp, lambda_pp,
        delta_m1, delta_m2,
    ) = params

    m_1 = numpy.asarray(m_1)

    # Compute normalization constants if not provided.
    if pdf_const_powerlaw is None:
        pdf_const_powerlaw = mass_powerlaw_component_normalization(
            alpha_m, beta_m, m_min, m_max, M_max,
            delta_m1, delta_m2,
        )
    if pdf_const_ppsn is None:
        pdf_const_ppsn = mass_ppsn_component_normalization(
            beta_m, m_min,
            mu_pp, sigma_pp,
            delta_m1, delta_m2,
        )

    def func(m_2, m_1):
        return mass_powerlaw_ppsn_mixture_pdf(
            m_1, m_2,
            params, M_max,
            is_sorted=True,
            pdf_const_powerlaw=pdf_const_powerlaw,
            pdf_const_ppsn=pdf_const_ppsn,
        )

    pdf = numpy.empty_like(m_1)

    for i in range(len(pdf)):
        pdf[i] = scipy.integrate.quad(
            func,
            m_min, m_1[i],
            args=(m_1[i],),
        )[0]

    return pdf
