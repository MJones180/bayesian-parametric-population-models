import typing

import numpy

from .population import Population
from .types import Observables, Parameters

class ObservableQuantiles(object):
    def __init__(
            self,
            population: Population,
        ):
        if len(population.coord_system) != 1:
            raise ValueError(
                "Coordinate system must be 1-D for quantiles to work."
            )

        self.population = population

    def quantile(
            self,
            parameters: Parameters,
            q: float,
            n_samples: typing.Optional[int]=1000,
            random_state: typing.Optional[numpy.random.RandomState]=None,
        ) -> numpy.ndarray:
        if random_state is None:
            random_state = numpy.random.RandomState()

        x_samples, = self.population.rvs(
            n_samples, parameters,
            random_state=random_state,
        )

        return numpy.percentile(x_samples, 100*q, axis=-1)
