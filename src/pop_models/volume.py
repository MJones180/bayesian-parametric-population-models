from .coordinate import CoordinateSystem
from .types import Numeric, Observables

## TODO: Add optional ``max`` method

class SensitiveVolume(object):
    r"""
    Abstract representation of a survey's sensitive volume :math:`\mathcal{V}`,
    in a given coordinate system.
    """
    def __init__(self, coord_system: CoordinateSystem):
        self.coord_system = coord_system

    def __call__(self, observables: Observables) -> Numeric:
        raise NotImplementedError()

    def max(self) -> float:
        """
        Returns the maximum volume for any possible observable.
        """
        raise NotImplementedError()
