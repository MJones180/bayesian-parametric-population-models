__all__ = [
    "Detection",
    "TransformedDetection",
    "PreSampledDetection",
    "GaussianDetection",
    "GaussianProcessDetection",
    "DetectionLikelihood",
    "ReweightedPosteriorsDetectionLikelihood",
    "PopulationMonteCarloDetectionLikelihood",
]

import typing
import warnings

import numpy

from pop_models.coordinate import (
    CoordinateSystem,
    CoordinateTransformFunction, CoordinateTransforms, transformations_nil,
)
from pop_models.integrators import MCIntegrator, AdaptiveMCIntegrator
from pop_models.population import Population, ConditionalPopulation
from pop_models.types import (
    Numeric, WhereType, Observables, Parameters,
    vectorize_over_parameters_to_numeric,
)
from pop_models.utils import debug_verbose

class Detection(object):
    r"""
    Abstract base class for representing the observables from objects detected
    by your survey.  Observables are represented probabalistically, either by a
    likelihood function specified by the :py:meth:`likelihood` method, or by a
    posterior sampler, specified by the :py:meth:`posterior_samples` method.
    It is up to sub-classes how to implement these methods, and only one should
    be necessary based on the manner in which it is used.

    If the posterior sampler only has access to a finite number of samples
    (e.g., in the case of re-using pre-computed samples), then the
    ``max_samples`` argument should be set to that number, otherwise it defaults
    to ``None`` which indicates there is no limit.  In the finite sample case,
    ``posterior_samples`` should accept an integer as the ``random_state``
    argument, and in that case it should provide samples from indices on the
    interval ``[random_state, random_state+n_samples)``, rather than do anything
    truly random.
    """
    def __init__(
            self,
            coord_system: CoordinateSystem,
            transformations: CoordinateTransforms=transformations_nil,
            truth: typing.Optional[numpy.ndarray]=None,
            max_samples: typing.Optional[int]=None,
        ):
        self.coord_system = coord_system
        self.transformations = transformations
        self.truth = truth
        self.max_samples = max_samples

    def likelihood(self, observables: Observables) -> Numeric:
        raise NotImplementedError()

    def posterior_samples(
            self,
            n_samples: int,
            random_state:
              typing.Optional[typing.Union[numpy.random.RandomState,int]]=None,
        ) -> typing.Tuple[Observables, typing.Optional[Numeric]]:
        raise NotImplementedError()

    def to_coords(self, coord_system: CoordinateSystem) -> "Detection":
        if coord_system == self.coord_system:
            return self

        # Use a known transformation if possible.
        transform_to_self = None
        try:
            transform_to_self = self.transformations[
                (coord_system, self.coord_system)
            ]
        except KeyError:
            # Won't be able to evaluate likelihood method.
            pass

        transform_from_self = None
        try:
            transform_from_self = self.transformations[
                (self.coord_system, coord_system)
            ]
        except KeyError:
            # Won't be able to evaluate posterior_samples method.
            pass

        if (transform_to_self is not None) or (transform_from_self is not None):
            return TransformedDetection(
                self, coord_system,
                transform_to_self, transform_from_self,
            )

        raise ValueError("Unknown coordinate transformation.")


class TransformedDetection(Detection):
    def __init__(
            self,
            base_detection: Detection,
            coord_system: CoordinateSystem,
            transform_to_base: typing.Optional[CoordinateTransformFunction],
            transform_from_base: typing.Optional[CoordinateTransformFunction],
        ) -> None:
        base_truth = base_detection.truth
        truth = None
        if (transform_from_base is not None) and (base_truth is not None):
            truth = transform_from_base(base_truth)

        super().__init__(
            coord_system,
            base_detection.transformations,
            truth=truth,
            max_samples=base_detection.max_samples,
        )

        self.base_detection = base_detection
        self.transform_to_base = transform_to_base
        self.transform_from_base = transform_from_base

        # Determine whether any of the coordinate systems have limits that will
        # require setting the likelihood to zero.
        # TODO: Also keep track of precisely which coordinates that is true of,
        # so we don't have to iterate over all of them except when __init__ is
        # called.
        self.requires_cond = any(
            coord.lower_limit is not None for coord in coord_system
        ) or any(
            coord.upper_imit is not None for coord in coord_system
        )

    def likelihood(self, observables: Observables) -> Numeric:
        if self.transform_to_base is None:
            raise NotImplementedError(
                "Transformation needed to evaluate likelihood is unknown."
            )

        # Transform observables to base coordinate system.
        base_observables = self.transform_to_base(observables)

        # Compute the likelihood in the base coordinate system.
        likelihood = self.base_detection.likelihood(base_observables)

        # If there are no conditions on the coordinates, return
        if not self.requires_cond:
            return likelihood

        # Determine where we need to zero-out the likelihood because it's
        # outside the bounds of the coordinate system.
        cond = numpy.ones_like(likelihood, dtype=bool)
        for coord, obs in zip(self.coord_system, observables):
            if coord.lower_limit is not None:
                cond &= coord.lower_limit <= obs
            if coord.upper_limit is not None:
                cond &= obs <= coord.upper_limit

        # Return the likelihood, zeroing out invalid regions.
        return numpy.where(cond, likelihood, 0.0)

    def posterior_samples(
            self,
            n_samples: int,
            random_state:
              typing.Optional[typing.Union[numpy.random.RandomState,int]]=None,
        ) -> typing.Tuple[Observables, typing.Optional[Numeric]]:
        if self.transform_from_base is None:
            raise NotImplementedError(
                "Transformation needed to draw posterior samples is unknown."
            )

        warnings.warn(
            "Jacobians for coordinate transformations not yet implemented. "
            "Prior values returned by 'posterior_samples' will not be accurate."
        )

        # Draw samples in base coordinate system
        base_samples, prior_samples = self.base_detection.posterior_samples(
            n_samples, random_state=random_state,
        )
        # Transform to desired coordinate system
        samples = self.transform_from_base(base_samples)

        return samples, prior_samples


class PreSampledDetection(Detection):
    def __init__(
            self,
            coord_system: CoordinateSystem,
            post_samples: typing.Tuple[numpy.ndarray,...],
            prior_probs: typing.Optional[numpy.ndarray]=None,
            transformations: CoordinateTransforms=transformations_nil,
            truth: typing.Optional[numpy.ndarray]=None,
            base_system_samples_truth:
              typing.Optional[typing.Tuple[
                  CoordinateSystem, Observables, typing.Optional[Observables],
              ]]=None
        ):
        n_params = len(post_samples)
        n_samples = numpy.size(post_samples[0])

        super().__init__(
            coord_system,
            transformations=transformations,
            truth=truth,
            max_samples=n_samples,
        )

        for obs in post_samples[1:]:
            if numpy.size(obs) != n_samples:
                raise ValueError(
                    "Posterior samples for each coordinate must have the same "
                    "length"
                )

        if prior_probs is not None:
            if prior_probs.shape != (n_samples,):
                raise ValueError(
                    "Prior probability array must have shape (n_samples,)"
                )

        # Assert that coord_system and post_samples are compatible in size.
        if len(coord_system) != n_params:
            raise ValueError(
                "Posterior samples must have one element for each coordinate "
                "in the coordinate system"
            )

        self.post_samples = post_samples
        self.prior_probs = prior_probs

        if base_system_samples_truth is None:
            self.base_coord_system = self.coord_system
            self.base_post_samples = self.post_samples
            self.base_truth = self.truth
        else:
            self.base_coord_system, self.base_post_samples, self.base_truth = (
                base_system_samples_truth
            )

    def posterior_samples(
            self,
            n_samples: int,
            random_state: int,
        ) -> typing.Tuple[Observables, typing.Optional[Numeric]]:
        # Special case: return all samples
        if (n_samples == self.max_samples) and (random_state == 0):
            return self.post_samples, self.prior_probs

        last_sample = random_state+n_samples

        if last_sample > typing.cast(int, self.max_samples):
            raise IndexError(
                "Posterior samples requested outside of range"
            )

        idx = slice(random_state, last_sample)

        post_samples = tuple(obs[idx] for obs in self.post_samples)
        if self.prior_probs is not None:
            prior_probs = self.prior_probs[idx]
        else:
            prior_probs = None

        return post_samples, prior_probs

    def to_coords(
            self,
            coord_system: CoordinateSystem,
        ) -> "PreSampledDetection":
        if coord_system == self.coord_system:
            return self

        transform_fn = (
            self.transformations[self.base_coord_system, coord_system]
        )

        post_samples = transform_fn(self.base_post_samples)

        truth = None
        if self.base_truth is not None:
            truth = transform_fn(self.base_truth)

        base_system_samples_truth = (
            self.base_coord_system, self.base_post_samples, self.base_truth,
        )

        return type(self)(
            coord_system,
            post_samples,
            prior_probs=self.prior_probs,
            transformations=self.transformations,
            truth=truth,
            base_system_samples_truth=base_system_samples_truth,
        )


class UniformDetection(Detection):
    def __init__(
            self,
            coord_system: CoordinateSystem,
            bounds: typing.Tuple[typing.Tuple[float,float],...],
            transformations: CoordinateTransforms=transformations_nil,
            truth: typing.Optional[numpy.ndarray]=None,
        ) -> None:
        if len(bounds) != len(coord_system):
            raise ValueError(
                "Must have one pair of bounds for each coordinate."
            )
        for bound_pair in bounds:
            if len(bound_pair) != 2:
                raise ValueError(
                    "Each set of bounds must be a pair, got {} for one."
                    .format(len(bound_pair))
                )

        super().__init__(coord_system, transformations, truth=truth)
        self._bounds = bounds
        self._prob_const = numpy.reciprocal(
            numpy.prod(numpy.diff(bounds, axis=1))
        )

    @property
    def bounds(self):
        return self._bounds

    def likelihood(self, observables: Observables) -> Numeric:
        # Determine where support for the likelihood exists for the first
        # observable.  Done separately since we need a baseline for the
        # accumulator, and this saves us from needing to apply the logical
        # operator for the first step.
        lower, upper = self.bounds[0]
        obs = observables[0]
        i_support = lower <= obs
        i_support &= obs <= upper

        # Determine where support exists for the rest of the observables.
        for (lower, upper), obs in zip(self.bounds[1:], observables[1:]):
            i_support &= lower <= obs
            i_support &= obs <= upper

        return numpy.where(i_support, self._prob_const, 0.0)


class GaussianDetection(Detection):
    def __init__(
            self,
            coord_system: CoordinateSystem,
            mean: numpy.ndarray,
            covariance: typing.Optional[numpy.ndarray]=None,
            inv_covariance: typing.Optional[numpy.ndarray]=None,
            transformations: CoordinateTransforms=transformations_nil,
            truth: typing.Optional[numpy.ndarray]=None,
        ) -> None:
        import scipy.stats
        import warnings

        has_covariance = covariance is not None
        has_inv_covariance = inv_covariance is not None

        # Ensure we have one form of the covariance matrix.
        if not (has_covariance or has_inv_covariance):
            raise ValueError(
                "Must provide one of 'covariance' and 'inv_covariance'."
            )
        # If we have the inverse covariance, take its inverse.
        if has_inv_covariance:
            inv_inv_covariance = numpy.linalg.inv(inv_covariance)

        # Now one of three cases can occur.
        # If we just have the covariance or just the inverse covariance, simply
        # use the result of one of those.
        if has_covariance and not has_inv_covariance:
            covar = covariance
        elif not has_covariance and has_inv_covariance:
            covar = inv_inv_covariance
        # If both are provided, just use 'covariance', but raise a warning if
        # 'inv_inv_covariance' is not within a small tolerance of 'covariance'.
        else:
            covars_close = numpy.allclose(
                covariance, inv_inv_covariance,
                rtol=1e-5, atol=1e-8, equal_nan=True,
            )
            if not covars_close:
                warnings.warn(
                    "Covariance and inverse covariance arrays not equal."
                )
            covar = covariance

        assert isinstance(covar, numpy.ndarray)

        super().__init__(coord_system, transformations, truth=truth)

        n_dim = len(coord_system)

        if mean.shape != (n_dim,):
            raise ValueError(
                "Mean dimension {} does not match coordinate system {}."
                .format(mean.shape, (n_dim,))
            )
        if covar.shape != (n_dim, n_dim):
            raise ValueError(
                "Covariance matrix dimensions do not match coordinate system."
            )

        self.mean = mean
        self.covariance = covar

        eigvals, eigvecs = numpy.linalg.eig(covar)
        if any(x < 0 for x in eigvals):
            warnings.warn(
                "Covariance has negative eigenvalues: {}".format(eigvals)
            )

        self.dist = scipy.stats.multivariate_normal(
            mean=mean, cov=covar,
            allow_singular=True,
        )

        # Determine whether any of the coordinate systems have limits that will
        # require sampling with replacement or setting the likelihood to zero.
        # TODO: Also keep track of precisely which coordinates that is true of,
        # so we don't have to iterate over all of them except when __init__ is
        # called.
        self.requires_cond = any(
            coord.lower_limit is not None for coord in coord_system
        ) or any(
            coord.upper_imit is not None for coord in coord_system
        )

    @classmethod
    def from_truth(
            cls,
            coord_system: CoordinateSystem,
            truth: numpy.ndarray,
            covariance: typing.Optional[numpy.ndarray]=None,
            inv_covariance: typing.Optional[numpy.ndarray]=None,
            transformations: CoordinateTransforms=transformations_nil,
            random_state: typing.Optional[numpy.random.RandomState]=None,
        ) -> "GaussianDetection":
        import scipy.stats

        # Create a GaussianDetection object centered at the truth
        true_det = cls(
            coord_system,
            truth, covariance=covariance, inv_covariance=inv_covariance,
        )

        # Take a random Gaussian draw as the mean of the distribution.
        mean = true_det.dist.rvs(1, random_state=random_state)

        # In 1-D, `scipy.stats` reduces `mean` to a scalar instead of a 1-D
        # array with one element.  To preserve compatibility, we undo this.
        if len(coord_system) == 1:
            mean = numpy.asarray([mean])

        return cls(
            coord_system,
            mean,
            covariance=covariance,
            inv_covariance=inv_covariance,
            transformations=transformations,
            truth=truth,
        )


    def likelihood(self, observables: Observables) -> Numeric:
        x = numpy.stack(observables, axis=-1)
        likelihood = self.dist.pdf(x)

        # TRIAGE: scipy.stats will squeeze dimensions when they have length 1.
        # This contradicts our API, so we put them back in.  In the future we
        # may want to write our own multivariate normal PDF.
        if x.shape[:-1] != likelihood.shape:
            # Put the dimensions back in by iterating over the dimensions of
            # ``x``, and whenever the dimension has 1 element, index it with
            # ``numpy.newaxis``, and whenever it has multiple, index it with
            # ``slice(None)``, which is the equivalent of ``:``.
            # Last dimension is not included, because that just represents the
            # different observables.
            idx = tuple(
                numpy.newaxis if n_elem == 1 else slice(None)
                for n_elem in x.shape[:-1]
            )
            likelihood = likelihood[idx]

        # If there are no conditions on the coordinates, return
        if not self.requires_cond:
            return likelihood

        # Determine where we need to zero-out the likelihood because it's
        # outside the bounds of the coordinate system.
        cond = numpy.ones_like(likelihood, dtype=bool)
        for coord, obs in zip(self.coord_system, observables):
            if coord.lower_limit is not None:
                cond &= coord.lower_limit <= obs
            if coord.upper_limit is not None:
                cond &= obs <= coord.upper_limit

        # Return the likelihood, zeroing out invalid regions.
        return numpy.where(cond, likelihood, 0.0)

    def posterior_samples(
            self,
            n_samples: int,
            random_state: typing.Optional[numpy.random.RandomState]=None,
        ) -> typing.Tuple[Observables, typing.Optional[Numeric]]:
        # Draw Gaussian distributed samples.
        base_samples = self.dist.rvs(n_samples)

        # If none of the coordinates have a cutoff, we're done here.
        if not self.requires_cond:
            return tuple(base_samples.T), None

        # At least one coordinate has an upper or lower cutoff.
        # Iteratively overwrite samples that are outside the condition's range.
        i_good = numpy.zeros((n_samples,), dtype=bool)
        while True:
            # Update array of bad samples.
            for i, coord in enumerate(self.coord_system):
                if coord.lower_limit is not None:
                    i_good &= coord.lower_limit <= base_samples[...,i]
                if coord.upper_limit is not None:
                    i_good &= base_samples[...,i] <= coord.upper_limit

            # Invert the boolean array so it represents the bad indices.
            # Do it in place.
            i_bad = numpy.invert(i_good, out=i_good)

            # Count how many samples were rejected.
            n_rejected = numpy.count_nonzero(i_bad)
            # If no samples were rejected, we're done here.
            if n_rejected == 0:
                break

            # Draw new samples to replace those that were rejected.
            new_samples = self.dist.rvs(n_rejected)
            base_samples[i_bad] = new_samples

            # On the next iteration, ensure that all samples are inspected again
            # for potential replacement.
            # TODO: Be more clever and keep track of which samples have already
            # been accepted.
            i_good[()] = True

        return tuple(base_samples.T), None


class GaussianProcessDetection(Detection):
    def __init__(
            self,
            coord_system: CoordinateSystem,
            gaussian_process,
            shift=0.0, scale=1.0,
            transformations: CoordinateTransforms=transformations_nil,
            truth: typing.Optional[numpy.ndarray]=None,
            backend="sklearn",
        ) -> None:
        super().__init__(coord_system, transformations, truth=truth)

        self.gaussian_process = gaussian_process
        self.backend = backend

        self.shift = numpy.broadcast_to(shift, len(coord_system))
        self.scale = scale

        # Determine whether any of the coordinate systems have limits that will
        # require sampling with replacement or setting the likelihood to zero.
        # TODO: Also keep track of precisely which coordinates that is true of,
        # so we don't have to iterate over all of them except when __init__ is
        # called.
        self.requires_cond = any(
            coord.lower_limit is not None for coord in coord_system
        ) or any(
            coord.upper_limit is not None for coord in coord_system
        )


    def likelihood(self, observables: Observables) -> Numeric:
        scaled_observables = [
            (obs-sft) / self.scale
            for obs, sft in zip(observables, self.shift)
        ]
        data = numpy.stack(scaled_observables, axis=-1)
        # Compute log-likelihood from the GP.
        if self.backend == "sklearn":
            log_likelihood = self.gaussian_process.predict(data)
        elif self.backend == "gp_api":
            log_likelihood = self.gaussian_process.mean(data)
        else:
            raise NotImplementedError(
                "Using unknown backend: {}".format(self.backend)
            )
        # Undo the log in the log-likelihood, in-place.
        likelihood = numpy.exp(log_likelihood, out=log_likelihood)

        # If there are no conditions on the coordinates, return
        if not self.requires_cond:
            return likelihood

        # Determine where we need to zero-out the likelihood because it's
        # outside the bounds of the coordinate system.
        cond = numpy.ones_like(likelihood, dtype=bool)
        for coord, obs in zip(self.coord_system, scaled_observables):
            if coord.lower_limit is not None:
                cond &= coord.lower_limit <= obs
            if coord.upper_limit is not None:
                cond &= obs <= coord.upper_limit

        # Return the likelihood, zeroing out invalid regions.
        return numpy.where(cond, likelihood, 0.0)


class RandomForestDetection(Detection):
    def __init__(
            self,
            coord_system: CoordinateSystem,
            random_forest,
            transformations: CoordinateTransforms=transformations_nil,
            truth: typing.Optional[numpy.ndarray]=None,
            backend="sklearn",
        ) -> None:
        super().__init__(coord_system, transformations, truth=truth)

        self.random_forest = random_forest
        self.backend = backend

        # Determine whether any of the coordinate systems have limits that will
        # require sampling with replacement or setting the likelihood to zero.
        # TODO: Also keep track of precisely which coordinates that is true of,
        # so we don't have to iterate over all of them except when __init__ is
        # called.
        self.requires_cond = any(
            coord.lower_limit is not None for coord in coord_system
        ) or any(
            coord.upper_limit is not None for coord in coord_system
        )

    def likelihood(self, observables: Observables) -> Numeric:
        data = numpy.stack(observables, axis=-1)
        # Compute log-likelihood from the GP.
        if self.backend == "sklearn":
            log_likelihood = self.random_forest.predict(data)
        else:
            raise NotImplementedError(
                "Using unknown backend: {}".format(self.backend)
            )
        # Undo the log in the log-likelihood, in-place.
        likelihood = numpy.exp(log_likelihood, out=log_likelihood)

        # If there are no conditions on the coordinates, return
        if not self.requires_cond:
            return likelihood

        # Determine where we need to zero-out the likelihood because it's
        # outside the bounds of the coordinate system.
        cond = numpy.ones_like(likelihood, dtype=bool)
        for coord, obs in zip(self.coord_system, observables):
            if coord.lower_limit is not None:
                cond &= coord.lower_limit <= obs
            if coord.upper_limit is not None:
                cond &= obs <= coord.upper_limit

        # Return the likelihood, zeroing out invalid regions.
        return numpy.where(cond, likelihood, 0.0)


class DetectionLikelihood(object):
    r"""
    Abstract base class for representing the integral

    .. math::
       \mathcal{D}_n(\Lambda) =
       \int p(d_n | \lambda) \rho(\lambda | \Lambda) \mathrm{d}\lambda

    which appears in the likelihood for population inference.  It can be thought
    of as the marginalized likelihood of a detection, under the prior assumption
    of a population model.

    Sub-classes must implement this integral by overriding the
    :py:meth:`__call__` method.  May be offset by a multiplicative constant,
    which cannot vary with the population parameters :math:`\Lambda`.
    """
    def __init__(
            self,
            population: Population, detection: Detection,
        ) -> None:
        self.population = population
        self.detection = detection.to_coords(population.coord_system)

    def __call__(
            self,
            parameters: Parameters, where: WhereType=True,
        ) -> Numeric:
        r"""
        Evaluates the integral

        .. math::
           \mathcal{D}_n(\Lambda) =
           \int p(d_n | \lambda) \rho(\lambda | \Lambda) \mathrm{d}\lambda

        using the :py:attr:`population` and :py:attr:`detection` attributes of
        this object.
        """
        raise NotImplementedError()


class DeterministicReweightedPosteriorsDetectionLikelihood(DetectionLikelihood):
    """
    Deterministic version of :py:class:`ReweightedPosteriorsDetectionLikelihood`
    which expects a :py:class:`PreSampledDetection`, and uses every posterior
    sample from it.
    """
    def __init__(
            self,
            population: Population, detection: PreSampledDetection,
        ) -> None:
        super().__init__(population, detection)

    def __call__(
            self,
            parameters: Parameters, where: WhereType=True,
        ) -> Numeric:
        integrand = self.population.intensity(
            self.detection.post_samples, parameters,
            where=where,
        )

        if self.detection.prior_probs is not None:
            integrand /= self.detection.prior_probs

        return integrand.sum(axis=-1)


class ReweightedPosteriorsDetectionLikelihood(DetectionLikelihood):
    r"""
    Concrete class for evaluating the integral

    .. math::
       \mathcal{D}_n(\Lambda) =
       \int p(d_n | \lambda) \rho(\lambda | \Lambda) \mathrm{d}\lambda

    by computing a Monte Carlo integral which draws samples from the detection's
    posterior distribution, :math:`p(\lambda | d_n)`.

    Uses Bayes' theorem to recast the integral as

    .. math::
       \int p(d_n | \lambda) \rho(\lambda | \Lambda) \mathrm{d}\lambda
       \propto
       \int
         \frac{p(\lambda | d_n)}{\pi_n(\lambda)}
         \rho(\lambda | \Lambda)
       \mathrm{d}\lambda,

    where :math:`\pi_n(\lambda)` is the reference prior assumed in the
    posterior.  The Monte Carlo integral is performed by drawing :math:`S`
    samples from the detection's posterior, :math:`\lambda_1`, ...,
    :math:`\lambda_S`, and evaluating

    .. math::
       \int
         \frac{p(\lambda | d_n)}{\pi_n(\lambda)}
         \rho(\lambda | \Lambda)
       \mathrm{d}\lambda
       \approx
       \frac{1}{S} \sum_{i=1}^S
         \frac{\rho(\lambda_i | \Lambda)}{\pi_n(\lambda_i)}.
    """
    def __init__(
            self,
            population: Population, detection: Detection,
            mc_integrator: MCIntegrator=AdaptiveMCIntegrator(),
        ) -> None:
        super().__init__(population, detection)
        self.mc_integrator = mc_integrator

    def integrand(
            self,
            samples: typing.Tuple[Observables, typing.Optional[Numeric]],
            parameters: Parameters,
            where: WhereType=True,
        ) -> Numeric:
        import numpy

        posterior_samples, prior_samples = samples

        rho_samples = self.population.intensity(
            posterior_samples, parameters,
            where=where,
        )

        if prior_samples is None:
            return rho_samples
        else:
            return numpy.divide(rho_samples, prior_samples, where=where)

    @vectorize_over_parameters_to_numeric(1, has_where=True)
    def __call__(
            self,
            parameters: Parameters, where: bool=True,
        ) -> Numeric:
        # If detection has a max number of samples, hack the integrator so it
        # uses all samples (this really needs to be done in a cleaner way).
        if self.detection.max_samples is not None:
            random_state_init = self.mc_integrator.random_state
            iter_max_init = self.mc_integrator.iter_max
            iter_start_init = self.mc_integrator.iter_start

            self.mc_integrator.random_state = 0
            self.mc_integrator.iter_max = self.detection.max_samples
            self.mc_integrator.iter_start = self.detection.max_samples

        output = self.mc_integrator(
            self.detection.posterior_samples, self.integrand,
            integrand_args=[parameters], integrand_kwargs={"where": where},
        )

        # Restore the integrator to its original state.
        if self.detection.max_samples is not None:
            self.mc_integrator.random_state = random_state_init
            self.mc_integrator.iter_max = iter_max_init
            self.mc_integrator.iter_start = iter_start_init

        return output


class PopulationMonteCarloDetectionLikelihood(DetectionLikelihood):
    r"""
    Concrete class for evaluating the integral

    .. math::
       \mathcal{D}_n(\Lambda) =
       \int p(d_n | \lambda) \rho(\lambda | \Lambda) \mathrm{d}\lambda

    by computing a Monte Carlo integral which draws samples from the
    population's PDF, :math:`p(\lambda | \Lambda)`, and uses the
    :py:meth:`Population.normalization` method to get the overall object rate
    :math:`\mathcal{R}`, needed to recast the integral as

    .. math::
       \int p(d_n | \lambda) \rho(\lambda | \Lambda) \mathrm{d}\lambda
       =
       \int
         p(d_n | \lambda) \,
         \mathcal{R} \, p(\lambda | \Lambda)
       \mathrm{d}\lambda

    The Monte Carlo integral is performed by drawing :math:`S`
    samples from the population's :py:meth:`Population.rvs` method,
    :math:`\lambda_1`, ..., :math:`\lambda_S`, and evaluating

    .. math::
       \int
         p(d_n | \lambda) \,
         \mathcal{R} \, p(\lambda | \Lambda)
       \mathrm{d}\lambda
       \approx
       \frac{1}{S} \sum_{i=1}^S
         \mathcal{R} \, p(d_n | \lambda_i)
    """
    def __init__(
            self,
            population: Population, detection: Detection,
            mc_integrator: MCIntegrator=AdaptiveMCIntegrator(),
        ) -> None:
        self.mc_integrator = mc_integrator
        super().__init__(population, detection)

    def integrand(
            self,
            samples: Observables,
            parameters: Parameters,
            where: WhereType=True,
        ) -> Numeric:

        return (
            self.population.normalization(parameters, where=where) *
            self.detection.likelihood(samples)
        )

    @vectorize_over_parameters_to_numeric(1, has_where=True)
    def __call__(self, parameters: Parameters, where: bool=True) -> Numeric:
        return self.mc_integrator(
            self.population.rvs, self.integrand,
            sampler_args=[parameters], sampler_kwargs={"where": where},
            integrand_args=[parameters], integrand_kwargs={"where" : where},
        )


_jacobian_type = typing.Callable[[Observables],numpy.ndarray]
class BasicAdaptivePopulationMonteCarloDetectionLikelihood(DetectionLikelihood):
    r"""
    Concrete class for evaluating the integral

    .. math::
       \mathcal{D}_n(\Lambda) =
       \int p(d_n | \lambda) \rho(\lambda | \Lambda) \mathrm{d}\lambda

    by computing an adaptive Monte Carlo integral which ... TODO
    """
    def __init__(
            self,
            population: ConditionalPopulation, detection: Detection,
            bounding_box: typing.Tuple[typing.Tuple[float,float],...],
            adaptive_coord_system: CoordinateSystem,
            jacobian_fn: typing.Optional[_jacobian_type]=None,
            n_samples: int=1024,
            random_state: numpy.random.RandomState=numpy.random.RandomState(),
        ) -> None:
        # Validate inputs
        if len(bounding_box) != len(adaptive_coord_system):
            raise ValueError(
                "Bounding box must have one pair of bounds for each coordinate "
                "in the adaptive coord system.  Got {} but expected {}."
                .format(len(bounding_box), len(adaptive_coord_system))
            )
        for bound_pair in bounding_box:
            if len(bound_pair) != 2:
                raise ValueError(
                    "Each set of bounds must be a pair, got {} for one."
                    .format(len(bound_pair))
                )

        super().__init__(population, detection)

        # Store parameters not used by superclass.
        self._n_samples = n_samples
        self._bounding_box = bounding_box
        self._jacobian_fn = jacobian_fn
        self.random_state = random_state
        # Pre-compute the normalization constant for the sampling distribution.
        self._prob_const = (
            numpy.prod(numpy.diff(bounding_box, axis=1)) / n_samples
        )
        self._adaptive_coord_system = adaptive_coord_system

        # Determine the appropriate transformation from the original adaptive
        # coordinates to coord_system_y.
        self._transf_box_to_y = detection.transformations[
            adaptive_coord_system,
            population.coord_system_y,
        ]


    def __call__(self, parameters: Parameters, where: bool=True) -> Numeric:
        # Measure shapes
        params_shape = next(iter(parameters.values())).shape
        out_shape = params_shape + (self._n_samples,)

        # Broadcast ``where`` into an array matching the output shape.
        where_arr = (
            numpy.broadcast_to(numpy.transpose(where), reversed(params_shape)).T
        )

        ## TODO: The uniform samples are not necessarily in coord_system_y.
        ## Draw them as-is, and then transform to that system using the cached
        ## transformation function.
        # Draw adaptive samples in y-dimensions.
        observables_adapt = tuple(
            self.random_state.uniform(lower, upper, self._n_samples)
            for lower, upper in self._bounding_box
        )
        observables_y = self._transf_box_to_y(observables_adapt)

        # Compute the marginal intensity function for y.
        intensity_y = self.population.marg_intensity(
            observables_y, parameters,
            where=where,
        )
        # Determine where the intensity function is non-zero.  We will avoid
        # drawing samples anywhere it is zero.
        i_support = intensity_y != 0.0

        # Draw samples from p(x | y). TODO: vectorize
        observables_x = tuple(
            numpy.full(out_shape, fill_value=coord.fill_value, dtype=float)
            for coord in self.population.coord_system_x
        )
        for idx in numpy.ndindex(*params_shape):
            if not where_arr[idx]:
                continue

            i_support_at_idx = i_support[idx]
            parameters_at_idx = {
                name : values[idx]
                for name, values in parameters.items()
            }
            observables_y_at_idx = tuple(
                obs[i_support_at_idx] for obs in observables_y
            )
            observables_x_at_idx = self.population.cond_rvs(
                observables_y_at_idx, parameters_at_idx,
                random_state=self.random_state,
            )

            for obs_x, obs_x_out in zip(observables_x_at_idx, observables_x):
                obs_x_out[idx][i_support_at_idx] = obs_x


        # Broadcast arrays in observables_y to take full output shape.
        observables_y = tuple(
            numpy.broadcast_to(obs, out_shape)
            for obs in observables_y
        )
        # Combine all observables.
        observables = observables_x + observables_y

        likelihood = self.detection.likelihood(observables)

        if self._jacobian_fn is None:
            coeff = self._prob_const
        else:
            coeff = self._prob_const / self._jacobian_fn(observables_adapt)

        result = (
            coeff *
            intensity_y *
            likelihood
        ).sum(axis=-1)

        if debug_verbose.check("detection_likelihood"):
            # Print header
            header_items = [
                "result",
            ] + [
                name for name in parameters.keys()
            ] + [
                "intensity", "likelihood", "coeff",
            ] + [
                coord.name for coord in self._adaptive_coord_system
            ] + [
                coord.name for coord in self.population.coord_system
            ]
            debug_verbose(*header_items, sep="\t")

            for param_idx in numpy.ndindex(*params_shape):
                first_item = True
                for obs_idx in range(self._n_samples):
                    if first_item:
                        first_item = False
                        line = (
                            [result[param_idx]] +
                            [val[param_idx] for val in parameters.values()]
                        )
                    else:
                        line = ["----"] * (len(parameters)+1)

                    line += [
                        intensity_y[param_idx][obs_idx],
                        likelihood[param_idx][obs_idx],
                        coeff[obs_idx],
                    ] + [
                        obs[obs_idx] for obs in observables_adapt
                    ] + [
                        obs[param_idx][obs_idx] for obs in observables
                    ]

                    debug_verbose(*line, sep="\t", flush=True)

        return result
