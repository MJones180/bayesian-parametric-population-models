"""
Tools for specifying arbitrary bins for other computations.
"""
from abc import ABC, abstractmethod
import typing
import operator
from functools import reduce
import numpy as np
from pop_models.types import Observables, Parameters
from pop_models.coordinate import Coordinate, CoordinateSystem

class Bin(ABC):
    def __init__(self, coord_system: CoordinateSystem) -> None:
        self._coord_system = coord_system

    @property
    def coord_system(self) -> CoordinateSystem:
        return self._coord_system

    @abstractmethod
    def contains(self, observables: Observables) -> np.ndarray:
        ...

    def __or__(self, other: "Bin") -> "UnionBins":
        return UnionBin(self, other)

    def __mul__(self, other: "Bin") -> "ProductBins":
        return ProductBin(self, other)


class UnionBin(Bin):
    def __init__(self, *bins: Bin) -> None:
        # Must have at least one Bin
        if len(bins) == 0:
            raise ValueError("Must provide at least one bin.")

        # Break up any UnionBin objects passed in
        ## TODO

        # Validate coordinate system.
        coord_system = bins[0].coord_system
        for b in bins[1:]:
            if b.coord_system != coord_system:
                raise ValueError("Bins did not have same coordinate systems.")

        super().__init__(coord_system)

        self._components = bins

    @property
    def components(self) -> typing.Tuple[Bin,...]:
        return self._components

    def contains(self, observables: Observables) -> np.ndarray:
        return reduce(
            operator.or_,
            (b.contains(observables) for b in self.components),
        )

    def __repr__(self) -> str:
        return " | ".join(map(str, self.components))


class ProductBin(Bin):
    def __init__(self, *bins: Bin) -> None:
        # Must have at least one Bin
        if len(bins) == 0:
            raise ValueError("Must provide at least one Bin.")

        # Break up any ProductBin objects passed in
        ## TODO

        # Validate coordinate systems all non-overlapping.
        n_uniq_coords = len(
            reduce(operator.or_, (set(b.coord_system) for b in bins))
        )
        n_coords = sum(len(b.coord_system) for b in bins)
        if n_coords != n_uniq_coords:
            raise ValueError(
                "Some coordinates were repeated, all must be unique."
            )

        # Coordinate system is the concatenation of all input bins' coordinate
        # systems.
        coord_system = sum(b.coord_system for b in bins)

        super().__init__(coord_system)

        self._components = bins

    @property
    def components(self) -> typing.Tuple[Bin,...]:
        return self._components

    def contains(self, observables: Observables) -> np.ndarray:
        return reduce(
            operator.and_,
            (b.contains(observables) for b in self.components),
        )

    def __repr__(self) -> str:
        return " * ".join(map(str, self.components))


class BoxBin(Bin):
    def __init__(
        self,
        coord_system: CoordinateSystem,
        lower: typing.Tuple[float,...], upper: typing.Tuple[float,...],
    ) -> None:
        super().__init__(coord_system)

        # Validate correct number of bounds.
        if len(lower) != len(coord_system):
            raise ValueError(
                "Lower bounds must have one value per coordinate.  "
                "Expected {}, got {}."
                .format(len(coord_system), len(lower))
            )
        if len(upper) != len(coord_system):
            raise ValueError(
                "Upper bounds must have one value per coordinate.  "
                "Expected {}, got {}."
                .format(len(coord_system), len(upper))
            )

        # Store lower and upper
        self._lower = lower
        self._upper = upper

        # Store bounds, paired up instead of being separated into lower/upper.
        self._bounds = tuple(zip(lower, upper))

        # Validate that each pair of bounds is sorted properly.
        for i, (l, u) in enumerate(self.bounds):
            if l >= u:
                raise ValueError(
                    "Bounds for coordinate {} are unsorted: {} >= {}"
                    .format(coord_system[i].name, l, u)
                )

    @property
    def bounds(self) -> typing.Tuple[typing.Tuple[float,float],...]:
        return self._bounds

    def contains(self, observables: Observables) -> np.ndarray:
        result_per_coord = (
            (obs >= l) & (obs <= u)
            for obs, (l, u) in zip(observables, self.bounds)
        )
        return reduce(operator.and_, result_per_coord)

    def __repr__(self) -> str:
        return (
            "Box({coords}: {lower}; {upper})"
            .format(
                coords=", ".join(coord.name for coord in self.coord_system),
                lower=", ".join(map(str, self._lower)),
                upper=", ".join(map(str, self._upper)),
            )
        )


supported_bins = {
    "Box" : BoxBin,
}


class BinFactory:
    def __init__(self, name_to_coord: typing.Dict[str, Coordinate]) -> None:
        self._name_to_coord = name_to_coord
        self._init_parser()

        # Initialize empty result
        self._result = {}

    def __call__(self, bin_spec: str) -> Bin:
        """
        Constructs a bin by parsing a string.
        """
        # Initialize empty result.
        self._result = {}

        # Accumulate all assignments into self._result
        for line in bin_spec.splitlines():
            # Skip whitespace and comments
            line_pre_comment = line.split("#")[0]
            if line_pre_comment.strip() == "":
                continue
            self._line_parser.parseString(line_pre_comment)

        # Clear out self._result to avoid side effects, and store in local var
        result = self._result
        self._result = {}

        # Return result.
        return result


    def _resolve_assignment(self, tokens) -> typing.Tuple[str, Bin]:
        identifier, value = tokens
        self._result[identifier] = value
        return tokens

    def _resolve_base_bin(self, parts) -> Bin:
        cls, coord_system, *values = parts
        return cls(coord_system, *values)

    def _resolve_coord_names(self, coord_names) -> CoordinateSystem:
        coords = (self._name_to_coord[name] for name in coord_names)
        return CoordinateSystem(*coords)

    def _resolve_floats(self, float_strs) -> typing.List[float]:
        return [float(s) for s in float_strs]

    def _resolve_product(self, tokens) -> ProductBin:
        factors = tokens[0]
        A, B = factors
        return A * B

    def _resolve_union(self, tokens) -> UnionBin:
        summands = tokens[0]
        A, B = summands
        return A | B

    def _init_parser(self):
        """
        Constructs a parser using PyParsing.
        """
        from pyparsing import (
            Literal, Keyword, Word, Group, Forward,
            delimitedList, infixNotation,
            alphas, alphanums, nums, opAssoc,
            pyparsing_common,
        )

        # Define special characters.
        lparen = Literal("(").suppress()
        rparen = Literal(")").suppress()
        colon = Literal(":").suppress()
        semicolon = Literal(";").suppress()
        equals = Literal("=").suppress()
        product = Literal("*").suppress()
        union = Literal("|").suppress()

        # Bin identifiers are keywords
        bin_id_keywords = (
            Keyword(name).setParseAction(lambda _: cls)
            for name, cls in supported_bins.items()
        )
        bin_id = reduce(operator.or_, bin_id_keywords)

        # Define coordinate names as keywords, and coordinate systems as lists
        # of said keywords.
        coord = reduce(
            operator.or_,
            (Keyword(name) for name in self._name_to_coord.keys()),
        )
        coord_system = delimitedList(coord)
        coord_system.setParseAction(self._resolve_coord_names)

        # Comma-separated list of numbers.
        fnumber_list = delimitedList(pyparsing_common.fnumber)
        fnumber_list.setParseAction(self._resolve_floats)

        # Initialize a bin by listing the coordinate system followed by a colon,
        # then a semicolon-delimited list of lists, with each sub-list
        # specifying some numbers.
        # For example, specify a Box spanning (x,y) = (0,0) to (4,5) with
        #   Box(x, y: 0, 0; 4, 5)
        args = (
            coord_system + colon +
            delimitedList(Group(fnumber_list), delim=";")
        )
        base_bin = bin_id + lparen + args + rparen
        base_bin.setParseAction(self._resolve_base_bin)

        # Can combine bins spanning independent coordinates with `*`, and
        # combine bins spanning the same coordinates with `|`.  Operator
        # precedence follows the same as for multiplication and addition.
        total_bin = infixNotation(
            base_bin,
            [
                (product, 2, opAssoc.LEFT, self._resolve_product),
                (union, 2, opAssoc.LEFT, self._resolve_union),
            ],
        )

        # Each total bin must be assigned to a label
        bin_label = Word(alphas+"_", alphanums+"_")
        assignment = bin_label + equals + total_bin
        assignment.setParseAction(self._resolve_assignment)

        self._line_parser = assignment


def make_parser():
    """
    Command line parser for plotting bins.
    """
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "bin_spec",
        help="File specifying bins.",
    )

    parser.add_argument(
        "x_name",
        help="Name of x-coordinate.",
    )
    parser.add_argument(
        "y_name",
        help="Name of y-coordinate.",
    )

    parser.add_argument(
        "output",
        help="Filename of plot to produce.",
    )

    parser.add_argument(
        "--n-grid-points",
        metavar=("NUM_X", "NUM_Y"),
        type=int, nargs=2, default=[100, 100],
        help="Number of grid points for each coordinate.",
    )

    parser.add_argument(
        "--x-lim",
        metavar=("X_MIN", "X_MAX"),
        type=float, nargs=2, default=[0.0, 1.0],
        help="Range of values for the x-coordinate.",
    )
    parser.add_argument(
        "--y-lim",
        metavar=("Y_MIN", "Y_MAX"),
        type=float, nargs=2, default=[0.0, 1.0],
        help="Range of values for the y-coordinate.",
    )

    # TODO: add options for log-scaling

    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib (default is Agg).",
    )

    return parser


def main():
    cli_parser = make_parser()
    cli_args = cli_parser.parse_args()

    import matplotlib as mpl
    mpl.use(cli_args.mpl_backend)
    import matplotlib.pyplot as plt

    coord_x = Coordinate(cli_args.x_name)
    coord_y = Coordinate(cli_args.y_name)

    name_to_coord = {
        coord_x.name: coord_x,
        coord_y.name: coord_y,
    }

    bin_parser = BinFactory(name_to_coord)
    with open(cli_args.bin_spec, "r") as bin_spec_file:
        bin_lookup = bin_parser(bin_spec_file.read())

    n_points_x, n_points_y = cli_args.n_grid_points
    x_min, x_max = cli_args.x_lim
    y_min, y_max = cli_args.y_lim

    x_grid = np.linspace(x_min, x_max, n_points_x)
    y_grid = np.linspace(y_min, y_max, n_points_y)

    x_mesh, y_mesh = meshes = np.meshgrid(x_grid, y_grid)

    contained_lookup = {
        label : b.contains(meshes)
        for label, b in bin_lookup.items()
    }

    fig, ax = plt.subplots(figsize=(5,4), constrained_layout=True)

    for i, (label, contained) in enumerate(contained_lookup.items()):
        color = "C{}".format(i)
        ax.scatter(
            x_mesh[contained], y_mesh[contained],
            color=color, s=2, label=label,
        )

    fig.legend()

    fig.savefig(cli_args.output)
