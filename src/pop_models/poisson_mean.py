__all__ = [
    "PoissonMean",
    "SummedPoissonMean",
    "VolumeIntegralPoissonMean",
    "MonteCarloVolumeIntegralPoissonMean",
]

import pop_models
from pop_models.optimization import jit
from pop_models.utils import debug_verbose

from .integrators import MCIntegrator, AdaptiveMCIntegrator
from .population import Population
from .types import (
    Numeric, WhereType, Observables, Parameters,
    vectorize_over_parameters_to_numeric,
)
from .volume import SensitiveVolume

import numpy

class PoissonMean(object):
    r"""
    Abstract representation of the average number of detections one would obtain
    from some population, for a given survey.  Sub-classes must determine the
    nature of the survey, and implement the evaluation.
    """
    def __init__(
            self,
            population: Population,
        ) -> None:
        self.population = population

    def __call__(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        raise NotImplementedError()


class SummedPoissonMean(PoissonMean):
    r"""
    Combines multiple :class:`PoissonMean` objects through summation.
    """
    def __init__(
            self,
            *poisson_mean_parts: PoissonMean,
        ) -> None:
        self._poisson_mean_parts = poisson_mean_parts

    @property
    def poisson_mean_parts(self):
        return self._poisson_mean_parts

    def __call__(
            self,
            parameters: Parameters,
            where: WhereType=True,
            **kwargs
        ) -> Numeric:
        return sum(
            poisson_mean(parameters, where=where, **kwargs)
            for poisson_mean in self.poisson_mean_parts
        )


class VolumeIntegralPoissonMean(PoissonMean):
    r"""
    Abstract representation of the average number of detections one would obtain
    from some population, for a given survey, which explores some abstract
    volume, where the effective volume may be a function of the population's
    observables.  Sub-classes must choose how to implement the integral, but it
    should take the form

    .. math::
       \mu(\Lambda; \mathcal{S}) =
       \int
         \mathcal{V}(\lambda; \mathcal{S}) \,
         \rho(\lambda | \Lambda)
       \mathrm{d}\lambda
    """
    def __init__(
            self,
            population: Population, volume: SensitiveVolume,
        ) -> None:
        self.volume = volume
        super().__init__(population.to_coords(volume.coord_system))


class MonteCarloVolumeIntegralPoissonMean(VolumeIntegralPoissonMean):
    r"""
    Concrete class for computing the average number of detections one would
    obtain from some population, for a given survey, which explores some
    abstract volume, where the effective volume may be a function of the
    population's observables.  Computes the integral by taking Monte Carlo draws
    from the population's PDF, and re-scaling by its normalization
    :math:`\mathcal{R}`

    .. math::
       \mu(\Lambda; \mathcal{S}) =
       \int
         \mathcal{V}(\lambda; \mathcal{S}) \,
         \rho(\lambda | \Lambda)
       \mathrm{d}\lambda =
       \int
         \mathcal{V}(\lambda; \mathcal{S}) \,
         \mathcal{R} \, p(\lambda | \Lambda)
       \mathrm{d}\lambda \approx
       \frac{1}{S} \sum_{i=1}^S
         \mathcal{V}(\lambda_i; \mathcal{S}) \, \mathcal{R}

    where the :math:`\lambda_1`, ..., :math:`\lambda_S` are drawn from the
    population PDF.
    """
    def __init__(
            self,
            population: Population,
            volume: SensitiveVolume,
            mc_integrator: MCIntegrator=AdaptiveMCIntegrator(),
        ) -> None:
        self.mc_integrator = mc_integrator
        super().__init__(population, volume)

    @vectorize_over_parameters_to_numeric(1, has_where=True)
    @jit(parallel=True, forceobj=True)
    def __call__(
            self,
            parameters: Parameters,
            where: bool=True,
            **kwargs
        ) -> Numeric:
        return self.mc_integrator(
            self.mc_samples, self.mc_function,
            sampler_args=[parameters], integrand_args=[parameters],
            sampler_kwargs={"where": where},
        )


    def mc_samples(
            self,
            n_samples: int, parameters: Parameters,
            where: WhereType=True,
            random_state: numpy.random.RandomState=numpy.random.RandomState(),
        ) -> Observables:
        return self.population.rvs(
            n_samples, parameters,
            where=where,
            random_state=random_state,
        )


    def mc_function(
            self,
            observables: Observables, parameters: Parameters,
        ) -> Numeric:
        import numpy

        norm = self.population.normalization(parameters)
        V = self.volume(observables)

        return numpy.transpose(numpy.transpose(norm) * numpy.transpose(V))


class MonteCarloVolumeIntegralFixedSamplePoissonMean(VolumeIntegralPoissonMean):
    r"""
    Concrete class for computing the average number of detections one would
    obtain from some population, for a given survey, which explores some
    abstract volume, where the effective volume may be a function of the
    population's observables.  Computes the integral by taking Monte Carlo draws
    from the population's PDF, and re-scaling by its normalization
    :math:`\mathcal{R}`

    .. math::
       \mu(\Lambda; \mathcal{S}) =
       \int
         \mathcal{V}(\lambda; \mathcal{S}) \,
         \rho(\lambda | \Lambda)
       \mathrm{d}\lambda =
       \int
         \mathcal{V}(\lambda; \mathcal{S}) \,
         \mathcal{R} \, p(\lambda | \Lambda)
       \mathrm{d}\lambda \approx
       \frac{1}{S} \sum_{i=1}^S
         \mathcal{V}(\lambda_i; \mathcal{S}) \, \mathcal{R}

    where the :math:`\lambda_1`, ..., :math:`\lambda_S` are drawn from the
    population PDF.
    """
    def __init__(
            self,
            population: Population,
            volume: SensitiveVolume,
            n_samples: int=4096,
            random_state: numpy.random.RandomState=numpy.random.RandomState()
        ) -> None:
        super().__init__(population, volume)
        self.n_samples = n_samples
        self.random_state = random_state

    def __call__(
            self,
            parameters: Parameters,
            where: bool=True,
            **kwargs
        ) -> Numeric:
        debug_verbose(
            "Drawing", self.n_samples, "samples from the population",
            mode="poisson_mean", flush=True,
        )
        observables = self.population.rvs(
            self.n_samples, parameters,
            where=where,
            random_state=self.random_state,
        )
        debug_verbose(
            "Samples drawn from the population:", observables,
            mode="poisson_mean", flush=True,
        )

        debug_verbose(
            "Computing normalization",
            mode="poisson_mean", flush=True,
        )
        norm = self.population.normalization(parameters)
        debug_verbose(
            "Computed normalization:", norm,
            mode="poisson_mean", flush=True,
        )

        debug_verbose(
            "Computing volume",
            mode="poisson_mean", flush=True,
        )
        V = self.volume(observables)
        debug_verbose(
            "Volume computed:", V,
            mode="poisson_mean", flush=True,
        )

        debug_verbose(
            "Evaluating mean",
            mode="poisson_mean", flush=True,
        )
        result = _eval_MonteCarloVolumeIntegralFixedSamplePoissonMean(norm, V)
        result /= self.n_samples
        debug_verbose(
            "Mean evaluated to:", result,
            mode="poisson_mean", flush=True,
        )

        return result

if pop_models.get_backend() == "numpy":
    def _eval_MonteCarloVolumeIntegralFixedSamplePoissonMean(norm, V):
        return numpy.sum(
            numpy.transpose(numpy.transpose(norm) * numpy.transpose(V)),
            axis=-1,
        )
elif pop_models.get_backend() == "numba":
    @jit(nopython=True)
    def _eval_MonteCarloVolumeIntegralFixedSamplePoissonMean(norm, V):
        return (norm.T * V.T).T.sum(axis=-1)


class FiducialMonteCarloVolumeIntegralFixedSamplePoissonMean(VolumeIntegralPoissonMean):
    r"""
    """
    def __init__(
            self,
            population: Population,
            volume: SensitiveVolume,
            population_fiducial: Population, parameters_fiducial: Parameters,
            n_samples: int=4096,
            random_state: numpy.random.RandomState=numpy.random.RandomState()
        ) -> None:
        if population.coord_system != population_fiducial.coord_system:
            raise ValueError(
                "`population` and `population_fiducial` must have same "
                "coordinate systems."
            )

        super().__init__(population, volume)
        self._n_samples = n_samples
        self._population_desired = population
        self._population_fiducial = population_fiducial
        self._parameters_fiducial = parameters_fiducial

        # Store the RNG state.
        self.random_state = random_state

        # Draw set of fiducial observable samples.
        self._observables_fiducial = self.population_fiducial.rvs(
            self.n_samples, self.parameters_fiducial,
            random_state=self.random_state,
        )
        # Evaluate PDF at fiducial observable samples.
        self._pdf_at_observables_fiducial = self.population_fiducial.pdf(
            self.observables_fiducial, self.parameters_fiducial,
        )
        # Evaluate volume at fiducial observable samples.
        transf = population.transformations[
            population.coord_system, volume.coord_system,
        ]
        self._volume_at_observables = self.volume(
            transf(self.observables_fiducial)
        )

    @property
    def n_samples(self) -> int:
        return self._n_samples

    @property
    def population_desired(self) -> Population:
        return self._population_desired

    @property
    def population_fiducial(self) -> Population:
        return self._population_fiducial

    @property
    def parameters_fiducial(self) -> Parameters:
        return self._parameters_fiducial

    @property
    def observables_fiducial(self) -> Observables:
        return self._observables_fiducial

    @property
    def pdf_at_observables_fiducial(self) -> Numeric:
        return self._pdf_at_observables_fiducial

    @property
    def volume_at_observables(self) -> Numeric:
        return self._volume_at_observables


    def __call__(
            self,
            parameters: Parameters,
            where: bool=True,
            **kwargs
        ) -> Numeric:
        debug_verbose(
            "Evaluating desired population PDF at fiducial samples",
            mode="poisson_mean", flush=True,
        )
        pdf_at_observables_desired = self.population_desired.pdf(
            self.observables_fiducial, parameters,
            where=where, **kwargs
        )
        # Reuse this array for final output.
        result = pdf_at_observables_desired

        debug_verbose(
            "Computing ratio of desired and fiducial PDFs",
            mode="poisson_mean", flush=True,
        )
        weights = numpy.divide(
            pdf_at_observables_desired, self.pdf_at_observables_fiducial,
            out=result,
        )
        del pdf_at_observables_desired

        # Multiply on the volume term.
        result *= self.volume_at_observables

        debug_verbose(
            "Computing normalization",
            mode="poisson_mean", flush=True,
        )
        norm = self.population.normalization(parameters)
        debug_verbose(
            "Computed normalization:", norm,
            mode="poisson_mean", flush=True,
        )

        # Multiply on the normalization term.
        result *= norm[...,numpy.newaxis]

        # Compute the average over the Monte Carlo samples.
        return numpy.mean(result, axis=-1)
