__all__ = ['Prior']

import json
import numpy as np
import re
import types
import typing
from pop_models.astro_models.building_blocks.beta import BetaPopulation
from pop_models.astro_models.coordinates import chi1_coord, chi2_coord
from pop_models.coordinate import CoordinateSystem
from pop_models.posterior import get_params as posterior_get_params
from pop_models.prior import KDEPriorHelper
from pop_models.utils import debug_verbose, format_set_in_quotes
from .prior_distributions import Uniform, LogUniform, Gaussian, KDE


class Prior:
    def __init__(
        self,
        random_state: np.random.RandomState,
        population,
        settings,
        constants={},
        duplicates={},
        xpy: types.ModuleType = np,
        distributions={
            'gaussian': Gaussian,
            'uniform': Uniform,
            'log-uniform': LogUniform,
            'kde': KDE,
        },
        **kwargs,
    ):
        """
        Base Prior class that the driver processes use.

        Parameters
        ----------
        random_state : RandomState
            Object for generating random seeded numbers.
        population : population object
            The underlying population model.
        settings : dict
            Mapping of parameter names to their distribution and param values.
            Contains all variables, constants, and duplicates.
        constants : dict
            Mapping of parameter names to their constant values.
        duplicates : dict
            Mapping of parameter names to the parameter they're duplicating.
        xpy : numpy or numba
            Either a 'numpy' or 'numba' object.
        distributions : dist
            Mapping of distribution names to their corresponding classes.
        **kwargs : dict, optional
            Extra arguments that are passed to the Prior class.

        Notes
        -----
        Variables are another name for the subset of all parameters that are
        not either constants or duplicates.
        """
        self._rand_state = random_state
        self._population = population
        self._constants = constants
        self._duplicates = duplicates
        self._xpy = xpy
        # Names of all params; should match up to the keys in `settings`
        self._param_names = population.param_names
        # Names of all constants and duplicates, 'set' for O(1) existance check
        self._params_to_skip = set(list(constants) + list(duplicates))
        # Names of all params that are not constants or duplicates
        self._var_names = tuple(param for param in self._param_names
                                if param not in self._params_to_skip)
        # Mapping of variables to their individual prior distributions,
        # prevents having to check variable's distribution every time
        self._var_dist_map = {
            var: distributions[dist](
                var,
                params,
                xpy,
                # The KDE distribution requires a special parameter; will
                # default to None if the KDE distribution is not being used,
                # thus will have no effect on the distribution
                kde_helper=(KDEPriorHelper(params) if dist == 'kde' else None),
            )
            for var, dist, params in ((
                var,
                settings[var]['dist'],
                settings[var]['params'],
            ) for var in self._var_names)
        }
        # Setup spin_info for the BetaPopulation hack; used in `sample_prior`
        self._spin_info = {}
        self._setup_spin_info()

    def _setup_spin_info(self):
        """
        Pre-computes data for initializing spin magnitude distributions.

        Specifically, this maps each pair of spin magnitude parameter names
        (mean and variance for a given subpopulation and binary component) to
        their respective BetaPopulation object.

        Sets up the `self._spin_info` attribute.

        Notes
        -----
        This is a hacky way of doing it. In the future, should be implemented
        directly in the BetaPopulation Class.
        """
        def grab_suffixes(name):
            """
            Grab all of the suffixes that match a given identifier in the
            parameter names.

            Parameters
            ----------
            name : str
                Identifier to check for in the parameter names.

            Returns
            -------
            tuple of str
                All of the found matching suffixes.
            """
            # Split out just the suffixes
            suffixes = [p.split('_')[-1] for p in self._param_names]
            # Grab the int value associated with each suffix
            counts = [int(p[len(name):]) for p in suffixes if name in p]
            # Handle the case of no values found
            if counts == []:
                return tuple()
            return tuple(f'_{name}{i}' for i in range(max(counts) + 1))

        # Iterate over each subpopulation suffix for both 'pl' and 'g'
        for suffix in (grab_suffixes('pl') + grab_suffixes('g')):
            # Get the subpopulation
            sub_pop = self._population.sub_populations[
                self._population.suffixes.index(suffix)]
            # Iterate over the chi1 and chi2 components of the subpopulation
            for comp, coord in enumerate((chi1_coord, chi2_coord)):
                # The component number and suffix
                ending = f'{comp}{suffix}'
                # Names of the parameters
                param_names = (f'E_chi{ending}', f'Var_chi{ending}')
                # Get the separable part of the subpopulation corresponding to
                # the given spin component
                coords = sub_pop.to_coords(CoordinateSystem(coord))
                spin_pop = typing.cast(BetaPopulation, coords)
                debug_verbose(
                    ('Storing spin parameters and population: '
                     f'{param_names} {spin_pop}'),
                    mode='prior',
                )
                self._spin_info[param_names] = spin_pop

    def log_prior(self, params):
        """
        Evaluates the log(prior) for the given population params.

        Parameters
        ----------
        params : str
            Mapping of parameter names to their values.

        Returns
        -------
        ndarray
            The accumulated log(prior), or -inf where there's no support.
        """
        shape = next(iter(params.values())).shape
        # Initialize an empty array for the prior values to take the log of
        prior = self._xpy.zeros(shape, dtype=np.float64)
        # Grab the array of valid parameters
        valid = self._population.params_valid(params)
        # Create an out array to save time on having to allocate new arrays
        tmp = self._xpy.empty_like(valid)
        for name, values in params.items():
            # Skip if constant or duplicate
            if name in self._params_to_skip:
                continue
            # Distribution of the current parameter
            dist = self._var_dist_map[name]
            prior, valid = dist.log_prior(values, prior, valid, tmp)
        # Return the accumulated log(prior), or -inf where there's no support
        return self._xpy.where(valid, prior, self._xpy.NINF)

    def sample_prior(self, sample_count):
        """
        Iterate over all free params and draw samples from their priors.

        Parameters
        ----------
        sample_count : int
            Number of samples to draw.

        Returns
        -------
        tuple
            Array where each column holds the values from one parameter.
        """
        # Separately initialize spin magnitude parameters
        special_params = {}
        for (E_chi_name, Var_chi_name), pop in self._spin_info.items():
            samples = pop.random_mean_variance(sample_count, self._rand_state)
            special_params[E_chi_name] = samples[0]
            special_params[Var_chi_name] = samples[1]
        return tuple(
            # Attempt to use the special params, but default to the dist obj
            special_params.get(
                name,
                dist.sample_prior(sample_count, self._rand_state),
            ) for (name, dist) in self._var_dist_map.items())

    def init_walkers(self, walkers_total, pop_inf=None):
        """
        Draw samples from the pop prior distribution to initialize the walkers.

        Parameters
        ----------
        walkers_total : int
            Number of walkers to initialize.
        pop_inf : posterior object, optional
            Inference object for vetoing initial values that are not in the
            posterior's support.

        Returns
        -------
        ndarray
            The initializing variables as a column array.
        """
        def debug_out(message):
            """
            Function to reduce logging boilerplate.

            Parameters
            ----------
            message : str
                Message to print to the terminal.
            """
            debug_verbose(message, mode='mcmc_init', flush=True)

        # Initialize index
        start_idx = 0
        # Number of walkers left that need initializing
        walkers_left = walkers_total
        # Number of variables
        n_dim = len(self._var_names)
        # Tuple of arrays of variables to initialize with
        variables_init = tuple(np.empty(walkers_total) for _ in range(n_dim))
        # Keep drawing new candidate initialization states from the prior,
        # rejecting those which don't meet our requirements, until all walkers
        # have been initialized
        while walkers_left > 0:
            debug_out(f'Initializing {walkers_left}/{walkers_total} walkers')
            # Draw `walkers_total` new candidate variables, regardless of how
            # many we still need
            vars_init_trial = self.sample_prior(walkers_total)
            debug_out(f'Trial variables: {vars_init_trial}')
            # Convert variables to full set of parameters, including constants
            # and duplicates, and check which have support, and how many new
            # samples we will keep
            params_trial = posterior_get_params(
                np.column_stack(vars_init_trial),
                self._constants,
                self._duplicates,
                self._param_names,
            )
            debug_out('Validating parameters')
            if pop_inf is None:
                # Return the log(prior)
                valid = self.log_prior(params_trial)
            else:
                # Return the log(posterior)
                valid = pop_inf.log_posterior_from_params(params_trial)[..., 0]
            valid = np.isfinite(valid)
            valid_count = np.count_nonzero(valid)
            debug_out(f'{valid_count} parameters are valid')
            # If all candidate variables are invalid, skip to next iteration
            if valid_count == 0:
                continue
            # Number of samples to keep; only keep the minimum amount needed
            to_keep = min(valid_count, walkers_left)
            debug_out(f'{to_keep} parameters to be kept')
            # For each variable, we append the new samples
            for i in range(n_dim):
                variables_init[i][start_idx:start_idx + to_keep] = (
                    vars_init_trial[i][valid][:to_keep])
            # Update bookkeeping
            start_idx += to_keep
            walkers_left -= to_keep
        # Return the initializing variables as a column array.
        return np.column_stack(variables_init)

    @staticmethod
    def parse_param_names(
        param_names,
        variables_file,
        constants_file=None,
        duplicates_file=None,
    ):
        """
        Parse the files containing the param names.

        Parameters
        ----------
        param_names : array
            Names of all population parameters.
        variables_file : str
            Path to the file containing the variables.
        constants_file : str, optional
            Path to the file containing the constants.
        duplicates_file : str, optional
            Path to the file containing the duplicates.

        Returns
        -------
        tuple
            Parsed values as (prior_settings, constants, duplicate).
        """
        def _load_verify_json(file, err_msg):
            """
            Load in a json file and ensure it has a valid structure.

            Parameters
            ----------
            file : str
                Path to the file to load in.
            err_msg : str
                Error message to throw if the format is not valid.

            Returns
            -------
            dict
                The loaded json data.
            """
            with open(file, 'r') as f:
                raw_data = json.load(f)
                # Ensure that it's a dict
                if not isinstance(raw_data, dict):
                    raise TypeError(err_msg)
                return raw_data

        def _map_re_to_params(all_REs, name):
            """
            Map regular expressions to their associated parameter names.

            Parameters
            ----------
            all_REs : dict
                Mapping of regular expressions to their dist and params.
            name : str
                Name of the file the regular expressions are from.

            Returns
            -------
            dict
                Mapping of parameter names to their dist and params.
            """
            found_params = {}
            no_match_REs = []
            # Loop through each regular expression
            for regex, value in all_REs.items():
                # Check if the regular expression matches to any param names
                matched_params = [
                    param for param in param_names if isinstance(regex, str)
                    and re.fullmatch(regex, param) is not None
                ]
                # Update the found params with the associated param names
                if matched_params != []:
                    for matched_param in matched_params:
                        found_params[matched_param] = value
                # This RE did not map to any params
                else:
                    no_match_REs.append(regex)
            # Quit if an RE was not matched to any params
            if len(no_match_REs) != 0:
                raise ValueError(f'The following regexes from the {name} '
                                 'file did not match any known params: \n' +
                                 format_set_in_quotes(no_match_REs))
            return found_params

        # Check if the variables's file has a json format
        if 'json' in variables_file:
            err = ('Variables file must be a dictionary mapping '
                   'param RE -> value')
            prior_REs = _load_verify_json(variables_file, err)
        else:
            # Match any int or float (can be negative)
            num = r'(?:-?\d*.\d+|\d+)'
            # Match any argument named followed by a `=` and then a number
            name_num = rf'(?:[a-zA-Z]+\s*=\s*){num}'
            # Regex to parse a prior line
            parse_prior = re.compile(
                rf'''
^                       # Match start of line
\s*                     # Allow for spaces at the start of the line
(\S+)                   # 1+ non-space characters; regex for param names
\s*\~\s*                # `~` surrounded by optional spaces
([a-zA-Z\-]*)           # 1+ letters with `-` allowed; distribution name
\(                      # `(` to wrap around arguments
(                       # Start matching group for distribution's arguments
  {num}                 # Number (int or float)
  (?:,\s*{num})*        # Additional numbers separated by commas
  |                     # Or flag to allow either named or unnamed arguments
  {name_num}            # Argument name with a number (int or float)
  (?:,\s*{name_num})*   # Additional names and numbers separated by commas
)                       # End matching group for distribution's arguments
\)                      # `)` to wrap around arguments
\s*                     # Allow for spaces at the end of the line
$                       # Match end of line
                ''',
                re.VERBOSE,
            )
            prior_REs = {}
            bad_lines = []
            with open(variables_file) as lines:
                for line in lines:
                    match = parse_prior.match(line.strip())
                    if match is None:
                        bad_lines.append(line)
                        continue
                    param_regex = match.group(1)
                    dist = match.group(2).lower()
                    params = match.group(3).split(',')
                    if '=' in params[0]:
                        params = [p.split('=') for p in params]
                        params = {p[0].strip(): float(p[1]) for p in params}
                    else:
                        params = [float(p) for p in params]
                    prior_REs[param_regex] = {'dist': dist, 'params': params}
            if bad_lines:
                raise ValueError('The following lines from the priors '
                                 f'file were not valid: {bad_lines}')
        # Map all of the priors regexes to the associated param_names
        prior_settings = _map_re_to_params(prior_REs, 'priors')

        def _load_parse_file(file, name, load_error, valid_val_func):
            """
            Load in a json file, parse its contents, and map the regexes to
            their associated param names.

            Parameters
            ----------
            file : str
                Path to the file to load in.
            name : str
                Name of the file the regular expressions are from.
            load_error : str
                Error message to throw if the format is not valid.
            valid_val_func : function
                Function to ensure that each value is valid.

            Returns
            -------
            dict
                Mapping of parameter names to their dist and params.
            """
            # Load the parameters from a json file
            REs = _load_verify_json(file, load_error)
            # Map all of the regexes to the associated parameter names
            params = _map_re_to_params(REs, f'{name}s')
            # Check that all of the param names map to valid values
            bad_vals = [p for p, v in params.items() if not valid_val_func(v)]
            if len(bad_vals) != 0:
                raise TypeError(f'The following {name}s mapped to invalid '
                                f'values:\n{format_set_in_quotes(bad_vals)}')
            # Check if parameter has already been given a prior. If so, we're
            # going to raise an exception listing the offending params.
            # We could just override those priors, but it's better to be safe.
            conflicts = [p for p in params if p in prior_settings]
            if len(conflicts) != 0:
                raise ValueError(
                    f'The following parameters were set to {name}s, but '
                    'also had priors set in \'variables\' file. Those '
                    'priors would need to be ignored, so remove them from '
                    'the \'variables\' file and rerun if that is desired.\n' +
                    format_set_in_quotes(conflicts))
            # For bookkeeping purposes, set the parameter's prior distribution.
            # Map 'params' to an empty dict for consistency with other
            # distributions that require params.
            value = {'dist': name, 'params': {}}
            prior_settings.update({param: value for param in params})
            return params

        # Set the constant parameters
        constants = {}
        if constants_file is not None:
            constants = _load_parse_file(
                constants_file,
                'constant',
                'Constants file must be a dictionary mapping ParamRE -> value',
                # The values must be floats
                lambda v: isinstance(v, float),
            )
        # Set the duplicate parameters
        duplicates = {}
        if duplicates_file is not None:
            duplicates = _load_parse_file(
                duplicates_file,
                'duplicate',
                'Duplicates file must be a dictionary mapping param -> param',
                # The values must be existing parameters
                lambda v: isinstance(v, str) and v in prior_settings,
            )
        # Raise an exception if any params do not have priors specified now.
        missing_params = [p for p in param_names if p not in prior_settings]
        if len(missing_params) != 0:
            raise ValueError(
                'The following parameters have not been given priors:\n' +
                format_set_in_quotes(missing_params))
        return prior_settings, constants, duplicates
