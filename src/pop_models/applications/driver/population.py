__all__ = [
    'BrokenPowerlawSimple',
    'MixturePLGauss',
    'O2ModelAB',
    'SimpleMassSpinCorrelation',
]

import typing
import types
import numpy

from pop_models.population import (
    MixturePopulation,
    SeparableCoordinatePopulation,
)
from pop_models.astro_models.building_blocks.broken_powerlaw import (
    BrokenPowerlawPopulation)
from pop_models.astro_models.building_blocks.powerlaw import (
    ComponentMassPowerlawPopulation)
from pop_models.astro_models.building_blocks.gaussian_mass import (
    ComponentMassGaussianPopulation)
from pop_models.astro_models.building_blocks.generic import (
    GenericPowerlawPopulation)
from pop_models.astro_models.building_blocks.beta import BetaPopulation
from pop_models.astro_models.building_blocks.truncnorm import (
    TruncnormPopulation)
from pop_models.astro_models.building_blocks.beta_conditional import (
    LinearTanhBetaConditionalPopulation)
from pop_models.astro_models.coordinates import (
    m1_source_coord,
    m2_source_coord,
    chi1_coord,
    chi2_coord,
    costilt1_coord,
    costilt2_coord,
    q_coord,
    transformations,
)


def BrokenPowerlawSimple(
    n_parts: int,
    xpy: types.ModuleType = numpy,
    **otherArgs,
):
    n_parts = int(n_parts)

    population_m1 = BrokenPowerlawPopulation(
        m1_source_coord,
        n_parts,
        norm_name='rate',
        index_name='alpha',
        break_point_name='mass_break',
        negative_index=True,
        transformations=transformations,
        xpy=xpy,
    )
    # TODO: Need to actually make this p(m_2) to get Jacobians right,
    # everything will be off by an extra multiplicative factor of m_1 as-is.
    population_q = GenericPowerlawPopulation(
        q_coord,
        norm_name='rate',
        index_name='beta',
        lower_name='q_min',
        upper_name='q_max',
        transformations=transformations,
        xpy=xpy,
    )

    population_mass = SeparableCoordinatePopulation(
        population_m1,
        population_q,
        transformations=transformations,
    )

    return population_mass


SpinScalingDictType = typing.Dict[str, typing.Tuple[float, float]]


def MixturePLGauss(
    n_powerlaw: int,
    n_gaussian: int,
    m_max: float,
    xpy: types.ModuleType = numpy,
    same_gauss_mass_cutoffs: bool = False,
    no_spin_singularities: bool = False,
    spin_scalings: typing.Optional[SpinScalingDictType] = None,
    **otherArgs,
) -> MixturePopulation:
    n_powerlaw = int(n_powerlaw)
    n_gaussian = int(n_gaussian)
    m_max = float(m_max)
    same_gauss_mass_cutoffs = same_gauss_mass_cutoffs.lower() == 'true'
    no_spin_singularities = no_spin_singularities.lower() == 'true'

    def get_spin(
        no_singularities: bool = False,
        scale_chi1: float = 1.0,
        scale_chi2: float = 1.0,
    ) -> SeparableCoordinatePopulation:
        population_a1 = BetaPopulation(
            chi1_coord,
            loc=0.0,
            scale=scale_chi1,
            parameterization='mean_variance',
            rate_name='rate',
            mean_name='E_chi1',
            variance_name='Var_chi1',
            transformations=transformations,
            xpy=xpy,
            no_singularities=no_singularities,
        )
        population_a2 = BetaPopulation(
            chi2_coord,
            loc=0.0,
            scale=scale_chi2,
            parameterization='mean_variance',
            rate_name='rate',
            mean_name='E_chi2',
            variance_name='Var_chi2',
            transformations=transformations,
            xpy=xpy,
            no_singularities=no_singularities,
        )

        population_cos1 = TruncnormPopulation(
            costilt1_coord,
            lower=-1.0,
            upper=+1.0,
            rate_name='rate',
            mu_name='mu_cos1',
            sigma_name='sigma_cos1',
            transformations=transformations,
            xpy=xpy,
        )
        population_cos2 = TruncnormPopulation(
            costilt2_coord,
            lower=-1.0,
            upper=+1.0,
            rate_name='rate',
            mu_name='mu_cos2',
            sigma_name='sigma_cos2',
            transformations=transformations,
            xpy=xpy,
        )

        return SeparableCoordinatePopulation(
            population_a1,
            population_a2,
            population_cos1,
            population_cos2,
            transformations=transformations,
        )

    def get_gaussian(
        same_mass_cutoffs: bool = False,
        no_spin_singularities: bool = False,
        scale_chi1: float = 1.0,
        scale_chi2: float = 1.0,
    ) -> SeparableCoordinatePopulation:
        population_mass = ComponentMassGaussianPopulation(
            rate_name='rate',
            mean1_name='mass_mean1',
            stdev1_name='mass_sigma1',
            mean2_name='mass_mean2',
            stdev2_name='mass_sigma2',
            mmin1_name='m1_min',
            mmax1_name='m1_max',
            mmin2_name='m2_min',
            mmax2_name='m2_max',
            mmin_name='m_min',
            mmax_name='m_max',
            same_mass_cutoffs=same_mass_cutoffs,
            xpy=xpy,
        )
        population_spin = get_spin(
            no_singularities=no_spin_singularities,
            scale_chi1=scale_chi1,
            scale_chi2=scale_chi2,
        )

        return SeparableCoordinatePopulation(
            population_mass,
            population_spin,
            transformations=transformations,
        )

    def get_powerlaw(
        m_max: float,
        no_spin_singularities: bool = False,
        scale_chi1: float = 1.0,
        scale_chi2: float = 1.0,
    ) -> SeparableCoordinatePopulation:
        population_mass = ComponentMassPowerlawPopulation(
            m_max,
            rate_name='rate',
            m1_index_name='alpha',
            q_index_name='beta',
            mmin_name='m_min',
            mmax_name='m_max',
            xpy=xpy,
        )
        population_spin = get_spin(
            no_singularities=no_spin_singularities,
            scale_chi1=scale_chi1,
            scale_chi2=scale_chi2,
        )

        return SeparableCoordinatePopulation(
            population_mass,
            population_spin,
            transformations=transformations,
        )

    subpops = {}  # typing.Mapping[str, Population]

    # If default spin scaling used, we can use the same population object for
    # all subpopulations of the same type.
    if n_powerlaw > 0:
        pop_powerlaw = get_powerlaw(
            m_max,
            no_spin_singularities=no_spin_singularities,
        )
    if n_gaussian > 0:
        pop_gauss = get_gaussian(
            same_mass_cutoffs=same_gauss_mass_cutoffs,
            no_spin_singularities=no_spin_singularities,
        )

    # Accumulate power law subpopulations.
    for i in range(n_powerlaw):
        suffix_nounderscore = f'pl{i}'
        suffix = '_' + suffix_nounderscore
        # Reuse same population object if spin scalings are same for all, or if
        # this subpopulation is not listed.
        if spin_scalings is None or suffix_nounderscore not in spin_scalings:
            subpops[suffix] = pop_powerlaw
        # Otherwise we need to create a unique population object.
        else:
            # Get the scale for chi1 and chi2
            scale_chi1, scale_chi2 = spin_scalings[suffix_nounderscore]

            # Create scaled population to use here.
            subpops[suffix] = get_powerlaw(
                m_max,
                no_spin_singularities=no_spin_singularities,
                scale_chi1=scale_chi1,
                scale_chi2=scale_chi2,
            )

    # Accumulate Gaussian subpopulations.
    for i in range(n_gaussian):
        suffix_nounderscore = f'g{i}'
        suffix = '_' + suffix_nounderscore
        # Reuse same population object if spin scalings are same for all, or if
        # this subpopulation is not listed.
        if spin_scalings is None or suffix_nounderscore not in spin_scalings:
            subpops[suffix] = pop_gauss
        # Otherwise we need to create a unique population object.
        else:
            # Get the scale for chi1 and chi2
            scale_chi1, scale_chi2 = spin_scalings[suffix_nounderscore]

            # Create scaled population to use here.
            subpops[suffix] = get_gaussian(
                same_mass_cutoffs=same_gauss_mass_cutoffs,
                no_spin_singularities=no_spin_singularities,
                scale_chi1=scale_chi1,
                scale_chi2=scale_chi2,
            )

    return MixturePopulation(
        list(subpops.values())[0].coord_system,
        subpops,
        transformations=transformations,
    )


def O2ModelAB(
    m_max: float,
    xpy: types.ModuleType = numpy,
    **otherArgs,
):
    m_max = float(m_max)

    population_mass = ComponentMassPowerlawPopulation(
        m_max,
        rate_name="rate",
        m1_index_name="alpha_m",
        q_index_name="beta_q",
        mmin_name="m_min",
        mmax_name="m_max",
    )

    population_a1 = BetaPopulation(
        chi1_coord,
        loc=0.0,
        scale=1.0,
        parameterization="mean_variance",
        rate_name="rate",
        mean_name="E_chi1",
        variance_name="Var_chi1",
        transformations=transformations,
        xpy=xpy,
    )
    population_a2 = BetaPopulation(
        chi2_coord,
        loc=0.0,
        scale=1.0,
        parameterization="mean_variance",
        rate_name="rate",
        mean_name="E_chi2",
        variance_name="Var_chi2",
        transformations=transformations,
        xpy=xpy,
    )

    population_cos1 = TruncnormPopulation(
        costilt1_coord,
        lower=-1.0,
        upper=+1.0,
        rate_name="rate",
        mu_name="mu_cos1",
        sigma_name="sigma_cos1",
        transformations=transformations,
        xpy=xpy,
    )
    population_cos2 = TruncnormPopulation(
        costilt2_coord,
        lower=-1.0,
        upper=+1.0,
        rate_name="rate",
        mu_name="mu_cos2",
        sigma_name="sigma_cos2",
        transformations=transformations,
        xpy=xpy,
    )

    return SeparableCoordinatePopulation(
        population_mass,
        population_a1,
        population_a2,
        population_cos1,
        population_cos2,
        transformations=transformations,
    )


def SimpleMassSpinCorrelation(
    m_max: float,
    xpy: types.ModuleType = numpy,
    **otherArgs,
):
    m_max = float(m_max)

    population_mass = ComponentMassPowerlawPopulation(
        m_max,
        rate_name='rate',
        m1_index_name='alpha_m',
        q_index_name='beta_q',
        mmin_name='m_min',
        mmax_name='m_max',
    )

    population_mass_a1 = LinearTanhBetaConditionalPopulation(
        population_mass,
        chi1_coord,
        m1_source_coord,
        loc=0.0,
        scale=1.0,
        rate_name='rate',
        a_name='a1',
        b_name='b1',
        c_name='c1',
        y_prime_name='m1_ref',
        variance_name='Var_chi1',
        xpy=xpy,
    )
    population_mass_a1_a2 = LinearTanhBetaConditionalPopulation(
        population_mass_a1,
        chi2_coord,
        m2_source_coord,
        loc=0.0,
        scale=1.0,
        rate_name='rate',
        a_name='a2',
        b_name='b2',
        c_name='c2',
        y_prime_name='m2_ref',
        variance_name='Var_chi2',
        xpy=xpy,
    )

    population_cos1 = TruncnormPopulation(
        costilt1_coord,
        lower=-1.0,
        upper=+1.0,
        rate_name='rate',
        mu_name='mu_cos1',
        sigma_name='sigma_cos1',
        transformations=transformations,
        xpy=xpy,
    )
    population_cos2 = TruncnormPopulation(
        costilt2_coord,
        lower=-1.0,
        upper=+1.0,
        rate_name='rate',
        mu_name='mu_cos2',
        sigma_name='sigma_cos2',
        transformations=transformations,
        xpy=xpy,
    )

    return SeparableCoordinatePopulation(
        population_mass_a1_a2,
        population_cos1,
        population_cos2,
        transformations=transformations,
    )
