def add_default_cli_args(subparser, args):
    """
    Contains CLI arguments that are shared across multiple driver scripts.
    This function can be called to quickly add the following arguments to
    the provided subparser:
        config_file (positional)
        verbose (optional)
        swmr_mode (optional)
        debug (optional)

    Parameters
    ----------
    subparser : argparse.ArgumentParser
        Object to add the arguments to.
    args : list[str]
        Values to populate the subparser with; order does matter.
    """
    if 'config_file' in args:
        subparser.add_argument(
            'config_file',
            help='Path to the config file to base this run on.',
        )
    if 'verbose' in args:
        subparser.add_argument(
            '-v',
            '--verbose',
            action='store_true',
            help='Enable verbose logging to the console; default is False.',
        )
    if 'swmr_mode' in args:
        subparser.add_argument(
            '-s',
            '--swmr_mode',
            action='store_true',
            help=('Other processes may read from the file while running, will '
                  'be more resistant to data corruption; default is False.'),
        )
    if 'debug' in args:
        subparser.add_argument(
            '-d',
            '--debug',
            action='store_true',
            help='Output debugging messages; default is False.',
        )
