import configparser
from importlib import import_module, util as import_util
from pathlib import Path

# Section to load nested configs in from
APPEND_CONFIGS = 'append_configs'
# Value is a string that represents a path
PATH_STR = 'path_str'
# A required field in the config
REQUIRED = 'required'


def _resolve_path(base, path):
    """
    Resolve a relative path if necessary, or use an absolute path if provided.

    Parameters
    ----------
    base : str
        Path to the original config holding the desired path in question.
    path : str
        The path which needs to be resolved.

    Returns
    -------
    str
        If the desired path is an absolute path, it will immediately be
        returned; this includes `~/` syntax. If the desired path is relative,
        it will be appended to the base path.
    """
    # Convert to a Path object and expand the `~/` if present
    path = Path(path).expanduser()
    # Take just the absolute path
    if path.is_absolute():
        return str(path)
    # Build path relative to the config file
    return str(Path(base).parent.joinpath(path))


class Config:
    def __init__(self, config_path, defaults, overrides):
        """
        Create a Config object for accessing the contents of a config file.

        Parameters
        ----------
        config_path : ConfigParser
            Path to ini config file.
        defaults : dict
            Dictionary containing default values and types for fields in the
            config; must have the structure of:
            { 'section': { 'valueKey': [default, type], ... }, ... }
            `default` can be any type, including REQUIRED which means that an
            error will be raised if the value is mising, `type` is what the
            value will be typecasted to. All possible values do not have to be
            present in the this dictionary.
        overrides : list
            Config file values to override; each list element must be in the
            form of section.key=value with no spaces around the equals sign.
        """
        parsed = Config.parse(config_path)
        self._config_values, self._section_base_paths = parsed
        self._defaults = defaults
        # Perform config value overrides
        for override in overrides:
            # Break apart the override information
            try:
                identifier, value = override.split('=')
                section, key = identifier.split('.')
            except Exception:
                print(f'Skipping `{override}`: unknown format. '
                      'Please use the format: <section>.<key>=<value>')
                continue
            # Ensure both the section and key exist in the config
            self.grab(section, key)
            # Override the value
            self._config_values[section][key] = value
            print(f'Config value at `{identifier}` overridden with `{value}`.')

    def grab(self, section, value=None):
        """
        Grab values from the config.

        Parameters
        ----------
        section : str
            Section of the config to grab the values from.
        value : str, optional
            The specific value to grab from the section; if None (default) is
            passed, the whole section will be returned.

        Raises
        ------
        ValueError
            If the section is not found, OR the requested value is not found
            in the section and it is required.

        Returns
        -------
        SectionProxy or typecasted value
            If just the section is passed, it will be returned; if a value is
            passed, it will attempt to return it, but if it is not found,
            the default value will be returned in its place; the section must
            be found or an error will be raised.
        """
        # Ensure key is lowercase
        section = section.lower()
        # The section is required to exist
        if section in self._config_values:
            section_values = self._config_values[section]
            # Return just the section's values
            if value is None:
                return section_values
            # Attempt to grab the value from the loaded config
            grabbed_value = section_values.get(value, None)
            # Check if a default value exists, along with any typecast info
            if section in self._defaults and value in self._defaults[section]:
                default_value, val_type = self._defaults[section][value]
                # No value passed in the config, so set to the default value
                if grabbed_value is None or grabbed_value.lower() == 'none':
                    grabbed_value = default_value
                # Value represents a path
                elif val_type == PATH_STR:
                    grabbed_value = self.resolve_path_from_section(
                        section,
                        grabbed_value,
                    )
                # Value is boolean
                elif val_type == bool:
                    grabbed_value = grabbed_value.lower() == 'true'
                # Value is a castable type
                else:
                    grabbed_value = val_type(grabbed_value)
            # Value was not found, but required
            if grabbed_value == REQUIRED:
                raise ValueError(f'Required value {section}.{value} '
                                 'not found.')
            return grabbed_value
        else:
            raise ValueError(f'Section {section} not found.')

    def __call__(self, *params):
        """
        Allow created Config objects to be called, this will in turn pass all
        arguments to the `grab` function to grab from the config's values.
        """
        return self.grab(*params)

    def load_func_or_class(self, section, value, default_module):
        """
        Load a function or class from a path or from the default module.

        Parameters
        ----------
        section : str
            Section of the config to grab the values from.
        value : str
            Name of the value that contains where to load the func/class from,
            example values are (function/class is 'Foo'):
                1. 'Foo'; will be imported from the default location
                2. '../file.py:Foo'; path relative to config
                3. '/path/to/file.py:Foo'; absolute path
                4. 'pop_models.path.to.module:Foo'; absolute package path
        default_module : str
            Relative package path to the module that should be grabbed from
            by default.

        Raises
        ------
        ValueError
            If the function or class is not found.

        Returns
        -------
        Class or Function
            The loaded function or class.
        """
        name = self.grab(section, value)
        # If a colon is in the name, the path to the module has been specified
        if ':' in name:
            # Split apart the location and the name
            location, name = name.split(':')
            # Load module from path; https://stackoverflow.com/a/67692
            # Example: ../file.py:Foo or /path/to/file.py:Foo
            try:
                path = self.resolve_path_from_section(section, location)
                spec = import_util.spec_from_file_location(name, path)
                module = import_util.module_from_spec(spec)
                spec.loader.exec_module(module)
                return getattr(module, name)
            except Exception:
                pass
            # Load module from absolute package path
            # Example: pop_models.path.to.module:Foo
            try:
                return getattr(import_module(location), name)
            except Exception:
                pass
            raise ValueError(f'{name} not found for {section}.{value} '
                             f'in {location}')
        else:
            # Load from the default module
            # Example: Foo
            try:
                # Location of the driver
                default_package = 'pop_models.applications.driver'
                # Relative import requires a default package location
                module = import_module(default_module, default_package)
                return getattr(module, name)
            except Exception:
                pass
            raise ValueError(f'{name} not found for {section}.{value} '
                             f'in {default_module}')

    def resolve_path_from_section(self, section, path):
        """
        Resolve a path based on the section its from.

        Parameters
        ----------
        section : str
            Name of the section in the config that the value is from.
        path : str
            The path which needs to be resolved.

        Returns
        -------
        str
            The resolved path based on the section.
        """
        section_path = self._section_base_paths[section]
        return _resolve_path(section_path, path)

    @staticmethod
    def parse(config_path, recursive_load_appended_configs=True):
        """
        Static method to load and parse an ini config file.

        Parameters
        ----------
        config_path : str
            Path to ini file that needs to be parsed.
        recursive_load_appended_configs : bool
            Recursively load in any configs present under the APPEND_CONFIGS
            section; defaults to True. If set to False, the APPEND_CONFIGS
            section will not be parsed, nor will any nested configs be loaded.

        Returns
        -------
        tuple[ConfigParser, dict]
            First element in the tuple contains the config's data object.
            The second element contains a dictionary mapping each section
            to its original config file's path.
        """
        def read_config(path):
            """
            Load in an ini file.

            Parameters
            ----------
            path : str
                Path to the ini file to load.

            Raises
            ------
            FileNotFoundError
                If the config file is not found.

            Returns
            -------
            ConfigParser
                Object containing the config's data.
            """
            config = configparser.ConfigParser()
            files_found = config.read(path)
            # An empty array signifies no found ini files
            if len(files_found) == 0:
                raise FileNotFoundError(f'Config file at {path} not found.')
            return config

        base_config = read_config(config_path)
        # Mapping of each section to its original config's path
        section_base_paths = {section: config_path for section in base_config}

        def recurse_load_configs(current_config, current_path):
            """
            Recursively load in config files from the APPEND_CONFIGS section.
            The same section cannot be loaded twice.

            Raises
            ------
            ValueError
                If the same section is loaded twice.

            Parameters
            ----------
            current_config : ConfigParser
                Current config file, may contain an APPEND_CONFIGS section.
            current_path : str
                Relative or absolute path to current config file.
            """
            # Grab all config that need to be merged in
            configs = current_config[APPEND_CONFIGS]
            for config in configs:
                # Build the path to the nested config's path
                nested_path = _resolve_path(current_path, configs.get(config))
                # Load in the nested config
                nested_config = read_config(nested_path)
                for section in nested_config.sections():
                    # Check if the nested config has its own nested configs
                    if section == APPEND_CONFIGS:
                        recurse_load_configs(nested_config, nested_path)
                    elif section in section_base_paths:
                        original_file = section_base_paths[section]
                        raise ValueError(
                            f'{section} already loaded from {original_file}, '
                            f'duplicated in {nested_path}')
                    else:
                        section_base_paths[section] = nested_path
                        base_config[section] = nested_config[section]

        # Only merge in nested configs if requested
        if recursive_load_appended_configs:
            # Check if there are any nested configs to merge in
            if APPEND_CONFIGS in base_config:
                recurse_load_configs(base_config, config_path)
        return (base_config, section_base_paths)
