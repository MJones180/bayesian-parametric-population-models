from pop_models.posterior import H5CleanedPosteriorSamples
from pop_models.ppd import PPDSampler
from pop_models.utils import debug_verbose
import numpy as np
from .add_default_cli_args import add_default_cli_args
from .Config import Config, PATH_STR, REQUIRED


class PPD:
    @staticmethod
    def populate_subparser(subparsers):
        """
        Parse the command line arguments for this class.

        Parameters
        ----------
        subparsers : argparse._SubParsersAction
            Object to add the subparser to for parsing the CLI arguments.
        """
        subparser = subparsers.add_parser('ppd')
        subparser.set_defaults(main_class=PPD)
        add_default_cli_args(subparser, ['config_file', 'debug'])

    def __init__(self, config_file, overrides=[], debug=False):
        """
        Draw samples from the posterior predictive distribution (PPD) using
        values from a provided config file. More information can be found
        in the `ppd.ini` template.

        Parameters
        ----------
        config_file : str
            Path to the config file to base this run on.
        overrides : list
            Config file values to override, must be passed in the form of
            section.key=value with no spaces around the equals sign.
        debug : bool, optional
            Output debugging messages; default is False.

        Attributes
        ----------
        config : Config
            Object containing all config information.
        """
        # A list of the default config values along with their types
        defaults = {
            'main': {
                'n_samples': [REQUIRED, int],
                'posterior_file': [REQUIRED, PATH_STR],
                'ppd_output': [REQUIRED, PATH_STR],
                'isolate_subpops': [None, str],
                'thin_stuck': [False, bool],
                'record_hyperparameters': [False, bool],
                'shuffle': [False, bool],
            },
            'random': {
                'seed': [None, int],
            },
            'population': {
                'function': [REQUIRED, str],
            },
        }
        self.config = Config(config_file, defaults, overrides)
        self._debug = debug

    def run(self):
        """
        Draw samples from the posterior predictive distribution (PPD).
        """

        # Enable debugging
        if self._debug:
            debug_verbose.full_output_on()

        # Seed the NP RandomState for consistent, reproducible results
        random_state = np.random.RandomState(self.config('random', 'seed'))

        # Load in the posterior samples
        posterior_file = self.config('main', 'posterior_file')
        with H5CleanedPosteriorSamples(posterior_file) as samples:
            params_samples = samples.get_params(...)

            # Pass all metadata to the population, it will take what it needs
            population = self.config.load_func_or_class(
                'population',
                'function',
                '.population',
            )(**dict(samples.metadata))

            # Isolate sub-populations if requested
            isolate_subpops = self.config('main', 'isolate_subpops')
            if isolate_subpops is not None:
                if not hasattr(population, 'get_subpops'):
                    quit('isolate_subpops only works with MixturePopulation.')
                # Add in the underscore prefixes for convenience
                suffixes = [f'_{s}' for s in isolate_subpops.split(' ')]
                # Replace population with sub-populations
                population = population.get_subpops(suffixes)

            # Thin samples with log(prob) == -inf if requested
            if self.config('main', 'thin_stuck'):
                log_post = samples.get_posterior_log_prob(...)
                i_non_stuck = log_post != np.NINF
                for name in list(params_samples.keys()):
                    params_samples[name] = params_samples[name][i_non_stuck]

        # Create the PPD sampler, draw samples, and save to a file
        PPDSampler(population, random_state=random_state).sample_to_ascii(
            self.config('main', 'ppd_output'),
            self.config('main', 'n_samples'),
            params_samples,
            keep_params=self.config('main', 'record_hyperparameters'),
            shuffle=self.config('main', 'shuffle'),
        )
