# =================================================================
# The following config should be used for `inference`.
# All sections and keys are expected to be lowercase.
# Fields that aren't REQUIRED may be commented out or have the
# value of None, in which case the default value will be used.
# Fields labeled as 'CNI' are Currently Not Implemented.
# Paths may be either absolute or relative to the config file.
# For fields that import a function or class, the following options
# for specifying the location are valid (function/class is 'Foo'):
#   1. 'Foo'; will be imported from the default location
#   2. '../file.py:Foo'; path relative to config
#   3. '/path/to/file.py:Foo'; absolute path
#   4. 'pop_models.path.to.module:Foo'; absolute package path
# Fields under the `append_configs` section may be taken out, but
# their contents must be implemented directly in this file.
# Config last updated on 10/30/2021.
# =================================================================

[append_configs]
# Path to the detection config, view the 'detection.ini' template for the
# required structure that this file must have; REQUIRED
detection_spec=path/to/detection/config.ini
# Path to the population config, view the 'population.ini' template for the
# required structure that this file must have; REQUIRED
population_spec=path/to/population/config.ini
# Path to the sensitivity config, view the 'sensitivity.ini' template for the
# required structure that this file must have; REQUIRED
sensitivity_spec=path/to/sensitivity/config.ini

[main]
# Path to the events json file; paths in the json file should be relative to
# the current working directory; REQUIRED
events=path/to/events/file.json

[prior]
# Prior class to use; defaults to 'Prior'; default location is 'prior.py'
class=Prior
# Path to the variables json file (or custom format); REQUIRED
variables=path/to/variables.json
# Path to the constants json file
constants=path/to/constants.json
# Path to the duplicates json file
duplicates=path/to/duplicates.json

[posterior]
# Path to the raw/cleaned HDF5 samples archive; 'init_type' must not be None
init_data=path/to/samples.hdf5
# Value must be: 'init_from_raw_samples', 'init_from_cleaned_samples', or None
# - init_from_raw_samples: use the final positions of all the walkers as the
#                          new starting point for the raw samples
# - init_from_cleaned_samples: starting point is a set of randomly selected
#                              cleaned samples specified by 'mcmc.n_walkers'
init_type=None
# Path to the directory to store the posterior samples in; REQUIRED
posterior_output=path/to/output/
# Name of field in input posteriors that specifies weights to apply
# to posterior samples; will multiply by these values if provided
weights_field=None
# Name of field in input posteriors that specifies inverse weights to apply
# to posterior samples; will divide by these values if provided
inv_weights_field=None

[mcmc]
# Number of walkers to use; defaults to twice the number of dimensions,
# dimension count = len(pop params - constants - duplicates) = variable count
n_walkers=None
# Number of samples per walker; defaults to 100
n_samples=100
# Number of samples to store in each posterior sample sub-file; defaults to 1024
chunk_size=1024
# Path to the json file that sets the ensemble move options
moves=path/to/moves.json
# Path to the json file that maps parameter names to rescalings
rescale_params=path/to/rescalings.json
# <CNI> Number of threads to use; defaults to 1
n_threads=1

[random]
# Random seed value to use
seed=None

[misc]
# Force HDF5 output, will overwrite if file already exists; defaults to False
force=False
# <CNI> Use 'numpy' or 'numba'; defaults to 'numpy'
backend=numpy

[poisson]
# Poisson class to use; defaults to 'MonteCarloVolumeIntegralPoissonMean';
# default location is 'pop_models.poisson_mean'
class=MonteCarloVolumeIntegralPoissonMean
# <CNI> Allowed absolute error for Monte Carlo integrator; defaults to 1e-5
mc_err_abs=1e-5
# <CNI> Allowed relative error for Monte Carlo integrator; defaults to 1e-5
mc_err_rel=1e-3
