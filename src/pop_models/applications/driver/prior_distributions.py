__all__ = ['Uniform', 'LogUniform', 'Gaussian', 'KDE']

from pop_models.astro_models.building_blocks.gaussian_mass import truncnorm_rvs
from pop_models.stats import truncnorm_pdf
from pop_models.utils import debug_verbose
from abc import ABCMeta, abstractmethod


class BaseDistribution(metaclass=ABCMeta):
    def __init__(self, name, params, xpy, **kwargs):
        """
        Base class for a distribution.
        Subclasses must implement the `sample_prior` and `log_prior` methods.

        Parameters
        ----------
        name : str
            Name of the param associated with this distribution.
        params : dict
            Arguments associated with this parameter.
        xpy : numpy or numba
            Either a 'numpy' or 'numba' object.
        **kwargs : dict, optional
            Extra arguments that need to be provided to the distribution.
        """
        self.name = name
        self.params = params
        self.xpy = xpy
        self.__dict__.update(kwargs)

    def debug_out(self, message):
        """
        Method to reduce logging boilerplate.

        Parameters
        ----------
        message : str
            Message to print to the terminal.
        """
        debug_verbose(message, mode='prior', flush=True)

    @abstractmethod
    def sample_prior(self, samples_count, random_state):
        """
        Sample from the prior. This method is abstract.

        Parameters
        ----------
        samples_count : int
            Number of samples to draw.
        random_state : RandomState
            Object for generating random seeded numbers.

        Returns
        -------
        array
            The sampled values.
        """
        pass

    @abstractmethod
    def log_prior(self, values, prior=None, valid=None, tmp=None):
        """
        Take the log of the prior. This method is abstract.

        Parameters
        ----------
        values : array
            Values for the current population parameter.
        prior : array, optional
            Current prior values that the log will be taken of.
        valid : array, optional
            Array of the valid population parameters.
        tmp : array, optional
            Existing array for efficiency; prevents having to allocate a new
            output array for every call.

        Returns
        -------
        tuple
            Updated values as (prior, valid).
        """
        pass


class Uniform(BaseDistribution):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if isinstance(self.params, list):
            self.min, self.max = self.params
        else:
            self.min = self.params['min']
            self.max = self.params['max']

    def sample_prior(self, samples_count, random_state):
        return random_state.uniform(self.min, self.max, samples_count)

    def log_prior(self, values, prior=None, valid=None, tmp=None):
        if valid is not None:
            large_enough = self.xpy.greater_equal(values, self.min, out=tmp)
            self.debug_out(
                f'Parameter {self.name} large enough?{large_enough}')
            valid &= large_enough
            small_enough = self.xpy.less_equal(values, self.max, out=tmp)
            self.debug_out(
                f'Parameter {self.name} small enough?{small_enough}')
            valid &= small_enough
        return prior, valid


class LogUniform(BaseDistribution):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if isinstance(self.params, list):
            self.min, self.max = self.params
        else:
            self.min = self.params['min']
            self.max = self.params['max']

    def sample_prior(self, samples_count, random_state):
        return self.xpy.exp(
            random_state.uniform(
                self.xpy.log(self.min),
                self.xpy.log(self.max),
                samples_count,
            ))

    def log_prior(self, values, prior=None, valid=None, tmp=None):
        if valid is not None:
            large_enough = self.xpy.greater_equal(values, self.min, out=tmp)
            self.debug_out(
                f'Parameter {self.name} large enough?{large_enough}')
            valid &= large_enough
            small_enough = self.xpy.less_equal(values, self.max, out=tmp)
            self.debug_out(
                f'Parameter {self.name} small enough?{small_enough}')
            valid &= small_enough
        if prior is not None:
            prior -= self.xpy.log(values)
        return prior, valid


class Gaussian(BaseDistribution):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if isinstance(self.params, list):
            self.min, self.max, self.mu, self.sigma = self.params
        else:
            self.min = self.params.get('min', self.xpy.NINF)
            self.max = self.params.get('max', self.xpy.inf)
            self.mu = self.params['mu']
            self.sigma = self.params['sigma']

    def sample_prior(self, samples_count, random_state):
        return truncnorm_rvs(
            samples_count,
            self.mu,
            self.sigma,
            self.min,
            self.max,
            random_state=random_state,
        )

    def log_prior(self, values, prior=None, valid=None, tmp=None):
        if prior is not None:
            prior += self.xpy.log(
                truncnorm_pdf(
                    values,
                    self.mu,
                    self.sigma,
                    self.min,
                    self.max,
                ))
        return prior, valid


class KDE(BaseDistribution):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def sample_prior(self, samples_count, random_state):
        return self.kde_helper.rvs(samples_count)

    def log_prior(self, values, prior=None, valid=None, tmp=None):
        if prior is not None:
            prior += self.kde_helper.log_pdf(values)
        return prior, valid
