from pop_models.astro_models.coordinates import coordinates
from pop_models.coordinate import CoordinateSystem
from pop_models.posterior import H5CleanedPosteriorSamples
from pop_models.quantiles import ObservableQuantiles as ObservableQuantilesExt
from corner import corner as corner_plot
import h5py
import numpy as np
from .add_default_cli_args import add_default_cli_args
from .Config import Config, PATH_STR, REQUIRED


class ObservableQuantiles:
    @staticmethod
    def populate_subparser(subparsers):
        """
        Parse the command line arguments for this class.

        Parameters
        ----------
        subparsers : argparse._SubParsersAction
            Object to add the subparser to for parsing the CLI arguments.
        """
        subparser = subparsers.add_parser('observable_quantiles')
        subparser.set_defaults(main_class=ObservableQuantiles)
        add_default_cli_args(subparser, ['config_file'])

    def __init__(self, config_file, overrides=[]):
        """
        Compute and plot the observable quantiles. More information can be
        found in the `observable_quantiles.ini` template.

        Parameters
        ----------
        config_file : str
            Path to the config file to base this run on.
        overrides : list
            Config file values to override, must be passed in the form of
            section.key=value with no spaces around the equals sign.

        Attributes
        ----------
        config : Config
            Object containing all config information.
        """
        # A list of the default config values along with their types
        defaults = {
            'main': {
                'posterior_file': [REQUIRED, PATH_STR],
                'output_file': [REQUIRED, PATH_STR],
                'output_plot': [REQUIRED, PATH_STR],
                'n_samples_per_realization': [1000, int],
                'observable': ['m1_source', str],
                'quantiles': ['0.95', str],
                'isolate_subpops': [None, str],
            },
            'random': {
                'seed': [None, int],
            },
            'misc': {
                'force': [False, bool],
            },
            'population': {
                'function': [REQUIRED, str],
            },
        }
        # Create the config object
        self.config = Config(config_file, defaults, overrides)

    def run(self):
        """
        Compute and plot the observable quantiles.
        """
        # Load in the posterior samples
        posterior_file = self.config('main', 'posterior_file')
        with H5CleanedPosteriorSamples(posterior_file) as samples:
            observable = self.config('main', 'observable')
            coord_system = CoordinateSystem(coordinates[observable])
            # Pass all metadata to the population, it will take what it needs
            population = self.config.load_func_or_class(
                'population',
                'function',
                '.population',
            )(**dict(samples.metadata)).to_coords(coord_system)
            # Isolate sub-populations if requested
            isolate_subpops = self.config('main', 'isolate_subpops')
            if isolate_subpops is not None:
                if not hasattr(population, 'get_subpops'):
                    quit('isolate_subpops only works with MixturePopulation.')
                # Add in the underscore prefixes for convenience
                suffixes = [f'_{s}' for s in isolate_subpops.split(' ')]
                # Replace population with sub-populations
                population = population.get_subpops(suffixes)
            # Create object for computing quantiles
            obs_quant = ObservableQuantilesExt(population)
            # Draw all posterior samples
            params_samples = samples.get_params(...)
            quantile_vals = self.config('main', 'quantiles')
            quantile_vals = [float(x) for x in quantile_vals.split(' ')]
            # Compute quantiles
            n_samples = self.config('main', 'n_samples_per_realization')
            # Seed the NP RandomState for consistent, reproducible results
            rand_state = np.random.RandomState(self.config('random', 'seed'))
            quantiles = {
                q: obs_quant.quantile(params_samples, q, n_samples, rand_state)
                for q in quantile_vals
            }
            # Save to file.
            mode = 'w' if self.config('misc', 'force') else 'w-'
            with h5py.File(self.config('main', 'output_file'), mode) as out:
                for q, values in quantiles.items():
                    out.create_dataset(str(q), data=values)
            sorted_keys = sorted(quantiles.keys())
            data = np.column_stack(tuple(quantiles[q] for q in sorted_keys))
            labels = [f'{observable} ({q})' for q in sorted_keys]
            fig = corner_plot(
                data,
                labels=labels,
                color='black',
                levels=[0.5, 0.9],
                plot_density=False,
                plot_datapoints=False,
                no_fill_contours=True,
                hist_kwargs={'density': True},
                contour_kwargs={
                    'linestyles': ['dashed', 'solid'],
                    'color': 'black',
                },
            )
            fig.savefig(self.config('main', 'output_plot'))
