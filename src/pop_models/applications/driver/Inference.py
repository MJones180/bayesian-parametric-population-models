from pop_models.astro_models.gw_ifo_vt import (
    RegularGridVTInterpolator,
    correction_bases,
    CorrectedVT,
)
from pop_models.posterior import (
    H5RawPosteriorSamples,
    H5CleanedPosteriorSamples,
    PopulationInference,
    PopulationInferenceEmceeSampler,
)
from pop_models.utils import debug_verbose
from .add_default_cli_args import add_default_cli_args
from .Config import Config, PATH_STR, REQUIRED
import h5py
import json
import numpy
from os.path import isfile, join as pathJoin
from warnings import warn


class Inference:
    @staticmethod
    def populate_subparser(subparsers):
        """
        Parse the command line arguments for this class.

        Parameters
        ----------
        subparsers : argparse._SubParsersAction
            Object to add the subparser to for parsing the CLI arguments.
        """
        subparser = subparsers.add_parser('inference')
        subparser.set_defaults(main_class=Inference)
        add_default_cli_args(subparser, [
            'config_file',
            'verbose',
            'swmr_mode',
            'debug',
        ])

    def __init__(
        self,
        config_file,
        overrides=[],
        verbose=False,
        swmr_mode=False,
        debug=False,
    ):
        """
        Run population inference using values from a provided config file.
        More information can be found in the `inference.ini` template.

        Parameters
        ----------
        config_file : str
            Path to the config file to base this run on.
        overrides : list, optional
            Config file values to override, must be passed in the form of
            section.key=value with no spaces around the equals sign.
        verbose : bool, optional
            Enable verbose logging to the console; default is False.
        swmr_mode : bool, optional
            Other processes may read from the file while running, will be
            more resistant to data corruption; default is False.
        debug : bool, optional
            Output debugging messages; default is False.

        Attributes
        ----------
        config : Config
            Object containing all config information.
        """
        # A list of the default config values along with their types
        defaults = {
            'main': {
                'events': [REQUIRED, PATH_STR],
            },
            'prior': {
                'class': ['Prior', str],
                'variables': [REQUIRED, PATH_STR],
                'constants': [None, PATH_STR],
                'duplicates': [None, PATH_STR],
            },
            'posterior': {
                'init_data': [None, PATH_STR],
                'init_type': [None, str],
                'posterior_output': [REQUIRED, PATH_STR],
                'weights_field': [None, str],
                'inv_weights_field': [None, str],
            },
            'mcmc': {
                'n_walkers': [None, int],
                'n_samples': [100, int],
                'chunk_size': [1024, int],
                'moves': [None, PATH_STR],
                'rescale_params': [None, PATH_STR],
                # 'n_threads' not currently used
                'n_threads': [1, int],
            },
            'random': {
                'seed': [None, int],
            },
            'misc': {
                'force': [False, bool],
                # 'backend' not currently used
                'backend': ['numpy', str],
            },
            'poisson': {
                'class': ['MonteCarloVolumeIntegralPoissonMean', str],
                # 'mc_err_*' not currently used
                'mc_err_abs': [1e-5, float],
                'mc_err_rel': [1e-3, float],
            },
            'detection': {
                'loader': ['CBCPreSampledDetection', str],
                'likelihood':
                ['DeterministicReweightedPosteriorsDetectionLikelihood', str],
            },
            'sensitivity': {
                'vts': [REQUIRED, PATH_STR],
                'vt_calibration': [None, PATH_STR],
                'scale_factor': [1.0, float],
            },
            'population': {
                'function': [REQUIRED, str],
            },
        }
        self.config = Config(config_file, defaults, overrides)
        self._verbose = verbose
        self._swmr_mode = swmr_mode
        self._debug = debug

    def run(self):
        """
        Run population inference.
        """

        # =========================
        # Setup and handy functions
        # =========================

        # Whether to enable debugging outputs
        if self._debug:
            debug_verbose.full_output_on()

        def debug_out(message):
            """
            Function to reduce logging boilerplate.

            Parameters
            ----------
            message : str
                Message to print to the terminal.
            """
            debug_verbose(message, mode='inference_setup', flush=True)

        def load_json(json_file):
            """
            Load JSON data from a file.

            Parameters
            ----------
            json_file : str
                Path to the JSON file to load the data of.

            Returns
            -------
            dict
                The JSON data.
            """
            with open(json_file, 'r') as file:
                return json.load(file)

        # Seed the NP RandomState for consistent, reproducible results
        random_state = numpy.random.RandomState(self.config('random', 'seed'))

        # ==========================
        # Grab classes and functions
        # ==========================

        # Dynamically grab functions/classes from their associated files
        population_func = self.config.load_func_or_class(
            'population',
            'function',
            '.population',
        )
        prior_class = self.config.load_func_or_class(
            'prior',
            'class',
            '.prior',
        )
        poisson_class = self.config.load_func_or_class(
            'poisson',
            'class',
            'pop_models.poisson_mean',
        )
        detection_loader_class = self.config.load_func_or_class(
            'detection',
            'loader',
            'pop_models.astro_models.detections.cbc.pre_sampled',
        )
        detection_likelihood_class = self.config.load_func_or_class(
            'detection',
            'likelihood',
            'pop_models.detection',
        )

        # ========================
        # Check if resuming or not
        # ========================

        # Check if a posterior output file already exists
        posterior_out = self.config('posterior', 'posterior_output')
        exists = isfile(pathJoin(posterior_out, 'master.hdf5'))
        # Force a new run
        force = self.config('misc', 'force')
        # Resume if the posterior samples already exist and force is false
        resume = exists and not force

        # =======================================
        # Handle resuming, or starting of new run
        # =======================================

        if resume:
            debug_out('Attempting to continue existing run')
            # Load in existing posterior samples
            post_samples = H5RawPosteriorSamples(posterior_out, 'r+',
                                                 self._swmr_mode)
            debug_out(f'Loaded existing run with {len(post_samples)} samples '
                      f'and {post_samples.n_walkers} walkers')
            metadata = post_samples.metadata
            debug_out(f'Metadata: {dict(metadata)}')
            # Pass all metadata to the popoulation, it will take what it needs
            population = population_func(**dict(metadata))
            debug_out(f'Population initialized: {population}')
            VT_scale_factor = metadata['VT_scale_factor']
            prior_settings = json.loads(metadata['prior_settings'])
            constants = post_samples.constants
            duplicates = post_samples.duplicates
        else:
            debug_out('Attempting to start new run')
            # Pass in all population config key-value pairs
            population = population_func(**dict(self.config('population')))
            debug_out(f'Population initialized: {population}')
            VT_scale_factor = self.config('sensitivity', 'scale_factor')
            # Validate and get list of param names
            prior_settings, constants, duplicates = (
                prior_class.parse_param_names(
                    population.param_names,
                    self.config('prior', 'variables'),
                    self.config('prior', 'constants'),
                    self.config('prior', 'duplicates'),
                ))
            debug_out(f'Prior settings parsed: {prior_settings}')
            debug_out(f'Constants parsed: {constants}')
            debug_out(f'Duplicates parsed: {duplicates}')

        # ========================
        # Construct a prior object
        # ========================

        prior = prior_class(
            random_state,
            population,
            prior_settings,
            constants=constants,
            duplicates=duplicates,
        )
        debug_out(f'Prior initialized: {prior}')

        # ======================
        # Load in all detections
        # ======================

        events_file = self.config('main', 'events')
        debug_out(f'Opening {events_file} to read in events files.')
        # Load in and resolve the paths to each event
        all_events = [
            self.config.resolve_path_from_section('main', event_path)
            for event_path in load_json(events_file)
        ]
        debug_out(f'Loading in detections from files: {all_events}')
        weights_field = self.config('posterior', 'weights_field')
        inv_weights_field = self.config('posterior', 'inv_weights_field')
        detections = [
            detection_loader_class.load(
                event_path,
                coord_system=population.coord_system,
                weights_field=weights_field,
                inv_weights_field=inv_weights_field,
            ) for event_path in all_events
        ]
        debug_out(f'Detections loaded: {detections}')

        # ===============
        # Load in VT data
        # ===============

        VTs = self.config('sensitivity', 'vts')
        debug_out(f'Loading in VT from file: {VTs}, '
                  f'with scale factor: {VT_scale_factor}')
        with h5py.File(VTs, 'r') as vtFile:
            VT = RegularGridVTInterpolator(vtFile, VT_scale_factor)
        vt_calibration = self.config('sensitivity', 'vt_calibration')
        # Load in VT calibration if provided
        if vt_calibration is not None:
            debug_out(f'Loading VT calibration from file: {vt_calibration}')
            # Load calibration info from file.
            cal_info = load_json(vt_calibration)
            # Pull out needed calibration info.
            coeffs = cal_info['coeffs']
            basis_functions = correction_bases[VT.mode][cal_info['basis']]
            # Replace VT with the calibrated VT
            VT = CorrectedVT(VT, coeffs, basis_functions)

        # =========================
        # Create the Poisson object
        # =========================

        # Evaluates expected number of detections for a given population and VT
        debug_out('Constructing Poisson mean object.')
        expval = poisson_class(population, VT)
        debug_out(f'Poisson mean object constructed: {expval}')

        # ===================================
        # Calculate the detection likelihoods
        # ===================================

        # Create all of detection*population integral evaluators
        debug_out('Constructing in detection likelihood integrators')
        detection_likelihoods = [
            detection_likelihood_class(population, detection)
            for detection in detections
        ]
        debug_out('Detection likelihood integrators '
                  f'loaded: {detection_likelihoods}')

        # ==============================================================
        # Init MCMC and create posterior samples HDF archive for new run
        # ==============================================================

        # Create posterior samples HDF5 archive and object interface
        if not resume:
            # Variables shared between initialization types
            init_type = self.config('posterior', 'init_type')
            if init_type is not None:
                init_type = init_type.lower()
                init_data = self.config('posterior', 'init_data')
            chunk_size = self.config('mcmc', 'chunk_size')
            n_walkers = self.config('mcmc', 'n_walkers')
            # Names of all population parameters
            param_names = population.param_names
            # Number of variables (params - constants - duplicates)
            dims = len(param_names) - len(constants) - len(duplicates)
            # Default the number of walkers to 2*dims
            if n_walkers is None:
                n_walkers = dims * 2
                debug_out(f'n_walkers is None, defaulting to {n_walkers}')
            # Walkers should be at least 2x greater than the dims
            elif n_walkers < (2 * dims):
                warn(f'The number of walkers, {n_walkers}, is less than '
                     f'twice the number of dimensions, {dims}*2={dims*2}. '
                     'This will cause some MCMC Moves to fail.')
            # Initialize from a raw samples file
            if init_type == 'init_from_raw_samples':
                debug_out('Initializing sampler from existing raw '
                          f'posteriors file: {init_data}')
                init_post = H5RawPosteriorSamples(init_data, 'r')
                with init_post:
                    post_samples = (
                        H5RawPosteriorSamples.create_from_raw_samples(
                            posterior_out,
                            init_post,
                            constants=constants,
                            chunk_size=chunk_size,
                            force=force,
                            swmr_mode=self._swmr_mode,
                        ))
            # Initialize from a cleaned samples file
            elif init_type == 'init_from_cleaned_samples':
                debug_out('Initializing sampler from existing '
                          f'cleaned posteriors file: {init_data}')
                # Set up a stripped down inference object for vetoing
                # initial values that are not in the posterior's support
                pop_inference_for_init = PopulationInference(
                    expval,
                    detection_likelihoods,
                    prior.log_prior,
                    param_names,
                )
                init_post = H5CleanedPosteriorSamples(init_data)
                with init_post:
                    post_samples = H5RawPosteriorSamples.create_from_samples(
                        posterior_out,
                        n_walkers,
                        init_post,
                        constants=constants,
                        chunk_size=chunk_size,
                        force=force,
                        swmr_mode=self._swmr_mode,
                        pop_inf=pop_inference_for_init,
                    )
            # Initialize from prior
            else:
                debug_out('Initializing sampler from prior')
                init_state = prior.init_walkers(
                    n_walkers,
                    # Set up a stripped down inference object for vetoing
                    # initial values that are not in the posterior's support
                    PopulationInference(
                        expval,
                        detection_likelihoods,
                        prior.log_prior,
                        param_names,
                    ))
                post_samples = H5RawPosteriorSamples.create(
                    posterior_out,
                    param_names,
                    init_state,
                    constants=constants,
                    duplicates=duplicates,
                    chunk_size=chunk_size,
                    force=force,
                    swmr_mode=self._swmr_mode,
                )

            # Store metadata for the current run
            for key, val in self.config('population').items():
                post_samples.metadata[key.lower()] = val
            post_samples.metadata['VT_scale_factor'] = VT_scale_factor
            post_samples.metadata['VT_mode'] = VT.mode
            post_samples.metadata['prior_settings'] = json.dumps(
                prior_settings)
            debug_out(f'Samples initialized: {post_samples}')

        # =======================================
        # Construct and sample from the posterior
        # =======================================

        with post_samples:
            # Set initial moves
            moves = self.config('mcmc', 'moves')
            if moves is not None:
                debug_out('Parsing MCMC move options.')
                with open(moves, 'r') as moves_file:
                    moves = (PopulationInferenceEmceeSampler.
                             parse_moves_from_file(moves_file))
                debug_out(f'MCMC move options parsed as: {moves}')

            # Grab the rescale params
            rescale_params = self.config('mcmc', 'rescale_params')
            if rescale_params is not None:
                debug_out('Parsing rescale_params.')
                rescale_params = load_json(rescale_params)
                debug_out(f'rescale_params parsed as: {rescale_params}')

            # Create the sampler object
            debug_out('Constructing posterior sampler.')
            pop_inference = PopulationInferenceEmceeSampler(
                post_samples,
                expval,
                detection_likelihoods,
                prior.log_prior,
                moves=moves,
                rescale_params=rescale_params,
                random_state=random_state,
                verbose=self._verbose,
            )
            debug_out(f'Posterior sampler constructed: {pop_inference}')

            # Sample from the posterior
            n_samples = self.config('mcmc', 'n_samples')
            debug_out(f'Beginning to sample {n_samples} posteriors.')
            pop_inference.posterior_samples(n_samples)
            debug_out(f'Finished sampling {n_samples} posteriors.')
