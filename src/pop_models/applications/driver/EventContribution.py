from pop_models.posterior import H5CleanedPosteriorSamples
from pop_models.utils import debug_verbose
import h5py
from .add_default_cli_args import add_default_cli_args
from .Config import Config, PATH_STR, REQUIRED


class EventContribution:
    @staticmethod
    def populate_subparser(subparsers):
        """
        Parse the command line arguments for this class.

        Parameters
        ----------
        subparsers : argparse._SubParsersAction
            Object to add the subparser to for parsing the CLI arguments.
        """
        subparser = subparsers.add_parser('event_contribution')
        subparser.set_defaults(main_class=EventContribution)
        add_default_cli_args(subparser, ['config_file', 'debug'])

    def __init__(self, config_file, overrides=[], debug=False):
        """
        Calculate the event contribution for a given event datafile using
        values from a provided config file. More information can be found
        in the `event_contribution.ini` template.

        Parameters
        ----------
        config_file : str
            Path to the config file to base this run on.
        overrides : list
            Config file values to override, must be passed in the form of
            section.key=value with no spaces around the equals sign.
        debug : bool, optional
            Output debugging messages; default is False.

        Attributes
        ----------
        config : Config
            Object containing all config information.
        """
        # A list of the default config values along with their types
        defaults = {
            'main': {
                'event': [REQUIRED, PATH_STR],
                'output_file': [REQUIRED, PATH_STR],
                'batch_size': [128, int],
            },
            'posterior': {
                'posterior_file': [REQUIRED, PATH_STR],
                'weights_field': [None, str],
                'inv_weights_field': [None, str],
            },
            'misc': {
                'force': [False, bool],
            },
            'detection': {
                'loader': ['CBCPreSampledDetection', str],
                'likelihood':
                ['DeterministicReweightedPosteriorsDetectionLikelihood', str],
            },
            'population': {
                'function': [REQUIRED, str],
            },
        }
        self.config = Config(config_file, defaults, overrides)
        self._debug = debug

    def run(self):
        """
        Run event contribution calculations.
        """

        # Whether to enable debugging outputs
        if self._debug:
            debug_verbose.full_output_on()

        def debug_out(message):
            """
            Function to reduce logging boilerplate.

            Parameters
            ----------
            message : str
                Message to print to the terminal.
            """
            debug_verbose(message, mode='event_contribution', flush=True)

        # Load in the requested classes
        detection_loader_class = self.config.load_func_or_class(
            'detection',
            'loader',
            'pop_models.astro_models.detections.cbc.pre_sampled',
        )
        detection_likelihood_class = self.config.load_func_or_class(
            'detection',
            'likelihood',
            'pop_models.detection',
        )
        # Load input posterior samples.
        posterior_file = self.config('posterior', 'posterior_file')
        with H5CleanedPosteriorSamples(posterior_file) as samples:
            metadata = samples.metadata
            debug_out(f'Metadata: {dict(metadata)}')
            # Pass all metadata to the population, it will take what it needs
            population = self.config.load_func_or_class(
                'population',
                'function',
                '.population',
            )(**dict(metadata))
            debug_out(f'Population initialized: {population}')
            # Open tabular files for event posteriors.
            event = self.config('main', 'event')
            debug_out(f'Loading in detection from file: {event}')
            weights_field = self.config('posterior', 'weights_field')
            inv_weights_field = self.config('posterior', 'inv_weights_field')
            detection = detection_loader_class.load(
                event,
                coord_system=population.coord_system,
                weights_field=weights_field,
                inv_weights_field=inv_weights_field,
            )
            debug_out(f'Detection loaded: {detection}')
            # Create detection*population integral evaluator.
            debug_out('Constructing detection likelihood integrator')
            detection_likelihood = detection_likelihood_class(
                population,
                detection,
            )
            debug_out('Detection likelihood integrator '
                      f'loaded: {detection_likelihood}')
            # Open output file for writing.
            output_file = self.config('main', 'output_file')
            debug_out(f'Creating output file: {output_file}')
            mode = 'w' if self.config('misc', 'force') else 'w-'
            with h5py.File(output_file, mode) as out_file:
                n_samples = len(samples)
                debug_out('Creating output dataset')
                event_contributions = out_file.create_dataset(
                    'event_contribution',
                    (n_samples, ),
                )
                batch_size = self.config('main', 'batch_size')
                debug_out(f'Computing event contributions in {batch_size} '
                          'sample batches')
                for i_start in range(0, n_samples, batch_size):
                    i_stop = min(i_start + batch_size, n_samples)
                    slc = slice(i_start, i_stop)
                    params = samples.get_params(slc)
                    event_contributions[slc] = (
                        detection_likelihood(params) /
                        population.normalization(params))
                    debug_out(f'Computed contribution from samples {i_start} '
                              f'to {i_stop - 1}')
            debug_out('Completed.')
