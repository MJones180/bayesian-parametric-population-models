import argparse
from sys import argv
from .EventContribution import EventContribution
from .Inference import Inference
from .ObservableQuantiles import ObservableQuantiles
from .PPD import PPD

# List of classes to load the subparsers from
subparser_classes = [
    EventContribution,
    Inference,
    ObservableQuantiles,
    PPD,
]


def main(raw_args=None):
    """
    Setup the argparser, parse the arguments, and call the correct class.

    Parameters
    ----------
    raw_args : list, optional
        Arguments to pass to the subparser.
    """
    # Set the default for the `raw_args`
    if raw_args is None:
        raw_args = argv[1:]
    # Create an argparser object
    cli_parser = argparse.ArgumentParser()
    # Allow for subparsers to be created
    subparsers = cli_parser.add_subparsers()
    # Add the subparser from each class
    for sub_class in subparser_classes:
        sub_class.populate_subparser(subparsers)
    # Parse the `raw_args`, will automatically call the correct subparser
    cli_args, cli_overrides = cli_parser.parse_known_args(raw_args)
    # Convert to a dictionary so values can be accessed
    cli_args_dict = vars(cli_args)
    # Add all CLI overrides to the cli_args to make them accessible
    cli_args_dict['overrides'] = cli_overrides
    # Ensure a subparser was called
    if 'main_class' not in cli_args_dict:
        cli_parser.error('No command provided')
    # Pass all argparser arguments to the popped off `main_class` and run it
    cli_args_dict.pop('main_class')(**cli_args_dict).run()
