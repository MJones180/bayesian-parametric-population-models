def populate_subparser(subparsers):
    subparser = subparsers.add_parser("ppd")

    subparser.add_argument(
        "n_samples",
        type=int,
        help="Number of PPD samples to draw.",
    )
    subparser.add_argument(
        "posterior_file",
        help="HDF5 file containing cleaned posterior samples.",
    )

    subparser.add_argument(
        "ppd_output",
        help="Text file to write PPD samples to.",
    )

    subparser.add_argument(
        "--record-hyperparameters",
        action="store_true", dest="keep_params",
        help="Record hyperparameters corresponding to each PPD sample.",
    )
    subparser.add_argument(
        "--shuffle",
        action="store_true",
        help="Shuffle samples to remove correlations due to being drawn from "
             "the same hyperparameter sample.",
    )

    subparser.add_argument(
        "--thin-stuck",
        action="store_true",
        help="Pre-process posterior samples, removing any with log(post) = "
             "-inf, which typically correspond to stuck MCMC walkers.",
    )

    subparser.add_argument(
        "--seed",
        type=int, default=None,
        help="Random seed.",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_population

    from ....posterior import H5CleanedPosteriorSamples
    from ....ppd import PPDSampler

    import numpy

    # Seed the RNG.
    random_state = numpy.random.RandomState(cli_args.seed)

    # Load in the posterior samples.
    with H5CleanedPosteriorSamples(cli_args.posterior_file) as post_samples:
        parameters = post_samples.get_params(...)
        population = get_population(post_samples.metadata["n_powerlaw_parts"])

        # Thin samples with log(prob) == -inf if requested.
        if cli_args.thin_stuck:
            log_post = post_samples.get_posterior_log_prob(...)
            i_non_stuck = log_post != numpy.NINF

            for param_name in list(parameters.keys()):
                parameters[param_name] = parameters[param_name][i_non_stuck]

    # Create the PPD sampler, draw samples, and save to a file.
    ppd_sampler = PPDSampler(population, random_state=random_state)
    ppd_sampler.sample_to_ascii(
        cli_args.ppd_output,
        cli_args.n_samples,
        parameters,
        keep_params=cli_args.keep_params, shuffle=cli_args.shuffle,
    )
