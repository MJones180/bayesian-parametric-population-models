def populate_subparser(subparsers):
    from ....astro_models import eos

    subparser = subparsers.add_parser("plot_mass_distribution")

    subparser.add_argument("cleaned_posterior_samples_hdf5")
    subparser.add_argument("output_plot_intensity_m1")
    subparser.add_argument("output_plot_pdf_m1")
    subparser.add_argument("output_plot_intensity_q")
    subparser.add_argument("output_plot_pdf_q")

    subparser.add_argument(
        "--output-files",
        metavar=(
            "FILE_INTENSITY_M1", "FILE_PDF_M1",
            "FILE_INTENSITY_Q", "FILE_PDF_Q",
        ),
        nargs=4,
    )

    subparser.add_argument("--n-plot-points", type=int, default=50)
    subparser.add_argument("--n-samples-per-realization", type=int, default=300)

#    subparser.add_argument("--overlay-synthetic-truth", action="store_true")

    subparser.add_argument("--seed", type=int)

    subparser.add_argument(
        "--force",
        action="store_true",
        help="Overwrite existing HDF5 output files.",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_population

    from ....posterior import H5CleanedPosteriorSamples
    from ....coordinate import CoordinateSystem
    from ....credible_regions import CredibleRegions1D
    from ....astro_models.coordinates import (
        m1_source_coord, m2_source_coord, q_coord,
    )

    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import scipy.stats

    random_state = numpy.random.RandomState(cli_args.seed)

    # Determine whether HDF5 outputs are being made, and pull out filenames
    hdf5_out = cli_args.output_files is not None
    if hdf5_out:
        (
            output_file_intensity_m1, output_file_pdf_m1,
            output_file_intensity_q, output_file_pdf_q,
        ) = cli_args.output_files

        hdf5_mode = "w" if cli_args.force else "w-"

    # Load in the posterior samples
    post_samples = H5CleanedPosteriorSamples(
        cli_args.cleaned_posterior_samples_hdf5
    )
    with post_samples:
        # Determine how many powerlaws are in the broken powerlaw
        n_parts = post_samples.metadata["n_powerlaw_parts"]
        # Get population object.
        population = get_population(n_parts)
        # Split into p(m1) and p(q)
        population_m1 = population.to_coords(CoordinateSystem(m1_source_coord))
        population_q = population.to_coords(CoordinateSystem(q_coord))

        # Create wrapper functions for the PDF's and intensity functions, since
        # they need to be used with a single input observable, but the
        # population objects expect a list of input observables.
        def m1_intensity(m1, parameters):
            return population_m1.intensity([m1], parameters)
        def m1_pdf(m1, parameters):
            return population_m1.pdf([m1], parameters)
        def q_intensity(q, parameters):
            return population_q.intensity([q], parameters)
        def q_pdf(q, parameters):
            return population_q.pdf([q], parameters)


        # Draw all posterior samples.
        parameter_samples = post_samples.get_params(...)

        # Determine m_min and m_max
        m_min_name, m_max_name = "mass_break0", "mass_break"+str(n_parts)
        if m_min_name in post_samples.constants:
            # Use the constant value.
            m_min = post_samples.constants[m_min_name]
        else:
            # Use the lowest value in the posterior sample set.
            m_min = parameter_samples[m_min_name].min()
        if m_max_name in post_samples.constants:
            # Use the constant value.
            m_max = post_samples.constants[m_max_name]
        else:
            # Use the highest value in the posterior sample set.
            m_max = parameter_samples[m_max_name].max()
        # Determine q_min and q_max
        if "q_min" in post_samples.constants:
            # Use the constant value.
            q_min = post_samples.constants["q_min"]
        else:
            # Use the lowest value in the posterior sample set.
            q_min = parameter_samples["q_min"].min()
        if "q_max" in post_samples.constants:
            # Use the constant value.
            q_max = post_samples.constants["q_max"]
        else:
            # Use the highest value in the posterior sample set.
            q_max = parameter_samples["q_max"].max()

        ## Plot m1
        m1_grid = numpy.logspace(
            numpy.log10(m_min), numpy.log10(m_max),
            cli_args.n_plot_points,
        )
        m1_intensity_ci = CredibleRegions1D.from_samples(
            m1_intensity, m1_grid, parameter_samples,
        )

        fig, ax = plt.subplots(figsize=[6,4])

        m1_intensity_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
            cache=hdf5_out,
        )
        # Save to file if provided
        if hdf5_out:
            m1_intensity_ci.save(output_file_intensity_m1, mode=hdf5_mode)


        ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$m_{1,\mathrm{source}} / M_\odot$")
        ax.set_ylabel(r"$\mathcal{R} \, p(m_{1,\mathrm{source}} / M_\odot)$")

        ax.set_xscale("log")
        ax.set_yscale("log")

        fig.savefig(cli_args.output_plot_intensity_m1)
        # Free up memory
        plt.close(fig)
        del m1_intensity_ci

        m1_pdf_ci = CredibleRegions1D.from_samples(
            m1_pdf, m1_grid, parameter_samples,
        )

        fig, ax = plt.subplots(figsize=[6,4])

        m1_pdf_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
            cache=hdf5_out,
        )
        # Save to file if provided
        if hdf5_out:
            m1_pdf_ci.save(output_file_pdf_m1, mode=hdf5_mode)


        ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$m_{1,\mathrm{source}} / M_\odot$")
        ax.set_ylabel(r"$p(m_{1,\mathrm{source}} / M_\odot)$")

        ax.set_xscale("log")
        ax.set_yscale("log")

        fig.savefig(cli_args.output_plot_pdf_m1)
        # Free up memory
        plt.close(fig)
        del m1_grid, m1_pdf_ci


        ## Plot q
        q_grid = numpy.linspace(q_min, q_max, cli_args.n_plot_points)
        q_intensity_ci = CredibleRegions1D.from_samples(
            q_intensity, q_grid, parameter_samples,
        )

        fig, ax = plt.subplots(figsize=[6,4])

        q_intensity_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
            cache=hdf5_out,
        )
        # Save to file if provided
        if hdf5_out:
            q_intensity_ci.save(output_file_intensity_q, mode=hdf5_mode)

        ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$m_2 / m_1$")
        ax.set_ylabel(r"$\mathcal{R} \, p(q)$")

        ax.set_yscale("log")

        fig.savefig(cli_args.output_plot_intensity_q)
        # Free up memory
        plt.close(fig)
        del q_intensity_ci

        q_pdf_ci = CredibleRegions1D.from_samples(
            q_pdf, q_grid, parameter_samples,
        )

        fig, ax = plt.subplots(figsize=[6,4])

        q_pdf_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
            cache=hdf5_out,
        )
        # Save to file if provided
        if hdf5_out:
            q_pdf_ci.save(output_file_pdf_q, mode=hdf5_mode)

        ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$m_2 / m_1$")
        ax.set_ylabel(r"$p(q)$")

        ax.set_yscale("log")

        fig.savefig(cli_args.output_plot_pdf_q)
