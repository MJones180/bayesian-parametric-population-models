import types
import numpy
from ....astro_models.building_blocks.beta import mean_variance_to_alpha_beta

__all__ = [
    "Prior",
]

class Prior(object):
    def __init__(
            self,
            random_state,
            settings, n_parts,
            constants=None, duplicates=None,
            xpy: types.ModuleType=numpy,
        ):
        self.random_state = random_state
        self.settings = settings
        self.n_parts = n_parts

        if constants is None:
            constants = {}
        if duplicates is None:
            duplicates = {}

        self.constants = constants
        self.duplicates = duplicates
        self.skip_params = set(list(constants.keys()) + list(duplicates.keys()))

        self.xpy = xpy

    def log_prior(self, parameters):
        """
        Evaluates the log(prior) for the given population parameters.
        """
        shape = next(iter(parameters.values())).shape
        log_prior = self.xpy.zeros(shape, dtype=numpy.float64)
        valid = self.xpy.ones(shape, dtype=bool)

        if "rate" not in self.skip_params:
            valid &= parameters["rate"] > 0.0
            settings = self.settings["rate"]
            dist = settings["dist"]
            params = settings["params"]

            if dist == "uniform":
                valid &= parameters["rate"] >= params["min"]
                valid &= parameters["rate"] <= params["max"]
            elif dist == "log-uniform":
                valid &= parameters["rate"] >= params["min"]
                valid &= parameters["rate"] <= params["max"]
                log_prior -= self.xpy.log(parameters["rate"])
            elif dist == "jeffries":
                log_prior -= 0.5 * self.xpy.log(parameters["rate"])
            else:
                raise NotImplementedError()

        for param, values in parameters.items():
            # Rate handled specially.
            if param == "rate":
                continue
            # Skip if constant or duplicate.
            if param in self.skip_params:
                continue

            settings = self.settings[param]
            dist = settings["dist"]
            params = settings["params"]

            if dist == "uniform":
                valid &= values >= params["min"]
                valid &= values <= params["max"]
            elif dist == "log-uniform":
                valid &= values >= params["min"]
                valid &= values <= params["max"]
                log_prior -= self.xpy.log(values)
            else:
                raise NotImplementedError()

        # Enforce mass cutoff limits well defined
        for i in range(self.n_parts):
            lo = parameters["mass_break"+str(i)]
            hi = parameters["mass_break"+str(i+1)]
            valid &= lo < hi

        # Return the accumulated log(prior), or -inf where there's no support.
        return self.xpy.where(valid, log_prior, self.xpy.NINF)

    def init_walkers(self, n_walkers):
        """
        Draw samples from the population prior distribution to initialize the
        walkers.
        """
        from ....posterior import get_params
        from ....prob import sample_with_cond
        from .population import get_param_names

        param_names = get_param_names(self.n_parts)

        def sample_prior_single(N, prior_settings):
            """
            Samples from a prior with given settings.
            """
            # Determines the type of distribution
            dist = prior_settings["dist"]
            # Determines the parameters of the distribution (e.g., {min,max})
            params = prior_settings["params"]

            # Sample from uniform distribution.
            if dist == "uniform":
                return self.random_state.uniform(
                    params["min"], params["max"], N,
                )
            elif dist == "log-uniform":
                return self.xpy.exp(
                    self.random_state.uniform(
                        self.xpy.log(params["min"]),
                        self.xpy.log(params["max"]),
                        N,
                    )
                )
            # Type of distribution is unknown.
            else:
                raise NotImplementedError(
                    "No implementation for prior '{}'".format(dist)
                )

        def sample_prior(N):
            """
            Iterate over all free parameters and draw ``N`` samples from their
            priors. Then combine into an array, where each column holds the values
            from one parameter.
            """
            return self.xpy.column_stack(tuple((
                sample_prior_single(N, self.settings[param])
                for param in param_names
                # Skip over non-free params.
                if param not in self.skip_params
            )))

        def cond(samples):
            n_samples, n_params = self.xpy.shape(samples)

            parameters = get_params(
                samples,
                self.constants, self.duplicates,
                param_names,
            )

            # Initialize condition array to all True (keep all samples).
            # We will downselect later.
            valid = self.xpy.ones(n_samples, dtype=bool)

            # Enforce mass cutoff limits well defined
            for i in range(self.n_parts):
                lo = parameters["mass_break"+str(i)]
                hi = parameters["mass_break"+str(i+1)]
                valid &= lo < hi

            return valid

        # Sample from the prior, rejecting invalid samples.
        return sample_with_cond(sample_prior, shape=n_walkers, cond=cond)
