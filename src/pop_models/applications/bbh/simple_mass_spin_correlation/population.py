import types
import numpy

from ....coordinate import CoordinateSystem
from ....population import SeparableCoordinatePopulation
from ....astro_models.building_blocks import (
    powerlaw, beta_conditional, truncnorm,
)
from ....astro_models.coordinates import (
    m1_source_coord, m2_source_coord,
    chi1_coord, chi2_coord,
    costilt1_coord, costilt2_coord,
    transformations,
)


coord_system = CoordinateSystem(
    m1_source_coord, m2_source_coord,
    chi1_coord, chi2_coord,
    costilt1_coord, costilt2_coord,
)

def get_population(M_max: float, xpy: types.ModuleType=numpy):
    population_mass = powerlaw.ComponentMassPowerlawPopulation(
        M_max,
        rate_name="rate", m1_index_name="alpha_m", q_index_name="beta_q",
        mmin_name="m_min", mmax_name="m_max",
    )

    population_mass_a1 = (
        beta_conditional.LinearTanhBetaConditionalPopulation(
            population_mass,
            chi1_coord, m1_source_coord,
            loc=0.0, scale=1.0,
            rate_name="rate",
            a_name="a1", b_name="b1", c_name="c1", y_prime_name="m1_ref",
            variance_name="Var_chi1",
            xpy=xpy,
        )
    )
    population_mass_a1_a2 = (
        beta_conditional.LinearTanhBetaConditionalPopulation(
            population_mass_a1,
            chi2_coord, m2_source_coord,
            loc=0.0, scale=1.0,
            rate_name="rate",
            a_name="a2", b_name="b2", c_name="c2", y_prime_name="m2_ref",
            variance_name="Var_chi2",
            xpy=xpy,
        )
    )

    population_cos1 = truncnorm.TruncnormPopulation(
        costilt1_coord,
        lower=-1.0, upper=+1.0,
        rate_name="rate", mu_name="mu_cos1", sigma_name="sigma_cos1",
        transformations=transformations,
        xpy=xpy,
    )
    population_cos2 = truncnorm.TruncnormPopulation(
        costilt2_coord,
        lower=-1.0, upper=+1.0,
        rate_name="rate", mu_name="mu_cos2", sigma_name="sigma_cos2",
        transformations=transformations,
        xpy=xpy,
    )

    return SeparableCoordinatePopulation(
        population_mass_a1_a2,
        population_cos1, population_cos2,
        transformations=transformations,
    ).to_coords(coord_system, use_transformations=False)


param_names = [
    "rate",
    "alpha_m", "beta_q", "m_min", "m_max",
    "a1", "b1", "c1", "m1_ref", "Var_chi1", "mu_cos1", "sigma_cos1",
    "a2", "b2", "c2", "m2_ref", "Var_chi2", "mu_cos2", "sigma_cos2",
]
