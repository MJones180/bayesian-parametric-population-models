def populate_subparser(subparsers):
    subparser = subparsers.add_parser("plot_delta_mean")

    subparser.add_argument("cleaned_posterior_samples_hdf5")
    subparser.add_argument("output_hdf5")
    subparser.add_argument("output_plot")

#    subparser.add_argument("--overlay-synthetic-truth", action="store_true")

    subparser.add_argument(
        "--force",
        action="store_true",
        help="Force HDF5 output even if file exists.",
    )

    subparser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Matplotlib backend to use for plotting.",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    import h5py

    import numpy
    import matplotlib
    matplotlib.use(cli_args.mpl_backend)
    import matplotlib.pyplot as plt
    import corner

    from .population import get_population

    from ....posterior import H5CleanedPosteriorSamples
    from ....astro_models.building_blocks.beta_conditional import (
        LinearTanhBetaConditionalPopulation,
    )
    from ....astro_models.coordinates import m1_source_coord, chi1_coord

    # Load in the colors to use.
    colors = ["C{}".format(i) for i in range(10)]

    # Construct the spin distribution, with no base m1 population, as we'll be
    # directly accessing the conditional pdf/rvs functions.
    pop_chi1_given_m1 = LinearTanhBetaConditionalPopulation(
        None,
        chi1_coord, m1_source_coord,
        loc=0.0, scale=1.0,
        rate_name="rate",
        a_name="a1", b_name="b1", c_name="c1", y_prime_name="m1_ref",
        variance_name="Var_chi1",
        xpy=numpy,
    )

    # Load in the posterior samples
    post_samples = H5CleanedPosteriorSamples(
        cli_args.cleaned_posterior_samples_hdf5
    )
    with post_samples:
        # Draw all posterior samples.
        parameter_samples = post_samples.get_params(...)

        # Determine endpoints.
        m_min, m_max = parameter_samples["m_min"], parameter_samples["m_max"]

        # Compute deltas
        delta_mean = pop_chi1_given_m1.get_delta_mean(
            (m_min,), (m_max,),
            parameter_samples,
        )

        # Write samples to HDF5 file
        h5_mode = "w" if cli_args.force else "w-"
        with h5py.File(cli_args.output_hdf5, h5_mode) as h5_file:
            h5_file.create_dataset("delta_mean", data=delta_mean)

        # Extract Var[chi] for corner plot, and combine into one array.
        Var_chi = parameter_samples["Var_chi1"]
        data = numpy.column_stack((delta_mean, Var_chi))

        fig = corner.corner(
            data,
            labels=[r"$\Delta\mathbb{E}[\chi]$", r"$\mathrm{Var}[\chi]$"],
            levels=[0.5, 0.9],
            plot_density=False,
            plot_datapoints=False,
            no_fill_contours=True,
            contour_kwargs={"linestyles" : ["dashed", "solid"]},
        )
        fig.savefig(cli_args.output_plot)
