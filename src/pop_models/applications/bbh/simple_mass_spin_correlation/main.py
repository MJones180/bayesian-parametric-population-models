from . import (
    inference,
    ppd,
    plot_mass_distribution,
    plot_mean_spin,
    plot_delta_mean,
    plot_spin_ks_test,
    pearson_correlation,
    synthetic_observations,
    test_pop_sampling,
)

subparser_modules = [
    inference,
    ppd,
    plot_mass_distribution,
    plot_mean_spin,
    plot_delta_mean,
    plot_spin_ks_test,
    pearson_correlation,
    synthetic_observations,
    test_pop_sampling,
]

def make_parser():
    import argparse

    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest="command")

    for module in subparser_modules:
        module.populate_subparser(subparsers)

    return parser


def main(raw_args=None):
    if raw_args is None:
        import sys
        raw_args = sys.argv[1:]

    cli_parser = make_parser()
    cli_args = cli_parser.parse_args(raw_args)

    if cli_args.command is None:
        cli_parser.error("No command provided")

    return cli_args.main_func(cli_args)
