def populate_subparser(subparsers):
    subparser = subparsers.add_parser("plot_mean_spin")

    subparser.add_argument("cleaned_posterior_samples_hdf5")
    subparser.add_argument("output_plot")

    subparser.add_argument("--n-plot-points", type=int, default=50)

#    subparser.add_argument("--overlay-synthetic-truth", action="store_true")

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt

    from .population import get_population

    from ....posterior import H5CleanedPosteriorSamples
    from ....credible_regions import CredibleRegions1D
    from ....astro_models.building_blocks.beta_conditional import (
        linear_tanh_transform,
    )

    # Load in the posterior samples
    post_samples = H5CleanedPosteriorSamples(
        cli_args.cleaned_posterior_samples_hdf5
    )
    with post_samples:
        def mean_spin_from_m1(m1, parameters):
            return linear_tanh_transform(
                m1,
                parameters["a1"], parameters["b1"], parameters["c1"],
                parameters["m1_ref"],
                True,
            )

        # Draw all posterior samples.
        parameter_samples = post_samples.get_params(...)

        # Determine m_min and m_max
        m_min_name, m_max_name = "m_min", "m_max"
        if m_min_name in post_samples.constants:
            # Use the constant value.
            m_min = post_samples.constants[m_min_name]
        else:
            # Use the lowest value in the posterior sample set.
            m_min = parameter_samples[m_min_name].min()
        if m_max_name in post_samples.constants:
            # Use the constant value.
            m_max = post_samples.constants[m_max_name]
        else:
            # Use the highest value in the posterior sample set.
            m_max = parameter_samples[m_max_name].max()

        # Set up grid, and compute CI's at each point on grid.
        m1_grid = numpy.logspace(
            numpy.log10(m_min), numpy.log10(m_max),
            cli_args.n_plot_points,
        )
        mean_spin_ci = CredibleRegions1D.from_samples(
            mean_spin_from_m1, m1_grid, parameter_samples,
        )

        # Make the plot
        fig, ax = plt.subplots(figsize=[6,4])

        mean_spin_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
        )

        ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$m_{1,\mathrm{source}} / M_\odot$")
        ax.set_ylabel(r"$\mathbb{E}[\chi_1]$")

        ax.set_xscale("log")

        fig.savefig(cli_args.output_plot)
