import types
import numpy

from pop_models.coordinate import CoordinateSystem
from pop_models.population import SeparableCoordinatePopulation
from pop_models.astro_models.building_blocks import (
    powerlaw, beta, truncnorm,
)
from pop_models.astro_models.coordinates import (
    m1_source_coord, m2_source_coord,
    chi1_coord, chi2_coord,
    costilt1_coord, costilt2_coord,
    transformations,
)


coord_system = CoordinateSystem(
    m1_source_coord, m2_source_coord,
    chi1_coord, chi2_coord,
    costilt1_coord, costilt2_coord,
)

def get_population(M_max: float, xpy: types.ModuleType=numpy):
    population_mass = powerlaw.ComponentMassPowerlawPopulation(
        M_max,
        rate_name="rate", m1_index_name="alpha_m", q_index_name="beta_q",
        mmin_name="m_min", mmax_name="m_max",
    )

    population_a1 = beta.BetaPopulation(
        chi1_coord,
        loc=0.0, scale=1.0,
        parameterization="mean_variance",
        rate_name="rate", mean_name="E_chi1", variance_name="Var_chi1",
        transformations=transformations,
        xpy=xpy,
    )
    population_a2 = beta.BetaPopulation(
        chi2_coord,
        loc=0.0, scale=1.0,
        parameterization="mean_variance",
        rate_name="rate", mean_name="E_chi2", variance_name="Var_chi2",
        transformations=transformations,
        xpy=xpy,
    )

    population_cos1 = truncnorm.TruncnormPopulation(
        costilt1_coord,
        lower=-1.0, upper=+1.0,
        rate_name="rate", mu_name="mu_cos1", sigma_name="sigma_cos1",
        transformations=transformations,
        xpy=xpy,
    )
    population_cos2 = truncnorm.TruncnormPopulation(
        costilt2_coord,
        lower=-1.0, upper=+1.0,
        rate_name="rate", mu_name="mu_cos2", sigma_name="sigma_cos2",
        transformations=transformations,
        xpy=xpy,
    )

    return SeparableCoordinatePopulation(
        population_mass,
        population_a1, population_a2,
        population_cos1, population_cos2,
        transformations=transformations,
    )
