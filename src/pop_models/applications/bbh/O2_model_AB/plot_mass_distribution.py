def populate_subparser(subparsers):
    subparser = subparsers.add_parser("plot_mass_distribution")

    subparser.add_argument("cleaned_posterior_samples_hdf5")
    subparser.add_argument("output_plot_intensity_m1")
    subparser.add_argument("output_plot_pdf_m1")
    subparser.add_argument("output_plot_intensity_q")
    subparser.add_argument("output_plot_pdf_q")

    subparser.add_argument(
        "--output-files",
        metavar=(
            "FILE_INTENSITY_M1", "FILE_PDF_M1",
            "FILE_INTENSITY_Q", "FILE_PDF_Q",
        ),
        nargs=4,
    )

    subparser.add_argument("--n-plot-points", type=int, default=50)
    subparser.add_argument("--n-samples-per-realization", type=int, default=300)

#    subparser.add_argument("--overlay-synthetic-truth", action="store_true")

    subparser.add_argument(
        "--n-oom",
        type=int,
        help="Limit plots' y-axes to the specified number of OoM from the "
             "peak.",
    )

    subparser.add_argument("--seed", type=int)

    subparser.add_argument(
        "--force",
        action="store_true",
        help="Overwrite existing HDF5 output files.",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_population

    from pop_models.posterior import H5CleanedPosteriorSamples
    from pop_models.coordinate import CoordinateSystem
    from pop_models.credible_regions import CredibleRegions1D
    from pop_models.astro_models.coordinates import (
        m1_source_coord, m2_source_coord, q_coord,
    )
    from pop_models.utils.plotting import limit_ax_oom

    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import scipy.stats

    random_state = numpy.random.RandomState(cli_args.seed)

    # Determine whether HDF5 outputs are being made, and pull out filenames
    hdf5_out = cli_args.output_files is not None
    if hdf5_out:
        (
            output_file_intensity_m1, output_file_pdf_m1,
            output_file_intensity_q, output_file_pdf_q,
        ) = cli_args.output_files

        hdf5_mode = "w" if cli_args.force else "w-"

    # Load in the posterior samples
    post_samples = H5CleanedPosteriorSamples(
        cli_args.cleaned_posterior_samples_hdf5
    )
    with post_samples:
        # Determine maximum mass
        M_max = post_samples.metadata["M_max"]
        # Get population object.
        population = get_population(M_max)
        # Split into p(m1) and p(q)
        population_m1 = population.to_coords(CoordinateSystem(m1_source_coord))
        population_q = population.to_coords(CoordinateSystem(q_coord))

        # Create wrapper functions for the PDF's and intensity functions, which
        # currently need to be built up with KDE's from samples, as there aren't
        # explicit mass marginals available right now.
        def m1_pdf(m1, parameters):
            m1_samples, = population_m1.rvs(
                cli_args.n_samples_per_realization, parameters,
                random_state=random_state,
            )

            obs_shape = m1.shape
            params_shape = m1_samples.shape[:-1]
            out_shape = params_shape + obs_shape

            pdf = numpy.empty(out_shape, dtype=numpy.float64)

            for idx in numpy.ndindex(*params_shape):
                m1_kde = scipy.stats.gaussian_kde(
                    [m1_samples[idx]],
                    bw_method="scott",
                )

                pdf[idx] = m1_kde([m1])

            return pdf
        def q_pdf(q, parameters):
            q_samples, = population_q.rvs(
                cli_args.n_samples_per_realization, parameters,
                random_state=random_state,
            )

            obs_shape = q.shape
            params_shape = q_samples.shape[:-1]
            out_shape = params_shape + obs_shape

            pdf = numpy.empty(out_shape, dtype=numpy.float64)

            for idx in numpy.ndindex(*params_shape):
                q_kde = scipy.stats.gaussian_kde(
                    [q_samples[idx]],
                    bw_method="scott",
                )

                pdf[idx] = q_kde([q])

            return pdf

        # Draw all posterior samples.
        parameter_samples = post_samples.get_params(...)

        # Determine m_min and m_max
        if "m_min" in post_samples.constants:
            # Use the constant value.
            m_min = post_samples.constants["m_min"]
        else:
            # Use the lowest value in the posterior sample set.
            m_min = parameter_samples["m_min"].min()
        if "m_max" in post_samples.constants:
            # Use the constant value.
            m_max = post_samples.constants["m_max"]
        else:
            # Use the highest value in the posterior sample set.
            m_max = parameter_samples["m_max"].max()

        # Determine q_min and q_max
        ## TODO: Actually figure out from posterior samples
        q_min, q_max = 1e-4, 1.0

        ## Plot m1
        m1_grid = numpy.logspace(
            numpy.log10(m_min), numpy.log10(m_max),
            cli_args.n_plot_points,
        )
        ## TODO: plot intensity

        m1_pdf_ci = CredibleRegions1D.from_samples(
            m1_pdf, m1_grid, parameter_samples,
        )

        fig, ax = plt.subplots(figsize=[6,4])

        m1_pdf_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
            cache=hdf5_out,
        )
        # Save to file if provided
        if hdf5_out:
            m1_pdf_ci.save(output_file_pdf_m1, mode=hdf5_mode)

        ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$m_{1,\mathrm{source}} / M_\odot$")
        ax.set_ylabel(r"$p(m_{1,\mathrm{source}} / M_\odot)$")

        ax.set_xscale("log")
        ax.set_yscale("log")

        if cli_args.n_oom is not None:
            limit_ax_oom(ax, cli_args.n_oom, "y")

        fig.savefig(cli_args.output_plot_pdf_m1)
        # Free up memory
        plt.close(fig)
        del m1_grid, m1_pdf_ci


        ## Plot q
        q_grid = numpy.linspace(q_min, q_max, cli_args.n_plot_points)
        ## TODO: plot intensity

        q_pdf_ci = CredibleRegions1D.from_samples(
            q_pdf, q_grid, parameter_samples,
        )

        fig, ax = plt.subplots(figsize=[6,4])

        q_pdf_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
            cache=hdf5_out,
        )
        # Save to file if provided
        if hdf5_out:
            q_pdf_ci.save(output_file_pdf_q, mode=hdf5_mode)

        ## TODO: implement synthetic truth overlay

        ax.set_xlabel(r"$m_2 / m_1$")
        ax.set_ylabel(r"$p(q)$")

        ax.set_yscale("log")

        if cli_args.n_oom is not None:
            limit_ax_oom(ax, cli_args.n_oom, "y")

        fig.savefig(cli_args.output_plot_pdf_q)
