import types
import numpy
from pop_models.utils import debug_verbose
from pop_models.astro_models.building_blocks.beta import (
    mean_variance_to_alpha_beta,
)

__all__ = [
    "Prior",
]

class Prior(object):
    def __init__(
            self,
            random_state, param_names,
            settings, M_max, no_spin_singularities=False,
            constants=None, duplicates=None,
            xpy: types.ModuleType=numpy,
        ):
        self.random_state = random_state
        self.settings = settings
        self.M_max = M_max
        self.no_spin_singularities = no_spin_singularities

        if constants is None:
            constants = {}
        if duplicates is None:
            duplicates = {}

        self.constants = constants
        self.duplicates = duplicates
        self.skip_params = set(list(constants.keys()) + list(duplicates.keys()))

        self.param_names = param_names
        self.variable_names = tuple(
            param_name for param_name in self.param_names
            if (param_name not in self.constants)
            and (param_name not in self.duplicates)
        )
        self.n_variables = len(self.variable_names)

        self.xpy = xpy

    def log_prior(self, parameters):
        """
        Evaluates the log(prior) for the given population parameters.
        """
        shape = next(iter(parameters.values())).shape
        log_prior = self.xpy.zeros(shape, dtype=numpy.float64)
        valid = numpy.ones(shape, dtype=bool)

        if "rate" not in self.skip_params:
            valid &= parameters["rate"] > 0.0
            settings = self.settings["rate"]
            dist = settings["dist"]
            params = settings["params"]

            if dist == "uniform":
                valid &= parameters["rate"] >= params["min"]
                valid &= parameters["rate"] <= params["max"]
            elif dist == "log-uniform":
                valid &= parameters["rate"] >= params["min"]
                valid &= parameters["rate"] <= params["max"]
                log_prior -= self.xpy.log(parameters["rate"])
            elif dist == "jeffries":
                log_prior -= 0.5 * self.xpy.log(parameters["rate"])
            else:
                raise NotImplementedError()

        for param, values in parameters.items():
            # Rate handled specially.
            if param == "rate":
                continue
            # Skip if constant or duplicate.
            if param in self.skip_params:
                continue

            settings = self.settings[param]
            dist = settings["dist"]
            params = settings["params"]

            if dist == "uniform":
                valid &= values >= params["min"]
                valid &= values <= params["max"]
            elif dist == "log-uniform":
                valid &= values >= params["min"]
                valid &= values <= params["max"]
                log_prior -= self.xpy.log(values)
            else:
                raise NotImplementedError()


        # Enforce mass cutoff limits well defined
        valid &= parameters["m_min"] < parameters["m_max"]

        # Enforce maximum total mass obeyed.
        if self.M_max is not None:
            valid &= parameters["m_min"] + parameters["m_max"] <= self.M_max

        # Enforce spin magnitude parameters well defined
        E_chi1 = parameters["E_chi1"]
        E_chi2 = parameters["E_chi2"]
        Var_chi1 = parameters["Var_chi1"]
        Var_chi2 = parameters["Var_chi2"]

        alpha_chi1, beta_chi1 = mean_variance_to_alpha_beta(E_chi1, Var_chi1)
        alpha_chi2, beta_chi2 = mean_variance_to_alpha_beta(E_chi2, Var_chi2)

        spin_params = [alpha_chi1, beta_chi1, alpha_chi2, beta_chi2]
        if self.no_spin_singularities:
            for spin_param in spin_params:
                valid &= spin_param >= 1.0
        else:
            for spin_param in spin_params:
                valid &= spin_param >= 0.0

        # Enforce spin tilt parameters well defined
        valid &= parameters["sigma_cos1"] > 0.0
        valid &= parameters["sigma_cos2"] > 0.0

        # Return the accumulated log(prior), or -inf where there's no support.
        return self.xpy.where(valid, log_prior, self.xpy.NINF)

    def init_walkers(self, n_walkers, pop_inf=None):
        """
        Draw samples from the population prior distribution to initialize the
        walkers.
        """
        from pop_models import posterior

        if pop_inf is None:
            def validate_prob(parameters):
                # Return the log(prior)
                return self.log_prior(parameters)
        else:
            def validate_prob(parameters):
                log_post_and_prior = (
                    pop_inf.log_posterior_from_params(parameters)
                )
                # Return the log(posterior)
                return log_post_and_prior[...,0]


        def sample_prior_single(N, prior_settings):
            """
            Samples from a prior with given settings.
            """
            # Determines the type of distribution
            dist = prior_settings["dist"]
            # Determines the parameters of the distribution (e.g., {min,max})
            params = prior_settings["params"]

            # Sample from uniform distribution.
            if dist == "uniform":
                return self.random_state.uniform(
                    params["min"], params["max"], N,
                )
            elif dist == "log-uniform":
                return self.xpy.exp(
                    self.random_state.uniform(
                        self.xpy.log(params["min"]),
                        self.xpy.log(params["max"]),
                        N,
                    )
                )
            # Type of distribution is unknown.
            else:
                raise NotImplementedError(
                    "No implementation for prior '{}'".format(dist)
                )

        def sample_prior(N):
            """
            Iterate over all free parameters and draw ``N`` samples from their
            priors. Then combine into an array, where each column holds the
            values from one parameter.
            """
            return tuple(
                sample_prior_single(N, self.settings[param])
                for param in self.param_names
                # Skip over non-free params.
                if param not in self.skip_params
            )

        n_walkers_to_go = n_walkers

        params_init = [[] for _ in range(self.n_variables)]

        while n_walkers_to_go > 0:
            debug_verbose(
                "Initializing {}/{} walkers".format(n_walkers_to_go, n_walkers),
                mode="mcmc_init", flush=True,
            )

            params_init_trial = sample_prior(n_walkers_to_go)
            parameters_trial = posterior.get_params(
                numpy.column_stack(params_init_trial),
                self.constants, self.duplicates,
                self.param_names,
            )

            i_valid = numpy.isfinite(validate_prob(parameters_trial))
            n_valid = numpy.count_nonzero(i_valid)

            n_walkers_to_go -= n_valid

            for i in range(self.n_variables):
                params_init[i].append(params_init_trial[i][i_valid])

        return numpy.column_stack(tuple(
            numpy.concatenate(tuple(param_list))
            for param_list in params_init
        ))
