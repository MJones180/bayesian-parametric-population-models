import typing

def populate_subparser(subparsers):
    subparser = subparsers.add_parser("event_contribution")

    subparser.add_argument(
        "event",
        help="Posterior sample file for the event of interest.",
    )
    subparser.add_argument(
        "posterior_input",
        help="HDF5 file containing posterior samples where you would like to "
             "compute the event's contribution.",
    )
    subparser.add_argument(
        "output",
        help="HDF5 file to store event contribution in.",
    )

    subparser.add_argument(
        "--weights-field",
        help="Name of field in input posteriors that specifies weights to "
             "apply to posterior samples.  Will multiply by these values if "
             "provided.",
    )
    subparser.add_argument(
        "--inv-weights-field",
        help="Name of field in input posteriors that specifies inverse weights "
             "to apply to posterior samples.  Will divide by these values if "
             "provided.",
    )

    subparser.add_argument(
        "--batch-size",
        type=int, default=128,
        help="Number of posterior samples to process at once.",
    )

    subparser.add_argument(
        "--force",
        action="store_true",
        help="Force HDF5 output, even if file already exists. Will overwrite.",
    )

    subparser.add_argument(
        "--seed",
        type=int, default=None,
        help="Random seed.",
    )

    subparser.add_argument(
        "--swmr-mode",
        action="store_true",
        help="Open file in single-writer multiple-reader (SWMR) mode, so that "
             "other processes may read from the file while it's running, and "
             "so that it will be more resistant to data corruption.",
    )

    subparser.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="Use verbose output.",
    )
    subparser.add_argument(
        "--debug",
        nargs="*",
        help="Output debugging messages.  Use with no arguments to display all "
             "debugging information, or specify the debugging mode(s).",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_population, coord_system

    from pop_models.detection import (
        DeterministicReweightedPosteriorsDetectionLikelihood,
    )
    from pop_models.posterior import (
        H5CleanedPosteriorSamples,
        PopulationInference,
    )
    from pop_models.utils import debug_verbose

    from pop_models.astro_models.detections.cbc.pre_sampled import (
        CBCPreSampledDetection
    )

    import h5py
    import json
    import numpy

    import os

    # Set debugging mode
    if cli_args.debug is not None:
        if len(cli_args.debug) == 0:
            debug_verbose.full_output_on()
        else:
            debug_verbose.enable_modes(*cli_args.debug)

    random_state = numpy.random.RandomState(cli_args.seed)

    # Load input posterior samples.
    post_samples = H5CleanedPosteriorSamples(cli_args.posterior_input)

    metadata = post_samples.metadata
    n_powerlaws = metadata["n_powerlaws"]
    n_gaussians = metadata["n_gaussians"]
    M_max = metadata["M_max"]
    no_spin_singularities = metadata["no_spin_singularities"]
    # Default behavior was True before this was stored in metadata.
    same_gauss_mass_cutoffs = metadata.get("same_gauss_mass_cutoffs", True)

    debug_verbose(
        "Metadata: {}".format(dict(metadata)),
        mode="event_contribution", flush=True,
    )

    # Get appropriate population object.
    population = get_population(
        n_powerlaws, n_gaussians, M_max,
        same_gauss_mass_cutoffs=same_gauss_mass_cutoffs,
        no_spin_singularities=no_spin_singularities,
    )
    debug_verbose(
        "Population initialized: {}".format(population),
        mode="event_contribution", flush=True,
    )

    # Open tabular files for event posteriors.
    debug_verbose(
        "Loading in detection from file: {}".format(cli_args.event),
        mode="event_contribution", flush=True,
    )
    detection = CBCPreSampledDetection.load(
        cli_args.event,
        coord_system=coord_system,
        weights_field=cli_args.weights_field,
        inv_weights_field=cli_args.inv_weights_field,
    )
    debug_verbose(
        "Detection loaded: {}".format(detection),
        mode="event_contribution", flush=True,
    )

    # Create detection*population integral evaluator.
    debug_verbose(
        "Constructing detection likelihood integrator",
        mode="event_contribution", flush=True,
    )
    detection_likelihood = DeterministicReweightedPosteriorsDetectionLikelihood(
        population, detection,
    )
    debug_verbose(
        "Detection likelihood integrator loaded: {}"
        .format(detection_likelihood),
        mode="event_contribution", flush=True,
    )

    # Open output file for writing.
    debug_verbose(
        "Creating output file:", cli_args.output,
        mode="event_contribution", flush=True,
    )
    mode = "w" if cli_args.force else "w-"
    with h5py.File(cli_args.output, mode) as out_file:
        n_samples = len(post_samples)

        debug_verbose(
            "Creating output dataset",
            mode="event_contribution", flush=True,
        )
        event_contributions = out_file.create_dataset(
            "event_contribution", (n_samples,),
        )

        debug_verbose(
            "Computing event contributions in",
            cli_args.batch_size, "sample batches",
            mode="event_contribution", flush=True,
        )
        for i_start in range(0, n_samples, cli_args.batch_size):
            i_stop = min(i_start+cli_args.batch_size, n_samples)
            slc = slice(i_start, i_stop)

            params = post_samples.get_params(slc)
            event_contributions[slc] = (
                detection_likelihood(params) / population.normalization(params)
            )

            debug_verbose(
                "Computed contribution from samples", i_start, "to", i_stop-1,
                mode="event_contributions", flush=True,
            )

    debug_verbose(
        "Completed.",
        mode="event_contributions", flush=True,
    )
