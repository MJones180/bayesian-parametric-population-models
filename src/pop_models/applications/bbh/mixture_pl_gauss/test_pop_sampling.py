def populate_subparser(subparsers):
    subparser = subparsers.add_parser("test_pop_sampling")

    subparser.add_argument(
        "constants",
        help="A constants.json file specifying every parameter.",
    )
    subparser.add_argument(
        "output_plot",
        help="File to save plot to.",
    )

    subparser.add_argument(
        "--n-powerlaws",
        type=int, default=0,
        help="Number of powerlaw components to include (default is 0).",
    )
    subparser.add_argument(
        "--n-gaussians",
        type=int, default=0,
        help="Number of Gaussian components to include (default is 0).",
    )

    subparser.add_argument(
        "--total-mass-max",
        type=float, default=100.0,
        help="Maximum total mass allowed.",
    )

    subparser.add_argument(
        "--n-samples",
        type=int, default=1000,
        help="Number of random samples to draw from model.",
    )
    subparser.add_argument(
        "--seed",
        type=int,
        help="Seed for the random-number-generator.",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from . import utils
    from .population import get_param_names, get_population, coord_system
    from ....astro_models.coordinates import (
        m1_source_coord, m2_source_coord,
        chi1_coord, chi2_coord,
        costilt1_coord, costilt2_coord,
    )

    import warnings

    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import corner

    import numbers

    import json

    # Initialize RNG
    random_state = numpy.random.RandomState(cli_args.seed)

    # Get list of param names.
    param_names = get_param_names(cli_args.n_powerlaws, cli_args.n_gaussians)

    # Load in constants, and validate they are properly structured.
    with open(cli_args.constants, "r") as constants_file:
        constants = json.load(constants_file)
    assert isinstance(constants, dict)
    for k, v in constants.items():
        assert isinstance(k, str)
        assert isinstance(v, numbers.Number)
    # Expand any shorthands in `constants`
    constants = utils.expand_mixture_labels(
        constants,
        {"pl" : cli_args.n_powerlaws, "g" : cli_args.n_gaussians},
    )

    # Validate that we have precisely the needed parameters specified.
    missing_params = set(param_names) - set(constants.keys())
    extra_params = set(constants.keys()) - set(param_names)
    if (len(missing_params) != 0) or (len(extra_params) != 0):
        msg_parts = []
        if len(missing_params) != 0:
            msg_parts.append("Missing parameters: {}".format(missing_params))
        if len(extra_params) != 0:
            msg_parts.append("Extra parameters: {}".format(extra_params))
        raise ValueError("; ".join(msg_parts))

    # Convert values in `constants` to numpy arrays.
    constants = {k : numpy.asarray(v) for k, v in constants.items()}

    # Construct the population object.
    population = get_population(
        cli_args.n_powerlaws, cli_args.n_gaussians,
        M_max=cli_args.total_mass_max,
    )

    # Draw random samples.
    samples = population.rvs(
        cli_args.n_samples, constants,
        random_state=random_state,
    )
    n_params = len(samples)

    # Put up a warning if there are any NaN's.
    n_nans = numpy.count_nonzero(numpy.isnan(samples), axis=1)
    if numpy.any(n_nans != 0):
        warnings.warn(
            "Found NaN's: {}"
            .format(", ".join([
                "{} = {}".format(coord.name, n)
                for coord, n in zip(coord_system, n_nans)
            ]))
        )

    fig, axes = plt.subplots(ncols=n_params, figsize=(3*n_params, 4))

    log_scale_coords = {m1_source_coord, m2_source_coord}

    for i, coord in enumerate(coord_system):
        ax = axes[i]

        if coord in log_scale_coords:
            ax.set_yscale("log")

        ax.hist(
            samples[i],
            density=True, histtype="step",
            color="C0",
        )
        axes[i].set_xlabel(coord.name)

    fig.savefig(cli_args.output_plot)
