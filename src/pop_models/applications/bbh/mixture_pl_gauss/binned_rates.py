def populate_subparser(subparsers):
    subparser = subparsers.add_parser("binned_rates")

    subparser.add_argument(
        "bin_spec",
        help="File specifying bin locations.",
    )
    subparser.add_argument(
        "post_samples",
        help="Cleaned posterior samples file.",
    )
    subparser.add_argument(
        "output",
        help="Text file to output rates to.",
    )

    subparser.add_argument(
        "--abs-err",
        type=float, default=1e-2,
        help="Absolute error tolerance in each rate estimate.",
    )
    subparser.add_argument(
        "--rel-err",
        type=float, default=1e-6,
        help="Relative error tolerance in each rate estimate.",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    import json
    import numpy as np

    from .population import get_population

    from pop_models.bins import BinFactory
    from pop_models.binned_rates import compute_rates
    from pop_models.posterior import H5CleanedPosteriorSamples
    from pop_models.astro_models.coordinates import coordinates, transformations

    bin_parser = BinFactory(coordinates)
    with open(cli_args.bin_spec, "r") as bin_spec_file:
        bin_lookup = bin_parser(bin_spec_file.read())

    labels = list(bin_lookup.keys())

    out_dtype = [(label, float) for label in labels]

    with H5CleanedPosteriorSamples(cli_args.post_samples) as post_samples:
        n_powerlaws = post_samples.metadata["n_powerlaws"]
        n_gaussians = post_samples.metadata["n_gaussians"]
        M_max = post_samples.metadata["M_max"]
        same_gauss_mass_cutoffs = (
            post_samples.metadata["same_gauss_mass_cutoffs"]
        )
        no_spin_singularities = post_samples.metadata["no_spin_singularities"]
        spin_scalings = post_samples.metadata.get("spin_scalings")
        if spin_scalings is not None:
            spin_scalings = json.loads(spin_scalings)

        population = get_population(
            n_powerlaws, n_gaussians, M_max,
            same_gauss_mass_cutoffs=same_gauss_mass_cutoffs,
            no_spin_singularities=no_spin_singularities,
            spin_scalings=spin_scalings,
        )

        n_samples = len(post_samples)
        out_arr = np.empty(n_samples, dtype=out_dtype)

        for i in range(n_samples):
            parameters_i = post_samples.get_params(i)

            rates_i = compute_rates(
                population, bin_lookup, parameters_i,
                abs_err=cli_args.abs_err, rel_err=cli_args.rel_err,
                transformations=transformations,
            )

            for label in labels:
                out_arr[label][i] = rates_i[label]

    np.savetxt(
        cli_args.output, out_arr,
        header=" ".join(labels),
    )
