def populate_subparser(subparsers):
    subparser = subparsers.add_parser("plot_mu")

    subparser.add_argument("cleaned_posterior_samples_hdf5")
    subparser.add_argument(
        "VTs",
        help="HDF5 file containing VTs.",
    )
    subparser.add_argument("output_plot")
    subparser.add_argument("output_data")

    subparser.add_argument(
        "--overlay-truth",
        help="JSON file specifying parameters for a population to overlay.",
    )

    subparser.add_argument("--seed", type=int)

    subparser.add_argument(
        "--debug",
        nargs="*",
        help="Output debugging messages.  Use with no arguments to display all "
             "debugging information, or specify the debugging mode(s).",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_population

    from pop_models.posterior import H5CleanedPosteriorSamples
    from pop_models.poisson_mean import (
        MonteCarloVolumeIntegralFixedSamplePoissonMean,
    )
    from pop_models.coordinate import CoordinateSystem
    from pop_models.credible_regions import CredibleRegions1D
    from pop_models.astro_models.coordinates import (
        m1_source_coord, m2_source_coord, q_coord,
    )
    from pop_models.astro_models.gw_ifo_vt import RegularGridVTInterpolator
    from pop_models.utils.plotting import limit_ax_oom
    from pop_models.utils import debug_verbose

    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import corner
    import scipy.stats
    import h5py

    # Set debugging mode
    if cli_args.debug is not None:
        if len(cli_args.debug) == 0:
            debug_verbose.full_output_on()
        else:
            debug_verbose.enable_modes(*cli_args.debug)

    random_state = numpy.random.RandomState(cli_args.seed)

    # Load in true parameters if provided
    if cli_args.overlay_truth:
        import json
        with open(cli_args.overlay_truth, "r") as truths_file:
            parameters_true = json.load(truths_file)
        # Initialize list of true mu values.
        mu_true = []
    else:
        # No true values for mu.
        mu_true = None

    # Initialize list of mu posterior samples, as well as field names.
    mu = []
    mu_names = []

    # Load in the posterior samples
    post_samples = H5CleanedPosteriorSamples(
        cli_args.cleaned_posterior_samples_hdf5
    )
    with post_samples:
        # Draw all posterior samples.
        parameter_samples = post_samples.get_params(...)

        # Get appropriate population object.
        metadata = post_samples.metadata
        n_powerlaws = metadata["n_powerlaws"]
        n_gaussians = metadata["n_gaussians"]
        M_max = metadata["M_max"]
        same_gauss_mass_cutoffs = metadata.get("same_gauss_mass_cutoffs", True)
        VT_scale_factor = metadata["VT_scale_factor"]

        # Construct full population
        population = get_population(
            n_powerlaws, n_gaussians, M_max,
            same_gauss_mass_cutoffs=same_gauss_mass_cutoffs,
        )

        # Load in VT file.
        debug_verbose(
            "Loading in VT from file: {}, with scale factor: {}"
            .format(cli_args.VTs, VT_scale_factor),
            mode="plot_mu", flush=True,
        )
        with h5py.File(cli_args.VTs, "r") as vt_file:
            VT = RegularGridVTInterpolator(
                vt_file,
                scale_factor=VT_scale_factor,
            )

        def compute_mu(population, parameters):
            mu_fn = MonteCarloVolumeIntegralFixedSamplePoissonMean(
                population, VT,
            )
            return mu_fn(parameters)

        # Compute mu for full population.
        mu.append(compute_mu(population, parameter_samples))
        if cli_args.overlay_truth:
            mu_true.append(compute_mu(population, parameters_true))
        mu_names.append("mu")

        # Compute mu's for power law components.
        for k in range(n_powerlaws):
            suffix = "_pl{}".format(k)
            subpop = population.get_subpops([suffix])
            mu.append(compute_mu(subpop, parameter_samples))
            if cli_args.overlay_truth:
                mu_true.append(compute_mu(subpop, parameters_true))
            mu_names.append("mu"+suffix)

        # Compute mu's for Gaussian components.
        for k in range(n_gaussians):
            suffix = "_g{}".format(k)
            subpop = population.get_subpops([suffix])
            mu.append(compute_mu(subpop, parameter_samples))
            if cli_args.overlay_truth:
                mu_true.append(compute_mu(subpop, parameters_true))
            mu_names.append("mu"+suffix)

        # Create single array for mu's.
        mu_arr = numpy.column_stack(tuple(mu))

        # Create and save corner plot.
        fig = corner.corner(
            mu_arr, truths=mu_true,
            labels=mu_names,
            levels=[0.9, 0.5], plot_density=False, no_fill_contours=True,
        )
        fig.savefig(cli_args.output_plot)

        # Save data behind figure.
        numpy.savetxt(
            cli_args.output_data, mu_arr,
            header=" ".join(mu_names),
        )
