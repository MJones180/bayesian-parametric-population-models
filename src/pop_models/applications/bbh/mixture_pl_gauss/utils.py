import typing
import re

__all__ = [
    "MixtureSpecificationDecodeError",
    "expand_mixture_labels",
]

class MixtureSpecificationDecodeError(ValueError):
    pass

T = typing.TypeVar("T")
def expand_mixture_labels(
        mapping: typing.Mapping[str, T],
        component_counts: typing.Mapping[str, int],
    ) -> typing.Dict[str, T]:
    r"""
    Expands regular expressions in mixture labels in a mapping from labels to
    some sort of values.  Each key in the mapping should be structured as
    ``[base name]_[mixture type][number]``.  ``[mixture type]`` can be the name
    of any type of mixture component, an asterisk (``*``) to include any type,
    or a list of types enclosed in curly braces and separated by commas (e.g.,
    ``{a,b,c}``).  ``[number]`` can be the index of any component of that type,
    as well as an asterisk or list, or a range ``[number start]-[number stop]``,
    where both ends are inclusive, wrapped with square-brackets.

    ``component_counts`` should map each possible mixture type to the number of
    components expected of that type.  For example, if there are two components
    of type "A", and one of type "B", it should be ``{"A" : 2, "B" : 1}``.  This
    is used to determine how to expand wildcards for the numbers, and to
    validate that the numbers are not outside the expected range, and similarly
    to expand wildcards for mixture types, and to validate that any mixture
    types in ``mapping`` are valid.
    """
    # Initialize output dict.
    ret = {} # type: typing.Mapping[str, T]

    # Iterate over each item in the mapping.
    for param_spec, value in mapping.items():
        # Parse out the base of the parameter name, and the mixture label.
        base_name, label_spec = param_spec.rsplit("_", maxsplit=1)
        # Iterate over all possible expansions of the label.
        for label in expand_mixture_label(label_spec, component_counts):
            # Build the full parameter name from he base name and label.
            param_name = "_".join([base_name, label])
            # Do not support duplicate parameter names.
            if param_name in ret:
                raise KeyError(
                    "Duplicate parameter name {} encountered when parsing {}"
                    .format(param_name, param_spec)
                )
            # Store entry.
            ret[param_name] = value

    # Return accumulated result
    return ret


# Matches string starting with '*'
_re_mix_type_wildcard = re.compile(r"^\*.*")
# Matches string starting with a list like {a,b,c}, capturing the list contents
_re_mix_type_list = re.compile(r"^\{([a-zA-Z]+(,[a-zA-Z]+)*)\}.*")
# Matches a string containing only '*'
_re_mix_num_wildcard = re.compile(r"^\*$")
# Matches string containing only a numeric list like {0,3,9}, capturing the list
# contents
_re_mix_num_list = re.compile(r"^\{([0-9]+(,[0-9]+)*)\}$")
# Matches string containing only a numeric range like [1-3], capturing the two
# numbers separately.
_re_mix_num_range = re.compile(r"^\[([0-9]+)\-([0-9]+)\]$")
# Matches a string containing only a number
_re_mix_num_explicit = re.compile(r"^([0-9]+)$")

def expand_mixture_label(
        label_spec: str,
        component_counts: typing.Mapping[str, int],
    ) -> typing.Iterator[str]:
    r"""
    Helper function to expand a single mixture label specifier, parsed out in
    :func:`expand_mixture_labels`, yielding all possible expansions.
    """
    # Pull out supported mixture types
    supported_mixture_types = list(component_counts.keys())

    ## Parse mixture types ##

    # Handle case of wildcard ('*') mixture type
    if _re_mix_type_wildcard.search(label_spec):
        # Use all types.
        mixture_types = supported_mixture_types
        # Remove first character (the '*') from the portion to be parsed for the
        # mixture numbers.
        number_spec = label_spec[1:]
    # Handle the case of a list of mixture types (e.g., '{a,b,c}')
    elif _re_mix_type_list.search(label_spec):
        # Extract everything inside the braces
        match = _re_mix_type_list.search(label_spec)
        raw_list = match.expand(r"\1")
        # Split the comma-delimited list to get the mixture types.
        mixture_types = raw_list.split(",")
        # Isolate the portion of the string not part of the list, which is
        # everything past ``raw_list`` and the two curly braces surrounding it.
        number_spec = label_spec[2+len(raw_list):]
        # Assert that all mixture types are supported
        unsupported_mixture_types = (
            set(mixture_types) - set(supported_mixture_types)
        )
        if len(unsupported_mixture_types) != 0:
            raise KeyError(
                "Unknown mixture types in '{}': {}"
                .format(label_spec, ", ".join(unsupported_mixture_types))
            )
    # Remaining possibility is that one of the supported mixture types is
    # specified explicitly, or there's a syntax error.  Handle both cases.
    else:
        # Handle explicit mixture types.
        for candidate_mixture_type in supported_mixture_types:
            # Handle a match, and break out of the loop if successful.
            if label_spec.startswith(candidate_mixture_type):
                mixture_types = [candidate_mixture_type]
                number_spec = label_spec[len(candidate_mixture_type):]
                break
        # Nothing worked, raise exception.
        else:
            raise MixtureSpecificationDecodeError(
                "Improperly formatted mixture type in '{}'"
                .format(label_spec)
            )

    ## Parse mixture numbers, and yield results ##

    # Handle case of wildcard ('*') mixture number.
    if _re_mix_num_wildcard.search(number_spec):
        # Yield all mixture numbers for all requested mixture types.
        for mixture_type in mixture_types:
            for mixture_number in range(component_counts[mixture_type]):
                yield "{}{}".format(mixture_type, mixture_number)
    # Handle case of list of mixture numbers (e.g., {0,3,5}).
    elif _re_mix_num_list.search(number_spec):
        # Extract everything inside the braces
        match = _re_mix_num_list.search(number_spec)
        raw_list = match.expand(r"\1")
        # Split the comma-delimited list to get the mixture types.
        mixture_numbers = [int(num_str) for num_str in raw_list.split(",")]
        # Validate that highest mixture number does not exceed highest value for
        # any component.
        max_mixture_number = max(mixture_numbers)
        for mixture_type in mixture_types:
            if component_counts[mixture_type] <= max_mixture_number:
                raise ValueError(
                    "Some mixture numbers in '{}' out of range for mixture "
                    "type '{}' which has {} components."
                    .format(
                        mixture_numbers, mixture_type,
                        component_counts[mixture_type],
                    )
                )
        # Yield all requested mixture numbers for all requested mixture types.
        for mixture_type in mixture_types:
            for mixture_number in mixture_numbers:
                yield "{}{}".format(mixture_type, mixture_number)
    # Handle case of range of mixture numbers (e.g., [1-3]).
    elif _re_mix_num_range.search(number_spec):
        # Extract everything inside the brackets
        match = _re_mix_num_range.search(number_spec)
        lower = int(match.expand(r"\1"))
        upper = int(match.expand(r"\2"))
        # Create the range of numbers.
        mixture_numbers = list(range(lower, upper+1))
        # Validate that highest mixture number does not exceed highest value for
        # any component.
        for mixture_type in mixture_types:
            if component_counts[mixture_type] <= upper:
                raise ValueError(
                    "Some mixture numbers in '{}' out of range for mixture "
                    "type '{}' which has {} components."
                    .format(
                        mixture_numbers, mixture_type,
                        component_counts[mixture_type],
                    )
                )
        # Yield all requested mixture numbers for all requested mixture types.
        for mixture_type in mixture_types:
            for mixture_number in mixture_numbers:
                yield "{}{}".format(mixture_type, mixture_number)
    # Handle case of explicit number.
    elif _re_mix_num_explicit.search(number_spec):
        # Parse the number out.
        mixture_number = int(number_spec)
        # Validate that the mixture number does not exceed highest value for any
        # component.
        for mixture_type in mixture_types:
            if component_counts[mixture_type] <= mixture_number:
                raise ValueError(
                    "Mixture number '{}' out of range for mixture type '{}' "
                    "which has {} components."
                    .format(
                        mixture_number, mixture_type,
                        component_counts[mixture_type],
                    )
                )
        # Yield requested mixture number for all requested mixture types.
        for mixture_type in mixture_types:
            yield "{}{}".format(mixture_type, mixture_number)
    # Nothing worked, raise exception.
    else:
        raise MixtureSpecificationDecodeError(
            "Improperly formatted mixture number in '{}'"
            .format(label_spec)
        )


SpinScalingDictType = typing.Dict[str, typing.Tuple[float,float]]
