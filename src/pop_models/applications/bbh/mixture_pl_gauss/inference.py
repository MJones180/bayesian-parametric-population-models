import typing

def populate_subparser(subparsers):
    subparser = subparsers.add_parser("inference")

    subparser.add_argument(
        "events",
        nargs="*",
        help="List of posterior sample files, one for each event.",
    )
    subparser.add_argument(
        "VTs",
        help="HDF5 file containing VTs.",
    )
    subparser.add_argument(
        "pop_priors",
        help="JSON file specifying population priors.",
    )
    subparser.add_argument(
        "posterior_output",
        help="HDF5 file to store posterior samples in.",
    )

    init_from_file_group = subparser.add_mutually_exclusive_group()
    init_from_file_group.add_argument(
        "--init-from-raw-samples",
        help="Initialize chains from the given HDF5 raw samples archive, "
             "taking the final positions of all the walkers as the new "
             "starting point.",
    )
    init_from_file_group.add_argument(
        "--init-from-cleaned-samples",
        help="Initialize chains from the given HDF5 cleaned samples file, "
             "taking a random set of samples, specified by --n-walkers, as the "
             "starting point.",
    )

    subparser.add_argument(
        "--constants",
        help="JSON file fixing population parameters to constants.",
    )
    subparser.add_argument(
        "--duplicates",
        help="JSON file setting population parameters to duplicate the values "
             "of others.",
    )

    subparser.add_argument(
        "--moves",
        help="JSON file setting ensemble MCMC move options.",
    )
    subparser.add_argument(
        "--rescale-params",
        help="JSON file mapping parameter names to rescalings, for better MCMC "
             "efficiency.",
    )

    subparser.add_argument(
        "--n-powerlaws",
        type=int, default=0,
        help="Number of powerlaw components to include (default is 0).",
    )
    subparser.add_argument(
        "--n-gaussians",
        type=int, default=0,
        help="Number of Gaussian components to include (default is 0).",
    )

    subparser.add_argument(
        "--iid-spins",
        action="store_true",
        help="Assume that spin magnitudes are independent and identically "
             "distributed (iid). This means that the model parameters for chi1 "
             "and chi2 are forced to be the same. This is accomplished in "
             "practice by using the parameter for chi1 anywhere it is needed "
             "for chi2. If any chi2 parameters were specified by the user, an "
             "exception will be raised.",
    )
    subparser.add_argument(
        "--no-spin-singularities",
        action="store_true",
        help="Restrict the spin distribution such that spin distributions with "
             "singularities are not allowed by the prior.",
    )
    subparser.add_argument(
        "--spin-scalings",
        help="Rather than restrict all spin magnitudes to [0, 1], use a JSON "
             "file to specify an alternative upper limit on any subpopulation, "
             "down to the specific component.  File should specify a "
             "dictionary mapping subpopulation indices to a pair of limits for "
             "each binary components.  Any subpopulation omitted will use the "
             "default.  As an example, limiting chi1 <= 1.0 and chi2 <= 0.05 "
             "for the subpopulation 'g0', the file would look like "
             "{'g0' : [1.0, 0.05]}.",
    )

    subparser.add_argument(
        "--same-gauss-mass-cutoffs",
        action="store_true",
        help="Force Gaussian mmin and mmax to be the same for m1 and m2.",
    )

    subparser.add_argument(
        "--scale-factor",
        type=float, default=None,
        help="Multiply all VTs by this constant factor.",
    )

    subparser.add_argument(
        "--vt-calibration",
        help="Calibration file for VT's.",
    )

    subparser.add_argument(
        "--n-walkers",
        default=None, type=int,
        help="Number of walkers to use, defaults to twice the number of "
             "dimensions.",
    )
    subparser.add_argument(
        "--n-samples",
        default=100, type=int,
        help="Number of MCMC samples per walker.",
    )
    subparser.add_argument(
        "--n-threads",
        default=1, type=int,
        help="Number of threads to use in MCMC.",
    )
    subparser.add_argument(
        "--chunk-size",
        type=int, default=1024,
        help="Number of samples to store in each posterior sample sub-file.",
    )

    subparser.add_argument(
        "--weights-field",
        help="Name of field in input posteriors that specifies weights to "
             "apply to posterior samples.  Will multiply by these values if "
             "provided.",
    )
    subparser.add_argument(
        "--inv-weights-field",
        help="Name of field in input posteriors that specifies inverse weights "
             "to apply to posterior samples.  Will divide by these values if "
             "provided.",
    )

    subparser.add_argument(
        "--total-mass-max",
        type=float, default=100.0,
        help="Maximum total mass allowed.",
    )

    subparser.add_argument(
        "--mc-err-abs",
        type=float, default=1e-5,
        help="Allowed absolute error for Monte Carlo integrator.",
    )
    subparser.add_argument(
        "--mc-err-rel",
        type=float, default=1e-3,
        help="Allowed relative error for Monte Carlo integrator.",
    )

    subparser.add_argument(
        "--force",
        action="store_true",
        help="Force HDF5 output, even if file already exists. Will overwrite.",
    )

    subparser.add_argument(
        "--seed",
        type=int, default=None,
        help="Random seed.",
    )

    subparser.add_argument(
        "--swmr-mode",
        action="store_true",
        help="Open file in single-writer multiple-reader (SWMR) mode, so that "
             "other processes may read from the file while it's running, and "
             "so that it will be more resistant to data corruption.",
    )

    subparser.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="Use verbose output.",
    )
    subparser.add_argument(
        "--debug",
        nargs="*",
        help="Output debugging messages.  Use with no arguments to display all "
             "debugging information, or specify the debugging mode(s).",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_population, coord_system
    from .prior import Prior

    from pop_models.detection import (
        DeterministicReweightedPosteriorsDetectionLikelihood,
    )
    from pop_models.poisson_mean import (
        MonteCarloVolumeIntegralFixedSamplePoissonMean,
    )
    from pop_models.posterior import (
        H5RawPosteriorSamples, H5CleanedPosteriorSamples,
        PopulationInference, PopulationInferenceEmceeSampler,
    )
    from pop_models.utils import debug_verbose

    from pop_models.astro_models.gw_ifo_vt import (
        RegularGridVTInterpolator,
        correction_bases, CorrectedVT,
    )
    from pop_models.astro_models.detections.cbc.pre_sampled import (
        CBCPreSampledDetection
    )

    import h5py
    import json
    import numpy

    import os

    # Set debugging mode
    if cli_args.debug is not None:
        if len(cli_args.debug) == 0:
            debug_verbose.full_output_on()
        else:
            debug_verbose.enable_modes(*cli_args.debug)

    random_state = numpy.random.RandomState(cli_args.seed)

    # Load existing posterior samples if resuming (file exists and --force flag
    # not set).
    exists = os.path.isfile(
        os.path.join(cli_args.posterior_output, "master.hdf5")
    )
    resumed = exists and not cli_args.force
    if resumed:
        debug_verbose(
            "Attempting to continue existing run",
            mode="inference_setup", flush=True,
        )
        post_samples = H5RawPosteriorSamples(
            cli_args.posterior_output, "r+",
            swmr_mode=cli_args.swmr_mode,
        )
        param_names = post_samples.param_names

        debug_verbose(
            "Loaded existing run with {} samples and {} walkers"
            .format(len(post_samples), post_samples.n_walkers),
            mode="inference_setup", flush=True,
        )

        metadata = post_samples.metadata
        pop_prior_settings = json.loads(metadata["pop_prior_settings"])
        n_powerlaws = metadata["n_powerlaws"]
        n_gaussians = metadata["n_gaussians"]
        M_max = metadata["M_max"]
        VT_scale_factor = metadata["VT_scale_factor"]
        no_spin_singularities = metadata["no_spin_singularities"]
        # 'spin_scalings' is either a JSON string or non-existant.
        if "spin_scalings" in metadata:
            spin_scalings = json.loads(metadata["spin_scalings"])
        else:
            spin_scalings = None
        # Default behavior was True before this was stored in metadata.
        same_gauss_mass_cutoffs = metadata.get("same_gauss_mass_cutoffs", True)

        debug_verbose(
            "Metadata: {}".format(dict(metadata)),
            mode="inference_setup", flush=True,
        )

        # Get appropriate population object.
        population = get_population(
            n_powerlaws, n_gaussians, M_max,
            same_gauss_mass_cutoffs=same_gauss_mass_cutoffs,
            no_spin_singularities=no_spin_singularities,
            spin_scalings=spin_scalings,
        )
        debug_verbose(
            "Population initialized: {}".format(population),
            mode="inference_setup", flush=True,
        )

        # Construct prior object from metadata in posterior samples.
        prior = Prior(
            random_state, population, population.param_names,
            pop_prior_settings,
            M_max, n_powerlaws, n_gaussians,
            constants=post_samples.constants,
            duplicates=post_samples.duplicates,
        )
        debug_verbose(
            "Prior initialized: {}".format(prior),
            mode="inference_setup", flush=True,
        )
    # Create object for storing posterior samples, with an HDF5 backend, if
    # not resuming (file doesn't exist, or --force flag set).
    else:
        debug_verbose(
            "Attempting to start new run",
            mode="inference_setup", flush=True,
        )

        # Extract settings from command line arguments.
        M_max = cli_args.total_mass_max
        n_powerlaws = cli_args.n_powerlaws
        n_gaussians = cli_args.n_gaussians
        no_spin_singularities = cli_args.no_spin_singularities
        iid_spins = cli_args.iid_spins
        same_gauss_mass_cutoffs = cli_args.same_gauss_mass_cutoffs
        VT_scale_factor = (
            cli_args.scale_factor if cli_args.scale_factor is not None
            else 1.0
        )

        # Read spin_scalings from file if provided.
        if cli_args.spin_scalings is None:
            spin_scalings = None
        else:
            with open(cli_args.spin_scalings, "r") as spin_scalings_file:
                spin_scalings = json.load(spin_scalings_file)

        # Get appropriate population object.
        population = get_population(
            n_powerlaws, n_gaussians, M_max,
            same_gauss_mass_cutoffs=same_gauss_mass_cutoffs,
            no_spin_singularities=no_spin_singularities,
            spin_scalings=spin_scalings,
        )
        debug_verbose(
            "Population initialized: {}".format(population),
            mode="inference_setup", flush=True,
        )

        # Ensure at least one powerlaw or gaussian component was requested.
        if (cli_args.n_powerlaws == 0) and (cli_args.n_gaussians == 0):
            raise ValueError(
                "No powerlaws or gaussians requested.  Must pass a non-zero "
                "number to either of --n-powerlaws or --n-gaussians command "
                "line options."
            )

        # Get list of param names.
        pop_prior_settings, constants, duplicates = parse_consts_and_prior(
            cli_args.pop_priors, cli_args.constants, cli_args.duplicates,
            population.param_names,
            cli_args.n_powerlaws, cli_args.n_gaussians,
            iid_spins=cli_args.iid_spins,
        )
        debug_verbose(
            "Prior settings parsed: {}".format(pop_prior_settings),
            mode="inference_setup", flush=True,
        )
        debug_verbose(
            "Constants parsed: {}".format(constants),
            mode="inference_setup", flush=True,
        )
        debug_verbose(
            "Duplicates parsed: {}".format(duplicates),
            mode="inference_setup", flush=True,
        )

        # Construct prior object.
        prior = Prior(
            random_state, population, population.param_names,
            pop_prior_settings,
            M_max, n_powerlaws, n_gaussians,
            constants=constants, duplicates=duplicates,
        )
        debug_verbose(
            "Prior initialized: {}".format(prior),
            mode="inference_setup", flush=True,
        )

    # Open tabular files for event posteriors.
    debug_verbose(
        "Loading in detections from files: {}".format(cli_args.events),
        mode="inference_setup", flush=True,
    )
    detections = [
        CBCPreSampledDetection.load(
            event_fname,
            coord_system=coord_system,
            weights_field=cli_args.weights_field,
            inv_weights_field=cli_args.inv_weights_field,
        )
        for event_fname in cli_args.events
    ]
    debug_verbose(
        "Detections loaded: {}".format(detections),
        mode="inference_setup", flush=True,
    )

    # Load in VT file.
    debug_verbose(
        "Loading in VT from file: {}, with scale factor: {}"
        .format(cli_args.VTs, VT_scale_factor),
        mode="inference_setup", flush=True,
    )
    with h5py.File(cli_args.VTs, "r") as vt_file:
        VT = RegularGridVTInterpolator(vt_file, scale_factor=VT_scale_factor)
    # Load in VT calibration if provided.
    if cli_args.vt_calibration is not None:
        debug_verbose(
            "Loading VT calibration from file: {}"
            .format(cli_args.vt_calibration),
            mode="inference_setup", flush=True,
        )
        # Load calibration info from file.
        with open(cli_args.vt_calibration, "r") as cal_file:
            cal_info = json.load(cal_file)
        # Pull out needed calibration info.
        coeffs = cal_info["coeffs"]
        basis_fns = correction_bases[VT.mode][cal_info["basis"]]
        # Replace VT with the calibrated VT.
        VT = CorrectedVT(VT, coeffs, basis_fns)

    # Evaluates expected number of detections for a given population and VT.
    debug_verbose(
        "Constructing Poisson mean object.",
        mode="inference_setup", flush=True,
    )
    expval = MonteCarloVolumeIntegralFixedSamplePoissonMean(population, VT)
    debug_verbose(
        "Poisson mean object constructed: {}".format(expval),
        mode="inference_setup", flush=True,
    )

    # Create all of detection*population integral evaluators.
    debug_verbose(
        "Constructing in detection likelihood integrators",
        mode="inference_setup", flush=True,
    )
    detection_likelihoods = [
        DeterministicReweightedPosteriorsDetectionLikelihood(
            population, detection,
        )
        for detection in detections
    ]
    debug_verbose(
        "Detection likelihood integrators loaded: {}"
        .format(detection_likelihoods),
        mode="inference_setup", flush=True,
    )


    # Initialize MCMC if this is a new run.
    if not resumed:
        # Create posterior samples HDF5 archive, and object interface.
        if cli_args.init_from_raw_samples is not None:
            debug_verbose(
                "Initializing sampler from existing raw posteriors file: {}"
                .format(cli_args.init_from_raw_samples),
                mode="inference_setup", flush=True,
            )

            # Initialize from raw samples file.
            init_post = H5RawPosteriorSamples(
                cli_args.init_from_raw_samples, "r",
            )
            with init_post:
                post_samples = H5RawPosteriorSamples.create_from_raw_samples(
                    cli_args.posterior_output,
                    init_post,
                    constants=constants,
                    chunk_size=cli_args.chunk_size,
                    force=cli_args.force,
                    swmr_mode=cli_args.swmr_mode,
                )
        elif cli_args.init_from_cleaned_samples is not None:
            debug_verbose(
                "Initializing sampler from existing cleaned posteriors file: {}"
                .format(cli_args.init_from_cleaned_samples),
                mode="inference_setup", flush=True,
            )

            # Set up a stripped down inference object only used for vetoing
            # initial values that are not within the posterior's support.
            pop_inference_for_init = PopulationInference(
                expval, detection_likelihoods, prior.log_prior,
                population.param_names,
            )

            # Initialize from cleaned samples file.
            init_post = H5CleanedPosteriorSamples(
                cli_args.init_from_cleaned_samples
            )
            with init_post:
                post_samples = H5RawPosteriorSamples.create_from_samples(
                    cli_args.posterior_output,
                    cli_args.n_walkers,
                    init_post,
                    constants=constants,
                    chunk_size=cli_args.chunk_size,
                    force=cli_args.force,
                    swmr_mode=cli_args.swmr_mode,
                    pop_inf=pop_inference_for_init,
                )
        else:
            debug_verbose(
                "Initializing sampler from prior",
                mode="inference_setup", flush=True,
            )

            # Set up a stripped down inference object only used for vetoing
            # initial values that are not within the posterior's support.
            pop_inference_for_init = PopulationInference(
                expval, detection_likelihoods, prior.log_prior,
                population.param_names,
            )
            # Initialize from prior.
            init_state = prior.init_walkers(
                cli_args.n_walkers, pop_inf=pop_inference_for_init,
            )
            post_samples = H5RawPosteriorSamples.create(
                cli_args.posterior_output,
                population.param_names,
                init_state,
                constants=constants, duplicates=duplicates,
                chunk_size=cli_args.chunk_size,
                force=cli_args.force,
                swmr_mode=cli_args.swmr_mode,
            )

        # Store metadata
        post_samples.metadata["M_max"] = M_max
        post_samples.metadata["n_powerlaws"] = n_powerlaws
        post_samples.metadata["n_gaussians"] = n_gaussians
        post_samples.metadata["no_spin_singularities"] = no_spin_singularities
        post_samples.metadata["pop_prior_settings"] = json.dumps(
            pop_prior_settings,
        )
        post_samples.metadata["iid_spins"] = iid_spins
        post_samples.metadata["same_gauss_mass_cutoffs"] = (
            same_gauss_mass_cutoffs
        )
        post_samples.metadata["VT_scale_factor"] = VT_scale_factor
        post_samples.metadata["VT_mode"] = VT.mode

        if spin_scalings is not None:
            post_samples.metadata["spin_scalings"] = json.dumps(spin_scalings)

        debug_verbose(
            "Samples initialized: {}".format(post_samples),
            mode="inference_setup", flush=True,
        )

    with post_samples:
        if cli_args.moves is None:
            moves = None
        else:
            debug_verbose(
                "Parsing MCMC move options.",
                mode="inference_setup", flush=True,
            )
            with open(cli_args.moves, "r") as moves_file:
                moves = PopulationInferenceEmceeSampler.parse_moves_from_file(
                    moves_file
                )
            debug_verbose(
                "MCMC move options parsed as:", moves,
                mode="inference_setup", flush=True,
            )

        if cli_args.rescale_params is None:
            rescale_params = None
        else:
            # TODO: Add wildcard evaluation, as in prior/constants/duplicates.
            with open(cli_args.rescale_params, "r") as rescale_params_file:
                rescale_params = json.load(rescale_params_file)

        debug_verbose(
            "Constructing posterior sampler.",
            mode="inference_setup", flush=True,
        )
        pop_inference = PopulationInferenceEmceeSampler(
            post_samples,
            expval, detection_likelihoods, prior.log_prior,
            moves=moves, rescale_params=rescale_params,
            random_state=random_state,
            verbose=cli_args.verbose,
        )
        debug_verbose(
            "Posterior sampler constructed: {}".format(pop_inference),
            mode="inference_setup", flush=True,
        )

        debug_verbose(
            "Beginning to sample {} posteriors.".format(cli_args.n_samples),
            mode="inference_setup", flush=True,
        )
        pop_inference.posterior_samples(cli_args.n_samples)
        debug_verbose(
            "Finished sampling {} posteriors.".format(cli_args.n_samples),
            mode="inference_setup", flush=True,
        )


def parse_consts_and_prior(
        priors_fname: str,
        constants_fname: typing.Optional[str],
        duplicates_fname: typing.Optional[str],
        param_names: typing.Tuple[str,...],
        n_powerlaws: int, n_gaussians: int,
        iid_spins: bool=False,
    ):
    import six
    import json
    from . import utils
    from pop_models.utils import format_set_in_quotes

    # Make sure `iid_spins` and `duplicates_fname` are not provided together.
    if iid_spins and (duplicates_fname is not None):
        raise ValueError(
            "Cannot manually provide duplicates while --iid-spins flag given."
        )

    # Load the population priors from a JSON file.
    with open(priors_fname, "r") as priors_file:
        pop_prior_settings = json.load(priors_file)

        # TODO: validate file structure

    # Expand any wildcards in `pop_prior_settings`
    pop_prior_settings = utils.expand_mixture_labels(
        pop_prior_settings, {"pl" : n_powerlaws, "g" : n_gaussians},
    )


    # Load in constants file.
    if constants_fname is None:
        # No constants to be loaded.
        constants = {}
    else:
        # Load constants in from a JSON file.
        with open(constants_fname, "r") as constants_file:
            constants = json.load(constants_file)

        ## Validate file structured properly. ##
        # Check that it's a dict.
        if not isinstance(constants, dict):
            raise TypeError(
                "Constants file must be a dictionary mapping param -> value"
            )
        # Expand any wildcards in `constants`
        constants = utils.expand_mixture_labels(
            constants, {"pl" : n_powerlaws, "g" : n_gaussians},
        )
        # Check that the keys are all param names.
        unknown_params = [
            param for param in constants
            if param not in param_names
        ]
        if len(unknown_params) != 0:
            raise ValueError(
                "Unrecognized params in constants file:\n{}"
                .format(format_set_in_quotes(unknown_params))
            )
        # Check that all of the param names map to floats.
        # NOTE: May have to change this if we ever allow for MCMC's over things
        # other than floats (integers?), but currently our underlying sampler
        # only supports floats.
        bad_values = []
        for param, value in six.iteritems(constants):
            if not isinstance(value, float):
                bad_values.append(param)
        if len(bad_values) != 0:
            raise TypeError(
                "The following constants mapped to invalid values:\n{}"
                .format(format_set_in_quotes(bad_values))
            )


    # Parse constants file, and check for any conflicts with priors file.
    conflicting_priors = []
    for param in constants:
        # Check if parameter has already been given a prior. If so, we're going
        # to raise an exception once we've found all such parameters. We could
        # just override those priors, but it's better to be safe.
        if param in pop_prior_settings:
            conflicting_priors.append(param)
        # For bookkeeping purposes, set the parameter's prior distribution to
        # "constant". Map "params" to an empty dict for consistency with other
        # distributions that require params.
        else:
            pop_prior_settings[param] = {
                "dist": "constant",
                "params": {}
            }

    # Raise an exception if any parameters were set to constants, when they were
    # already given priors. List all of the offending params so the user can
    # correct the issue.
    if len(conflicting_priors) != 0:
        raise ValueError(
            "The following parameters were set to constants, but also had "
            "priors set in 'pop_priors' file. Those priors would need to "
            "be ignored, so remove them from the 'pop_priors' file and rerun "
            "if that is desired.\n{}"
            .format(format_set_in_quotes(conflicting_priors))
        )


    # If the spins are assumed to be i.i.d., then we force all of the chi2
    # parameters to assume the values of the associated chi1 parameters.
    if iid_spins:
        duplicates = {}
        for i in range(n_powerlaws):
            suffix = "_pl{}".format(i)
            duplicates.update({
                "E_chi2"+suffix: "E_chi1"+suffix,
                "Var_chi2"+suffix: "Var_chi1"+suffix,
                "mu_cos2"+suffix: "mu_cos1"+suffix,
                "sigma_cos2"+suffix: "sigma_cos1"+suffix,
            })
        for i in range(n_gaussians):
            suffix = "_g{}".format(i)
            duplicates.update({
                "E_chi2"+suffix: "E_chi1"+suffix,
                "Var_chi2"+suffix: "Var_chi1"+suffix,
                "mu_cos2"+suffix: "mu_cos1"+suffix,
                "sigma_cos2"+suffix: "sigma_cos1"+suffix,
            })
    elif duplicates_fname is not None:
        # Load constants in from a JSON file.
        with open(duplicates_fname, "r") as duplicates_file:
            duplicates = json.load(duplicates_file)

        ## Validate file structured properly. ##
        # Check that it's a dict.
        if not isinstance(duplicates, dict):
            raise TypeError(
                "Duplicates file must be a dictionary mapping param -> param"
            )
        # Check that the dict keys and values are all strings.
        bad_values = []
        for target, source in duplicates.items():
            if not isinstance(target, str):
                bad_values.append(target)
            if not isinstance(source, str):
                bad_values.append(target)

        if len(bad_values) != 0:
            raise TypeError(
                "Duplicates file must param names to param names (strings).  "
                "The following keys have or map to invalid values:\n"
                "{}"
                .format(format_set_in_quotes(set(bad_values)))
            )
    else:
        duplicates = {}


    if len(duplicates) != 0:
        conflicting_priors = []
        conflicting_consts = []
        for param in duplicates:
            # Check if i.i.d. param has already been given a prior or constant
            # value. If so, we're going to raise an exception once we've found
            # all such parameters, and tabulate them separately so users know
            # whether to check the priors or constants files. We could just
            # override those priors or constants, but it's better to be safe.
            if param in constants:
                conflicting_consts.append(param)
            elif param in pop_prior_settings:
                conflicting_priors.append(param)
            # For bookkeeping purposes, set the parameter's prior distribution
            # to "duplicate". Map "params" to an empty dict for consistency with
            # other distributions that require params.
            else:
                pop_prior_settings[param] = {
                    "dist": "duplicate",
                    "params": {},
                }

        # Raise an exception if any parameters were set to i.i.d., when they
        # were already given priors or constant values. List all of the
        # offending params so the user can correct the issue.
        if len(conflicting_priors) != 0 or len(conflicting_consts) != 0:
            dupe_option_used = "--iid-spins" if iid_spins else "--duplicates"
            raise ValueError(
                "The following params had priors and/or constants specified "
                "by the user, and would be overridden by {}. Please "
                "remove them from the priors and/or constants files.\n"
                "Priors:\n"
                "  {}\n"
                "Constants:\n"
                "  {}"
                .format(
                    dupe_option_used,
                    format_set_in_quotes(conflicting_priors),
                    format_set_in_quotes(conflicting_consts),
                )
            )


    # Raise an exception if any params do not have priors specified now.
    missing_params = [
        param for param in param_names
        if param not in pop_prior_settings
    ]
    if len(missing_params) != 0:
        raise ValueError(
            "The following parameters have not been given priors:\n{}"
            .format(format_set_in_quotes(missing_params))
        )

    return pop_prior_settings, constants, duplicates
