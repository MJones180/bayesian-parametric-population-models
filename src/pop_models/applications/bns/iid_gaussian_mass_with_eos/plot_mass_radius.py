def populate_subparser(subparsers):
    from ....astro_models import eos

    subparser = subparsers.add_parser("plot_mass_radius")

    subparser.add_argument("cleaned_posterior_samples_hdf5")
    subparser.add_argument("output_plot")

    subparser.add_argument("--n-samples-per-realization", type=int, default=10)
    subparser.add_argument("--n-mass-points", type=int, default=50)
    subparser.add_argument("--n-mass-samples", type=int, default=1000)

    subparser.add_argument("--overlay-synthetic-truth", action="store_true")
    subparser.add_argument("--synthetic-eos-name", default="AP4")
    subparser.add_argument("--synthetic-cmap", default="viridis")

    subparser.add_argument(
        "--eos-coordinates",
        choices=eos.eos_coordinate_options, default="spectral_legendre",
    )

    subparser.add_argument("--seed", type=int)

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    import lalsimulation

    from .population import get_param_names, get_population
    from .synthetic_observations import parameters_base as parameters_synth

    from ....posterior import H5CleanedPosteriorSamples
    from ....astro_models import eos

    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import scipy.stats
    import corner

    random_state = numpy.random.RandomState(cli_args.seed)

    # Get population and coord_names in proper EOS coordinates.
    param_names = get_param_names(eos_coordinates=cli_args.eos_coordinates)
    population = get_population(eos_coordinates=cli_args.eos_coordinates)

    post_samples = H5CleanedPosteriorSamples(
        cli_args.cleaned_posterior_samples_hdf5
    )
    with post_samples:
        parameters = post_samples.get_params(...)

        samples = population.rvs(
            cli_args.n_samples_per_realization, parameters,
            random_state=random_state,
        )
        m1_samples, m2_samples = samples[:2]
        # Combined mass samples (we don't care whether it's m1 or m2)
        mass_samples = numpy.concatenate((m1_samples, m2_samples), axis=-1)

        radius_samples = numpy.empty_like(mass_samples)

        param_combinations = mass_samples.shape[0]

        spectral_eos_parameters = eos.eos_coords_to_spectral(
            eos.get_eos_parameters(
                parameters,
                eos_coordinates=cli_args.eos_coordinates,
            ),
            eos_coordinates=cli_args.eos_coordinates,
        )

        for param_idx in range(param_combinations):
            eos_fam = lalsimulation.CreateSimNeutronStarFamily(
                eos.spectral_eos(
                    gamma[param_idx]
                    for gamma in spectral_eos_parameters
                )
            )
            radius_samples[param_idx] = eos.radius_from_eos_and_mass(
                eos_fam, mass_samples[param_idx],
            )

        # Convert from meters to kilometers
        radius_samples *= 1e-3

        radius_mass_samples = numpy.column_stack((
            radius_samples.flatten(), mass_samples.flatten(),
        ))

        fig = corner.corner(
            radius_mass_samples,
            levels=[0.50, 0.90],
            labels=[r"$R \, [km]$", "$m \, [M_\odot]$"],
            color="C0",
            hist_kwargs={
                "linestyle": "solid", "linewidth": 2.5,
                "density" : True,
                "zorder" : 1,
            },
            contour_kwargs={
                "linestyles": "solid", "linewidths": [2, 1],
                "zorder": 1,
            },
            no_fill_contours=True, plot_datapoints=False, plot_density=False,
        )

        if cli_args.overlay_synthetic_truth:
            parameters_eos = (
                eos.spectral_eos_parameter_examples[cli_args.synthetic_eos_name]
            )

            parameters_eos_tuple = tuple(
                parameters_eos["gamma{}".format(i)]
                for i in range(1, 5)
            )

            parameters_eos = {
                name : numpy.asarray(value)
                for name, value in parameters_eos.items()
            }
            parameters_synth.update(parameters_eos)


            samples = population.rvs(
                cli_args.n_mass_samples, parameters_synth,
                random_state=random_state,
            )

            m1_samples, m2_samples = samples[:2]
            # Combined mass samples (we don't care whether it's m1 or m2)
            mass_samples = numpy.concatenate((m1_samples, m2_samples), axis=-1)

            m_min, m_max = numpy.min(mass_samples), numpy.max(mass_samples)
            m_range = numpy.linspace(m_min, m_max, cli_args.n_mass_points)

            m_hist, m_hist_edges = numpy.histogram(
                mass_samples,
                bins="auto", range=(m_min, m_max), density=True,
            )
            condlist = [
                (bin_low <= m_range) & (m_range < bin_high)
                for bin_low, bin_high in pairwise(m_hist_edges)
            ]
            p_of_m = numpy.piecewise(
                m_range, condlist, m_hist,
            )

            eos_fam = lalsimulation.CreateSimNeutronStarFamily(
                eos.spectral_eos(parameters_eos_tuple)
            )
            r_of_m = 1e-3 * eos.radius_from_eos_and_mass(eos_fam, m_range)


            ax_radius_mass = fig.axes[2]

            sc = ax_radius_mass.scatter(
                r_of_m, m_range,
                c=numpy.log10(p_of_m), cmap=cli_args.synthetic_cmap,
                s=2,
                zorder=2,
            )

#            add_legend_and_cbar(fig, sc)


        fig.savefig(cli_args.output_plot)


def pairwise(iterable):
    from itertools import tee

    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)



def add_legend_and_cbar(fig, sc):
    import matplotlib

    legend_ax = fig.axes[1]

    lines = [
        matplotlib.lines.Line2D(
            [0.0], [0.0],
            color=color, linestyle=linestyle,
            linewidth=2.5,
        )
        for color, linestyle
        in zip(["C0", "C3"], ["solid", "dashed"])
    ]
    labels = [
        "Inferred PPD",
        "Synthetic truth",
    ]

#    fig.colorbar(sc, cax=legend_ax)
    legend_ax.legend(lines, labels)
