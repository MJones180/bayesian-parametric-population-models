def populate_subparser(subparsers):
    from ....astro_models import eos

    subparser = subparsers.add_parser("plot_mass_distribution")

    subparser.add_argument("cleaned_posterior_samples_hdf5")
    subparser.add_argument("output_plot")

    subparser.add_argument("--n-plot-points", type=int, default=50)
    subparser.add_argument("--n-samples-per-realization", type=int, default=300)

    subparser.add_argument("--overlay-synthetic-truth")

    subparser.add_argument("--seed", type=int)

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_population

    from pop_models.coordinate import CoordinateSystem
    from pop_models.posterior import H5CleanedPosteriorSamples
    from pop_models.credible_regions import CredibleRegions1D
    from pop_models.astro_models import eos
    from pop_models.astro_models.coordinates import m1_source_coord

    import json

    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import scipy.stats

    random_state = numpy.random.RandomState(cli_args.seed)

    post_samples = H5CleanedPosteriorSamples(
        cli_args.cleaned_posterior_samples_hdf5
    )
    with post_samples:
        # Get population in proper EOS coordinates.
        metadata = post_samples.metadata

        with_spin = post_samples.metadata["with_spin"]
        eos_coordinates = post_samples.metadata["eos_coordinates"]
        ## Earlier versions were hard coded to 'mean_variance' and did not
        ## store it in the metadata.  If no value is stored, then it must be
        ## the old hard-coded value.
        spin_parameterization = post_samples.metadata.get(
            "spin_parameterization",
            "mean_variance",
        )
        n_components = post_samples.metadata.get("n_components")

        # Get population and coord_names in proper EOS coordinates.
        population = get_population(
            with_spin=with_spin,
            eos_coordinates=eos_coordinates,
            n_comp=n_components,
            spin_parameterization=spin_parameterization,
        ).to_coords(CoordinateSystem(m1_source_coord))

        def mass_pdf(masses, parameters):
            m1_samples, = population.rvs(
                cli_args.n_samples_per_realization, parameters,
                random_state=random_state,
            )

            obs_shape = masses.shape
            params_shape = m1_samples.shape[:-1]
            out_shape = params_shape + obs_shape

            pdf = numpy.empty(out_shape, dtype=numpy.float64)

            for idx in numpy.ndindex(*params_shape):
                mass_kde = scipy.stats.gaussian_kde(
                    m1_samples[idx], bw_method="scott",
                )

                pdf[idx] = mass_kde(masses)

            return pdf


        m_min = 0.9
        m_max = 2.9

        mass_grid = numpy.linspace(m_min, m_max, cli_args.n_plot_points)
        mass_ci = CredibleRegions1D.from_samples(
            mass_pdf, mass_grid, post_samples.get_params(...),
        )

        fig, ax = plt.subplots(figsize=[6,4])

        mass_ci.plot(
            ax,
            include_median=True, include_mean=True,
            equal_prob_bounds=[0.5, 0.9],
        )

        if cli_args.overlay_synthetic_truth is not None:
            with open(cli_args.overlay_synthetic_truth, "r") as truths_file:
                parameters_true = json.load(truths_file)
                parameters_true = {
                    name : numpy.asarray(value)
                    for name, value in parameters_true.items()
                }

            mass_pdf_synthetic = mass_pdf(mass_grid, parameters_true)

            ax.plot(
                mass_grid, mass_pdf_synthetic,
                color="C3", linestyle="dashed",
            )

        ax.set_xlabel(r"$m_1$ [M$_\odot$]")
        ax.set_ylabel(r"pdf")

        fig.savefig(cli_args.output_plot)
