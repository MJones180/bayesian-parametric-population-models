def populate_subparser(subparsers):
    from ....astro_models import eos

    subparser = subparsers.add_parser("synthetic_observations")

    subparser.add_argument(
        "truths",
        help="JSON file mapping parameter names to their true values.",
    )

    subparser.add_argument(
        "--n-detections",
        type=int,
        help="Fixed number of detections.",
    )

    subparser.add_argument(
        "--VT",
        help="HDF5 file with VT tabulated.",
    )
    subparser.add_argument(
        "--VT-scale-factor",
        type=float,
        help="Scales VT file by this multiplicative factor.",
    )

    subparser.add_argument(
        "--with-spin",
        action="store_true",
        help="Include spin d.o.f. in population model.",
    )
    subparser.add_argument(
        "--eos-coordinates",
        choices=eos.eos_coordinate_options, default="spectral",
        help="Coordinate system used for EOS.",
    )
    subparser.add_argument(
        "--n-components",
        type=int,
        help="Number of mixture components.  If not provided, only a single "
             "component will be used, and no component suffixes will be "
             "expected in the parameter names.",
    )

    subparser.add_argument(
        "--psd",
        help="Input a PSD, either as a file (not implemented yet) or as a name "
             "available in LALSimulation, prefixed with 'LAL:'.  "
             "If provided, PSD-dependant luminosity distance will be output.",
    )

    subparser.add_argument("--seed", type=int)

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    import json

    import h5py
    import numpy

    from .population import get_population

    from ....poisson_mean import MonteCarloVolumeIntegralPoissonMean
    from ....coordinate import CoordinateSystem
    from ....synthesis import ObservationSynthesizer

    from ....astro_models.building_blocks.powerlaw import powerlaw_rvs
    from ....astro_models.gw_ifo_vt import RegularGridVTInterpolator

    # Parse PSD if given.
    psd = parse_psd(cli_args.psd)
    # Create a list of extra coordinate transformations
    transform_funcs = compute_psd_funcs(psd)
    transform_funcs += extra_funcs
    if psd is not None:
        transform_funcs += extra_funcs_psd

    # Get population and coord_system in default EOS coordinate sytem
    population = get_population(
        with_spin=cli_args.with_spin,
        eos_coordinates=cli_args.eos_coordinates,
        n_comp=cli_args.n_components,
    )

    # Load parameters from input file
    with open(cli_args.truths, "r") as truths_file:
        parameters_true = json.load(truths_file)

    # Ensure all parameter values are numpy arrays
    parameters_true = {
        name : numpy.asarray(value) for name, value in parameters_true.items()
    }

    # Seed the RNG
    random_state = numpy.random.RandomState(cli_args.seed)

    # Load in VT file if provided, and if so, produce a PoissonMean object if
    # a fixed number of detections was not provided.
    if cli_args.VT is None:
        VT = None
        expval = None
    else:
        with h5py.File(cli_args.VT, "r") as vt_file:
            VT = RegularGridVTInterpolator(
                vt_file, scale_factor=cli_args.VT_scale_factor,
            )
        if cli_args.n_detections is None:
            expval = MonteCarloVolumeIntegralPoissonMean(population, VT)
        else:
            expval = None

    obs_synth = ObservationSynthesizer(
        population,
        detection_count=cli_args.n_detections,
        poisson_mean=expval,
        volume=VT,
    )

    observations = list(
        obs_synth.sample(parameters_true, random_state=random_state)
    )

    n_samples = len(observations[0])

    snrs = powerlaw_rvs(n_samples, -4.0, 12.0, 10000.0)

    # Append snrs to observations
    observations.append(snrs)

    # Construct dict of observables
    names = [coord.name for coord in population.coord_system] + ["network_snr"]
    obs_dict = dict(zip(names, observations))

    # Compute additional observables.
    for name, func in transform_funcs:
        # Compute the value of the new observable.
        value = func(obs_dict)
        # Update the list-based bookkeeping.
        names.append(name)
        observations.append(value)
        # Update the dict-based bookkeeping.
        obs_dict[name] = value

    # Print header.
    print("#", "\t".join(names))
    # Print all observations.
    for i in range(n_samples):
        print("\t".join(str(obs[i]) for obs in observations))


def parse_psd(psd_specifier):
    if psd_specifier is None:
        return None
    elif psd_specifier.startswith("LAL:"):
        return _parse_psd_LAL(psd_specifier)
    else:
        return _parse_psd_file(psd_specifier)


def _parse_psd_LAL(psd_specifier):
    # Remove 'LAL:' prefix
    psd_name = psd_specifier[len("LAL:"):]

    # Load PSD from lalsim
    import lalsimulation
    import typing
    try:
        psd = getattr(lalsimulation, psd_name)
        if isinstance(psd, typing.Callable):
            status = "success"
        else:
            status = "invalid"
    except AttributeError:
        status = "missing"

    if status == "success":
        return psd
    elif status == "missing":
        raise KeyError(
            "PSD {} was not found in LALSimulation".format(psd_name)
        )
    elif status == "invalid":
        raise TypeError(
            "An object named {} was found in LALSimulation, but it is not a PSD"
            .format(psd_name)
        )
    else:
        raise RuntimeError("Inaccessible code reached.")

def _parse_psd_file(psd_specifier):
    raise NotImplementedError()


def create_chirp_mass_func(suffix=""):
    m1_name, m2_name = "m1{}".format(suffix), "m2{}".format(suffix)

    def chirp_mass_func(obs_dict):
        m1, m2 = obs_dict[m1_name], obs_dict[m2_name]

        return (m1*m2)**0.6 * (m1+m2)**-0.2

    return chirp_mass_func

def create_redshift_func(cosmology=None):
    import numpy
    from astropy import units
    import astropy.cosmology
    if cosmology is None:
        cosmology = astropy.cosmology.Planck15

    def redshift_func(obs_dict):
        return numpy.asarray([
            astropy.cosmology.z_at_value(
                cosmology.luminosity_distance,
                dL*units.Mpc,
                zmax=3,
            )
            for dL in obs_dict["dist"]
        ])

    return redshift_func


def create_redshifted_mass_func(name):
    def redshifted_mass_func(obs_dict):
        m = obs_dict[name]
        z = obs_dict["z"]

        return (1.0+z) * m

    return redshifted_mass_func


extra_funcs = [
    ("mc_source", create_chirp_mass_func(suffix="_source")),
]
extra_funcs_psd = [
    ("z", create_redshift_func()),
    ("m1", create_redshifted_mass_func("m1_source")),
    ("m2", create_redshifted_mass_func("m2_source")),
    ("mc", create_redshifted_mass_func("mc_source")),
]


def compute_psd_funcs(psd):
    import numpy

    psd_funcs = []

    # If no PSD is given, then there are no mappings.
    if psd is None:
        return psd_funcs

    # Add on the rho->d_L mapping

    psd_funcs.append(("dist", make_dist_func(psd)))
    # TODO: more

    return psd_funcs


def make_dist_func(psd):
    import numpy
    def inner(obs_dict):
        output = numpy.empty_like(next(iter(obs_dict.values())))
        n_samples = len(output)

        for i in range(n_samples):
            output[i] = dist_func(
                {
                    name : numpy.asarray(values[i])
                    for name, values in obs_dict.items()
                },
                psd,
            )

        return output

    return inner


def dist_func(obs_dict, psd):
    import lal
    import lalsimulation as ls
    from astropy.cosmology import Planck15 as cosmo
    from astropy import units
    import numpy

    psdstart = 20.0
    fmin, fmax, fref = 20.0, 2048.0, 40.0
    m1_source = obs_dict["m1_source"]
    m2_source = obs_dict["m2_source"]
    spin12xyz = 0.0
    incl = 0.0
    z_ref = 0.01

    approximant = ls.IMRPhenomPv2

    # Convert source-frame masses to detector-frame masses, in SI units.
    m1_det_SI = (1+z_ref)*m1_source * lal.MSUN_SI
    m2_det_SI = (1+z_ref)*m2_source * lal.MSUN_SI

    # Use cosmology to convert redshift to luminosity distance in SI units.
    dL_ref = cosmo.luminosity_distance(z_ref)
    dL_ref_Mpc = dL_ref.to(units.Mpc).value
    dL_ref_SI = dL_ref.to(units.meter).value

    # Conservative estimate of chirp time + merger-ringdown time (2 seconds)
    tmax = ls.SimInspiralChirpTimeBound(
        fmin,
        m1_det_SI, m2_det_SI,
        spin12xyz, spin12xyz,
    ) + 2.0

    # Convert into frequency-step.
    df = 1.0 / next_pow_two(tmax)

    # Compute h_+(f) and h_x(f)
    hp, hc = ls.SimInspiralChooseFDWaveform(
        m1_det_SI, m2_det_SI,
        spin12xyz, spin12xyz, spin12xyz, spin12xyz, spin12xyz, spin12xyz,
        dL_ref_SI,
        incl, 0.0, 0.0, 0.0, 0.0,
        df, fmin, fmax, fref, None, approximant,
    )

    # TODO: get correct h(f) using antenna pattern
    h_of_f = hp

    Nf = int(numpy.round(fmax / df)) + 1
    fs = numpy.linspace(0, fmax, Nf)
    sel = fs > psdstart

    # PSD
    sffs = lal.CreateREAL8FrequencySeries(
        "psds", 0, 0.0, df, lal.DimensionlessUnit, fs.size,
    )
    psd(sffs, psdstart)

    # Compute the SNR
    snr_ref = ls.MeasureSNRFD(h_of_f, sffs, psdstart, -1.0)

    # Compute power law constant
    pl_const = snr_ref * dL_ref_Mpc

    return pl_const / obs_dict["network_snr"]


def next_pow_two(x):
    """
    Return the next (integer) power of two above `x`.
    """
    x2 = 1
    while x2 < x:
        x2 = x2 << 1
    return x2
