def populate_subparser(subparsers):
    from pop_models.astro_models import eos

    subparser = subparsers.add_parser("plot_full_ppd")

    subparser.add_argument("cleaned_posterior_samples_hdf5")
    subparser.add_argument("output_plot")

    subparser.add_argument("--data-file")

    subparser.add_argument("--n-samples-per-realization", type=int, default=10)
    subparser.add_argument("--n-bins", type=int, default=20)

    subparser.add_argument("--overlay-synthetic-truth", action="store_true")
    subparser.add_argument("--synthetic-eos-name", default="AP4")

    subparser.add_argument(
        "--eos-coordinates",
        choices=eos.eos_coordinate_options, default="spectral_legendre",
    )

    subparser.add_argument(
        "--combine-components",
        action="store_true",
        help="Combine both components in each binary, so m1 and m2 are reduced "
             "to m, and Lambda1 and Lambda2 are reduced to Lambda.",
    )

    subparser.add_argument("--seed", type=int)

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_population

    from pop_models.posterior import H5CleanedPosteriorSamples
    from pop_models.astro_models import coordinates, eos

    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import scipy.stats

    random_state = numpy.random.RandomState(cli_args.seed)

    post_samples = H5CleanedPosteriorSamples(
        cli_args.cleaned_posterior_samples_hdf5
    )
    with post_samples:
        # Get population in proper EOS coordinates.
        metadata = post_samples.metadata

        with_spin = post_samples.metadata["with_spin"]
        eos_coordinates = post_samples.metadata["eos_coordinates"]
        ## Earlier versions were hard coded to 'mean_variance' and did not
        ## store it in the metadata.  If no value is stored, then it must be
        ## the old hard-coded value.
        spin_parameterization = post_samples.metadata.get(
            "spin_parameterization",
            "mean_variance",
        )
        n_components = post_samples.metadata.get("n_components")

        # Get population and coord_names in proper EOS coordinates.
        population = get_population(
            with_spin=with_spin,
            eos_coordinates=eos_coordinates,
            n_comp=n_components,
            spin_parameterization=spin_parameterization,
        )

        samples = population.rvs(
            cli_args.n_samples_per_realization, post_samples.get_params(...),
            random_state=random_state,
        )
        n_samples = samples[0].size

        # Write data to file if requested.
        if cli_args.data_file is not None:
            data = numpy.column_stack(tuple(s.ravel() for s in samples))
            coord_names = [
                coordinates.name_convert_to_lalinf[coord.name]
                for coord in population.coord_system
            ]
            numpy.savetxt(
                cli_args.data_file, data,
                header=" ".join(coord_names),
            )
            del data, coord_names

        # Make plot.
        fig = plot_corner(
            samples,
            "C0", "solid", 1,
            bins=cli_args.n_bins,
            combine_components=cli_args.combine_components,
            with_spin=with_spin,
        )
        # Overlay truths if provided.
        if cli_args.overlay_synthetic_truth:
            parameters_eos = (
                eos.spectral_eos_parameter_examples[cli_args.synthetic_eos_name]
            )
            parameters_eos = {
                name : numpy.asarray(value)
                for name, value in parameters_eos.items()
            }
            parameters_synth.update(parameters_eos)

            samples = population.rvs(
                n_samples, parameters_synth,
                random_state=random_state,
            )

            plot_corner(
                samples,
                "C3", "dashed", 2,
                with_spin=with_spin,
                bins=cli_args.n_bins,
                combine_components=cli_args.combine_components,
                fig=fig,
                true_eos_params=parameters_eos,
            )

            add_legend(fig)

        fig.savefig(cli_args.output_plot)


# Top-level dict keys are whether/not components are combined
# (e.g., m = m1 or m2)
label_options = {
    True : {
        "mass" : [
            r"$m_{\mathrm{source}} \, [M_\odot]$",
        ],
        "spin" : [
            r"$\chi_z$",
        ],
        "tide" : [
            r"$\Lambda$",
        ]
    },
    False : {
        "mass" : [
            r"$m_{1,\mathrm{source}} \, [M_\odot]$",
            r"$m_{2,\mathrm{source}} \, [M_\odot]$",
        ],
        "spin" : [
            r"$\chi_{1z}$",
            r"$\chi_{2z}$",
        ],
        "tide" : [
            r"$\Lambda_1$",
            r"$\Lambda_2$",
        ],
    },
}


# Dictionary mapping 'combine_components' -> 'with_spin' -> list of fig.axes
# indices to treat as special when overlaying a true EOS
special_eos_axes = {
    True: {
        True: [6],
        False: [2],
    },
    False: {
        True: [30, 37],
        False: [],
    }
}

def plot_corner(
        samples,
        color, linestyle, zorder,
        with_spin=False,
        bins=20,
        combine_components=False,
        fig=None,
        true_eos_params=None,
    ):
    import numpy
    import corner

    if fig is None:
        if true_eos_params is not None:
            raise ValueError(
                "Currently don't support true EOS overlay on blank figure"
            )

        label_dict = label_options[combine_components]
        labels = (
            label_dict["tide"] +
            label_dict["mass"] +
            (label_dict["spin"] if with_spin else [])
        )
    else:
        labels = None

    if combine_components:
        m1, m2 = samples[:2]
        L1, L2 = samples[-2:]
        if with_spin:
            chi1, chi2 = samples[2:-2]
            n_components = 3
        else:
            n_components = 2

        size, dtype = m1.size, m1.dtype

        data = numpy.empty((2*size, n_components), dtype=dtype)

        data[:size,0] = m1.flatten()
        data[size:,0] = m2.flatten()
        data[:size,-1] = L1.flatten()
        data[size:,-1] = L2.flatten()
        if with_spin:
            data[:size,1] = chi1.flatten()
            data[size:,1] = chi2.flatten()

    else:
        data = numpy.column_stack(tuple(s.flatten() for s in samples))

    n_samples, n_dim = data.shape

    if true_eos_params is not None:
        # Construct an EOS family object
        import lalsimulation
        from ....astro_models.eos import spectral_eos, lambda_from_eos_and_mass
        eos_fam = lalsimulation.CreateSimNeutronStarFamily(
            spectral_eos(tuple(
                true_eos_params["gamma{}".format(i)][()]
                for i in range(1,5)
            ))
        )

        # Calculate the indices within `samples` that all of the mass and tide
        # observables lie, which depends on whether components are combined, and
        # whether spin is present.
        mass_indices = [0] if combine_components else [0, 1]
        tide_offset = (1 if combine_components else 2) * (2 if with_spin else 1)
        tide_indices = [i + tide_offset for i in mass_indices]

        # Compute the indices within `fig.axes` that corresponds to anything
        # that can be replaced with the known Lambda(m) relation.
        suppressed_indices = [
            tide_i*n_dim + mass_i
            for mass_i, tide_i in zip(mass_indices, tide_indices)
        ]

        corner_fig = SuppressedFigure(fig, suppressed_indices)

        for mass_i, tide_i in zip(mass_indices, tide_indices):
            ax = fig.axes[tide_i*n_dim + mass_i]
            m_min, m_max = ax.get_xlim()
            # mass_samples = samples[mass_i]
            # m_min, m_max = mass_samples.min(), mass_samples.max()
            m_grid = numpy.linspace(m_min, m_max, 100)
            Lambda_grid = numpy.asarray([
                lambda_from_eos_and_mass(eos_fam, m)
                for m in m_grid
            ])
            ax.plot(
                m_grid, Lambda_grid,
                color=color,
                linestyle=linestyle, linewidth=2.5,
                zorder=zorder,
            )
    else:
        corner_fig = fig

    return corner.corner(
        data,
        levels=[0.50, 0.90],
        bins=bins,
        labels=labels,
        color=color,
        hist_kwargs={
            "linestyle": linestyle, "linewidth": 2.5,
            "density" : True,
            "zorder" : zorder,
        },
        contour_kwargs={
            "linestyles": linestyle, "linewidths": [2, 1],
            "zorder": zorder,
        },
        fig=corner_fig,
        no_fill_contours=True, plot_datapoints=False, plot_density=False,
    )


class SuppressedFigure(object):
    """
    A wrapper class for matplotlib Figure objects to trick corner.py into not
    overplotting on specific subplots.
    """
    def __init__(self, fig, suppressed_indices):
        import matplotlib.pyplot as plt
        # Make a copy of `fig.axes`, and overwrite any suppressed index's axis
        # object with a dummy one.
        self.axes = fig.axes.copy()
        for i in suppressed_indices:
            dummy_fig, dummy_ax = plt.subplots()
            self.axes[i] = dummy_ax

    def subplots_adjust(self, *args, **kwargs):
        """
        Overriding the one method of Figure that corner.py currently calls.
        If future versions call other methods of Figure, they will need to be
        implemented as well (but they should do nothing, unless corner.py
        changes its behavior significantly).
        """
        pass

def add_legend(fig):
    import math
    import matplotlib

    n_params = int(math.sqrt(len(fig.axes)))
    legend_ax = fig.axes[n_params-1]

    lines = [
        matplotlib.lines.Line2D(
            [0.0], [0.0],
            color=color, linestyle=linestyle,
            linewidth=2.5,
        )
        for color, linestyle
        in zip(["C0", "C3"], ["solid", "dashed"])
    ]
    labels = [
        "Inferred PPD",
        "Synthetic truth",
    ]

    legend_ax.legend(lines, labels)
