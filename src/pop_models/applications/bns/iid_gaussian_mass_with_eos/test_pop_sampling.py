def populate_subparser(subparsers):
    subparser = subparsers.add_parser("test_pop_sampling")

    subparser.add_argument("output_plot")
    subparser.add_argument("--n-samples", type=int, required=True)
    subparser.add_argument("--seed", type=int)

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_population

    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt

    population = get_population()

    mass_params = {
        "m_mean" : numpy.asarray(1.33),
        "m_stdev" : numpy.asarray(0.09),
        "m_min" : numpy.asarray(1.0),
        "m_max" : numpy.asarray(2.0),
    }
    # spin_params = {
    #     "spin_mean" : numpy.asarray(0.0),
    #     "spin_variance" : numpy.asarray(0.001),
    # }
    H4_params = {
        "gamma1" : numpy.asarray(+1.0526),
        "gamma2" : numpy.asarray(+0.1695),
        "gamma3" : numpy.asarray(-0.1200),
        "gamma4" : numpy.asarray(+0.0150),
    }

    parameters = {**mass_params, **H4_params}


    random_state = numpy.random.RandomState(cli_args.seed)

    m1_samples, m2_samples, Lambda1_samples, Lambda2_samples = population.rvs(
        cli_args.n_samples, parameters,
        random_state=random_state
    )
    Lambda_max = numpy.max([Lambda1_samples, Lambda2_samples])

    (
        fig,
        (
            (m1_ax, m2_ax),
            (mass_ax, Lambda_ax),
            (m1_L1_ax, m2_L2_ax),
        ),
    ) = plt.subplots(
        nrows=3, ncols=2,
        figsize=(12,16),
    )

    m1_ax.hist(m1_samples, bins=20, histtype="step")
    m2_ax.hist(m2_samples, bins=20, histtype="step")

    m1_ax.set_xlabel(r"$m_1$")
    m2_ax.set_xlabel(r"$m_2$")

    mass_ax.scatter(m1_samples, m2_samples)
    mass_ax.fill_between(
        [parameters["m_min"], parameters["m_max"]],
        [parameters["m_min"], parameters["m_max"]],
        [parameters["m_max"], parameters["m_max"]],
        color="#AAAAAA",
    )
    mass_ax.set_xlabel(r"$m_1$")
    mass_ax.set_ylabel(r"$m_2$")
    mass_ax.set_xlim([parameters["m_min"], parameters["m_max"]])
    mass_ax.set_ylim([parameters["m_min"], parameters["m_max"]])

    Lambda_ax.scatter(Lambda1_samples, Lambda2_samples)
    Lambda_ax.fill_between(
        [0, Lambda_max], [0, Lambda_max],
        color="#AAAAAA",
    )
    Lambda_ax.set_xlabel(r"$\Lambda_1$")
    Lambda_ax.set_ylabel(r"$\Lambda_2$")
    Lambda_ax.set_xlim([0, Lambda_max])
    Lambda_ax.set_ylim([0, Lambda_max])

    m1_L1_ax.scatter(m1_samples, Lambda1_samples)
    m1_L1_ax.set_xlabel(r"$m_1$")
    m1_L1_ax.set_ylabel(r"$\Lambda_1$")

    m2_L2_ax.scatter(m2_samples, Lambda2_samples)
    m2_L2_ax.set_xlabel(r"$m_2$")
    m2_L2_ax.set_ylabel(r"$\Lambda_2$")

    fig.tight_layout()
    fig.savefig(cli_args.output_plot)
