import typing

from ....coordinate import Coordinate, CoordinateSystem
from ....population import (
    Population,
    SeparableCoordinatePopulation, MixturePopulation
)

from ....astro_models import eos
from ....astro_models.coordinates import (
    mA_source_coord, mB_source_coord,
    m1_source_coord, m2_source_coord,
    chi1z_coord, chi2z_coord,
    Lambda1_coord, Lambda2_coord,
    transformations,
)
from ....astro_models.building_blocks import (
    beta, delta_fn, gaussian_mass, generic
)


def get_coord_system(with_spin=False):
    coords = (
        [m1_source_coord, m2_source_coord] +
        ([chi1z_coord, chi2z_coord] if with_spin else []) +
        [Lambda1_coord, Lambda2_coord]
    )
    return CoordinateSystem(*coords)

def get_aligned_spin_pop(
        aligned_spin_coord: Coordinate,
        abs_spin_max=0.05,
        parameterization: str="mean_log10variance",
    ):
    loc = -abs_spin_max
    scale = 2.0 * abs_spin_max
    return beta.BetaPopulation(
        aligned_spin_coord,
        loc=loc, scale=scale,
        parameterization=parameterization,
        rate_name="rate", mean_name="spin_mean", variance_name="spin_variance",
        transformations=transformations,
    )

def get_base_population(
        with_spin: bool=False,
        eos_coordinates: str="spectral",
        n_comp: typing.Optional[int]=None,
        spin_parameterization: str="mean_log10variance",
    ):
    pop_mass = gaussian_mass.ComponentMassIIDGaussianPopulation()

    if with_spin:
        pop = SeparableCoordinatePopulation(
            pop_mass,
            get_aligned_spin_pop(
                chi1z_coord,
                parameterization=spin_parameterization,
            ),
            get_aligned_spin_pop(
                chi2z_coord,
                parameterization=spin_parameterization,
            ),
            transformations=transformations,
        )
    else:
        pop = pop_mass

    # Return a single Gaussian population
    if n_comp is None:
        return pop
    # Return a Gaussian mixture population
    else:
        coord_system = pop.coord_system
        suffix_to_population = {
            "_{}".format(i) : pop
            for i in range(n_comp)
        }
        return MixturePopulation(
            coord_system, suffix_to_population, transformations=transformations,
        )


def get_population(
        with_spin: bool=False,
        eos_coordinates: str="spectral",
        n_comp: typing.Optional[int]=None,
        use_flat_dist: bool=False,
        spin_parameterization: str="mean_log10variance",
    ):
    """
    Construct a BNS population.

    :param with_spin:
        Includes spin d.o.f., ``False`` by default.
    :param eos_coordinates:
        Coordinate system used for EOS.  "spectral" by default.
    :n_comp:
        Number of mixture components to use.  Default is ``None``, which is
        equivalent to ``1``, but does not add the suffix ``_0`` to every
        parameter name.
    :use_flat_dist:
        Override everything but the ``with_spin`` argument, and just use a flat
        mass distribution.
    :spin_parameterization:
        Name of parameterization to use for the spin distribution.
    """
    # Short circuit everything if a flat distribution was requested.
    if use_flat_dist:
        # Create a uniform box in two unsorted masses, and convert to sorted
        # masses.
        pop_mass = generic.GenericNDUniformPopulation(
            CoordinateSystem(mA_source_coord, mB_source_coord),
            norm_name="rate",
            bound_names=(
                ("m1_min", "m1_max"),
                ("m2_min", "m2_max"),
            ),
            transformations=transformations,
        ).to_coords(CoordinateSystem(m1_source_coord, m2_source_coord))

        # Tack on a delta function in the rest of the parameters, all fixed to
        # zero: either just tidal deformabilities, or both that and aligned
        # spins.
        if with_spin:
            pop_rest = delta_fn.DeltaFnPopulation(
                CoordinateSystem(
                    chi1z_coord, chi2z_coord,
                    Lambda1_coord, Lambda2_coord,
                ),
                [0.0, 0.0, 0.0, 0.0],
                rate_name="rate",
                transformations=transformations,
            )
        else:
            pop_rest = delta_fn.DeltaFnPopulation(
                CoordinateSystem(Lambda1_coord, Lambda2_coord)
                [0.0, 0.0],
                transformations=transformations,
            )

        return SeparableCoordinatePopulation(
            pop_mass, pop_rest,
            transformations=transformations,
        )

    # Construct the population, modulo EOS effects.
    base_pop = get_base_population(
        with_spin=with_spin, n_comp=n_comp,
        eos_coordinates=eos_coordinates,
        spin_parameterization=spin_parameterization,
    )
    # Wrap the base population with EOS effects.
    # NOTE: Hard coded for spectral representations at the moment.
    return eos.EOSPopulation(
        base_pop, eos.spectral_eos,
        eos_coordinates=eos_coordinates,
    )
