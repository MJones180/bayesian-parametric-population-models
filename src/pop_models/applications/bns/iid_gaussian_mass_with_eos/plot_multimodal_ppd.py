def populate_subparser(subparsers):
    subparser = subparsers.add_parser("plot_multimodal_ppd")

    subparser.add_argument("cleaned_posterior_samples_hdf5")
    subparser.add_argument("output_plot")

    subparser.add_argument("--n-samples-per-realization", type=int, default=10)

#    subparser.add_argument("--overlay-synthetic-truth")

    subparser.add_argument("--seed", type=int)

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_population

    from pop_models.coordinate import CoordinateSystem
    from pop_models.posterior import H5CleanedPosteriorSamples
    from pop_models.astro_models.coordinates import (
        m1_source_coord, m2_source_coord,
        chi1z_coord, chi2z_coord,
    )

    import json

    import numpy
    import matplotlib
    matplotlib.use("Agg")
    import matplotlib.pyplot as plt
    import scipy.stats

    random_state = numpy.random.RandomState(cli_args.seed)

    coord_system = CoordinateSystem(
        m1_source_coord, m2_source_coord,
        chi1z_coord, chi2z_coord,
    )

    fig, ax = plt.subplots(figsize=(4, 4))

    ax.set_xlabel(r"$m_{\mathrm{source}}$ [M$_\odot$]")
    ax.set_ylabel(r"$\chi_z$")

    post_samples = H5CleanedPosteriorSamples(
        cli_args.cleaned_posterior_samples_hdf5
    )
    with post_samples:
        # Count the number of posterior samples.
        n_post_samples = len(post_samples)

        # Get population in proper EOS coordinates.
        metadata = post_samples.metadata

        with_spin = post_samples.metadata["with_spin"]
        eos_coordinates = post_samples.metadata["eos_coordinates"]
        ## Earlier versions were hard coded to 'mean_variance' and did not
        ## store it in the metadata.  If no value is stored, then it must be
        ## the old hard-coded value.
        spin_parameterization = post_samples.metadata.get(
            "spin_parameterization",
            "mean_variance",
        )
        n_components = post_samples.metadata.get("n_components")

        # Pick out mean mass parameter names for use later.
        m_mean_names = sorted(
            param_name for param_name in post_samples.param_names
            if param_name.startswith("m_mean_")
        )

        # Get population object, taking only a unimodal component of the
        # population.  We will re-use this object for each component, drawing
        # the samples separately so we know which is which.
        population = get_population(
            with_spin=with_spin,
            eos_coordinates=eos_coordinates,
            spin_parameterization=spin_parameterization,
        ).to_coords(coord_system)

        # Initialize arrays to store mass and spin PPD samples.  For each
        # population posterior sample, there are `--n-samples-per-realization`
        # samples from each component of the binary, and we don't care about
        # the individual binaries, hence the number of points.  The arrays also
        # have an extra axis corresponding to which mixture component the
        # samples belong to.
        m_source_samples = numpy.empty(
            (n_components,2,n_post_samples*cli_args.n_samples_per_realization),
            dtype=numpy.float64,
        )
        chiz_samples = numpy.empty_like(m_source_samples)

        # Bookkeeping for starting index for each sample.
        idx_start = 0

        # Iterate over each population posterior sample, and construct the PPD
        # samples.
        for i in range(n_post_samples):
            parameters_i = post_samples.get_params(i)

            # Determine sorting order based on mean masses, so samples go to
            # correct cluster in case sub-populations are unsorted.
            i_sort = numpy.argsort(m_mean_names)

            # Iterate over subpop indices.
            for i_subpop in i_sort:
                # Pull out parameters with subpop's suffix.
                parameters_subpop = {}
                suffix = "_{}".format(i_subpop)
                for param_name, values in parameters_i.items():
                    if param_name.endswith(suffix):
                        parameters_subpop[param_name[:-len(suffix)]] = values
                    # Also include EOS parameters.
                    elif param_name.startswith("gamma"):
                        parameters_subpop[param_name] = values

                # Draw samples from subpopulation.
                m1_samples, m2_samples, chi1z_samples, chi2z_samples = (
                    population.rvs(
                        cli_args.n_samples_per_realization, parameters_subpop,
                        random_state=random_state,
                    )
                )

                # Store samples in appropriate output arrays.
                sample_slice = slice(
                    idx_start,
                    idx_start+cli_args.n_samples_per_realization,
                )

                m_source_samples[i_subpop, 0, sample_slice] = m1_samples
                m_source_samples[i_subpop, 1, sample_slice] = m2_samples
                chiz_samples[i_subpop, 0, sample_slice] = chi1z_samples
                chiz_samples[i_subpop, 1, sample_slice] = chi2z_samples

            idx_start += cli_args.n_samples_per_realization

        # Compute quantiles for each observable, for each component.
        m_source_lower, m_source_median, m_source_upper = numpy.quantile(
            m_source_samples, [0.1, 0.5, 0.9],
            axis=(1,2),
        )
        chiz_lower, chiz_median, chiz_upper = numpy.quantile(
            chiz_samples, [0.1, 0.5, 0.9],
            axis=(1,2),
        )

        data = []

        for i_subpop in range(n_components):
            xerr = (
                [m_source_median[i_subpop] - m_source_lower[i_subpop]],
                [m_source_upper[i_subpop] - m_source_median[i_subpop]],
            )
            yerr = (
                [chiz_median[i_subpop] - chiz_lower[i_subpop]],
                [chiz_upper[i_subpop] - chiz_median[i_subpop]],
            )

            data.append({
                "m_source" : {
                    "median" : m_source_median[i_subpop],
                    "err" : [xerr[0][0], xerr[1][0]],
                },
                "chiz" :  {
                    "median" : chiz_median[i_subpop],
                    "err" : [yerr[0][0], yerr[1][0]],
                },
            })

            ax.errorbar(
                m_source_median[i_subpop], chiz_median[i_subpop],
                xerr=xerr, yerr=yerr,
                color="black", fmt="o",
            )

        print(json.dumps(data, indent=4))

        fig.tight_layout()
        fig.savefig(cli_args.output_plot)
