def populate_subparser(subparsers):
    from pop_models.astro_models import eos
    from pop_models.astro_models.building_blocks.beta import BetaPopulation

    subparser = subparsers.add_parser("inference")

    subparser.add_argument(
        "events",
        nargs="*",
        help="List of event files.",
    )
    subparser.add_argument(
        "VTs",
        help="HDF5 file containing VTs.",
    )
    subparser.add_argument(
        "pop_priors",
        help="JSON file specifying population priors.",
    )
    subparser.add_argument(
        "posterior_output",
        help="Directory to store posterior samples in.",
    )

    subparser.add_argument(
        "--no-spin",
        action="store_false", dest="with_spin",
    )
    subparser.add_argument("--fixed-eos")

    subparser.add_argument(
        "--eos-coordinates",
        choices=eos.eos_coordinate_options, default="spectral",
    )
    subparser.add_argument(
        "--spin-parameterization", metavar="NAME",
        default="mean_log10variance",
        choices=BetaPopulation.supported_parameterizations,
        help="Parameterization to use for the spin distribution.",
    )
    subparser.add_argument(
        "--n-components",
        type=int,
        help="Number of mixture components.  If not provided, only a single "
             "component will be used, and no component suffixes will be "
             "expected in the parameter names.",
    )

    subparser.add_argument(
        "--constants",
        help="JSON file mapping parameter names to constant values they should "
             "be assumed to hold.",
    )
    subparser.add_argument(
        "--duplicates",
        help="JSON file mapping parameter names to other parameter names that "
             "they should be assumed to be duplicates of.",
    )

    subparser.add_argument(
        "--moves",
        help="JSON file setting ensemble MCMC move options.",
    )

    init_from_file_group = subparser.add_mutually_exclusive_group()
    init_from_file_group.add_argument(
        "--init-from-raw-samples",
        help="Initialize chains from the given HDF5 raw samples archive, "
             "taking the final positions of all the walkers as the new "
             "starting point.",
    )
    init_from_file_group.add_argument(
        "--init-from-cleaned-samples",
        help="Initialize chains from the given HDF5 cleaned samples file, "
             "taking a random set of samples, specified by --n-walkers, as the "
             "starting point.",
    )
    init_from_file_group.add_argument(
        "--init-from-eos-samples",
        help="Initialize EOS parameters using samples from this whitespace "
             "delimited file.",
    )
    init_from_file_group.add_argument(
        "--init-from-prior-pca",
        nargs=2, metavar=("PCA_FILE", "PCA_BUFFER"),
        help="Initialize EOS parameters using a PCA-determined rotated "
             "coordinate system, specified by an HDF5 file.  Also apply a "
             "buffer to extend the bounds, or specify zero to add no buffer.",
    )

    subparser.add_argument(
        "--scale-factor",
        type=float, default=None,
        help="Multiply all VTs by this constant factor.",
    )

    subparser.add_argument("--n-samples", type=int, default=100)
    subparser.add_argument("--n-walkers", type=int, default=32)

    subparser.add_argument("--chunk-size", type=int, default=1024)

    subparser.add_argument("--seed", type=int)

    subparser.add_argument("--swmr-mode", action="store_true")
    subparser.add_argument("--force", action="store_true")

    subparser.add_argument("--verbose", action="store_true")
    subparser.add_argument(
        "--debug",
        nargs="*",
        help="Output debugging messages.  Use with no arguments to display all "
             "debugging information, or specify the debugging mode(s).",
    )

    subparser.set_defaults(main_func=main)

    return subparser


def main(cli_args):
    from .population import get_population
    from .prior import init_from_prior, Prior

    from pop_models.coordinate import CoordinateSystem
    from pop_models.detection import (
        BasicAdaptivePopulationMonteCarloDetectionLikelihood,
    )
    from pop_models.poisson_mean import (
        MonteCarloVolumeIntegralFixedSamplePoissonMean
    )
    from pop_models.posterior import (
        H5RawPosteriorSamples,
        PopulationInference, PopulationInferenceEmceeSampler,
    )
    from pop_models.utils import parse_prior, debug_verbose

    from pop_models.astro_models.coordinates import (
        m1_source_coord, m2_source_coord,
        Mc_source_coord, delta_coord,
    )
    from pop_models.astro_models.gw_ifo_vt import RegularGridVTInterpolator
    from pop_models.astro_models.detections.cbc import (
        load as load_cbc,
        CBCGaussianDetection, CBCRandomForestDetection,
    )

    import h5py
    import json
    import os
    import glob

    import numpy
    import scipy.stats

    # Set debugging mode
    if cli_args.debug is not None:
        if len(cli_args.debug) == 0:
            debug_verbose.full_output_on()
        else:
            debug_verbose.enable_modes(*cli_args.debug)

    # Seed RNG
    random_state = numpy.random.RandomState(cli_args.seed)

    # Load in the observed data
    detections = [load_cbc(file_spec) for file_spec in cli_args.events]


    #####

    # Load existing posterior samples if resuming (file exists and --force flag
    # not set).
    exists = os.path.isfile(
        os.path.join(cli_args.posterior_output, "master.hdf5")
    )
    resumed = exists and not cli_args.force
    if resumed:
        post_samples = H5RawPosteriorSamples(
            cli_args.posterior_output, "r+",
            swmr_mode=cli_args.swmr_mode,
        )

        metadata = post_samples.metadata
        pop_prior_settings = json.loads(metadata["pop_prior_settings"])

        with_spin = post_samples.metadata["with_spin"]
        eos_coordinates = post_samples.metadata["eos_coordinates"]
        ## Earlier versions were hard coded to 'mean_variance' and did not
        ## store it in the metadata.  If no value is stored, then it must be
        ## the old hard-coded value.
        spin_parameterization = post_samples.metadata.get(
            "spin_parameterization",
            "mean_variance",
        )
        n_components = post_samples.metadata.get("n_components")

        # Get population and coord_names in proper EOS coordinates.
        population = get_population(
            with_spin=with_spin,
            eos_coordinates=eos_coordinates,
            n_comp=n_components,
            spin_parameterization=spin_parameterization,
        )
        param_names = population.param_names

        # Construct prior object from metadata in posterior samples.
        prior = Prior(
            pop_prior_settings, population,
            constants=post_samples.constants,
            duplicates=post_samples.duplicates,
        )
    # Create object for storing posterior samples, with an HDF5 backend, if
    # not resuming (file doesn't exist, or --force flag set).
    else:
        # Get population and coord_names in proper EOS coordinates.
        population = get_population(
            with_spin=cli_args.with_spin,
            eos_coordinates=cli_args.eos_coordinates,
            n_comp=cli_args.n_components,
        )
        param_names = population.param_names

        pop_prior_settings, constants, duplicates = parse_prior(
            param_names,
            cli_args.pop_priors,
            constants_fname=cli_args.constants,
            duplicates_fname=cli_args.duplicates,
        )

        # Construct prior object.
        prior = Prior(
            pop_prior_settings, population,
            constants=constants, duplicates=duplicates,
        )


    # Load in VT file.
    with h5py.File(cli_args.VTs, "r") as vt_file:
        VT = RegularGridVTInterpolator(
            vt_file, scale_factor=cli_args.scale_factor,
        )

    # Evaluates expected number of detections for a given population and VT.
    expval = MonteCarloVolumeIntegralFixedSamplePoissonMean(population, VT)

    # Create all of detection*population integral evaluators.  For this we split
    # the population such that the only dependant coordinates are (m1, m2).
    # HACK: Currently assumes all detections have their masses in chirp-mass and
    # delta coordinates.
    coord_system_pop = CoordinateSystem(m1_source_coord, m2_source_coord)
    coord_system_x = CoordinateSystem(*(
        coord for coord in population.coord_system
        if coord not in coord_system_pop
    ))
    population_adaptive = population.to_reordered_coords(
        coord_system_x, coord_system_pop,
    )
    adaptive_coord_system = CoordinateSystem(Mc_source_coord, delta_coord)

    def jacobian_fn(m1m2):
        """
        Returns |d(Mc, delta)/d(m1, m2)| as a fn of (m1,m2).
        """
        m1, m2 = m1m2

        # Initialize output
        matrix = numpy.empty(m1.shape + (2,2), dtype=float)

        mprod = m1 * m2
        msum = m1 + m2

        dMc_denom = mprod**-0.4 * msum**-1.2
        ddelta_denom = msum**-2.0

        # dMc/dm1
        matrix[...,0,0] = 0.2 * m2 * (2*m1 + 3*m2) * dMc_denom
        # dMc/dm2
        matrix[...,0,1] = 0.2 * m1 * (2*m2 + 3*m1) * dMc_denom
        # ddelta/dm1
        matrix[...,1,0] = +2.0 * m2 * ddelta_denom
        # ddelta/dm2
        matrix[...,1,1] = -2.0 * m1 * ddelta_denom

        return numpy.abs(numpy.linalg.det(matrix))


    detection_likelihoods = []
    for detection in detections:
        indices = [
            detection.coord_system.index(coord)
            for coord in adaptive_coord_system
        ]
        # Determine the bounding box.  Currently only supports
        # ``CBCRandomForestDetection`` and ``CBCGaussianDetection``.
        if isinstance(detection, CBCRandomForestDetection):
            bounding_box = tuple(
                (
                    detection.random_forest.X_min[i],
                    detection.random_forest.X_max[i],
                )
                for i in indices
            )
        elif isinstance(detection, CBCGaussianDetection):
            bounding_box_lst = []
            for i, coord in zip(indices, adaptive_coord_system):
                # Take the mean +/- 3-sigma, modulo any coordinate cutoffs.
                mu = detection.mean[i]
                n_sigma = 3.0 * detection.covariance[i,i]
                lower = mu - n_sigma
                upper = mu + n_sigma
                if coord.lower_limit is not None:
                    lower = max(lower, coord.lower_limit)
                if coord.upper_limit is not None:
                    upper = min(upper, coord.upper_limit)
                bounding_box_lst.append((lower, upper))
            bounding_box = tuple(bounding_box_lst)
        else:
            raise TypeError(
                "Unexpected detection type: {}".format(type(detection))
            )
        det_like = BasicAdaptivePopulationMonteCarloDetectionLikelihood(
            population_adaptive, detection,
            bounding_box, adaptive_coord_system,
            jacobian_fn=jacobian_fn,
            ## TODO: generalize sample size
            n_samples=1024, random_state=random_state,
        )
        detection_likelihoods.append(det_like)

    if not resumed:
        # Set up a stripped down inference object only used for vetoing initial
        # values that are not within the posterior's support.
        pop_inference_for_init = PopulationInference(
            expval, detection_likelihoods, prior,
            param_names,
        )

        # Create posterior samples HDF5 archive, and object interface.
        if cli_args.init_from_raw_samples is not None:
            # Initialize from raw samples file.
            init_post = H5RawPosteriorSamples(
                cli_args.init_from_raw_samples, "r",
            )
            with init_post:
                post_samples = H5RawPosteriorSamples.create_from_raw_samples(
                    cli_args.posterior_output,
                    init_post,
                    constants=constants, duplicates=duplicates,
                    chunk_size=cli_args.chunk_size,
                    force=cli_args.force,
                    swmr_mode=cli_args.swmr_mode,
                )
        elif cli_args.init_from_cleaned_samples is not None:
            # Initialize from cleaned samples file.
            init_post = H5CleanedPosteriorSamples(
                cli_args.init_from_cleaned_samples
            )
            with init_post:
                post_samples = H5RawPosteriorSamples.create_from_samples(
                    cli_args.posterior_output,
                    cli_args.n_walkers,
                    init_post,
                    constants=constants,
                    chunk_size=cli_args.chunk_size,
                    force=cli_args.force,
                    swmr_mode=cli_args.swmr_mode,
                )
        else:
            # Choose method to initialize EOS parameters from
            if cli_args.init_from_eos_samples is not None:
                eos_method = "reuse_samples"
                extra_kwargs = {}
            elif cli_args.init_from_prior_pca is not None:
                eos_method = "pca"
                pca_fname, pca_buffer = cli_args.init_from_prior_pca
                extra_kwargs = {
                    "pca_fname" : pca_fname,
                    "pca_buffer" : float(pca_buffer),
                }
            else:
                eos_method = "named"
                extra_kwargs = {}
            # Initialize from prior.
            init_state = init_from_prior(
                cli_args.n_walkers, random_state, pop_prior_settings,
                param_names, prior,
                constants=constants,
#                n_components=cli_args.n_components,
#                with_spin=cli_args.with_spin,
                fixed_eos=cli_args.fixed_eos,
                eos_coordinates=cli_args.eos_coordinates,
                eos_method=eos_method,
                init_eos_samples=cli_args.init_from_eos_samples,
                pop_inf=pop_inference_for_init,
                **extra_kwargs
            )
            post_samples = H5RawPosteriorSamples.create(
                cli_args.posterior_output,
                param_names,
                init_state,
                constants=constants, duplicates=duplicates,
                chunk_size=cli_args.chunk_size,
                force=cli_args.force,
                swmr_mode=cli_args.swmr_mode,
            )

        # Store metadata
        post_samples.metadata["with_spin"] = cli_args.with_spin
        post_samples.metadata["eos_coordinates"] = cli_args.eos_coordinates
        post_samples.metadata["spin_parameterization"] = (
            cli_args.spin_parameterization
        )
        if cli_args.n_components is not None:
            post_samples.metadata["n_components"] = cli_args.n_components
        post_samples.metadata["pop_prior_settings"] = json.dumps(
            pop_prior_settings,
        )
        post_samples.metadata["VT_scale_factor"] = (
            cli_args.scale_factor if cli_args.scale_factor is not None
            else 1.0
        )


    with post_samples:
        if cli_args.moves is None:
            moves = None
        else:
            debug_verbose(
                "Parsing MCMC move options.",
                mode="inference_setup", flush=True,
            )
            with open(cli_args.moves, "r") as moves_file:
                moves = PopulationInferenceEmceeSampler.parse_moves_from_file(
                    moves_file
                )
            debug_verbose(
                "MCMC move options parsed as:", moves,
                mode="inference_setup", flush=True,
            )

        debug_verbose(
            "Constructing posterior sampler.",
            mode="inference_setup", flush=True,
        )
        pop_inference = PopulationInferenceEmceeSampler(
            post_samples,
            expval, detection_likelihoods, prior,
            moves=moves,
            random_state=random_state,
            verbose=cli_args.verbose,
        )
        debug_verbose(
            "Posterior sampler constructed: {}".format(pop_inference),
            mode="inference_setup", flush=True,
        )

        debug_verbose(
            "Beginning to sample {} posteriors.".format(cli_args.n_samples),
            mode="inference_setup", flush=True,
        )
        pop_inference.posterior_samples(cli_args.n_samples)
        debug_verbose(
            "Finished sampling {} posteriors.".format(cli_args.n_samples),
            mode="inference_setup", flush=True,
        )
