from __future__ import division, print_function

import numpy

LN_TEN = numpy.log(10.0)


def intensity(
        indiv_params, pop_params,
        prior_support, aux_info,
        M_max=None,
        **kwargs
    ):
    import numpy
    from . import prob

    rate = aux_info["rate"]
    pdf_const = aux_info["pdf_const"]

    # Compute the PDF term.
    out = prob.joint_pdf(
        indiv_params, pop_params, M_max,
        const=pdf_const,
        where=prior_support,
    )

    # Multiply the rate onto the PDF term to get the complete intensity
    # function.  We only want to do this where there is prior support, to save
    # computing time, so we first broadcast the prior_support index array
    # appropriately.  Multiplication has to be performed on the transposed
    # arrays since ``out`` has shape ``pop_params.shape+indiv_params.shape``,
    # and we want broadcasting to work along the ``pop_params`` axis.
    _, broadcast_prior_support = numpy.broadcast_arrays(out.T, prior_support.T)
    numpy.multiply(
        rate.T, out.T,
        out=out.T, where=broadcast_prior_support,
    )

    return out


class MeanPoissonMonteCarlo(object):
    def __init__(
            self,
            VT, M_max,
            err_abs=1e-5, err_rel=1e-3,
            rand_state=None,
        ):
        import numpy

        self.__VT = VT
        self.M_max = M_max
        self.err_abs = err_abs
        self.err_rel = err_rel

        if rand_state is None:
            rand_state = numpy.random.RandomState()

        self.rand_state = rand_state

        self.mode = VT.mode

    def __sample_zero_spin(self, N, pop_params):
        from . import prob

        alpha_m, beta_q, m_min, m_max = pop_params[1:5]

        return prob.mass_joint_rvs(
            N,
            alpha_m, beta_q, m_min, m_max, self.M_max,
            rand_state=self.rand_state,
        )

    def __sample_aligned_spin(self, N, pop_params):
        from . import prob

        # Draw samples in m1, m2, chi1, chi2, costheta1, costheta2 coordinates.
        samples = prob.joint_rvs(
            N,
            pop_params, self.M_max,
            rand_state=self.rand_state,
        )

        # Replace chi1 values with chi1z = chi1*costheta1 values
        samples[:,2] *= samples[:,4]

        # Replace chi2 values with chi2z = chi2*costheta2 values
        samples[:,3] *= samples[:,5]

        # Return first four columns, which now contain m1, m2, chi1z, chi2z
        return samples[:,:4]


    def __call__(self, pop_params, prior_support, aux_info, *args, **kwargs):
        import numpy
        from ..mc import integrate_adaptive

        shape = numpy.shape(pop_params)[:-1]

        rate = aux_info["rate"]
        return_err = kwargs.get("return_err", False)

        if self.mode == "zero spin":
            def make_p(idx):
                def p(N):
                    return self.__sample_zero_spin(N, pop_params[idx])
                return p

        elif self.mode == "aligned spin":
            def make_p(idx):
                def p(N):
                    return self.__sample_aligned_spin(N, pop_params[idx])
                return p

        else:
            raise NotImplementedError(
                "No implementatione exists for mode: '{mode}'"
                .format(mode=self.mode)
            )

        def VT(indiv_params):
            return self.__VT(*indiv_params.T)

        mu = numpy.empty(shape, dtype=numpy.float64)
        if return_err:
            err_abs = numpy.tile(numpy.nan, mu.shape)
            err_rel = numpy.tile(numpy.nan, mu.shape)

        for idx, _ in numpy.ndenumerate(mu):
            # Skip entries with no prior support
            if not prior_support[idx]:
                mu[idx] = 0.0
                continue

            result = integrate_adaptive(
                make_p(idx), VT,
                err_abs=self.err_abs, err_rel=self.err_rel,
            )

            mu[idx] = rate[idx] * result[0]
            if return_err:
                err_abs[idx] = rate[idx] * result[1]
                err_rel[idx] = result[2]

        if return_err:
            return mu, err_abs, err_rel
        else:
            return mu


def log_prior_pop(
        pop_params, aux_info,
        M_max=None,
        pop_prior_settings=None,
        no_spin_singularities=False,
        **kwargs
    ):
    from . import prob

    import numpy

    shape = numpy.shape(pop_params)[:-1]

    tmp_idx1 = numpy.empty(shape, dtype=numpy.bool)
    tmp_idx2 = numpy.empty(shape, dtype=numpy.bool)

    pop_params_dict = {
        name : pop_params[...,i]
        for i, name in enumerate(prob.param_names)
    }

    # Initialize variable to accumulate the value of log(prior)
    log_prior = numpy.zeros(shape, dtype=numpy.float64)

    for param in prob.param_names:
        prior_settings = pop_prior_settings[param]
        prior_dist = prior_settings["dist"]
        prior_params = prior_settings["params"]

        # Skip over constants and duplicates.
        if prior_dist in ["constant", "duplicate"]:
            continue
        # Add contribution for uniform prior.
        elif prior_dist == "uniform":
            min_val = prior_params["min"]
            max_val = prior_params["max"]
            val = pop_params_dict[param]

            log_prior[(val < min_val) | (val > max_val)] = -numpy.inf
        # Prior not implemented.
        else:
            raise NotImplementedError(
                "No implementation for prior '{}'".format(prior_dist)
            )

    ## Enforce mass cutoff limits well defined ##
    m_min = pop_params_dict["m_min"]
    m_max = pop_params_dict["m_max"]

    # Enforce ordering on m_min and m_max.
    log_prior[m_min >= m_max] = -numpy.inf
    # Enforce maximum total mass obeyed.
    log_prior[m_min + m_max > M_max] = -numpy.inf

    ## Enforce spin magnitude parameters well defined ##
    E_chi1 = pop_params_dict["E_chi1"]
    E_chi2 = pop_params_dict["E_chi2"]
    Var_chi1 = pop_params_dict["Var_chi1"]
    Var_chi2 = pop_params_dict["Var_chi2"]

    alpha_chi1, beta_chi1 = prob.spin_mu_sigma_sq_2_alpha_beta(E_chi1, Var_chi1)
    alpha_chi2, beta_chi2 = prob.spin_mu_sigma_sq_2_alpha_beta(E_chi2, Var_chi2)


    if no_spin_singularities:
        numpy.less(alpha_chi1, 1.0, out=tmp_idx1)

        for spin_param in [beta_chi1, alpha_chi2, beta_chi2]:
            numpy.less(spin_param, 1.0, out=tmp_idx2)
            numpy.logical_or(tmp_idx1, tmp_idx2, out=tmp_idx1)

        log_prior[tmp_idx1] = -numpy.inf

    else:
        numpy.less_equal(alpha_chi1, 0.0, out=tmp_idx1)

        for spin_param in [beta_chi1, alpha_chi2, beta_chi2]:
            numpy.less_equal(spin_param, 0.0, out=tmp_idx2)
            numpy.logical_or(tmp_idx1, tmp_idx2, out=tmp_idx1)

        log_prior[tmp_idx1] = -numpy.inf


    ## Enforce spin tilt parameters well defined ##
    sigma_cos1 = pop_params_dict["sigma_cos1"]
    sigma_cos2 = pop_params_dict["sigma_cos2"]

    log_prior[(sigma_cos1 <= 0) | (sigma_cos2 <= 0)] = -numpy.inf

    # Return accumulated log(prior).
    return log_prior




def init_from_prior(
        nwalkers,
        constants, duplicates, pop_prior_settings,
        no_spin_singularities=False,
        rand_state=None,
    ):
    """
    Draw samples from the population prior distribution to initialize the
    walkers.
    """
    import numpy
    from ..prob import sample_with_cond
    from ..utils import check_random_state
    from . import prob, utils

    rand_state = check_random_state(rand_state)

    def sample_prior_single(N, prior_settings):
        """
        Samples from a prior with given settings.
        """
        # Determines the type of distribution
        dist = prior_settings["dist"]
        # Determines the parameters of the distribution (e.g., {min,max})
        params = prior_settings["params"]

        # Sample from uniform distribution.
        if dist == "uniform":
            return rand_state.uniform(params["min"], params["max"], N)
        # Type of distribution is unknown.
        else:
            raise NotImplementedError(
                "No implementation for prior '{}'".format(dist)
            )

    def sample_prior(N):
        """
        Iterate over all free parameters and draw ``N`` samples from their
        priors. Then combine into an array, where each column holds the values
        from one parameter.
        """
        return numpy.column_stack(tuple((
            sample_prior_single(N, pop_prior_settings[param])
            for param in prob.param_names
            # Skip over params which are fixed to constants, or are duplicates
            # of other params.
            if pop_prior_settings[param]["dist"]
            not in ["constant", "duplicate"]
        )))

    def cond(samples):
        n_samples, n_params = numpy.shape(samples)

        params = dict(zip(
            prob.param_names,
            utils.get_params(samples, constants, duplicates, prob.param_names),
        ))

        # Initialize condition array to all True (keep all samples).
        # We will downselect later.
        condition = numpy.ones(n_samples, dtype=bool)

        # Enforce mass limits valid.
        condition &= params["m_min"] < params["m_max"]

        # Enforce spin magnitudes are valid Beta distributions.
        alpha_chi1, beta_chi1 = prob.spin_mu_sigma_sq_2_alpha_beta(
            params["E_chi1"], params["Var_chi1"],
        )
        alpha_chi2, beta_chi2 = prob.spin_mu_sigma_sq_2_alpha_beta(
            params["E_chi2"], params["Var_chi2"],
        )

        beta_params = [alpha_chi1, beta_chi1, alpha_chi2, beta_chi2]
        if no_spin_singularities:
            for beta_param in beta_params:
                condition &= beta_param >= 1.0
        else:
            for beta_param in beta_params:
                condition &= beta_param > 0.0

        # Enforce cosine spin tilts are valid Gaussian distributions.
        condition &= params["sigma_cos1"] > 0
        condition &= params["sigma_cos2"] > 0


        return condition

    # Sample from the prior, rejecting invalid samples.
    return sample_with_cond(sample_prior, shape=nwalkers, cond=cond)


def parse_consts_and_prior(
        priors_fname, constants_fname,
        iid_spins=False,
    ):
    import six
    import json
    from .prob import param_names
    from .. import utils

    # Load the population priors from a JSON file.
    with open(priors_fname, "r") as priors_file:
        pop_prior_settings = json.load(priors_file)

        # TODO: validate file structure


    # Load in constants file.
    if constants_fname is None:
        # No constants to be loaded.
        constants = {}
    else:
        # Load constants in from a JSON file.
        with open(constants_fname, "r") as constants_file:
            constants = json.load(constants_file)

        ## Validate file structured properly. ##
        # Check that it's a dict.
        if not isinstance(constants, dict):
            raise TypeError(
                "Constants file must be a dictionary mapping param -> value"
            )
        # Check that the keys are all param names.
        unknown_params = [
            param for param in constants
            if param not in param_names
        ]
        if len(unknown_params) != 0:
            raise ValueError(
                "Unrecognized params in constants file:\n{}"
                .format(utils.format_set_in_quotes(unknown_params))
            )
        # Check that all of the param names map to floats.
        # NOTE: May have to change this if we ever allow for MCMC's over things
        # other than floats (integers?), but currently our underlying sampler
        # only supports floats.
        bad_values = []
        for param, value in six.iteritems(constants):
            if not isinstance(value, float):
                bad_values.append(param)
        if len(bad_values) != 0:
            raise TypeError(
                "The following constants mapped to invalid values:\n{}"
                .format(utils.format_set_in_quotes(bad_values))
            )


    # Parse constants file, and check for any conflicts with priors file.
    conflicting_priors = []
    for param in constants:
        # Check if parameter has already been given a prior. If so, we're going
        # to raise an exception once we've found all such parameters. We could
        # just override those priors, but it's better to be safe.
        if param in pop_prior_settings:
            conflicting_priors.append(param)
        # For bookkeeping purposes, set the parameter's prior distribution to
        # "constant". Map "params" to an empty dict for consistency with other
        # distributions that require params.
        else:
            pop_prior_settings[param] = {
                "dist": "constant",
                "params": {}
            }

    # Raise an exception if any parameters were set to constants, when they were
    # already given priors. List all of the offending params so the user can
    # correct the issue.
    if len(conflicting_priors) != 0:
        raise ValueError(
            "The following parameters were set to constants, but also had "
            "priors set in 'pop_priors' file. Those priors would need to "
            "be ignored, so remove them from the 'pop_priors' file and rerun "
            "if that is desired.\n{}"
            .format(utils.format_set_in_quotes(conflicting_priors))
        )


    # If the spins are assumed to be i.i.d., then we force all of the chi2
    # parameters to assume the values of the associated chi1 parameters.
    if iid_spins:
        duplicates = {
            "E_chi2": "E_chi1",
            "Var_chi2": "Var_chi1",
            "mu_cos2": "mu_cos1",
            "sigma_cos2": "sigma_cos1",
        }
        conflicting_priors = []
        conflicting_consts = []
        for param in duplicates:
            # Check if i.i.d. param has already been given a prior or constant
            # value. If so, we're going to raise an exception once we've found
            # all such parameters, and tabulate them separately so users know
            # whether to check the priors or constants files. We could just
            # override those priors or constants, but it's better to be safe.
            if param in constants:
                conflicting_consts.append(param)
            elif param in pop_prior_settings:
                conflicting_priors.append(param)
            # For bookkeeping purposes, set the parameter's prior distribution
            # to "duplicate". Map "params" to an empty dict for consistency with
            # other distributions that require params.
            else:
                pop_prior_settings[param] = {
                    "dist": "duplicate",
                    "params": {},
                }

        # Raise an exception if any parameters were set to i.i.d., when they
        # were already given priors or constant values. List all of the
        # offending params so the user can correct the issue.
        if len(conflicting_priors) != 0 or len(conflicting_consts) != 0:
            raise ValueError(
                "The following params had priors and/or constants specified "
                "by the user, and would be overridden by --iid-spins. Please "
                "remove them from the priors and/or constants files.\n"
                "Priors:\n"
                "  {}\n"
                "Constants:\n"
                "  {}"
                .format(
                    utils.format_set_in_quotes(conflicting_priors),
                    utils.format_set_in_quotes(conflicting_consts),
                )
            )
    else:
        # No duplicates to be set.
        duplicates = {}


    # Raise an exception if any params do not have priors specified now.
    missing_params = [
        param for param in param_names
        if param not in pop_prior_settings
    ]
    if len(missing_params) != 0:
        raise ValueError(
            "The following parameters have not been given priors:\n{}"
            .format(utils.format_set_in_quotes(missing_params))
        )

    return pop_prior_settings, constants, duplicates



def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "events",
        nargs="*",
        help="List of posterior sample files, one for each event.",
    )
    parser.add_argument(
        "VTs",
        help="HDF5 file containing VTs.",
    )
    parser.add_argument(
        "pop_priors",
        help="JSON file specifying population priors.",
    )
    parser.add_argument(
        "posterior_output",
        help="HDF5 file to store posterior samples in.",
    )

    parser.add_argument(
        "--constants",
        help="JSON file fixing population parameters to constants.",
    )

    parser.add_argument(
        "--iid-spins",
        action="store_true",
        help="Assume that spin magnitudes are independent and identically "
             "distributed (iid). This means that the model parameters for chi1 "
             "and chi2 are forced to be the same. This is accomplished in "
             "practice by using the parameter for chi1 anywhere it is needed "
             "for chi2. If any chi2 parameters were specified by the user, an "
             "exception will be raised.",
    )
    parser.add_argument(
        "--no-spin-singularities",
        action="store_true",
        help="Restrict the spin distribution such that spin distributions with "
             "singularities are not allowed by the prior.",
    )

    parser.add_argument(
        "--scale-factor",
        type=float, default=None,
        help="Multiply all VTs by this constant factor.",
    )

    parser.add_argument(
        "--vt-calibration",
        help="Calibration file for VT's.",
    )

    parser.add_argument(
        "--n-walkers",
        default=None, type=int,
        help="Number of walkers to use, defaults to twice the number of "
             "dimensions.",
    )
    parser.add_argument(
        "--n-samples",
        default=100, type=int,
        help="Number of MCMC samples per walker.",
    )
    parser.add_argument(
        "--n-threads",
        default=1, type=int,
        help="Number of threads to use in MCMC.",
    )

    parser.add_argument(
        "--mass-prior",
        default="uniform",
        choices=["uniform"],
        help="Type of prior used for component masses.",
    )
    parser.add_argument(
        "--spin-prior",
        default="uniform",
        choices=["uniform"],
        help="Type of prior used for spin magnitudes.",
    )
    parser.add_argument(
        "--total-mass-max",
        type=float, default=100.0,
        help="Maximum total mass allowed.",
    )

    parser.add_argument(
        "--mc-err-abs",
        type=float, default=1e-5,
        help="Allowed absolute error for Monte Carlo integrator.",
    )
    parser.add_argument(
        "--mc-err-rel",
        type=float, default=1e-3,
        help="Allowed relative error for Monte Carlo integrator.",
    )

    parser.add_argument(
        "--force",
        action="store_true",
        help="Force HDF5 output, even if file already exists. Will overwrite.",
    )

    parser.add_argument(
        "--seed",
        type=int, default=None,
        help="Random seed.",
    )

    parser.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="Use verbose output.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    import sys
    import six
    import json
    import h5py
    import numpy

    from .. import vt
    from .. import mcmc
    from .. import utils
    from .prob import param_names, ndim_pop

    if raw_args is None:
        raw_args = sys.argv[1:]

    cli_args = _get_args(raw_args)

    rand_state = numpy.random.RandomState(cli_args.seed)

    M_max = cli_args.total_mass_max

    pop_prior_settings, constants, duplicates = parse_consts_and_prior(
        cli_args.pop_priors, cli_args.constants,
        iid_spins=cli_args.iid_spins,
    )

    # Open tabular files for event posteriors.
    data_posterior_samples = []
    for event_fname in cli_args.events:
        data_table = numpy.genfromtxt(event_fname, names=True)
        m1_m2_chi1_chi2_costheta1_costheta2 = numpy.column_stack(
            (
                data_table["m1_source"], data_table["m2_source"],
                data_table["a1"], data_table["a2"],
                data_table["costilt1"], data_table["costilt2"],
            ),
        )
        data_posterior_samples.append(m1_m2_chi1_chi2_costheta1_costheta2)

        del data_table

    n_events = len(cli_args.events)

    # Compute array of priors for each posterior sample, so that we are doing
    # a Monte Carlo integral over the likelihood instead of the posterior, when
    # computing the event-based terms in the full MCMC.
    # If the prior is uniform, then no re-weighting is required, so we just set
    # it to ``None``, and ``pop_models.mcmc.run_mcmc`` takes care of the rest.
    if cli_args.mass_prior == "uniform":
        prior = None
    else:
        raise NotImplementedError

    if cli_args.spin_prior == "uniform":
        pass
    else:
        raise NotImplementedError


    # Load in VT's.
    with h5py.File(cli_args.VTs, "r") as vt_file:
        VT = vt.RegularGridVTInterpolator(
            vt_file,
            scale_factor=cli_args.scale_factor,
        )

    # Load in VT correction factor (if provided), and replace VT
    if cli_args.vt_calibration is not None:
        # Load in the calibration file.
        with open(cli_args.vt_calibration, "r") as calibration_file:
            calibration_info = json.load(calibration_file)
            coeffs = calibration_info["coeffs"]
            basis_fns = vt.correction_bases[VT.mode][calibration_info["basis"]]

        # Replace `VT` with the corrected version.
        VT = vt.CorrectedVT(VT, coeffs, basis_fns)

    mu_poisson = MeanPoissonMonteCarlo(
        VT, M_max,
        err_abs=cli_args.mc_err_abs, err_rel=cli_args.mc_err_rel,
        rand_state=rand_state,
    )


    # Determine number of dimensions for MCMC
    ndim = ndim_pop - len(constants) - len(duplicates)
    # Set number of walkers for MCMC. If already provided use that, otherwise
    # use 2*ndim, which is the minimum allowed by the sampler.
    n_walkers = 2*ndim if cli_args.n_walkers is None else cli_args.n_walkers

    # Initialize walkers by drawing from priors.
    init_state = init_from_prior(
        n_walkers,
        constants, duplicates, pop_prior_settings,
        no_spin_singularities=cli_args.no_spin_singularities,
        rand_state=rand_state,
    )


    # Create file to store posterior samples.
    # Fails if file already exists, to avoid accidentally deleting precious
    # samples.
    # TODO: Come up with a mechanism to resume from last walker positions if
    #   file exists.
    # TODO: Periodically update a counter indicating the number of samples
    #   drawn so far. This way if it's interrupted, we know which samples can
    #   be trusted, and which might be corrupted or just have the contents of
    #   previous memory.
    io_mode = "w" if cli_args.force else "w-"
    with h5py.File(cli_args.posterior_output, io_mode) as posterior_output:
        # Store initial position.
        posterior_output.create_dataset(
            "init_pos", data=init_state,
        )
        # Create empty arrays for storing walker position and log_prob.
        posterior_pos = posterior_output.create_dataset(
            "pos", (cli_args.n_samples, n_walkers, ndim),
        )
        posterior_log_prob = posterior_output.create_dataset(
            "log_prob", (cli_args.n_samples, n_walkers),
        )

        # Store whether or not spins are i.i.d.
        posterior_output.attrs["iid_spins"] = cli_args.iid_spins
        # Store VT mode and scale factor used
        posterior_output.attrs["VT_mode"] = VT.mode
        posterior_output.attrs["VT_scale_factor"] = (
            cli_args.scale_factor if cli_args.scale_factor is not None
            else 1.0
        )
        # Store maximum total mass
        posterior_output.attrs["M_max"] = M_max
        # Store whether spin distribution singularities are allowed
        posterior_output.attrs["no_spin_singularities"] = (
            cli_args.no_spin_singularities
        )
        # Store priors
        posterior_output.attrs["priors"] = json.dumps(pop_prior_settings)
        # Store constants
        constants_group = posterior_output.create_group("constants")
        for param, value in six.iteritems(constants):
            constants_group.attrs[param] = value
        # Store duplicates
        duplicates_group = posterior_output.create_group("duplicates")
        for target, source in six.iteritems(duplicates):
            duplicates_group.attrs[target] = source
        # Store variable names
        variable_names = [
            name for name in param_names
            if (name not in constants) and (name not in duplicates)
        ]
        posterior_output.attrs["variable_names"] = ",".join(variable_names)

        args = []
        kwargs = {
            "M_max": M_max,
            "no_spin_singularities" : cli_args.no_spin_singularities,
            "pop_prior_settings": pop_prior_settings,
        }

        mcmc.run_mcmc(
            intensity, mu_poisson, data_posterior_samples,
            log_prior_pop,
            init_state,
            param_names,
            constants=constants, duplicates=duplicates,
            event_posterior_sample_priors=prior,
            args=args, kwargs=kwargs,
            before_prior_aux_fn=before_prior_aux_fn,
            after_prior_aux_fn=after_prior_aux_fn,
            out_pos=posterior_pos, out_log_prob=posterior_log_prob,
            nsamples=cli_args.n_samples,
            rand_state=rand_state,
            nthreads=cli_args.n_threads, pool=None,
            runtime_sortingfn=None,
            dtype=numpy.float64,
            verbose=cli_args.verbose,
        )


# Functions which pre-compute quantities that are used at multiple steps
# in the MCMC, to reduce run time. These specifically compute the rate
# from the log10(rate), and the normalization factor for the mass
# distribution.
def before_prior_aux_fn(pop_params, **kwargs):
    log10_rate = pop_params.T[0]
    rate = numpy.power(10.0, log10_rate)

    return {
        "rate" : rate,
    }

def after_prior_aux_fn(
        pop_params, prior_support, aux_info, M_max=None,
        **kwargs
    ):
#    from ..powerlaw import prob as pl_prob
    from . import prob

    alpha_m, beta_q, m_min, m_max = pop_params.T[1:5]
    aux_info["pdf_const"] = prob.mass_pdf_const(
        alpha_m, beta_q, m_min, m_max, M_max,
        where=prior_support,
    )

    return aux_info
