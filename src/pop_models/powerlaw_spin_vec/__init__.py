r"""
This module contains everything pertaining to the joint powerlaw mass and vector
spin distribution model. The mass distribution portion is the same model as used
in multiple LVC rate and population statements, such as the O1 BBH paper
[O1BBH]_ and the GW170104 detection paper [GW170104]_. The spin magnitude model
is a beta distribution for each object in the binary. The spin misalignment
model is a truncated Gaussian in :math:`\cos\theta` coordinates, and is taken
from [TalbotThrane]_. It defines the appropriate probability density functions
in :mod:`.prob`, and in :mod:`.mcmc` wraps the base :mod:`pop_models.mcmc` with
an MCMC module that implements this mass and vector spin distribution model.

This model assumes that the primary mass, :math:`m_1`, is drawn from a power law
:math:`p(m_1) \propto m_1^{-\alpha}`, for some power law index :math:`\alpha`,
and bounded between some lower and upper limits, :math:`m_{\mathrm{min}}` and
:math:`m_{\mathrm{max}}` (Equation D1 in [O1BBH]_). Then it assumes that the
secondary mass, :math:`m_2`, is drawn from a uniform distribution between the
lower limit :math:`m_{\mathrm{min}}` and the primary mass :math:`m_1` (Equation
D2 in [O1BBH]_). Then the constraint :math:`m_1 + m_2 \leq M_{\mathrm{max}}` is
imposed, which turns :math:`p(m_1)` into a broken power law.

The joint mass PDF is given by

.. math::
   p(m_1, m_2) =
   C(\alpha, m_{\mathrm{min}}, m_{\mathrm{max}}, M_{\mathrm{max}}) \,
   \frac{m_1^{-\alpha}}{m_1 - m_{\mathrm{min}}}

and the marginal PDF for the primary mass is given by

.. math::
   p(m_1) =
   C(\alpha, m_{\mathrm{min}}, m_{\mathrm{max}}, M_{\mathrm{max}}) \,
   m_1^{-\alpha} \,
   \frac{\min(m_1, M_{\mathrm{max}}-m_1) - m_{\mathrm{min}}}
        {m_1 - m_{\mathrm{min}}}

where :math:`C(\alpha, m_{\mathrm{min}}, m_{\mathrm{max}}, M_{\mathrm{max}})` is
a normalization constant, whose value is derived in [T1700479]_.

Each spin magnitude :math:`\chi_i` (:math:`i = 1, 2`) is assumed to be drawn
from a beta distribution with unknown parameters :math:`\alpha_{\chi_i}` and
:math:`\beta_{\chi_i}`.

.. math::
   p(\chi_i) =
   \frac{\chi_i^{\alpha_{\chi_i}-1} (1-\chi_i)^{\beta_{\chi_i}-1}}
        {\mathrm{B}(\alpha_{\chi_i}, \beta_{\chi_i})}

The two magnitudes are assumed to be independently distributed

.. math::
   p(\chi_1, \chi_2) = p(\chi_1) \, p(\chi_2)

although there is an option to make those two distributions identical (the
i.i.d. spins option), :math:`p(\chi_1) = p(\chi_2)`.

The cosine of each spin tilt :math:`\theta_i` (:math:`i = 1, 2`) is assumed to
be drawn from a truncated Gaussian with unknown standard deviation
:math:`\sigma_{\chi_i}`

.. math::
   p(\cos\theta_i) \propto
   \begin{cases}
     \mathcal{N}(\cos\theta_i; 1, \sigma_{\chi_i}),
     & \mathrm{if} -1 \leq \cos\theta_i \leq +1
     \\
     0, & \mathrm{otherwise}
   \end{cases}

where :math:`\mathcal{N}(x; \mu, \sigma)` represents a Gaussian distribution in
:math:`x` with mean :math:`\mu` and standard deviation :math:`\sigma`. Again,
the two tilts are assumed to be independently distributed

.. math::
   p(\cos\theta_1, \cos\theta_2) = p(\cos\theta_1) \, p(\cos\theta_2)

although there is an option to make those two distributions identical (the
i.i.d. spins option), :math:`p(\cos\theta_1) = p(\cos\theta_2)`.

The full joint distribution of all parameters is then given by

.. math::
   p(m_1, m_2, \chi_1, \chi_2, \cos\theta_1, \cos\theta_2) =
   p(m_1, m_2) \,
   p(\chi_1) \, p(\chi_2) \,
   p(\cos\theta_1) \, p(\cos\theta_2)


.. note::
   Need to add citation for PDF functions. Current best citation is
   https://git.ligo.org/RatesAndPopulations/O2Populations/blob/master/powerlaw/PowerLaw.ipynb,
   which is not properly citable at the moment.

.. [O1BBH]
    Binary Black Hole Mergers in the First Advanced LIGO Observing Run,
    LIGO Scientific Collaboration and Virgo Collaboration,
    2016,
    Phys. Rev. X 6, 041015,
    DOI:
    `10.1103/PhysRevX.6.041015 <https://doi.org/10.1103/PhysRevX.6.041015>`_

.. [GW170104]
    GW170104: Observation of a 50-Solar-Mass Binary Black Hole Coalescence at
    Redshift 0.2,
    LIGO Scientific and Virgo Collaboration,
    2017,
    Phys. Rev. Lett. 118, 221101,
    DOI:
    `10.1103/PhysRevLett.118.221101 <https://doi.org/10.1103/PhysRevLett.118.221101>`_

.. [TalbotThrane]
    Determining the population properties of spinning black holes,
    Colm Talbot and Eric Thrane,
    2017,
    Phys. Rev. D 96, 023012,
    DOI:
    `10.1103/PhysRevD.96.023012 <https://doi.org/10.1103/PhysRevD.96.023012>`


.. [T1700479]
    Normalization constant in power law BBH mass distribution model,
    Daniel Wysocki and Richard O'Shaughnessy,
    `LIGO-T1700479 <https://dcc.ligo.org/LIGO-T1700479>`_
"""
from __future__ import absolute_import

from . import (
    mcmc,
    prob,
)
