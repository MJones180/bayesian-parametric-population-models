from __future__ import division, print_function

labels = {
    "rate": r"\mathcal{R}",
    "log10_rate": r"\log_{10}\mathcal{R}",
    "alpha_m": r"\alpha_m",
    "beta_q": r"\beta_q",
    "m_min": r"m_{\mathrm{min}}",
    "m_max": r"m_{\mathrm{max}}",
    "M_max": r"M_{\mathrm{max}}",
    "E_chi1": r"\mathbb{E}[\chi_1]",
    "E_chi2": r"\mathbb{E}[\chi_2]",
    "Var_chi1": r"\mathrm{Var}[\chi_1]",
    "Var_chi2": r"\mathrm{Var}[\chi_2]",
    "log_alpha_chi1": r"\log\alpha_{\chi_1}",
    "log_beta_chi1": r"\log\beta_{\chi_1}",
    "log_alpha_chi2": r"\log\alpha_{\chi_2}",
    "log_beta_chi2": r"\log\beta_{\chi_2}",
    "log_sigma_cos1": r"\log\sigma_{\cos\theta_1}",
    "log_sigma_cos2": r"\log\sigma_{\cos\theta_2}",
    "alpha_chi1": r"\alpha_{\chi_1}",
    "beta_chi1": r"\beta_{\chi_1}",
    "alpha_chi2": r"\alpha_{\chi_2}",
    "beta_chi2": r"\beta_{\chi_2}",
    "mu_cos1": r"\mu_{\cos\theta_1}",
    "mu_cos2": r"\mu_{\cos\theta_2}",
    "sigma_cos1": r"\sigma_{\cos\theta_1}",
    "sigma_cos2": r"\sigma_{\cos\theta_2}",
}

units = {
    "rate": r"\mathrm{Gpc}^{-3} \, \mathrm{yr}^{-1}",
    "m_min": r"\mathrm{M}_{\odot}",
    "m_max": r"\mathrm{M}_{\odot}",
    "M_max": r"\mathrm{M}_{\odot}",
}
