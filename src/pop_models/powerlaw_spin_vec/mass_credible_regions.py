from __future__ import division, print_function


def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing MCMC samples.",
    )

    parser.add_argument(
        "VTs",
        help="HDF5 file containing grid of VTs.",
    )

    parser.add_argument(
        "output_samples",
        help="File to output credible region samples to.",
    )

    parser.add_argument(
        "output_plot_int",
        help="File to output corner plot of m1*, Rp(m1*), Rp(15Msun), "
             "using the intrinsic m1*.",
    )
    parser.add_argument(
        "output_plot_det",
        help="File to output corner plot of m1*, Rp(m1*), Rp(15Msun), "
             "using the detection-weighted m1*.",
    )

    parser.add_argument(
        "--quantile",
        type=float, default=0.95,
        help="Quantile to use for probable upper mass (default is 0.95).",
    )

    parser.add_argument(
        "--rate-logU-to-jeffereys",
        action="store_true",
        help="Assuming rate is log-uniform, switch to Jefferey's prior.",
    )

    parser.add_argument(
        "--truth-overlay",
        nargs=4, type=float,
        help="For mock data studies where the true model parameters are known, "
             "use this option to overlay the resulting values. Should be in "
             "order: (log10_rate, alpha_m, m_min, m_max).",
    )

    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib plots.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    import sys
    import h5py
    import numpy

    from . import prob
    from . import utils
    from .. import vt
    from ..powerlaw import prob as pl_prob
    from ..utils import gaussian_kde

    if raw_args is None:
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import matplotlib
    matplotlib.use(args.mpl_backend)
    import matplotlib.pyplot as plt
    import corner

    matplotlib.rcParams["text.usetex"] = True
    matplotlib.rcParams["text.latex.preamble"] = [
        "\\usepackage{amsmath}",
        "\\usepackage{amssymb}",
    ]

    percentile = int(100 * args.quantile)

    with h5py.File(args.VTs, "r") as vt_file:
        VT_interpolator = vt.RegularGridVTInterpolator(vt_file)

    # Note: This is resorting to zero spin VT's, despite potentially using
    # aligned spin VT's during inference.  This is producing a bias.  We should
    # instead be starting with the full VT(m1,m2,chi1z,chi2z), and then
    # integrating over the spin distribution Int VT * p(chi1z, chi2z) dchi{1,2},
    # and then using that as VT(m1, m2).
    if VT_interpolator.mode == "zero spin":
        def VT(m1, m2):
            m1 = numpy.tile(m1, len(m2))
            return VT_interpolator(m1, m2)
    elif VT_interpolator.mode == "aligned spin":
        def VT(m1, m2):
            m1 = numpy.tile(m1, len(m2))
            spin = numpy.zeros_like(m2)
            return VT_interpolator(m1, m2, spin, spin)

    with h5py.File(args.posteriors, "r") as post_file:
        M_max = post_file.attrs["M_max"]

        param_names = prob.param_names

        posteriors = post_file["pos"].value
        n_samples = len(posteriors)

        constants = dict(post_file["constants"].attrs)
        duplicates = dict(post_file["duplicates"].attrs)

        posteriors_dict = dict(zip(
            param_names,
            utils.get_params(
                posteriors, constants, duplicates, param_names,
            )
        ))

        if args.rate_logU_to_jeffereys:
            weights = numpy.sqrt(
                numpy.power(10.0, posteriors_dict["log10_rate"])
            )
            if numpy.isscalar(weights):
                weights = numpy.tile(weights, n_samples)
        else:
            weights = None


        m1_upper_int = numpy.empty(n_samples, dtype=numpy.float64)
        m1_upper_det = numpy.empty_like(m1_upper_int)
        R_p_m1_upper_int = numpy.empty_like(m1_upper_int)
        R_p_m1_upper_det = numpy.empty_like(m1_upper_int)
        R_p_15 = numpy.empty_like(m1_upper_int)

        for i, post_sample in enumerate(posteriors):
            (
                log10_rate,
                alpha_m, m_min, m_max,
                E_chi1, Var_chi1, mu_cos1, sigma_cos1,
                E_chi2, Var_chi2, mu_cos2, sigma_cos2,
            ) = utils.get_params(
                post_sample, constants, duplicates, param_names,
            )

            rate = 10**log10_rate

            m1_upper_int[i] = pl_prob.upper_mass_credible_region(
                args.quantile,
                alpha_m, m_min, m_max, M_max,
                n_samples=1000,
            )
            m1_upper_det[i] = (
                pl_prob.upper_mass_credible_region_detection_weighted(
                    args.quantile,
                    alpha_m, m_min, m_max, M_max,
                    VT,
                    # Note: might want to increase n_samples_m1 to get accuracy
                    # to the 0.1Msun level. I'm lowering it right now to make it
                    # fast.
                    dm2_ideal=1.0, n_samples_m1=300,
                )
            )
            R_p_m1_upper_int[i] = rate * prob.marginal_m1_pdf(
                [m1_upper_int[i]], alpha_m, m_min, m_max, M_max,
            )
            R_p_m1_upper_det[i] = rate * prob.marginal_m1_pdf(
                [m1_upper_det[i]], alpha_m, m_min, m_max, M_max,
            )
            R_p_15[i] = rate * prob.marginal_m1_pdf(
                [15.0], alpha_m, m_min, m_max, M_max,
            )


        data = numpy.column_stack((
            m1_upper_int, m1_upper_det,
            R_p_m1_upper_int, R_p_m1_upper_det,
            R_p_15,
        ))
        numpy.savetxt(
            args.output_samples, data,
            delimiter="\t",
            header="\t".join([
                "M_upper_int", "M_upper_det",
                "rate_p_M_upper_int", "rate_p_M_upper_det",
                "rate_p_15",
            ]),
        )

        del data


        if args.truth_overlay is not None:
            (
                log10_rate_true, alpha_true, m_min_true, m_max_true,
            ) = args.truth_overlay
            rate_true = 10**log10_rate_true

            m1_upper_int_true = pl_prob.upper_mass_credible_region(
                args.quantile,
                alpha_true, m_min_true, m_max_true, M_max,
                n_samples=1000,
            )
            m1_upper_det_true = (
                pl_prob.upper_mass_credible_region_detection_weighted(
                    args.quantile,
                    alpha_true, m_min_true, m_max_true, M_max,
                    VT,
                )
            )
            R_m1_upper_int_true = rate_true * prob.marginal_m1_pdf(
                [m1_upper_int_true], alpha_true, m_min_true, m_max_true, M_max,
            )
            R_m1_upper_det_true = rate_true * prob.marginal_m1_pdf(
                [m1_upper_det_true], alpha_true, m_min_true, m_max_true, M_max,
            )
            R_p_15_true = rate * prob.marginal_m1_pdf(
                [15.0], alpha_true, m_min_true, m_max_true, M_max,
            )
            truths_int = [m1_upper_int_true, R_m1_upper_int_true, R_p_15_true]
            truths_det = [m1_upper_det_true, R_m1_upper_det_true, R_p_15_true]
        else:
            truths_int = truths_det = None

        labels = [
            r"$m_1^*/\mathrm{M}_\odot$",
            r"$\mathcal{R} p(m_1^*/\mathrm{M}_\odot) / "
              r"(\mathrm{Gpc}^{-3} \mathrm{yr}^{-1})$",
            r"$\mathcal{R} p(15) / "
              r"(\mathrm{Gpc}^{-3} \mathrm{yr}^{-1})$",
        ]

        fig = corner.corner(
            numpy.column_stack((m1_upper_int, R_p_m1_upper_int, R_p_15)),
            weights=weights, truths=truths_int,
            labels=labels,
            levels=[0.50, 0.90],
            hist_kwargs={
                "linewidth": 2.5, "density": True,
            },
            contour_kwargs={
                "linestyles": "solid", "linewidths": [2, 1],
            },
            no_fill_contours=True, plot_datapoints=True, plot_density=False,
            data_kwargs={
                "color": "#ff7f0e",
            }
        ).savefig(args.output_plot_int)

        fig = corner.corner(
            numpy.column_stack((m1_upper_det, R_p_m1_upper_det, R_p_15)),
            weights=weights, truths=truths_det,
            labels=labels,
            levels=[0.50, 0.90],
            hist_kwargs={
                "linewidth": 2.5, "density": True,
            },
            contour_kwargs={
                "linestyles": "solid", "linewidths": [2, 1],
            },
            no_fill_contours=True, plot_datapoints=True, plot_density=False,
            data_kwargs={
                "color": "#ff7f0e",
            }
        ).savefig(args.output_plot_det)
