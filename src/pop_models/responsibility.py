import typing
import numpy as np
from pop_models.detection import Detection, DetectionLikelihood
from pop_models.population import Population, MixturePopulation
from pop_models.posterior import PosteriorSamples
from pop_models.types import Parameters

DetectionLikelihoodFactory = typing.Callable[
    [Population, Detection],
    DetectionLikelihood,
]
def responsibility(
        parameters: Parameters,
        population: MixturePopulation,
        detection: Detection,
        detection_likelihood_factory: DetectionLikelihoodFactory,
    ) -> typing.Mapping[str, np.ndarray]:
    # Initialize output
    result = {} # typing.Mapping[str, np.ndarray]

    # Initialize normalization array, which will accumulate the total for each
    # parameter.
    param_shape = next(iter(parameters.values())).shape
    norm = np.zeros(param_shape, dtype=float)

    # Compute detection likelihood for each subpopulation.
    for suffix in population.suffixes:
        # Extract the subpopulation for this suffix
        subpop = population.get_subpops([suffix])
        # Create the detection likelihood integrator and evaluate
        detection_likelihood = detection_likelihood_factory(subpop, detection)
        det_like = detection_likelihood(parameters)
        # Store the un-normalized result
        result[suffix] = det_like
        # Accumulate the contribution to the normalization
        norm += det_like

    # Re-normalize the result
    for k, v in result.items():
        v /= norm

    # return result
    return result
