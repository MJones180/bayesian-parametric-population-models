__all__ = [
    "Numeric",
    "Observables",
    "Parameters",
    "vectorize_over_parameters_to_numeric",
]

import numbers
import typing

import numpy

from .xpy_tools import get_xpy
xpy = get_xpy()

Numeric = typing.Union[
    int, float,
    numpy.number, numpy.ndarray,
    xpy.number, xpy.ndarray,
]

WhereType = typing.Union[bool, numpy.ndarray]
Observables = typing.Tuple[Numeric, ...]
Parameters = typing.Mapping[str, Numeric]

NumericSingle = typing.Union[
    int, float,
]
ObservablesSingle = typing.Tuple[NumericSingle, ...]
ParametersSingle = typing.Mapping[str, NumericSingle]

to_numeric = typing.Callable[..., Numeric]
decorator_type = typing.Callable[[to_numeric], to_numeric]


@typing.overload
def vectorize_over_parameters_to_numeric(
        parameters_place: int,
        has_where: bool=False,
    ) -> decorator_type:
    ...

@typing.overload
def vectorize_over_parameters_to_numeric(
        parameters_place: str,
        has_where: bool=False,
    ) -> decorator_type:
    ...


def vectorize_over_parameters_to_numeric(
        parameters_place: typing.Union[int, str],
        has_where: bool=False,
    ) -> decorator_type:
    def decorator(func: to_numeric) -> to_numeric:
        if has_where:
            import inspect
            func_signature = inspect.signature(func)
            where_default = func_signature.parameters["where"].default

        if isinstance(parameters_place, int):
            def wrapped(*args, **kwargs) -> Numeric:
                assert isinstance(parameters_place, int)

                args_before = args[:parameters_place]
                parameters = args[parameters_place]
                args_after = args[parameters_place+1:]

                any_param = next(iter(parameters.values()))
                shape = numpy.shape(any_param)
                output = numpy.empty_like(any_param)

                for idx in numpy.ndindex(*shape):
                    parameters_reduced = {
                        name : value[idx]
                        for name, value in parameters.items()
                    }
                    args_recombined = (
                        args_before + (parameters_reduced,) + args_after
                    )

                    if has_where:
                        kwargs_reduced = kwargs.copy()

                        if "where" in kwargs:
                            kwargs_reduced["where"] = kwargs["where"][idx]
                        else:
                            kwargs_reduced["where"] = where_default
                    else:
                        kwargs_reduced = kwargs

                    output[idx] = func(*args_recombined, **kwargs_reduced)

                return output

        elif isinstance(parameters_place, str):
            def wrapped(*args, **kwargs) -> Numeric:
                assert isinstance(parameters_place, str)

                parameters = kwargs.pop(parameters_place)

                any_param = next(iter(parameters.values()))
                shape = numpy.shape(any_param)
                output = numpy.empty_like(any_param)

                for idx in numpy.ndindex(*shape):
                    parameters_reduced = {
                        name : value[idx]
                        for name, value in parameters.items()
                    }
                    extra_kwarg = {parameters_place: parameters_reduced}

                    if has_where:
                        kwargs_reduced = kwargs.copy()

                        if "where" in kwargs:
                            kwargs_reduced["where"] = kwargs["where"][idx]
                        else:
                            kwargs_reduced["where"] = where_default
                    else:
                        kwargs_reduced = kwargs

                    output[idx] = func(*args, **extra_kwarg, **kwargs_reduced)

                return output

        else:
            raise TypeError(
                "parameters_place must be int or str, got type {}"
                .format(type(parameters_place))
            )

        return wrapped
    return decorator
