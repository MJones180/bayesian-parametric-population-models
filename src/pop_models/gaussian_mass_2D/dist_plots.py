from __future__ import division, print_function

def get_percentiles(data, weights=None, axis=0):
    import numpy
    from .. import utils

    percentiles = numpy.array([2.50, 16.0, 50.0, 84.0, 97.5])
    quantiles = percentiles / 100.0

    data_percentiles = utils.quantile(
        data, quantiles, weights=weights,
        axis=axis,
    )

    return {
        "median": data_percentiles[2],
        "1 sigma": (data_percentiles[1], data_percentiles[3]),
        "2 sigma": (data_percentiles[0], data_percentiles[4]),
    }

def plot_percentiles(ax, X, percentiles, color="black", alpha=0.25, label=None):
    for lo, hi in [percentiles["1 sigma"], percentiles["2 sigma"]]:
        ax.fill_between(
            X[1:-1], lo[1:-1], hi[1:-1],
            color=color, alpha=alpha,
        )
    ax.plot(
        X[1:-1], percentiles["median"][1:-1],
        color=color,
        label=label,
    )


def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "posteriors",
        help="HDF5 file containing MCMC samples.",
    )
    parser.add_argument(
        "output_plot_dir",
        help="Directory to output plots into.",
    )

    '''
    parser.add_argument(
        "plot_m1_pm1",
        help="File to store plot of mass probability distribution.",
    )
    parser.add_argument(
        "plot_m1_R_pm1",
        help="File to store plot of mass rate distribution.",
    )
    parser.add_argument(
        "plot_pchi",
        help="File to store plot of spin magnitude probability distributions.",
    )
    parser.add_argument(
        "plot_R_pchi",
        help="File to store plot of spin magnitude rate distributions.",
    )
    parser.add_argument(
        "plot_pcostheta",
        help="File to store plot of spin tilt probability distributions.",
    )
    parser.add_argument(
        "plot_R_pcostheta",
        help="File to store plot of spin tilt rate distributions.",
    )
    parser.add_argument(
        "plot_Pchi_eff",
        help="File to store plot of effective spin cumulative probability "
             "distributions.",
    )
    parser.add_argument(
        "plot_R_Pchi_eff",
        help="File to store plot of effective spin cumulative rate "
             "distributions.",
    )
    parser.add_argument(
        "plot_ppd_m",
        help="File to store plot of component mass PPDs.",
    )
    parser.add_argument(
        "plot_ppd_M",
        help="File to store plot of total mass PPD.",
    )
    parser.add_argument(
        "plot_ppd_q",
        help="File to store plot of mass ratio PPD.",
    )
    parser.add_argument(
        "plot_ppd_chi",
        help="File to store plot of spin magnitude PPDs.",
    )
    parser.add_argument(
        "plot_ppd_costheta",
        help="File to store plot of spin tilt PPDs.",
    )
    parser.add_argument(
        "plot_ppd_chi_eff",
        help="File to store plot of effective spin PPD.",
    )
    '''

    parser.add_argument(
        "--n-samples",
        default=1000, type=int,
        help="Number of samples to use in plots.",
    )

    parser.add_argument(
        "--n-kde-samples",
        default=4, type=int,
        help="Number of samples to draw from each population to compute KDEs.",
    )

    parser.add_argument(
        "--rate-logU-to-jeffereys",
        action="store_true",
        help="Assuming rate is log-uniform, switch to Jefferey's prior.",
    )

    parser.add_argument(
        "--log-scale-n-oom",
        type=int, default=6,
        help="Number of orders-of-magnitude below peak to show in log-scale "
             "plots.",
    )

    parser.add_argument(
        "--seed",
        type=int,
        help="Seed for random number generator.",
    )

    parser.add_argument(
        "--mpl-backend",
        default="Agg",
        help="Backend to use for matplotlib plots.",
    )

    parser.add_argument(
        "--plot-format",
        default="png",
        help="File format to save plots as.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    import os.path
    import sys
    import h5py
    import numpy
    import scipy.stats as stats

    from . import prob
    from . import utils
    from ..utils import empirical_distribution_function, gaussian_kde

    if raw_args is None:
        raw_args = sys.argv[1:]

    args = _get_args(raw_args)

    import matplotlib
    matplotlib.use(args.mpl_backend)
    import matplotlib.pyplot as plt

    import seaborn as sns

    matplotlib.rc("text", usetex=True)

    sns.set_context("talk", font_scale=2, rc={"lines.linewidth": 2.5})
    sns.set_style("ticks")
    sns.set_palette("colorblind")

    rand_state = numpy.random.RandomState(args.seed)

    with h5py.File(args.posteriors, "r") as post_file:
        posteriors = post_file["pos"]

        iid_spins = post_file.attrs["iid_spins"]

        param_names = prob.param_names
        constants = dict(post_file["constants"].attrs)
        duplicates = dict(post_file["duplicates"].attrs)

        params = utils.get_params(
            posteriors, constants, duplicates, param_names,
        )
        params = dict(zip(param_names, params))


        log10_rates = params["log_rate"]
        alphas = params["alpha"]
        m_mins = params["m_min"]
        m_maxs = params["m_max"]
        log10_alpha_chi1s = params["log_alpha_chi1"]
        log10_beta_chi1s = params["log_beta_chi1"]
        log10_alpha_chi2s = params["log_alpha_chi2"]
        log10_beta_chi2s = params["log_beta_chi2"]
        log10_sigma_chi1s = params["log_sigma_chi1"]
        log10_sigma_chi2s = params["log_sigma_chi2"]


        m_min = numpy.min(m_mins)
        m_max = numpy.max(m_maxs)
        M_max = post_file.attrs["M_max"]

        rates = numpy.power(10.0, log10_rates)
        alpha_chi1s = numpy.power(10.0, log10_alpha_chi1s)
        beta_chi1s = numpy.power(10.0, log10_beta_chi1s)
        alpha_chi2s = numpy.power(10.0, log10_alpha_chi2s)
        beta_chi2s = numpy.power(10.0, log10_beta_chi2s)
        sigma_chi1s = numpy.power(10.0, log10_sigma_chi1s)
        sigma_chi2s = numpy.power(10.0, log10_sigma_chi2s)

        (
            rates,
            alphas,
            m_mins, m_maxs,
            alpha_chi1s, beta_chi1s,
            alpha_chi2s, beta_chi2s,
            sigma_chi1s, sigma_chi2s,
        ) = utils.upcast_scalars((
            rates,
            alphas,
            m_mins, m_maxs,
            alpha_chi1s, beta_chi1s,
            alpha_chi2s, beta_chi2s,
            sigma_chi1s, sigma_chi2s,
        ))

        if args.rate_logU_to_jeffereys:
            weights = numpy.sqrt(rates)
        else:
            weights = None

        n_posterior = numpy.size(rates)

        m = numpy.linspace(m_min, m_max, args.n_samples)
        M = numpy.linspace(m_min, M_max, args.n_samples)
        q = numpy.linspace(0.0, 1.0, args.n_samples)
        chi = q
        chi_eff = numpy.linspace(-1.0, +1.0, args.n_samples)
        costheta = chi_eff

        m_pm = numpy.empty((n_posterior, args.n_samples), dtype=numpy.float64)
        m_R_pm = numpy.empty_like(m_pm)

        pchi1 = numpy.empty_like(m_pm)
        R_pchi1 = numpy.empty_like(m_pm)
        pchi2 = numpy.empty_like(m_pm)
        R_pchi2 = numpy.empty_like(m_pm)

        pcostheta1 = numpy.empty_like(m_pm)
        R_pcostheta1 = numpy.empty_like(m_pm)
        pcostheta2 = numpy.empty_like(m_pm)
        R_pcostheta2 = numpy.empty_like(m_pm)

        # Pchi_eff = numpy.empty_like(m_pm)
        # R_Pchi_eff = numpy.empty_like(m_pm)

        m1_samples = numpy.empty(
            (n_posterior*args.n_kde_samples,),
            dtype=numpy.float64,
        )
        m2_samples = numpy.empty_like(m1_samples)
        chi1_samples = numpy.empty_like(m1_samples)
        chi2_samples = numpy.empty_like(m1_samples)
        costheta1_samples = numpy.empty_like(m1_samples)
        costheta2_samples = numpy.empty_like(m1_samples)

        if args.rate_logU_to_jeffereys:
            weight_samples = numpy.empty_like(m1_samples)
        else:
            weight_samples = None

        iterables = zip(
            rates,
            alphas,
            m_mins, m_maxs,
            alpha_chi1s, beta_chi1s,
            alpha_chi2s, beta_chi2s,
            sigma_chi1s, sigma_chi2s,
        )

        for (
                i,
                (
                    rate,
                    alpha, m_min, m_max,
                    alpha_chi1, beta_chi1, alpha_chi2, beta_chi2,
                    sigma_chi1, sigma_chi2,
                )
            ) in enumerate(iterables):
            pm = prob.marginal_m1_pdf(m, alpha, m_min, m_max, M_max)

            m_pm[i] = m * pm
            m_R_pm[i] = m * rate * pm

            del pm

            pchi1[i] = prob.marginal_spin_mag_pdf(chi, alpha_chi1, beta_chi1)
            R_pchi1[i] = rate * pchi1[i]

            if not iid_spins:
                pchi2[i] = prob.marginal_spin_mag_pdf(chi, alpha_chi2, beta_chi2)
                R_pchi2[i] = rate * pchi2[i]

            pcostheta1[i] = prob.marginal_spin_tilt_pdf(costheta, sigma_chi1)
            R_pcostheta1[i] = rate * pcostheta1[i]

            if not iid_spins:
                pcostheta2[i] = prob.marginal_spin_tilt_pdf(costheta, sigma_chi2)
                R_pcostheta2[i] = rate * pcostheta2[i]

            i_samples = slice(i*args.n_kde_samples, (i+1)*args.n_kde_samples)

            (
                m1_samples[i_samples], m2_samples[i_samples],
                chi1_samples[i_samples], chi2_samples[i_samples],
                costheta1_samples[i_samples], costheta2_samples[i_samples],
            ) = prob.joint_rvs(
                args.n_kde_samples,
                alpha, m_min, m_max, M_max,
                alpha_chi1, beta_chi1, alpha_chi2, beta_chi2,
                sigma_chi1, sigma_chi2,
                rand_state=rand_state,
            ).T

            if args.rate_logU_to_jeffereys:
                weight_samples[i_samples] = numpy.sqrt(rate)

            '''
            (
                m1_samp, m2_samp,
                chi1_samp, chi2_samp, costheta1_samp, costheta2_samp,
            ) = prob.joint_rvs(
                args.n_kde_samples,
                alpha, m_min, m_max, M_max,
                alpha_chi1, beta_chi1, alpha_chi2, beta_chi2,
                sigma_chi1, sigma_chi2,
                rand_state=rand_state,
            ).T

            chi_eff_samp = (
                (
                    m1_samp*chi1_samp*costheta1_samp +
                    m2_samp*chi2_samp*costheta2_samp
                ) / (m1_samp + m2_samp)
            )

            Pchi_eff[i] = empirical_distribution_function(chi_eff, chi_eff_samp)
            R_Pchi_eff[i] = rate * Pchi_eff[i]

            # pchi_eff[i] = stats.gaussian_kde(
            #     chi_eff_samp, bw_method="scott",
            # )(chi_eff)
            # R_pchi_eff[i] = rate * pchi_eff[i]

            del chi_eff_samp
            del m1_samp, m2_samp, chi1_samp, chi2_samp
            '''

        m_pm_percentile = get_percentiles(m_pm, weights=weights)
        m_R_pm_percentile = get_percentiles(m_R_pm, weights=weights)

        pchi1_percentile = get_percentiles(pchi1, weights=weights)
        R_pchi1_percentile = get_percentiles(R_pchi1, weights=weights)
        if not iid_spins:
            pchi2_percentile = get_percentiles(pchi2, weights=weights)
            R_pchi2_percentile = get_percentiles(R_pchi2, weights=weights)

        pcostheta1_percentile = get_percentiles(pcostheta1, weights=weights)
        R_pcostheta1_percentile = get_percentiles(
            R_pcostheta1, weights=weights,
        )
        if not iid_spins:
            pcostheta2_percentile = get_percentiles(pcostheta2, weights=weights)
            R_pcostheta2_percentile = get_percentiles(
                R_pcostheta2, weights=weights,
            )


        '''
        Pchi_eff_percentile = get_percentiles(Pchi_eff, weights=weights)
        R_Pchi_eff_percentile = get_percentiles(R_Pchi_eff, weights=weights)

        Pchi_eff_marginal = numpy.average(Pchi_eff, weights=weights, axis=0)
        R_Pchi_eff_marginal = numpy.average(R_Pchi_eff, weights=weights, axis=0)
        '''

        # Posterior predictive distributions
        M_tot_samples = m1_samples + m2_samples
        q_samples = m2_samples / m1_samples
        chi_eff_samples = (
            (
                m1_samples * chi1_samples * costheta1_samples +
                m2_samples * chi2_samples * costheta2_samples
            ) / M_tot_samples
        )




        fig_m_pm, ax_m_pm = plt.subplots()
        fig_R_pm, ax_R_m_pm = plt.subplots()
        fig_pchi, ax_pchi = plt.subplots()
        fig_R_pchi, ax_R_pchi = plt.subplots()
        fig_pcostheta, ax_pcostheta = plt.subplots()
        fig_R_pcostheta, ax_R_pcostheta = plt.subplots()
        '''
        fig_Pchi_eff, ax_Pchi_eff = plt.subplots()
        fig_R_Pchi_eff, ax_R_Pchi_eff = plt.subplots()
        '''
        fig_ppd_m, ax_ppd_m = plt.subplots()
        fig_ppd_M, ax_ppd_M = plt.subplots()
        fig_ppd_q, ax_ppd_q = plt.subplots()
        fig_ppd_chi, ax_ppd_chi = plt.subplots()
        fig_ppd_costheta, ax_ppd_costheta = plt.subplots()
        fig_ppd_chi_eff, ax_ppd_chi_eff = plt.subplots()

        figs = [
            fig_m_pm,
            fig_R_pm,
            fig_pchi,
            fig_R_pchi,
            fig_pcostheta,
            fig_R_pcostheta,
#            fig_Pchi_eff,
#            fig_R_Pchi_eff,
            fig_ppd_m,
            fig_ppd_M,
            fig_ppd_q,
            fig_ppd_chi,
            fig_ppd_costheta,
            fig_ppd_chi_eff,
        ]

        axes_ppd = [
            ax_ppd_m,
            ax_ppd_M,
            ax_ppd_q,
            ax_ppd_chi,
            ax_ppd_costheta,
            ax_ppd_chi_eff,
        ]

        # Logarithmic x-axes
        ax_logx = [
            ax_m_pm, ax_R_m_pm,
            ax_ppd_m, ax_ppd_M,
        ]
        for ax in ax_logx:
            ax.set_xscale("log")

        # Logarithmic y-axes
        ax_logy = [
            ax_m_pm, ax_R_m_pm,
            ax_pchi, ax_R_pchi,
            ax_pcostheta, ax_R_pcostheta,
            ax_ppd_m, ax_ppd_M,
        ]
        for ax in ax_logy:
            ax.set_yscale("log")

        # Label axes
        ax_ppd_m.set_xlabel(r"$m$ [M$_\odot$]")
        ax_ppd_M.set_xlabel(r"$M$ [M$_\odot$]")
        ax_ppd_q.set_xlabel(r"$q$")

        for ax in [ax_m_pm, ax_R_m_pm]:
            ax.set_xlabel(r"$m_1$ [M$_\odot$]")

        for ax in [ax_pchi, ax_R_pchi, ax_ppd_chi]:
            ax.set_xlabel(r"$\chi$")

        for ax in [ax_pcostheta, ax_R_pcostheta, ax_ppd_costheta]:
            ax.set_xlabel(r"$\cos\theta$")

        for ax in [ax_ppd_chi_eff]:
            ax.set_xlabel(r"$\chi_{\mathrm{eff}}$")

        '''
        for ax in [ax_Pchi_eff, ax_R_Pchi_eff]:
            ax.set_xlabel(r"$\chi_{\mathrm{eff}}$")
        '''

        ax_m_pm.set_ylabel(
            r"$m_1 \, p(m_1)$"
        )
        ax_R_m_pm.set_ylabel(
            r"$m_1 \, \mathcal{R} \, p(m_1)$ [Gpc$^{-3}$ yr$^{-1}$]"
        )

        ax_pchi.set_ylabel(
            r"$p(\chi)$"
        )
        ax_R_pchi.set_ylabel(
            r"$\mathcal{R} \, p(\chi)$"
        )

        ax_pcostheta.set_ylabel(
            r"$p(\cos\theta)$"
        )
        ax_R_pcostheta.set_ylabel(
            r"$\mathcal{R} \, p(\cos\theta)$"
        )

        '''
        ax_Pchi_eff.set_ylabel(
            r"$p(< \chi_{\mathrm{eff}})$"
        )
        ax_R_Pchi_eff.set_ylabel(
            r"$\mathcal{R} \, p(< \chi_{\mathrm{eff}})$"
        )
        '''

        for ax in axes_ppd:
            ax.set_ylabel(r"$\mathrm{PPD}$")

        plot_percentiles(ax_m_pm, m, m_pm_percentile)
        ax_m_pm.set_ylim(utils.limited_oom_range(m_pm, args.log_scale_n_oom))

        plot_percentiles(ax_R_m_pm, m, m_R_pm_percentile)
        ax_R_m_pm.set_ylim(
            utils.limited_oom_range(m_R_pm, args.log_scale_n_oom)
        )

        plot_percentiles(ax_pchi, chi, pchi1_percentile, color="#1f77b4")
        if iid_spins:
            ax_pchi.set_ylim(
                utils.limited_oom_range(pchi1, args.log_scale_n_oom)
            )
        else:
            plot_percentiles(ax_pchi, chi, pchi2_percentile, color="#2ca02c")
            ax_pchi.set_ylim(
                utils.limited_oom_range([pchi1, pchi2], args.log_scale_n_oom)
            )

        plot_percentiles(ax_R_pchi, chi, R_pchi1_percentile, color="#1f77b4")
        if iid_spins:
            ax_R_pchi.set_ylim(
                utils.limited_oom_range(R_pchi1, args.log_scale_n_oom)
            )
        else:
            plot_percentiles(ax_R_pchi, chi, R_pchi2_percentile, color="#2ca02c")
            ax_R_pchi.set_ylim(
                utils.limited_oom_range([R_pchi1, R_pchi2], args.log_scale_n_oom)
            )

        plot_percentiles(
            ax_pcostheta, costheta, pcostheta1_percentile,
            color="#1f77b4",
        )
        if not iid_spins:
            plot_percentiles(
                ax_pcostheta, costheta, pcostheta2_percentile,
                color="#2ca02c",
            )

        plot_percentiles(
            ax_R_pcostheta, costheta, R_pcostheta1_percentile,
            color="#1f77b4",
        )
        if not iid_spins:
            plot_percentiles(
                ax_R_pcostheta, costheta, R_pcostheta2_percentile,
                color="#2ca02c",
            )


        '''
        plot_percentiles(ax_Pchi_eff, chi_eff, Pchi_eff_percentile)
        plot_percentiles(ax_R_Pchi_eff, chi_eff, R_Pchi_eff_percentile)

        ax_Pchi_eff.plot(
            chi_eff, Pchi_eff_marginal,
            color="#1f77b4", linestyle="--",
        )
        ax_R_Pchi_eff.plot(
            chi_eff, R_Pchi_eff_marginal,
            color="#1f77b4", linestyle="--",
        )
        '''

        # Plot PPDs
        pm1 = gaussian_kde(
            m1_samples, weights=weight_samples, bw_method="scott",
        )(m)
        pm2 = gaussian_kde(
            m2_samples, weights=weight_samples, bw_method="scott",
        )(m)

        ax_ppd_m.plot(m, pm1, color="#1f77b4")
        ax_ppd_m.plot(m, pm2, color="#2ca02c")

        ax_ppd_m.set_ylim(
            utils.limited_oom_range([pm1, pm2], args.log_scale_n_oom)
        )

        del pm1, pm2

        pM = gaussian_kde(
            M_tot_samples, weights=weight_samples, bw_method="scott",
        )(M)

        ax_ppd_M.plot(M, pM, color="black")

        ax_ppd_M.set_ylim(
            utils.limited_oom_range([pM], args.log_scale_n_oom)
        )

        del pM

        pq = gaussian_kde(
            q_samples, weights=weight_samples, bw_method="scott",
        )(q)
        ax_ppd_q.plot(q, pq, color="black")
        del pq

        pchi1 = gaussian_kde(
            chi1_samples, weights=weight_samples, bw_method="scott",
        )(chi)
        ax_ppd_chi.plot(chi, pchi1, color="#1f77b4")
        del pchi1

        if not iid_spins:
            pchi2 = gaussian_kde(
                chi2_samples, weights=weight_samples, bw_method="scott",
            )(chi)
            ax_ppd_chi.plot(chi, pchi2, color="#2ca02c")
            del pchi2

        pcostheta1 = gaussian_kde(
            costheta1_samples, weights=weight_samples, bw_method="scott",
        )(costheta)
        ax_ppd_costheta.plot(costheta, pcostheta1, color="#1f77b4")
        del pcostheta1

        if not iid_spins:
            pcostheta2 = gaussian_kde(
                costheta2_samples, weights=weight_samples, bw_method="scott",
            )(costheta)
            ax_ppd_costheta.plot(costheta, pcostheta2, color="#2ca02c")
            del pcostheta2

        pchi_eff = gaussian_kde(
            chi_eff_samples, weights=weight_samples, bw_method="scott",
        )(chi_eff)
        ax_ppd_chi_eff.plot(chi_eff, pchi_eff, color="black")
        del pchi_eff


        for ax in [ax_m_pm, ax_R_m_pm]:
            ax.set_xlim([m_min, M_max])

        for ax in [ax_pchi, ax_R_pchi]:
            ax.set_xlim([0.0, 1.0])

        for ax in [ax_pcostheta, ax_R_pcostheta]: #, ax_Pchi_eff, ax_R_Pchi_eff]:
            ax.set_xlim([-1.0, +1.0])

        for fig in figs:
            fig.tight_layout()

        def plot_fname(basename):
            return os.path.join(
                args.output_plot_dir,
                ".".join([basename, args.plot_format])
            )

        fig_m_pm.savefig(plot_fname("dist_m1_pm1"))
        fig_R_pm.savefig(plot_fname("dist_m1_R_pm1"))

        fig_pchi.savefig(plot_fname("dist_pchi"))
        fig_R_pchi.savefig(plot_fname("dist_R_pchi"))

        fig_pcostheta.savefig(plot_fname("dist_pcostheta"))
        fig_R_pcostheta.savefig(plot_fname("dist_R_pcostheta"))

        '''
        fig_Pchi_eff.savefig(args.plot_Pchi_eff)
        fig_R_Pchi_eff.savefig(args.plot_R_Pchi_eff)
        '''

        fig_ppd_m.savefig(plot_fname("ppd_m"))
        fig_ppd_M.savefig(plot_fname("ppd_M"))
        fig_ppd_q.savefig(plot_fname("ppd_q"))
        fig_ppd_chi.savefig(plot_fname("ppd_chi"))
        fig_ppd_costheta.savefig(plot_fname("ppd_costheta"))
        fig_ppd_chi_eff.savefig(plot_fname("ppd_chi_eff"))


if __name__ == "__main__":
    import sys
    raw_args = sys.argv[1:]
    sys.exit(_main(raw_args))
