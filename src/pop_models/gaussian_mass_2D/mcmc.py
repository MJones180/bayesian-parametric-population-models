from __future__ import division, print_function

import numpy

LN_TEN = numpy.log(10.0)


def intensity(
        indiv_params, pop_params,
        aux_info,
        M_max=None,
        **kwargs
    ):
    from . import prob

    rate = aux_info["rate"]

    return rate * prob.joint_pdf(indiv_params, pop_params)


def expval_mc(
        pop_params, aux_info,
        raw_interpolator=None,
        err_abs=1e-5, err_rel=1e-3,
        return_err=False,
        rand_state=None,
        **kwargs
    ):
    import numpy

    from .. import mc
    from . import prob

    rate = aux_info["rate"]

    def p(N):
        return prob.joint_rvs(N, pop_params, rand_state=rand_state)

    def efficiency_fn(m1_m2):
        import numpy

        m1, m2 = m1_m2.T

        return numpy.vectorize(raw_interpolator)(m1, m2)


    I, err_abs, err_rel = mc.integrate_adaptive(
        p, efficiency_fn,
        err_abs=err_abs, err_rel=err_rel,
    )

    if return_err:
        return rate*I, err_abs, err_rel
    else:
        return rate*I


def log_prior_pop(
        pop_params, aux_info,
        m_max=None, n_sigma_buffer=None,
        pop_prior_settings=None,
        **kwargs
    ):
    from . import prob
    import numpy

    log10_rate = pop_params[0]
    Mu, Sigma = Mu_Sigma_from_params(pop_params)

    mu1, mu2 = Mu
    sigma1, sigma2 = numpy.sqrt(numpy.diag(Sigma))

    # Initialize variable to accumulate the value of log(prior)
    # NOTE: Currently only support uniform priors, so this never gets modified.
    #       However, future additional priors may modify it, so we keep it in
    #       preparation.
    log_prior = 0.0

    # log10(rate) prior
    prior_settings = pop_prior_settings["log10_rate"]
    prior_dist = prior_settings["dist"]
    prior_params = prior_settings["params"]

    if prior_dist in ["constant", "duplicate"]:
        pass
    elif prior_dist == "uniform":
        min_val = prior_params["min"]
        max_val = prior_params["max"]
        if not (min_val <= log10_rate <= max_val):
            return -numpy.inf
    else:
        raise NotImplementedError(
            "No implementation for prior '{}' on 'log10_rate'."
            .format(prior_dist)
        )

    # (Mu, Sigma) prior
    prior_settings = pop_prior_settings["Mu_Sigma"]
    prior_dist = prior_settings["dist"]
    prior_params = prior_settings["params"]

    if mu2 > mu1:
        return -numpy.inf

    if prior_dist != "uniform":
        raise NotImplementedError(
            "No implementation for prior '{}' on 'Mu_Sigma'."
            .format(prior_dist)
        )

    n_sigmas = prior_params["n_sigmas"]
    m_min = prior_params["min"]
    m_max = prior_params["max"]

    in_range  = m_min <= mu1-n_sigmas*sigma1
    in_range &= m_max >= mu1+n_sigmas*sigma1
    in_range &= m_min <= mu2-n_sigmas*sigma2
    in_range &= m_max >= mu2+n_sigmas*sigma2

    if not in_range:
        return -numpy.inf

    # Enforce positive stdev
    if (sigma1 <= 0) or (sigma2 <= 0):
        return -numpy.inf

    # Return accumulated log(prior).
    return log_prior


def init_from_prior(
        nwalkers, nevents,
        constants, duplicates, pop_prior_settings,
        rand_state=None,
    ):
    """
    Draw samples from the population prior distribution to initialize the
    walkers.
    """
    import numpy
    from .. import utils
    from .prob import param_names

    rand_state = utils.check_random_state(rand_state)

    columns = []

    # log10(rate) prior
    prior_settings = pop_prior_settings["log10_rate"]
    prior_dist = prior_settings["dist"]

    if prior_dist == "constant":
        pass
    elif prior_dist == "uniform":
        prior_params = prior_settings["params"]
        columns.append(
            rand_state.uniform(
                prior_params["min"],
                prior_params["max"],
                nwalkers,
            )
        )
    else:
        raise NotImplementedError(
            "No implementation for prior '{}' on log10_rate.".format(dist)
        )

    # Mu_Sigma prior
    prior_settings = pop_prior_settings["Mu_Sigma"]
    prior_dist = prior_settings["dist"]

    if prior_dist == "constant":
        pass
    elif prior_dist == "uniform":
        pass
    else:
        raise NotImplementedError(
            "No implementation for prior '{}' on Mu_Sigma.".format(dist)
        )

    def sample_prior(prior_settings):
        """
        Samples from a prior with given settings.
        """
        # Determines the type of distribution
        dist = prior_settings["dist"]
        # Determines the parameters of the distribution (e.g., {min,max})
        params = prior_settings["params"]

        # Sample from uniform distribution.
        if dist == "uniform":
            return rand_state.uniform(params["min"], params["max"], nwalkers)
        # Type of distribution is unknown.
        else:

            raise NotImplementedError(
                "No implementation for prior '{}'".format(dist)
            )

    # Iterate over all free parameters and draw ``nwalkers`` samples from their
    # priors. Then combine into an array, where each column holds the values
    # from one parameter.
    return numpy.column_stack(tuple((
        sample_prior(pop_prior_settings[param])
        for param in param_names
        # Skip over params which are fixed to constants, or are duplicates of
        # other params.
        if pop_prior_settings[param]["dist"] not in ["constant", "duplicate"]
    )))


def _get_args(raw_args):
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "events",
        nargs="+",
        help="List of posterior sample files, one for each event.",
    )
    parser.add_argument(
        "VTs",
        help="HDF5 file containing VTs.",
    )
    parser.add_argument(
        "pop_priors",
        help="JSON file specifying population priors.",
    )
    parser.add_argument(
        "posterior_output",
        help="HDF5 file to store posterior samples in.",
    )

    parser.add_argument(
        "--constants",
        help="JSON file fixing population parameters to constants.",
    )

    parser.add_argument(
        "--iid-spins",
        action="store_true",
        help="Assume that spin magnitudes are independent and identically "
             "distributed (iid). This means that the model parameters for chi1 "
             "and chi2 are forced to be the same. This is accomplished in "
             "practice by using the parameter for chi1 anywhere it is needed "
             "for chi2. If any chi2 parameters were specified by the user, an "
             "exception will be raised.",
    )

    parser.add_argument(
        "--n-walkers",
        default=None, type=int,
        help="Number of walkers to use, defaults to twice the number of "
             "dimensions.",
    )
    parser.add_argument(
        "--n-samples",
        default=100, type=int,
        help="Number of MCMC samples per walker.",
    )
    parser.add_argument(
        "--n-threads",
        default=1, type=int,
        help="Number of threads to use in MCMC.",
    )

    parser.add_argument(
        "--mass-prior",
        default="uniform",
        choices=["uniform"],
        help="Type of prior used for component masses.",
    )
    parser.add_argument(
        "--spin-prior",
        default="uniform",
        choices=["uniform"],
        help="Type of prior used for spin magnitudes.",
    )
    parser.add_argument(
        "--total-mass-max",
        type=float, default=100.0,
        help="Maximum total mass allowed.",
    )

    parser.add_argument(
        "--mc-err-abs",
        type=float, default=1e-5,
        help="Allowed absolute error for Monte Carlo integrator.",
    )
    parser.add_argument(
        "--mc-err-rel",
        type=float, default=1e-3,
        help="Allowed relative error for Monte Carlo integrator.",
    )

    parser.add_argument(
        "--seed",
        type=int, default=None,
        help="Random seed.",
    )

    parser.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="Use verbose output.",
    )

    return parser.parse_args(raw_args)


def _main(raw_args=None):
    import sys
    import six
    import json
    import h5py
    import numpy

    from .. import vt
    from .. import mcmc
    from .. import utils
    from .prob import param_names, ndim_pop

    if raw_args is None:
        raw_args = sys.argv[1:]

    cli_args = _get_args(raw_args)

    rand_state = numpy.random.RandomState(cli_args.seed)

    M_max = cli_args.total_mass_max

    # Load the population priors from a JSON file.
    with open(cli_args.pop_priors, "r") as priors_file:
        pop_prior_settings = json.load(priors_file)

        # TODO: validate file structure


    # Load in constants file.
    if cli_args.constants is None:
        # No constants to be loaded.
        constants = {}
    else:
        # Load constants in from a JSON file.
        with open(cli_args.constants, "r") as constants_file:
            constants = json.load(constants_file)

        ## Validate file structured properly. ##
        # Check that it's a dict.
        if not isinstance(constants, dict):
            raise TypeError(
                "Constants file must be a dictionary mapping param -> value"
            )
        # Check that the keys are all param names.
        unknown_params = [
            param for param in constants
            if param not in param_names
        ]
        if len(unknown_params) != 0:
            raise ValueError(
                "Unrecognized params in constants file:\n{}"
                .format(utils.format_set_in_quotes(unknown_params))
            )
        # Check that all of the param names map to floats.
        # NOTE: May have to change this if we ever allow for MCMC's over things
        # other than floats (integers?), but currently our underlying sampler
        # only supports floats.
        bad_values = []
        for param, value in six.iteritems(constants):
            if not isinstance(value, float):
                bad_values.append(param)
        if len(bad_values) != 0:
            raise TypeError(
                "The following constants mapped to invalid values:\n{}"
                .format(utils.format_set_in_quotes(bad_values))
            )


    # Parse constants file, and check for any conflicts with priors file.
    conflicting_priors = []
    for param in constants:
        # Check if parameter has already been given a prior. If so, we're going
        # to raise an exception once we've found all such parameters. We could
        # just override those priors, but it's better to be safe.
        if param in pop_prior_settings:
            conflicting_priors.append(param)
        # For bookkeeping purposes, set the parameter's prior distribution to
        # "constant". Map "params" to an empty dict for consistency with other
        # distributions that require params.
        else:
            pop_prior_settings[param] = {
                "dist": "constant",
                "params": {}
            }

    # Raise an exception if any parameters were set to constants, when they were
    # already given priors. List all of the offending params so the user can
    # correct the issue.
    if len(conflicting_priors) != 0:
        raise ValueError(
            "The following parameters were set to constants, but also had "
            "priors set in 'pop_priors' file. Those priors would need to "
            "be ignored, so remove them from the 'pop_priors' file and rerun "
            "if that is desired.\n{}"
            .format(utils.format_set_in_quotes(conflicting_priors))
        )


    # If the spins are assumed to be i.i.d., then we force all of the chi2
    # parameters to assume the values of the associated chi1 parameters.
    if cli_args.iid_spins:
        duplicates = {
            "log_alpha_chi2": "log_alpha_chi1",
            "log_beta_chi2": "log_beta_chi1",
            "log_sigma_chi2": "log_sigma_chi1",
        }
        conflicting_priors = []
        conflicting_consts = []
        for param in duplicates:
            # Check if i.i.d. param has already been given a prior or constant
            # value. If so, we're going to raise an exception once we've found
            # all such parameters, and tabulate them separately so users know
            # whether to check the priors or constants files. We could just
            # override those priors or constants, but it's better to be safe.
            if param in constants:
                conflicting_consts.append(param)
            elif param in pop_prior_settings:
                conflicting_priors.append(param)
            # For bookkeeping purposes, set the parameter's prior distribution
            # to "duplicate". Map "params" to an empty dict for consistency with
            # other distributions that require params.
            else:
                pop_prior_settings[param] = {
                    "dist": "duplicate",
                    "params": {},
                }

        # Raise an exception if any parameters were set to i.i.d., when they
        # were already given priors or constant values. List all of the
        # offending params so the user can correct the issue.
        if len(conflicting_priors) != 0 or len(conflicting_consts) != 0:
            raise ValueError(
                "The following params had priors and/or constants specified "
                "by the user, and would be overridden by --iid-spins. Please "
                "remove them from the priors and/or constants files.\n"
                "Priors:\n"
                "  {}\n"
                "Constants:\n"
                "  {}"
                .format(
                    utils.format_set_in_quotes(conflicting_priors),
                    utils.format_set_in_quotes(conflicting_consts),
                )
            )
    else:
        # No duplicates to be set.
        duplicates = {}


    # Raise an exception if any params do not have priors specified now.
    missing_params = [
        param for param in param_names
        if param not in pop_prior_settings
    ]
    if len(missing_params) != 0:
        raise ValueError(
            "The following parameters have not been given priors:\n{}"
            .format(utils.format_set_in_quotes(missing_params))
        )


    # Open tabular files for event posteriors.
    data_posterior_samples = []
    for event_fname in cli_args.events:
        data_table = numpy.genfromtxt(event_fname, names=True)
        m1_m2_chi1_chi2_costheta1_costheta2 = numpy.column_stack(
            (
                data_table["m1_source"], data_table["m2_source"],
                data_table["a1"], data_table["a2"],
                data_table["costilt1"], data_table["costilt2"],
            ),
        )
        data_posterior_samples.append(m1_m2_chi1_chi2_costheta1_costheta2)

        del data_table

    n_events = len(cli_args.events)

    # Compute array of priors for each posterior sample, so that we are doing
    # a Monte Carlo integral over the likelihood instead of the posterior, when
    # computing the event-based terms in the full MCMC.
    # If the prior is uniform, then no re-weighting is required, so we just set
    # it to ``None``, and ``pop_models.mcmc.run_mcmc`` takes care of the rest.
    if cli_args.mass_prior == "uniform":
        prior = None
    else:
        raise NotImplementedError

    if cli_args.spin_prior == "uniform":
        pass
    else:
        raise NotImplementedError


    # Load in <VT>'s.
    with h5py.File(cli_args.VTs, "r") as VTs:
        raw_interpolator = vt.interpolate_hdf5(VTs)

    # Determine number of dimensions for MCMC
    ndim = ndim_pop - len(constants) - len(duplicates)
    # Set number of walkers for MCMC. If already provided use that, otherwise
    # use 2*ndim, which is the minimum allowed by the sampler.
    n_walkers = 2*ndim if cli_args.n_walkers is None else cli_args.n_walkers

    # Initialize walkers by drawing from priors.
    init_state = init_from_prior(
        n_walkers, n_events,
        constants, duplicates, pop_prior_settings,
        rand_state=rand_state,
    )


    # Create file to store posterior samples.
    # Fails if file already exists, to avoid accidentally deleting precious
    # samples.
    # TODO: Come up with a mechanism to resume from last walker positions if
    #   file exists.
    # TODO: Periodically update a counter indicating the number of samples
    #   drawn so far. This way if it's interrupted, we know which samples can
    #   be trusted, and which might be corrupted or just have the contents of
    #   previous memory.
    with h5py.File(cli_args.posterior_output, "w-") as posterior_output:
        # Store initial position.
        posterior_output.create_dataset(
            "init_pos", data=init_state,
        )
        # Create empty arrays for storing walker position and log_prob.
        posterior_pos = posterior_output.create_dataset(
            "pos", (cli_args.n_samples, n_walkers, ndim),
        )
        posterior_log_prob = posterior_output.create_dataset(
            "log_prob", (cli_args.n_samples, n_walkers),
        )

        # Store whether or not spins are i.i.d.
        posterior_output.attrs["iid_spins"] = cli_args.iid_spins
        # Store maximum total mass
        posterior_output.attrs["M_max"] = M_max
        # Store priors
        posterior_output.attrs["priors"] = json.dumps(pop_prior_settings)
        # Store constants
        constants_group = posterior_output.create_group("constants")
        for param, value in six.iteritems(constants):
            constants_group.attrs[param] = value
        # Store duplicates
        duplicates_group = posterior_output.create_group("duplicates")
        for target, source in six.iteritems(duplicates):
            duplicates_group.attrs[target] = source

        args = []
        kwargs = {
            "raw_interpolator": raw_interpolator,
            "M_max": M_max,
            "pop_prior_settings": pop_prior_settings,
            "err_abs": cli_args.mc_err_abs,
            "err_rel": cli_args.mc_err_rel,
            "rand_state": rand_state,
        }

        mcmc.run_mcmc(
            intensity, expval_mc, data_posterior_samples,
            log_prior_pop,
            init_state,
            param_names,
            constants=constants, duplicates=duplicates,
            event_posterior_sample_priors=prior,
            args=args, kwargs=kwargs,
            before_prior_aux_fn=before_prior_aux_fn,
            after_prior_aux_fn=after_prior_aux_fn,
            out_pos=posterior_pos, out_log_prob=posterior_log_prob,
            nsamples=cli_args.n_samples,
            rand_state=rand_state,
            nthreads=cli_args.n_threads, pool=None,
            runtime_sortingfn=None,
            dtype=numpy.float64,
            verbose=cli_args.verbose,
        )


# Functions which pre-compute quantities that are used at multiple steps
# in the MCMC, to reduce run time. These specifically compute the rate
# from the log10(rate), and the normalization factor for the mass
# distribution.
def before_prior_aux_fn(pop_params, **kwargs):
    (
        log_rate,
        alpha, m_min, m_max,
        log_alpha_chi1, log_beta_chi1,
        log_alpha_chi2, log_beta_chi2,
        log_sigma_chi1, log_sigma_chi2,
    ) = pop_params
    rate = 10 ** log_rate
    aux_info = {"rate": rate}

    return aux_info


def after_prior_aux_fn(
        pop_params, aux_info,
        M_max=None,
        **kwargs
    ):
    from ..powerlaw import prob as pl_prob
    import scipy.stats

    (
        log_rate,
        alpha, m_min, m_max,
        log_alpha_chi1, log_beta_chi1,
        log_alpha_chi2, log_beta_chi2,
        log_sigma_chi1, log_sigma_chi2,
    ) = pop_params

    alpha_chi1 = 10**log_alpha_chi1
    beta_chi1 = 10**log_beta_chi1
    alpha_chi2 = 10**log_alpha_chi2
    beta_chi2 = 10**log_beta_chi2
    sigma_chi1 = 10**log_sigma_chi1
    sigma_chi2 = 10**log_sigma_chi2

    aux_info["pdf_const"] = pl_prob.pdf_const(alpha, m_min, m_max, M_max)

    aux_info["alpha_chi1"] = alpha_chi1
    aux_info["beta_chi1"] = beta_chi1
    aux_info["alpha_chi2"] = alpha_chi2
    aux_info["beta_chi2"] = beta_chi2
    aux_info["sigma_chi1"] = sigma_chi1
    aux_info["sigma_chi2"] = sigma_chi2

    aux_info["Beta_rv_chi1"] = scipy.stats.beta(alpha_chi1, beta_chi1)
    aux_info["Beta_rv_chi2"] = scipy.stats.beta(alpha_chi2, beta_chi2)

    return aux_info
