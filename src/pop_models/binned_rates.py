"""
Computes rates within specified bins, rather than overall.
"""
import typing
from pop_models.types import Parameters

import numpy as np

from pop_models.bins import Bin
from pop_models.coordinate import (
    Coordinate, CoordinateSystem, CoordinateTransforms, transformations_nil,
)
from pop_models.population import Population
from pop_models.posterior import H5CleanedPosteriorSamples


def compute_rates(
        population: Population,
        bin_lookup: typing.Dict[str,Bin],
        parameters: Parameters,
        abs_err: float=1e-2, rel_err: float=1e-6, max_iter: int=100,
        transformations: CoordinateTransforms=transformations_nil,
    ):
    # Coordinate transforms for population to each bin.
    transforms_lookup = {
        label : transformations[(population.coord_system, b.coord_system)]
        for label, b in bin_lookup.items()
    }

    n_drawn = 0
    total_counts = {label : 0.0 for label in bin_lookup.keys()}

    rate_total = population.normalization(parameters)

    for i in range(max_iter):
        n_to_draw = 1024
        observables = population.rvs(n_to_draw, parameters)
        n_drawn += n_to_draw
        converged = {}

        for label, b in bin_lookup.items():
            transform = transforms_lookup[label]

            count = np.count_nonzero(b.contains(transform(observables)))
            total_counts[label] += count

            F = rate_total * total_counts[label]

            I_current = F / n_drawn
            abs_err_current = (F - F*F / n_drawn) / (n_drawn*(n_drawn-1.0))
            rel_err_current = abs_err_current / I_current

            converged[label] = (
                (abs_err_current <= abs_err) and
                (rel_err_current <= rel_err)
            )

        if all(converged.values()):
            break

    return {
        label : rate_total * count / n_drawn
        for label, count in total_counts.items()
    }
