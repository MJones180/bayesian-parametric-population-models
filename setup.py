"""Bayesian parametric population models (pop_models)

Long description goes here...
"""

from datetime import date

#-------------------------------------------------------------------------------
#   GENERAL
#-------------------------------------------------------------------------------
__name__ = "pop_models"
__version__ = "2.0.0a2"
__date__ = date(2021, 4, 14)
__keywords__ = [
    "astronomy",
    "information analysis",
    "machine learning",
    "physics",
]
__status__ = "Alpha"

#-------------------------------------------------------------------------------
#   URLS
#-------------------------------------------------------------------------------
__url__ = "https://git.ligo.org/daniel.wysocki/bayesian-parametric-population-models"
#__download_url__= "https://github.com/oshaughn/InferenceTools-LIGO-ScienceMode/releases/tag/{version}".format(version=__version__)
__bugtrack_url__ = "https://git.ligo.org/daniel.wysocki/bayesian-parametric-population-models/issues"

#-------------------------------------------------------------------------------
#   PEOPLE
#-------------------------------------------------------------------------------
__author__ = "Daniel Wysocki and Richard O'Shaughnessy"
__author_email__ = "daniel.wysocki@ligo.org"

__maintainer__ = "Daniel Wysocki"
__maintainer_email__ = "daniel.wysocki@ligo.org"

__credits__ = (
    "Daniel Wysocki",
    "Richard O'Shaughnessy",
)

#-------------------------------------------------------------------------------
#   LEGAL
#-------------------------------------------------------------------------------
__copyright__ = 'Copyright (c) 2017-2021 {author} <{email}>'.format(
    author=__author__, email=__author_email__)

__license__ = 'MIT License'
__license_full__ = '''
MIT License

{copyright}

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''.format(copyright=__copyright__).strip()

#-------------------------------------------------------------------------------
#   PACKAGE
#-------------------------------------------------------------------------------
DOCLINES = __doc__.split("\n")

CLASSIFIERS = """
Development Status :: 3 - Alpha
Programming Language :: Python :: 3
Operating System :: OS Independent
Intended Audience :: Science/Research
Topic :: Scientific/Engineering :: Astronomy
Topic :: Scientific/Engineering :: Physics
Topic :: Scientific/Engineering :: Information Analysis
""".strip()

REQUIREMENTS = {
    "install": [
        "astropy>=3.0.0,<5.0.0",
        "corner>=2.0.0,<3.0.0",
        "emcee>=2.2.0,<4.0.0",
        "h5py>=2.7.0,<4.0.0",
        "mpmath>=1.0.0,<1.1.0",
        "numpy>=1.13.0,<2.0.0",
        "matplotlib>=2.0.0,<4.0.0",
        "matplotlib-label-lines>=0.3.0,<0.4.0",
        "pyparsing>=2.4.0,<3.0.0",
        "scipy>=1.0.0,<2.0.0",
        "six>=1.10.0,<2.0.0",
        "typing_extensions>=3.6.2,<4.0.0",
    ],
    "setup": [
        "pytest-runner",
    ],
    "tests": [
        "pytest",
    ]
}

ENTRYPOINTS = {
    "console_scripts": [
        "pop_models_gw_ifo_vt = pop_models.astro_models.gw_ifo_vt:_main",
        "pop_models_gw_ifo_vt_combiner = pop_models.astro_models.gw_ifo_vt:VTCombiner.cli",
        "pop_models_gw_ifo_vt_plotter = pop_models.astro_models.gw_ifo_vt:VTPlotter.cli",
        "pop_models_h5_raw_samples = pop_models.posterior:H5RawPosteriorSamples.cli",
        "pop_models_h5_cleaned_samples = pop_models.posterior:H5CleanedPosteriorSamples.cli",
        "pop_models_overlay = pop_models.utils.plotting.corner_overlay:CornerOverlay.cli",
        "pop_models_cr_1d = pop_models.credible_regions:CredibleRegions1D.cli",
        "pop_models_eos_stats = pop_models.astro_models.eos:EOSStats.cli",
        "pop_models_extract_samples = pop_models.sample_extraction.cli:main",
        "pop_models_cbc_post_sample_utils = pop_models.astro_models.detections.cbc.pre_sampled:CommandLineTools.main",
        "pop_models_savage_dickey = pop_models.utils.savage_dickey:main",
        "pop_models_bbh_O2_model_AB = pop_models.applications.bbh.O2_model_AB.main:main",
        "pop_models_bbh_mixture_pl_gauss = pop_models.applications.bbh.mixture_pl_gauss.main:main",
        "pop_models_bbh_simple_mass_spin_correlation = pop_models.applications.bbh.simple_mass_spin_correlation.main:main",
        "pop_models_bbh_broken_powerlaw_simple = pop_models.applications.bbh.broken_powerlaw_simple.main:main",
        "pop_models_bns_iid_gaussian_mass_with_eos = pop_models.applications.bns.iid_gaussian_mass_with_eos.main:main",
        "pop_models_check_progress = pop_models.utils.check_progress:main",
        "pop_models_chain_hacking_gui = pop_models.utils.chain_hacking_gui:main",
        "pop_models_bin_plot = pop_models.bins:main",
        "pop_models_legacy = pop_models.legacy.main:main",
        "driver = pop_models.applications.driver.main:main",
    ]
}

from setuptools import find_packages, setup

metadata = dict(
    name=__name__,
    version=__version__,
    description=DOCLINES[0],
    long_description='\n'.join(DOCLINES[2:]),
    keywords=__keywords__,
    author=__author__,
    author_email=__author_email__,
    maintainer=__maintainer__,
    maintainer_email=__maintainer_email__,
    url=__url__,
    #    download_url=__download_url__,
    license=__license__,
    classifiers=[f for f in CLASSIFIERS.split('\n') if f],
    package_dir={"": "src"},
    packages=find_packages("src"),
    install_requires=REQUIREMENTS["install"],
    setup_requires=REQUIREMENTS["setup"],
    tests_require=REQUIREMENTS["tests"],
    entry_points=ENTRYPOINTS,
)

setup(**metadata)
