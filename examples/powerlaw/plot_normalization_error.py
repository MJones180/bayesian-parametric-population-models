r"""
Normalization error
===================

Tests that the power law PDF is normalized to unity by integrating the marginal
PDF :math:`p(m_1\mid\alpha,m_{\mathrm{min}},m_{\mathrm{max}},M_{\mathrm{max}})`
for a number of choices of
:math:`\alpha,m_{\mathrm{min}},m_{\mathrm{max}},M_{\mathrm{max}})`. The integral
is taken over :math:`m_1` using the trapezoid method (:func:`numpy.trapz`). Due
to the poor behavior of numerical integration of power laws, this actually
incurs a small amount of error itself, so any errors may be attributable to the
numerical integration used (Note: we should rewrite this test with a better
integration method, perhaps working in log-space and using something better than
the trapezoid method).

The plots show the relative error

.. math::
   \text{Rel err} =
   \frac{\text{(Computed norm)} - \text{(Exact norm)}}{\text{(Exact norm)}}

where the Exact norm should of course be exactly :math:`1`, so this simplifies
to

.. math::
   \text{Rel err} = \text{(Computed norm)} - 1

We perform this particular test using two choices of :math:`M_{\mathrm{max}}`,
one of which is low enough to give us a broken power law for :math:`p(m_1)`, and
the other is high enough that :math:`p(m_1)` should remain a perfect power law
throughout its support. We have color coded the points based on this, and made
them semi-transparent, so the behavior of both can be easily seen in the other
variables. It should be clear that most of the points are a blend of the two
colors, meaning the broken and perfect power laws behave approximately the same.
"""

print("SKIPPING")

'''
from __future__ import division, print_function

from itertools import product

import numpy
import matplotlib
import matplotlib.pyplot as plt

from pop_models.powerlaw import prob


n_samples = 500000

print("Numerical integration being performed with", n_samples, "samples.")


def check_norm(alpha, m_min, m_max, M_max, n_samples=n_samples):
    ms, dm = numpy.linspace(m_min, m_max, n_samples, retstep=True)

    return numpy.trapz(
        prob.marginal_pdf(ms, alpha, m_min, m_max, M_max)[0],
        dx=dm,
    )


alphas = [
    -4.0, -3.5, -3.0, -2.0, -1.0, -0.5,
    0.0,
    +0.5, +1.0, +2.0, +2.35, +3.0, +3.5, +4.0,
]

m_mins = [
    5.0,
]

m_maxs = [
    30.0, 50.0, 90.0, 95.0,
]

M_maxs = [
    100.0, 200.0,
]

worst_params = None
worst_err = 0.0

fig, (ax_a, ax_mmin, ax_mmax, ax_Mmax) = plt.subplots(
    1, 4,
    sharey=True,
    figsize=[12,4],
)

ax_a.set_ylabel(r"Rel err")
ax_a.set_xlabel(r"$\alpha$")
ax_mmin.set_xlabel(r"$m_{\mathrm{min}}$")
ax_mmax.set_xlabel(r"$m_{\mathrm{max}}$")
ax_Mmax.set_xlabel(r"$M_{\mathrm{max}}$")

print("The following lines have columns:")
print("alpha, m_min, m_max, M_max, computed norm")

for a, mmin, mmax, Mmax in product(alphas, m_mins, m_maxs, M_maxs):
    norm = check_norm(a, mmin, mmax, Mmax)
    print(
        a, mmin, mmax, Mmax,
        norm,
    )

    rel_err = norm - 1.0
    abs_err = abs(rel_err)

    if Mmax == 100.0:
        color = "#1f77b4"
    elif Mmax == 200.0:
        color = "#ff7f0e"
    else:
        color = "black"

    ax_a.scatter([a], [rel_err], color=color, alpha=0.2)
    ax_mmin.scatter([mmin], [rel_err], color=color, alpha=0.2)
    ax_mmax.scatter([mmax], [rel_err], color=color, alpha=0.2)
    ax_Mmax.scatter([Mmax], [rel_err], color=color, alpha=0.2)

    if abs_err > worst_err:
        worst_err = abs_err
        worst_params = a, mmin, mmax, Mmax

ax_a.set_ylim([-worst_err, +worst_err])

print(
    "Worst case: err = {err}, params = {params}"
    .format(err=worst_err, params=worst_params)
)

plt.show()
'''
