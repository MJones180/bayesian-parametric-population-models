r"""
Mass distribution: Marginal versus integral of joint density
============================================================

This demonstrates the form of the marginal pdf
:math:`p(m_1\mid\alpha,m_{\mathrm{min}},m_{\mathrm{max}},M_{\mathrm{max}})` for
various choices of :math:`\alpha`, :math:`m_{\mathrm{min}}`,
:math:`m_{\mathrm{max}}`, and :math:`M_{\mathrm{max}}`. It displays the absolute
value of the relative error between the marginal PDF and the integral of the
joint PDF

.. math::
   \frac{|p(m_1) - \int p(m_1,m_2) \mathrm{d}m_2|}{p(m_1)}

where the marginal PDF is provided by
:func:`pop_models.powerlaw.prob.marginal_pdf` and the joint PDF is provided by
:func:`pop_models.powerlaw.prob.joint_pdf`. The integral is taken over
:math:`m_2` using the trapezoid method (:func:`numpy.trapz`).
"""

print("SKIPPING")

'''
from __future__ import division, print_function

import six

import itertools

import numpy
import matplotlib
import matplotlib.pyplot as plt
import scipy.stats

from pop_models.powerlaw import prob


n_smooth = 1000

labels = {
    "alpha": r"$\alpha$",
    "m_min": r"$m_{\mathrm{min}}$ [$M_\odot$]",
    "m_max": r"$m_{\mathrm{max}}$ [$M_\odot$]",
    "M_max": r"$M_{\mathrm{max}}$ [$M_\odot$]",
}

def is_iterable(obj):
    """
    Returns boolean indicating whether object is iterable.
    """
    try:
        iter(obj)
    except TypeError:
        return False
    else:
        return True


def get_iterable(*objs):
    """
    Returns the first object in its arguments which is an iterable.
    """
    for obj in objs:
        if is_iterable(obj):
            return obj

    raise Exception("No iterable found.")



def discrete_colors(data, buf=0.5):
    colors = [
        (0.26700400000000002, 0.0048739999999999999, 0.32941500000000001),
        (0.12756799999999999, 0.5669490000000000400, 0.55055600000000005),
        (0.99324800000000002, 0.9061569999999999900, 0.14393600000000001),
    ]

    nbins = len(data)
    cmap_name = ""
    cmap = matplotlib.colors.LinearSegmentedColormap.from_list(
        cmap_name, colors, N=nbins,
    )
    nmap = matplotlib.colors.Normalize(
        vmin=min(data)-buf, vmax=max(data)+buf,
    )
    smap = matplotlib.cm.ScalarMappable(
        cmap=cmap, norm=nmap,
    )
    smap._A = [] # needs an array, even though it's not used

    return smap


def plot(
        ax,
        variable_name,
        alphas, m_mins, m_maxs, M_maxs,
        n_smooth=n_smooth,
    ):
    name2val = {
        "alpha": alphas,
        "m_min": m_mins,
        "m_max": m_maxs,
        "M_max": M_maxs,
    }

    var = name2val[variable_name]

    cmap = discrete_colors(var)
    colors = [cmap.to_rgba(x) for x in var]

    iterables = itertools.product(
        *(name2val[name] for name in ["alpha", "m_min", "m_max", "M_max"])
    )

    lowest = numpy.inf

    for color, (alpha, m_min, m_max, M_max) in zip(colors, iterables):
        log10_m_min, log10_m_max = numpy.log10([m_min, m_max])
        m_1_plot = numpy.logspace(log10_m_min, log10_m_max, n_smooth)

        pdf_const = prob.pdf_const(alpha, m_min, m_max, M_max)

        marginal_analytic = prob.marginal_pdf(
            m_1_plot,
            alpha, m_min, m_max, M_max,
            const=pdf_const,
        )[0]

        marginal_numerical = numpy.empty_like(marginal_analytic)
        for i, m_1 in enumerate(m_1_plot):
            m_2_grid, dm_2 = numpy.linspace(m_min, m_1, n_smooth, retstep=True)

            joint_analytic = prob.joint_pdf(
                m_1, m_2_grid,
                alpha, m_min, m_max, M_max,
                const=pdf_const,
            )

            marginal_numerical[i] = numpy.trapz(joint_analytic, dx=dm_2)

        delta = marginal_numerical - marginal_analytic

        # Plot the deltas
        ax.plot(
            m_1_plot, abs(delta) / marginal_analytic,
            color=color, linestyle="-",
        )

    ax.set_xscale("log")
    ax.set_yscale("log")

    cbar = ax.figure.colorbar(cmap, ax=ax)
    cbar.set_label(labels[variable_name])
    cbar.set_ticks(var)


fig, axes = plt.subplots(4, sharex=True, figsize=[8, 16])
ax_alpha, ax_mmin, ax_mmax, ax_Mmax = axes


plot(
    ax_alpha,
    "alpha",
    [-3.0, -2.0, -1.0, 0.0, 1.000001, 2.0, 3.0], [5.0], [95.0], [100.0],
)

plot(
    ax_mmin,
    "m_min",
    [2.35], [1.0, 2.0, 3.0, 4.0, 5.0], [95.0], [100.0],
)

plot(
    ax_mmax,
    "m_max",
    [2.35], [5.0], [30.0, 40.0, 50.0, 70.0, 80.0, 90.0, 95.0], [100.0],
)

plot(
    ax_Mmax,
    "M_max",
    [2.35], [5.0], [95.0], [100.0, 150.0, 200.0],
)

for ax in axes:
    ax.set_ylabel(r"$\frac{|p(m_1) - \int p(m_1,m_2) \mathrm{d}m_2|}{p(m_1)}$")

ax_Mmax.set_xlabel(r"$m_1$ [$M_\odot$]")

plt.show()
'''
