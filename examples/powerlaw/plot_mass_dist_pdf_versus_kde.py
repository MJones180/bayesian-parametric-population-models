r"""
Mass distribution: PDF versus KDE of samples
============================================

This demonstrates the form of the marginal pdf
:math:`p(m_1\mid\alpha,m_{\mathrm{min}},m_{\mathrm{max}},M_{\mathrm{max}})` for
various choices of :math:`\alpha`, :math:`m_{\mathrm{min}}`,
:math:`m_{\mathrm{max}}`, and :math:`M_{\mathrm{max}}`. It displays both the
functional form provided by :func:`pop_models.powerlaw.prob.marginal_pdf` and a
kernel density estimate of samples drawn from
:func:`pop_models.powerlaw.prob.marginal_rvs`.

Note that the KDE's are not always reliable at the extreme ends of the
distributions, due to edge effects (the KDE's assume the distribution is
continuous on :math:`(-\infty, +\infty)`) and low sample sizes where the
PDF is low. We try to alleviate some of the edge effects in two ways. One is by
fitting the KDE in log-space, where the functions are more well behaved. Another
is by reflecting the samples about the upper and lower mass cutoffs, and adding
the KDE's of the reflected samples, which at the very least guarantees the
normalization is correct.
"""

print("SKIPPING")

'''
from __future__ import division, print_function

import six

import itertools

import numpy
import matplotlib
import matplotlib.pyplot as plt
import scipy.stats

from pop_models.powerlaw import prob


n_smooth = 100
n_samples = 50000


labels = {
    "alpha": r"$\alpha$",
    "m_min": r"$m_{\mathrm{min}}$ [$M_\odot$]",
    "m_max": r"$m_{\mathrm{max}}$ [$M_\odot$]",
    "M_max": r"$M_{\mathrm{max}}$ [$M_\odot$]",
}

def is_iterable(obj):
    """
    Returns boolean indicating whether object is iterable.
    """
    try:
        iter(obj)
    except TypeError:
        return False
    else:
        return True


def get_iterable(*objs):
    """
    Returns the first object in its arguments which is an iterable.
    """
    for obj in objs:
        if is_iterable(obj):
            return obj

    raise Exception("No iterable found.")



def discrete_colors(data, buf=0.5):
    colors = [
        (0.26700400000000002, 0.0048739999999999999, 0.32941500000000001),
        (0.12756799999999999, 0.5669490000000000400, 0.55055600000000005),
        (0.99324800000000002, 0.9061569999999999900, 0.14393600000000001),
    ]

    nbins = len(data)
    cmap_name = ""
    cmap = matplotlib.colors.LinearSegmentedColormap.from_list(
        cmap_name, colors, N=nbins,
    )
    nmap = matplotlib.colors.Normalize(
        vmin=min(data)-buf, vmax=max(data)+buf,
    )
    smap = matplotlib.cm.ScalarMappable(
        cmap=cmap, norm=nmap,
    )
    smap._A = [] # needs an array, even though it's not used

    return smap


def plot(
        ax,
        variable_name,
        alphas, m_mins, m_maxs, M_maxs,
        n_smooth=n_smooth, n_samples=n_samples,
    ):
    name2val = {
        "alpha": alphas,
        "m_min": m_mins,
        "m_max": m_maxs,
        "M_max": M_maxs,
    }

    var = name2val[variable_name]

    cmap = discrete_colors(var)
    colors = [cmap.to_rgba(x) for x in var]

    iterables = itertools.product(
        *(name2val[name] for name in ["alpha", "m_min", "m_max", "M_max"])
    )

    lowest = numpy.inf

    for color, (alpha, m_min, m_max, M_max) in zip(colors, iterables):
        log10_m_min, log10_m_max = numpy.log10([m_min, m_max])
        m_1 = numpy.logspace(log10_m_min, log10_m_max, n_smooth)

        log_m_1 = numpy.log(m_1)
        log_m_min, log_m_max = numpy.log([m_min, m_max])

        # Plot marginal PDF using functional form
        pdf = prob.marginal_pdf(m_1, alpha, m_min, m_max, M_max)[0]
        p = ax.plot(m_1, m_1*pdf, color=color, linestyle="-")

        # Store the lowest point for y-axis cutoff later
        lowest = numpy.minimum(lowest, numpy.min((m_1*pdf)[pdf!=0]))

        # Plot marginal PDF using a KDE of random samples
        samples = prob.marginal_rvs(n_samples, alpha, m_min, m_max, M_max)
        log_samples = numpy.log(samples)

        # Take KDE in log-space to get dP/d(log m_1)
        kde = scipy.stats.gaussian_kde(log_samples, bw_method="scott")
        pdf = kde(log_m_1)

        # Store bandwidth used
        bw = kde.factor

        # Now add virtual points beyond the limits of the function as a
        # bandage for the Gaussian KDE's infinite range.
        pdf += scipy.stats.gaussian_kde(
            2*log_m_min-log_samples, bw_method=bw,
        )(log_m_1)
        pdf += scipy.stats.gaussian_kde(
            2*log_m_max-log_samples, bw_method=kde.factor,
        )(log_m_1)

        # Now get dP/d(m_1) = dP/d(log m_1) / m_1
        pdf /= m_1

        # Plot the PDF
        ax.plot(m_1, m_1*pdf, color=color, linestyle="--")

    ax.set_xscale("log")
    ax.set_yscale("log")

    _, y_max = ax.get_ylim()
    y_min = numpy.power(10.0, numpy.floor(numpy.log10(lowest)))
    ax.set_ylim([y_min, y_max])

    cbar = ax.figure.colorbar(cmap, ax=ax)
    cbar.set_label(labels[variable_name])
    cbar.set_ticks(var)


fig, axes = plt.subplots(4, sharex=True, figsize=[8, 16])
ax_alpha, ax_mmin, ax_mmax, ax_Mmax = axes


plot(
    ax_alpha,
    "alpha",
    [-3.0, -2.0, -1.0, 0.0, 1.000001, 2.0, 3.0], [5.0], [95.0], [100.0],
)

plot(
    ax_mmin,
    "m_min",
    [2.35], [1.0, 2.0, 3.0, 4.0, 5.0], [95.0], [100.0],
)

plot(
    ax_mmax,
    "m_max",
    [2.35], [5.0], [30.0, 40.0, 50.0, 70.0, 80.0, 90.0, 95.0], [100.0],
)

plot(
    ax_Mmax,
    "M_max",
    [2.35], [5.0], [95.0], [100.0, 150.0, 200.0],
)

for ax in axes:
    ax.set_ylabel(r"$m_1 p(m_1)$")

ax_Mmax.set_xlabel(r"$m_1$ [$M_\odot$]")

plt.show()
'''
