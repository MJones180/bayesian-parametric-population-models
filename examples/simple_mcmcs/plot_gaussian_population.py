r"""
Gaussian population
===================

This demonstrates reconstructing the parameters of a simple simulated
population, wherein the population follows a 1D normal distribution, and the
mean and variance are to be inferred. We apply an efficiency function, which
causes positive-valued samples to only be detected 50% of the time, and
negative-valued samples to always be detected.

Pieces of the inhomogeneous Poisson posterior formula
-----------------------------------------------------

The intensity function is

.. math::
   \rho(\lambda\mid\Lambda) = \mathcal{R} \mathcal{N}(\lambda; \mu, \sigma^2)

where :math:`\Lambda = (\mathcal{R}, \mu, \sigma^2)`.

The expected number of detections in an observing time :math:`T` is

.. math::
   \mu(\Lambda) =
   T \int_{-\infty}^{+\infty} \mathrm{d}\lambda \,
     \eta(\lambda) \, \rho(\lambda\mid\Lambda)

where :math:`\eta(\lambda)` is the efficiency function

.. math::
  \eta(\lambda) =
  \begin{cases}
    1/2, & \text{if } \lambda > 0,
    \\
    1, & \text{otherwise.}
  \end{cases}

We can evaluate :math:`\mu(\Lambda)` analytically to be

.. math::
   \mu(\Lambda) =
   \frac{T \cdot \mathcal{R}}{4}
   \left[ 3 + \mathrm{erf}\left(-\frac{\mu}{\sqrt{2\sigma^2}}\right)\right]

We assume a prior which is uniform in :math:`\log_{10}\mathcal{R}`, :math:`\mu`,
and :math:`\log_{10}(\sigma^2)`

.. math::
   \pi(\Lambda) =
   \mathcal{U}(
     \log_{10}\mathcal{R};
     \log_{10}\mathcal{R}_{\mathrm{min}}, \log_{10}\mathcal{R}_{\mathrm{max}}
   )
   \mathcal{U}(
     \log_{10}\mu;
     \log_{10}\mu_{\mathrm{min}}, \log_{10}\mu_{\mathrm{max}}
   )
   \mathcal{U}(
     \log_{10}(\sigma^2);
     \log_{10}(\sigma^2)_{\mathrm{min}}, \log_{10}(\sigma^2)_{\mathrm{max}}
   )

We also assume that our measurements of each detection have infinite precision,
which makes the both the likelihood and posterior a Dirac delta function
centered at the true value for that event, :math:`\lambda_n`, so long as the
prior has support there (which any reasonable prior would). We then only need
one posterior sample from each event – the true value – in order to describe it
accurately, and we set the prior samples to ``None``, which instructs the code
to perform no division by the prior, equivalent to a uniform prior.


Synthesizing our data set
-------------------------

We assume an event rate :math:`\mathcal{R}`, and an observing time :math:`T`,
which correspond to an *intrinsic* mean number of events
:math:`\mathcal{R} \times T` in that time. The *intrinsic* number of events
which actually occur is determined by a Poisson random variable

.. math::
   N_{\mathrm{int}} \sim \mathrm{Poi}(\mathcal{R} \times T)

We draw one sample for :math:`N_{\mathrm{int}}`. With this number in hand, we
draw :math:`N_{\mathrm{int}}` samples from our intrinsic population distribution

.. math::
   \lambda_i \sim \mathcal{N}(\mu, \sigma^2)

for some assumed values for :math:`(\mu, \sigma^2)`, and for
:math:`i \in \{1, \ldots, N_{\mathrm{int}}\}`. Due to our detection efficiency
:math:`\eta(\lambda)` described above, we then must discard positive-valued
samples with 50% probability each. Note: this does not necessarily mean we
discard half of the samples, as each sample is considered independently, and its
acception/rejection is random. In principle we could wind up discarding none of
them, all of them, half of them, or anywhere inbetween – though the expected
value is half. This leaves us with a (likely) smaller number of samples,
:math:`\lambda_i` for :math:`i \in \{1, \ldots, N\}`, where :math:`N` is the
number of samples after the rejection procedure, which satisfies
:math:`0 \leq N \leq N_{\mathrm{int}}`.


Population inference
--------------------

Putting together the pieces of the inhomogeneous Poisson posterior function, as
well as our :math:`N` synthesized detections, we perform a Markov chain Monte
Carlo using :func:`pop_models.mcmc.run_mcmc`. We initialize the MCMC by drawing
samples from our prior :math:`\pi(\Lambda)`. We evolve the chain using
``n_walkers`` in parallel, each obtaining ``n_samples``. We then remove the
first ``burnin`` samples from each chain (to remove the effects of burnin), and
after that only take one of every ``acorr`` samples (to remove the effects of
autocorrelation). These are set to fixed numbers for simplicity, though in
principle they should be determined empirically from the data. With all of the
posterior samples :math:`\Lambda_0, \Lambda_1, \ldots, \Lambda_S` in hand, we
create a `corner plot <http://corner.readthedocs.io/>`_ of the samples, with the
true values overplotted. If the code is working properly, there is a high
probability that the true values will lie in a region of high density.
"""
from __future__ import division, print_function

print("SKIPPING")

## Will be removing this possibly.  Commenting everything out to make docs
## build faster.

# import numpy
# import matplotlib.pyplot as plt
# import scipy.special
# import scipy.stats

# import corner

# import pop_models.mcmc

# seed = 4

# random = numpy.random.RandomState(seed)

# sqrt2 = numpy.sqrt(2.0)


# def accept(prob, random_state=random):
#     """
#     Returns ``True`` with probability ``prob``.
#     """
#     p = random_state.uniform()
#     return p < prob


# def after_prior_aux_fn(Lambda, aux_info, T=None, **kwargs):
#     """
#     Precomputes some transformed quantities which will be available to
#     ``expval_fn`` and ``intensity_fn`` through their argument ``aux_info``. This
#     allows us to save CPU cycles by avoiding re-computing the same
#     transformations in both of those functions, and also considering that
#     ``intensity_fn`` is called once per observed event we save additional CPU
#     cycles there.
#     """
#     log10_R, mu, log10_sigma2 = Lambda

#     R = numpy.power(10.0, log10_R)
#     sigma = numpy.power(10.0, 0.5*log10_sigma2)
#     Mu = T * R

#     return {
#         "R": R,
#         "sigma": sigma,
#         "Mu": Mu,
#     }


# def expval_fn(Lambda, aux_info, **kwargs):
#     """
#     Computes mu(Lambda)
#     """
#     log10_R, mu, log10_sigma2 = Lambda

#     Mu = aux_info["Mu"]
#     sigma = aux_info["sigma"]

#     return 0.25 * Mu * (3.0 + scipy.special.erf(-mu / (sqrt2*sigma)))


# def intensity_fn(lamb, Lambda, aux_info, **kwargs):
#     """
#     Computes rho(lambda | Lambda)
#     """
#     log10_R, mu, log10_sigma2 = Lambda

#     R = aux_info["R"]
#     sigma = aux_info["sigma"]

#     return R * scipy.stats.norm(loc=mu, scale=sigma).pdf(lamb)


# def log_prior_fn(
#         Lambda, aux_info,
#         log10_R_min=None, log10_R_max=None,
#         mu_min=None, mu_max=None,
#         log10_sigma2_min=None, log10_sigma2_max=None,
#         **kwargs
#     ):
#     """
#     Computes pi(Lambda)
#     """
#     log10_R, mu, log10_sigma2 = Lambda

#     if log10_R < log10_R_min or log10_R > log10_R_max:
#         return -numpy.inf

#     if mu < mu_min or mu > mu_max:
#         return -numpy.inf

#     if log10_sigma2 < log10_sigma2_min or log10_sigma2 > log10_sigma2_max:
#         return -numpy.inf

#     return 0.0

# ## Set MCMC options ##
# n_samples = 1000
# n_walkers = 10
# burnin = 500
# acorr = 5

# ## Define parameters of our population and observation ##
# param_names = ["log10_R", "mu", "log10_sigma2"]
# n_params = len(param_names)
# labels = [
#     r"$\log_{10}\mathcal{R}$", r"$\mu$", r"$\log_{10}\sigma^2$",
# ]

# # Observing time
# T = 5.0

# # Intrinsic rate in units of 1/T
# R = 10.0
# # Mean and standard deviation of our Gaussian
# mu = 2.0
# sigma = 3.0

# print("Observing for T =", T, "units")
# print("Intrinsic rate is R =", R, "per unit")
# print("Population has (mu, sigma) =", (mu, sigma))

# # Scaled versions of population parameters used later
# log10_R = numpy.log10(R)
# log10_sigma2 = 2.0 * numpy.log10(sigma)

# # Idealized results for our MCMC
# truths = [log10_R, mu, log10_sigma2]

# ## Parameters for our prior ##
# log10_R_min, log10_R_max = -3, +3
# mu_min, mu_max = -20, +20
# log10_sigma2_min, log10_sigma2_max = -2, +4

# print("Our prior is:")
# print("Uniform in log10(R) on", [log10_R_min, log10_R_max])
# print("Uniform in mu on", [mu_min, mu_max])
# print("Uniform in log10(sigma^2) on", [log10_sigma2_min, log10_sigma2_max])

# ## Determine the number of events that occurred in the Universe ##
# N = scipy.stats.poisson(R*T).rvs(random_state=random)
# print("There were", N, "samples intrinsically.")

# ## Apply selection effects, rejecting positive values with probability 50% ##
# lambs_intrinsic = scipy.stats.norm(mu, sigma).rvs(N, random_state=random)
# lambs = numpy.array([
#     lamb
#     for lamb in lambs_intrinsic
#     if lamb <= 0.0 or accept(0.50)
# ])

# ## Count the number of events which were actually observed ##
# N_obs = len(lambs)
# print("There were", N_obs, "samples observed.")

# ## Convert true values into posterior samples ##
# # For simplicity, we assume infinite precision measurements, so only one
# # posterior sample for each event is needed. We just need to ensure it has the
# # correct shape (1,1) instead of ().
# lambs = [numpy.reshape(lamb, (1,1)) for lamb in lambs]

# ## Determine initial state of MCMC walkers ##
# init_state = numpy.column_stack((
#     random.uniform(log10_R_min, log10_R_max, n_walkers),
#     random.uniform(mu_min, mu_max, n_walkers),
#     random.uniform(log10_sigma2_min, log10_sigma2_max, n_walkers)
# ))


# ## Set up inputs to MCMC ##
# kwargs = {
#     "T": T,
#     "log10_R_min": log10_R_min,
#     "log10_R_max": log10_R_max,
#     "mu_min": mu_min,
#     "mu_max": mu_max,
#     "log10_sigma2_min": log10_sigma2_min,
#     "log10_sigma2_max": log10_sigma2_max,
# }

# ## Sample from the inhomogeneous Poisson posterior ##
# pos, lnprob = pop_models.mcmc.run_mcmc(
#     intensity_fn, expval_fn, lambs, log_prior_fn, init_state, param_names,
#     after_prior_aux_fn=after_prior_aux_fn,
#     kwargs=kwargs,
#     nsamples=n_samples,
# )

# ## Clean the MCMC samples ##
# # Remove burnin at start, and take one sample per autocorrelation time.
# # Reshape array to be (n_samples*n_walkers, n_params), flattening out the walker
# # axis.
# pos_cleaned = pos[burnin::acorr,...].reshape((-1, n_params))

# ## Create a corner plot ##
# corner.corner(
#     pos_cleaned,
#     truths=truths,
#     labels=labels,
#     show_titles=True,
# )

# ## Display the plot ##
# plt.show()
