r"""
Gaussian population 1 (grid)
===================

A trivial example of using the PopModels population inference framework; return to the installation page to get started.
This example shows how to use several classes: Detection, Population, PoissonMean, ReweightedPosteriorsDetectionLikelihood, log_posterior,  and the Coordinate and CoordinateSystem framework.    For comparison, we also provide an explicit implementation and show the two are equal.



This example consists of a population with one property ("mass" or "x"), whose event rate and "mass distribution" parameters will be determined by fair draws.  The x distribution is assumed to be gaussian distributed with unknown mean and (known) unit variance; the event rate is "1 per unit time" and we observe for 10 time units.  Inference assumes a uniform prior in 'x' and rate.

The plot produced at the end shows
  * Top panel: Fair draws of x (thin lines), and inferred 'x' distribution
  * Corner plot: The inferred mass distribution and rate distribution (as one-dimensional marginal distributions), superposed with the likleihood.


In this example, PopModels is used for  *grid-based* inference: no MCMC (or Monte Carlo integral) is performed.
Instead, the likelihood is evaluated on a grid; marginal posteriors follow by direct quadrature.


The code  works as follows, referring to the ingredients on the main documentation page  (https://bayesian-parametric-population-models.readthedocs.io/)
  * Define the population (```SimplePopulation```)
  * Define the expected number of events 
  * Define a detection and parameter inference model (=perfect knowledge)
  * Pick synthetic parameters, and define a synthetic population
  * Define the likelihood of the observations, for the events (```detection_likelihoods```)
  * Evaluate the (log) posterior on a grid
    * Demonstrate the posterior agrees with the obvious analytic expression (LOG_POST == LOG_POST_ANALYTIC)
  * Compute marginal distributions by quadrature


"""

import numpy
import matplotlib
matplotlib.use("Agg")
from matplotlib import gridspec
import matplotlib.pyplot as plt
import scipy.stats

from pop_models.coordinate import Coordinate, CoordinateSystem
from pop_models.detection import (
    Detection, ReweightedPosteriorsDetectionLikelihood,
)
from pop_models.poisson_mean import PoissonMean
from pop_models.population import Population
from pop_models.posterior import PosteriorGrid, PopulationInferenceGrid


# Construct simple coordinate system from scratch
X = Coordinate("x")
coord_system = CoordinateSystem(X)

# Names of the parameters of the population.
param_names = ("rate", "mean")

class SimplePopulation(Population):
    """
    Defines a simple population with a single observable ("x"), which obey a
    Gaussian distribution with unknown mean "mean" and known unit variance.
    Normalization of this distribution is given by an unknown parameter "rate".
    """
    def __init__(self):
        # No special information needed, just pass the CoordinateSystem to the
        # parent's constructor method.
        super().__init__(coord_system, param_names)

    def pdf(self, observables, parameters, where=True, **kwargs):
        # First argument is a list of observables, but there's only one, "x", so
        # we pull that out of the list.
        x, = observables
        # The only parameter that affects the PDF is the "mean", so we extract
        # that.
        mean = parameters["mean"]

        # PDF is a normal distribution with unit variance.  This is the
        # functional form for such a distribution.  Note that by using
        # `numpy.subtract.outer(mean, x)` instead of `mean - x`, we compute
        # all combinations of `mean[i_1,...,i_n] - x[j_1,...,j_m]`, and the
        # shape of the output array is `numpy.shape(mean) + numpy.shape(x)`.
        # Handling array shapes like this is a requirement of the PopModels API.
        delta = numpy.subtract.outer(mean, x)
        return (
            numpy.exp(-0.5*numpy.square(delta)) /
            numpy.sqrt(2.0*numpy.pi)
        )

    def normalization(self, parameters, where=True, **kwargs):
        # The normalization of the intensity function is just the "rate"
        # parameter.
        return parameters["rate"]


class SimpleExpectedDetections(PoissonMean):
    """
    We assume a perfect survey, so the expected number of detections is just the
    event rate multiplied by the observing time.
    """
    def __init__(self, obs_time, population):
        super().__init__(population)
        self.obs_time = obs_time

    def __call__(self, parameters, where=True, **kwargs):
        return (
            self.population.normalization(parameters, where=where) *
            self.obs_time
        )


class SimpleDetection(Detection):
    """
    We assume detections with perfect measurements, so all posterior samples are
    of the correct value, and if the `likelihood` method were implemented, it
    would be a delta function at the true value (which would not integrate well
    numerically, so we must use `posterior_samples` since that makes a Monte
    Carlo integral perfectly adaptive).
    """
    def __init__(self, x_true):
        # Parent constructor needs to know the coordinate system.
        super().__init__(coord_system)
        # In addition, we need to know the true value.
        self.x_true = x_true

    def posterior_samples(self, n_samples, random_state=None):
        # This method returns a tuple of two things:
        # The first is posterior samples for a list of observables, and in this
        # case we have a single observable (x), so the list has one element. The
        # posterior samples all have the object's true value, as we've assumed
        # perfect measurement error, so we simply return an array with the
        # desired number of samples (`n_samples`), where every element is the
        # true value of "x".
        # The second element of the tuple must either be the prior of each
        # posterior sample, or `None` if the prior is uniform.  Since the prior
        # doesn't matter in the case of a delta-function likelihood, we just set
        # it to uniform for simplicity's sake.
        return [numpy.repeat(self.x_true, n_samples)], None


# Seed the RNG for reproducability
seed = 15
random = numpy.random.RandomState(seed)

# Set the observing time for the "survey"
obs_time = 10.0

# Set the true values for the parameters of the population, and also store them
# in a Parameters dict.
rate_true = 1.0
mean_true = 0.0
parameters_true = {"rate": rate_true, "mean": mean_true}

# Set the number of points to use in plotting and evaluation
n_plotting = 500
n_grid = (50, 50)

# Set the bounds for plotting and evaluation.
x_min_plotting = -10.0
x_max_plotting = +10.0
rate_min_grid = 0.01
rate_max_grid = 3.50
mean_min_grid = -1.5
mean_max_grid = +1.0

# Create an array of 'x' values for plotting the intensity function.
x_plotting = numpy.linspace(x_min_plotting, x_max_plotting, n_plotting)

# 1- and 2-D grids of the unknown parameters, as well as a Parameters dict
# mapping each variable to its 2-D grid.
rate_grid, rate_dx = numpy.linspace(
    rate_min_grid, rate_max_grid, n_grid[0],
    retstep=True,
)
mean_grid, mean_dx = numpy.linspace(
    mean_min_grid, mean_max_grid, n_grid[1],
    retstep=True,
)

# Initialize the Population and PoissonMean objects.
population = SimplePopulation()
expval = SimpleExpectedDetections(obs_time, population)

# Compute \mu for the true values -- this tells us the average number of
# detections we should make in our survey.
expval_true = expval(parameters_true)

# Take one random (seeded for reproducibility) Poisson draw for the number of
# detections, and then draw that many random (seeded) true values of objects
# detected from the population.
n_detections = scipy.stats.poisson(expval_true).rvs(1, random_state=random)
x_truths = (
    scipy.stats.norm(mean_true, 1.0).rvs(n_detections, random_state=random)
)

# PopModels needs a list of `DetectionLikelihood` objects.  We have already
# defined a `SimpleDetection` class (child of the `Detection` class), which
# implements the `posterior_samples` method.  The
# `ReweightedPosteriorsDetectionLikelihood` class is a subclass of
# `DetectionLikelihood`, which knows how to use a `Detection` object that's
# implemented the `posterior_samples` method, so it's the perfect fit.  We
# create a list of `DetectionLikelihood` objects to pass to PopModels' API.
detections = [SimpleDetection(x_true) for x_true in x_truths]
detection_likelihoods = [
    ReweightedPosteriorsDetectionLikelihood(population, detection)
    for detection in detections
]

# Define the `log_prior` function.
def log_prior(parameters, *args, **kwargs):
    """
    Assume a prior uniform in rate and mean.
    """
    rate = parameters["rate"]
    return numpy.zeros_like(rate)

# Independent implementation of `log_posterior`, which does everything
# analytically.
def log_posterior_analytic(parameters):
    """
    Computes the log posterior (+ constant) analytically.
    """
    rate = parameters["rate"]
    mean = parameters["mean"]

    mu = rate*obs_time
    log_event_contrib = numpy.zeros_like(rate)
    for x_truth in x_truths:
        delta = numpy.subtract.outer(mean, x_truth)
        log_event_contrib += (
            -0.5 * numpy.square(delta) -
             0.5 * numpy.log(2.0*numpy.pi)
        )

    return -mu + n_detections*numpy.log(rate) + log_event_contrib

# Create posterior grid object
variables = {"rate" : rate_grid, "mean" : mean_grid}
variable_dxs = {"rate" : rate_dx, "mean" : mean_dx}
post_grid = PosteriorGrid(
    variables,
    param_names,
    variable_dxs=variable_dxs,
)

# Create population inference object
pop_inference = PopulationInferenceGrid(
    post_grid,
    expval, detection_likelihoods, log_prior,
    vectorize_grid_eval=True,
)

# Can obtain normalized joint posterior on the grid like so.
posterior = post_grid.posterior_prob()

# Can also obtain any desired marginal posterior on a grid by passing a list of
# variables that you want it marginalized to.  Can be as many dimensions as you
# would like -- output will be an array with one dimension for each variable,
# in the order provided.  Note that these results are not cached, so each time
# you call the function it must re-compute.
posterior_rate = post_grid.marginal_posterior_prob(["rate"])
posterior_mean = post_grid.marginal_posterior_prob(["mean"])


# Plot the population
fig_pop, ax_pop = plt.subplots(figsize=(6,4))

# Plot the population's intensity function.
ax_pop.plot(
    x_plotting,
    population.intensity((x_plotting,), parameters=parameters_true),
)
_, ax_pop_ymax = ax_pop.get_ylim()

# Plot the detected events, and where they lie on the population.
for x_truth in x_truths:
    ax_pop.plot(
        [x_truth, x_truth], [0.0, 0.1*ax_pop_ymax],
        color="black", linestyle="solid", alpha=0.4,
    )

ax_pop.set_xlabel(r"$x$")
ax_pop.set_ylabel(r"$\rho(x | \Lambda_{\mathrm{true}})$")

fig_pop.show()

################################################################################

# Make a corner plot of the posteriors
fig_post = post_grid.plot_corner(levels=20, truths=parameters_true)

fig_post.show()
