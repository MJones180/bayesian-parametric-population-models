Populations of compact binaries
-------------------------------

These examples demonstrate applications for modeling compact binary coalescence (CBC) populations, and gravitational wave interferometers.
