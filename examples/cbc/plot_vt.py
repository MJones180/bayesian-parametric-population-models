r"""
Sensitive volume-time product (VT) usage
========================================

TODO: a brief description of what :math:`VT` is.  Link to another tutorial on
computing :math:`VT` yourself.
"""

################################################################################
# Import routines needed for interfacing with :math:`VT` and plotting.

import h5py
import numpy
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt

from pop_models.astro_models.gw_ifo_vt import RegularGridVTInterpolator

################################################################################
# Some plotting settings
n_eval_points = 200
n_levels = 100


################################################################################
# Download pre-computed :math:`VT` file
################################################################################
# Computing :math:`VT` is a computationally expensive procedure, so we will
# a download file which has pre-computed its values on a grid.  A future example
# will cover how to generate these grids yourself.  The grid we will download
# was computed using sensitivity representative of LIGO's first observing run
# (`aLIGOEarlyHighSensitivityP1200087 <https://dcc.ligo.org/P1200087/public>`_)
# using a detection threshold of SNR > 8 in one detector, scaled to an observing
# time of one day.
import os
os.system("wget https://git.ligo.org/daniel.wysocki/bayesian-parametric-population-models/-/raw/master/misc/vt_example.hdf5")

################################################################################
# Loading the file
################################################################################
# The VT file is a regular HDF5 file, which we can open with the ``h5py``
# module.  We do this with a ``with`` statement to ensure the file is closed
# properly if there is an error.
#
# The file object can be passed to ``RegularGridVTInterpolator``, which performs
# linear interpolation on :math:`\log(VT)` to let us evaluate anywhere within
# the grid's boundaries.  We can also pass a ``scale_factor``, which all
# :math:`VT` values are multipled by.  Since the :math:`VT` file we fetched is
# scaled to one day of observing time, we can simulate :math:`T` days of
# observing time by passing ``scale_factor =`` :math:`T`.  Here we do
# :math:`T = 100` to demonstrate.
with h5py.File("vt_example.hdf5", "r") as vt_file:
    VT = RegularGridVTInterpolator(vt_file, scale_factor=100.0)

################################################################################
# Plotting :math:`VT` versus mass
################################################################################
# Now we want to evaluate :math:`VT` at a number of points.  This file was
# computed on a grid which spans two masses :math:`(m_1, m_2)` and two aligned
# components of spin :math:`(\chi_{1z}, \chi_{2z})`.  Since it's hard to plot
# functions with 4-D inputs, we will start by plotting the 2-D slice with
# :math:`\chi_{1z} = \chi_{2z} = 0`.
#
# To start, we create a fine grid spanning the :math:`(m_1, m_2)` plane,
# inspecting the ``VT`` object to determine the minimum and maximum masses the
# grid can be evaluated on (values outside this range will give :math:`VT = 0`).

# Create a 1-D grid in mass
m_grid = numpy.linspace(VT.m_min, VT.m_max, n_eval_points)
# Create a 2-D grid spanning both masses
m1_mesh, m2_mesh = numpy.meshgrid(m_grid, m_grid)

################################################################################
# Now we need to pass the corresponding spin values, which are just zero
# everywhere.
chi1z_mesh = chi2z_mesh = numpy.broadcast_to(0.0, m1_mesh.shape)

################################################################################
# We can now evaluate VT by passing these all in a tuple.
observables = m1_mesh, m2_mesh, chi1z_mesh, chi2z_mesh
vt_versus_m1_m2 = VT(observables)

################################################################################
# Now we visualize the result with a contour plot.

# Create the canvas
fig, ax = plt.subplots()

# Make the contours
ctr = ax.contourf(
    m_grid, m_grid, vt_versus_m1_m2, n_levels,
    cmap="viridis",
)

# Create a colorbar
cbar = fig.colorbar(ctr, ax=ax)

# Label the axes and colorbar
ax.set_xlabel("$m_1 \\, [M_\\odot]$")
ax.set_ylabel("$m_2 \\, [M_\\odot]$")
cbar.set_label("$VT \\, [\\mathrm{Gpc} \\, \\mathrm{yr}]$")

# Show the plot
fig.show()


################################################################################
# Plotting :math:`VT` versus one mass and spin
################################################################################
# Now we take another 2-D slice of :math:`VT`, this time along the
# :math:`(m_1=m_2), (\chi_{1z}=\chi_{2z})` plane.  We reuse the 1-D mass grid
# from before, but now also create a spin grid spanning :math:`[-1, +1]`.
chi_grid = numpy.linspace(-1.0, +1.0, n_eval_points)

# Create a 2-D grid for each mass-spin pair
m_mesh, chi_mesh = numpy.meshgrid(m_grid, chi_grid)

################################################################################
# We can now evaluate VT by passing these all in a tuple.
observables = m_mesh, m_mesh, chi_mesh, chi_mesh
vt_versus_m_chi = VT(observables)

################################################################################
# Now we visualize the result with a contour plot.

# Create the canvas
fig, ax = plt.subplots()

# Make the contours
ctr = ax.contourf(
    m_grid, chi_grid, vt_versus_m_chi, n_levels,
    cmap="viridis",
)

# Create a colorbar
cbar = fig.colorbar(ctr, ax=ax)

# Label the axes and colorbar
ax.set_xlabel("$m_1 = m_2 \\, [M_\\odot]$")
ax.set_ylabel("$\\chi_{1z} = \\chi_{2z} \\, [M_\\odot]$")
cbar.set_label("$VT \\, [\\mathrm{Gpc} \\, \\mathrm{yr}]$")

# Show the plot
fig.show()
