"""
Generates the image used for the project's logo.
"""

from __future__ import division, print_function

import numpy

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

import scipy.stats


matplotlib.rcParams["text.usetex"] = True
matplotlib.rcParams["text.latex.preamble"] = [
    r"\usepackage{amsmath}",
]

fig = plt.figure(figsize=[4, 4], dpi=100, frameon=False)
ax = fig.add_axes([0, 0, 1, 1])
ax.axis("off")

x = numpy.linspace(-3.0, +3.0, 500)
px = scipy.stats.norm.pdf(x)

ax.plot(
    x, px,
    linestyle="-", linewidth=10,
    color="#1F77B4",
)

ax.text(
    0.00, 0.20,
    r"$\boldsymbol{\rho(\lambda\mid\Lambda)}$",
    fontsize=60, fontweight="heavy",
    horizontalalignment="center", verticalalignment="center",
)

ax.set_xlim([-3.00, +3.00])
ax.set_ylim([-0.05, +0.45])

fig.savefig("logo.png", dpi=50)
