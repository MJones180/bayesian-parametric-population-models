import tempfile
import numpy
from pop_models.astro_models.detections.cbc.gaussian import CBCGaussianDetection
from pop_models.coordinate import CoordinateSystem
from pop_models.astro_models.coordinates import m1_source_coord, m2_source_coord

def test_serialization():
    # Generate a temporary filename for storing JSON file
    filename = tempfile.mktemp(prefix="pop_models_test_", suffix=".json")

    # We'll use m1_source, m2_source coordinates.
    coord_system = CoordinateSystem(m1_source_coord, m2_source_coord)

    # Make up a covariance / inverse covariance matrix.
    mean = numpy.asarray([40.0, 35.0], dtype=numpy.float64)
    covar = numpy.asarray([[8.0, 0.1], [0.1, 6.0]], dtype=numpy.float64)

    # Create a detection object.
    det = CBCGaussianDetection(coord_system, mean, covar)
    # Serialize the detection object to our temporary filename.
    det.save(filename)

    # Load the detection object
    det_loaded = CBCGaussianDetection.load(filename, dtype=numpy.float64)

    # Assert that each each field matches.
    assert det.coord_system == det_loaded.coord_system

    assert numpy.allclose(det.mean, det_loaded.mean)

    assert numpy.allclose(det.covariance, det_loaded.covariance)

    if det.base_covariance is None:
        assert det_loaded.base_covariance is None
    else:
        assert numpy.allclose(
            det.base_covariance, det_loaded.base_covariance,
            rtol=1e-5, atol=1e-8,
        )

    if det.base_inv_covariance is None:
        assert det_loaded.base_inv_covariance is None
    else:
        assert numpy.allclose(
            det.base_inv_covariance, det_loaded.base_inv_covariance,
            rtol=1e-5, atol=1e-8,
        )
