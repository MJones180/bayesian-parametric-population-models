import pytest
from pop_models.applications.driver.Config import Config, REQUIRED


class TestConfig:

    BASIC_PATH = 'ini_files/basic.ini'

    # ================
    # Tests
    # ================

    def test_file_found(self):
        import configparser
        config, section_base_paths = Config.parse(self.BASIC_PATH)
        assert isinstance(config, configparser.ConfigParser)
        assert isinstance(section_base_paths, dict)

    def test_file_not_found(self):
        with pytest.raises(FileNotFoundError):
            Config.parse('this_file_does_not_exist.ini')

    def test_obj_call(self):
        config = Config(self.BASIC_PATH, {})
        assert config('main', 'a') == config.grab('main', 'a')

    def test_grab_whole_section(self):
        import configparser
        config = Config(self.BASIC_PATH, {})
        assert isinstance(config('main'), configparser.SectionProxy)

    def test_missing_section(self):
        config = Config(self.BASIC_PATH, {'section_should_exist': {}})
        with pytest.raises(ValueError):
            config('section_should_exist')

    def test_value_types(self):
        defaults = {
            'main': {
                'a': [None, int],
                'b': [None, str],
                'c': [None, bool],
            },
            'secondary': {
                'd': [None, float],
                'e': [None, bool],
            },
        }
        config = Config(self.BASIC_PATH, defaults)
        assert isinstance(config('main', 'a'), int)
        assert isinstance(config('main', 'b'), str)
        assert isinstance(config('main', 'c'), bool)
        assert isinstance(config('secondary', 'd'), float)
        assert isinstance(config('secondary', 'e'), bool)

    def test_present_required_value(self):
        defaults = {
            'main': {
                'a': [REQUIRED, int],
            },
        }
        config = Config(self.BASIC_PATH, defaults)
        assert config('main', 'a') is not None

    def test_missing_required_value(self):
        defaults = {
            'main': {
                'req': [REQUIRED, int],
            },
        }
        config = Config(self.BASIC_PATH, defaults)
        with pytest.raises(ValueError):
            config('main', 'req')

    def test_missing_values(self):
        defaults = {
            'main': {
                'g': [31415, int],
                'h': ['LovePi', str],
            },
            'secondary': {
                'i': [False, bool],
                'j': [3.14, float],
                'k': [None, bool],
            },
        }
        config = Config(self.BASIC_PATH, defaults)
        assert config('main', 'g') == 31415
        assert config('main', 'h') == 'LovePi'
        assert config('secondary', 'i') is False
        assert config('secondary', 'j') == 3.14
        assert config('secondary', 'k') is None

    def test_recursive_config_load(self):
        config = Config('ini_files/basic_nested.ini', {})
        root = config.grab('root')
        nested1 = config.grab('nested1')
        nested2 = config.grab('nested2')
        assert root is not None
        assert nested1 is not None
        assert nested2 is not None
